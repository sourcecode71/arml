﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;

namespace rBKK.WebTest
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ARMLEntities Context = new ARMLEntities();
            var results = from items in Context.SEC_USER
                          select new { items.USER_ID,items.TITLE,items.FIRST_NAME,items.LAST_NAME
                          };

            rptOscarNominees.DataSource = results;
            rptOscarNominees.DataBind();

        }
    }
}
