﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="rBKK.WebTest._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/DataTables-1.9.4/media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="Scripts/DataTables-1.9.4/media/js/jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#tblOscarNominees').dataTable({ "oLanguage": { "sSearch": "Search the nominees:" },
                "iDisplayLength": 10,
                "aaSorting": [[0, "desc"]]
            });
        });
    </script>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to ASP.NET!
    </h2>
    <p>
        To learn more about ASP.NET visit <a href="http://www.asp.net" title="ASP.NET Website">www.asp.net</a>.
    </p>
    <p>
    <asp:Repeater ID="rptOscarNominees" runat="server">
  <HeaderTemplate>
    <table id="tblOscarNominees" cellpadding="0" cellspacing="0" border="0" class="display">
      <thead>
        <tr>
            <th>USER_ID</th>
            <th>TITLE</th>
            <th>FIRST_NAME</th>
            <th>LAST_NAME</th>
        </tr>
      </thead>
      <tbody>
  </HeaderTemplate>
  <ItemTemplate>
    <tr>
      <td><%# Eval("USER_ID")%></td>
      <td><%# Eval("TITLE")%></td>
      <td><%# Eval("FIRST_NAME")%></td>
      <td><%# Eval("LAST_NAME")%></td>
    </tr>
  </ItemTemplate>
  <FooterTemplate>
    </tbody>
    </table>
  </FooterTemplate>
</asp:Repeater>
    </p>
</asp:Content>
