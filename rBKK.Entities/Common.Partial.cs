﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace rBKK.Entities
{
    public partial class INV_LOAN_DETAILS
    {
        public string ACCOUNT_HOLDER { get; set; }
    }

    public partial class INV_INVENTORY_DETAILS
    {
        public string CHALLAN_NO { get; set; }
        public string ISSUE_NO { get; set; }
        public string PRODUCT_NAME { get; set; }
        public string UNIT_OF_MEASUREMENT { get; set; }
        public string FACTORYNAME { get; set; }
        public string GODOWNNAME { get; set; }
        public DateTime? CHALLAN_DATE { get; set; }
    }

    public partial class HR_LEAVE_APPLICATION
    {
        //Employee Name
        public string NAME { get; set; }
        public string DESIGNATION { get; set; }
    }

    public partial class HR_EMPLOYEE_ATTENDANCE
    {
        public bool IS_PERMANENT { get; set; }
    }

    public partial class HR_EMPLOYEE
    {
        public string DESIGNATION { get; set; }
        public string GRADE_NAME { get; set; }
    }

}
