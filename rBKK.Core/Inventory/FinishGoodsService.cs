﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using rBKK.Entities;
using rBKK.Infrastructure.Inventory;

namespace rBKK.Core.Inventory
{
    public class FinishGoodsService : IFinishGoodsService
    {
        [Inject]
        public IFinishGoodsRepository _finishGoodsRepository { get; set; }

        public List<INV_INVENTORY_DETAILS> GetAllProductionGoods(string strTypeId)
        {
            try
            {
                return _finishGoodsRepository.GetAllProductionGoods(strTypeId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public List<INV_INVENTORY_DETAILS> GetAllFinishGoods(string strTypeId)
        {
            try
            {
           return _finishGoodsRepository.GetAllFinishGoods(strTypeId);

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public List<INV_INVENTORY_DETAILS> GetAllFinishGoodsReceivedByID(long intInvMID)
        {
            return _finishGoodsRepository.GetAllFinishGoodsReceivedByID(intInvMID);
        }
    }
}
