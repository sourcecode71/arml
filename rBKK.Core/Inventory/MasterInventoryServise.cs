﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;
using rBKK.Infrastructure.Inventory;
using rBKK.Infrastructure.Security;
using rBKK.Entities;
using System.Collections;

namespace rBKK.Core.Inventory
{
    public class MasterInventoryServise:IMasterInventoryServise
    {
        [Inject]
        public IMasterInventoryRepository _masterInventoryRepository { get; set; }

        #region Mostaq  ////
        #region Product Cateory
        public INV_PRODUCT_CATEGORY GetCategoryByID(string catId)
        {
            return _masterInventoryRepository.GetCategoryByID(catId);
        }
        public List<INV_PRODUCT_CATEGORY> GetAllCategory()
        {
            return _masterInventoryRepository.GetAllCategory();
        }
        public void AddProductType(INV_PRODUCT_CATEGORY productCategory, string TranType)
        {
            _masterInventoryRepository.AddProductType(productCategory, TranType);
        }
        public void DeleteProductType(string catId)
        {
            _masterInventoryRepository.DeleteProductType(catId);
        }
        #endregion

        #region Product
        public INV_PRODUCT GetProductByID(string prodId)
        {
            return _masterInventoryRepository.GetProductByID(prodId);
        }

        public IQueryable<object> GetProductByCatID(string strcategory)
        {
            return _masterInventoryRepository.GetProductByCatID(strcategory);
        }
        public IQueryable<object> GetAllProduct()
        {
            return _masterInventoryRepository.GetAllProduct();
        }

        public void AddProduct(INV_PRODUCT products, string TranType)
        {
            _masterInventoryRepository.AddProduct(products, TranType);
        }

        public void DeleteProduct(string prodId)
        {
            _masterInventoryRepository.DeleteProduct(prodId);
        }

        public void UpdateProductPrice(List<INV_PRODUCT> prodInfos)
        {
            _masterInventoryRepository.UpdateProductPrice(prodInfos);
        }

        #endregion

        #region  Machinery------
        public INV_MACHINERY_TYPE GetMachineryTypeByID(string TypeID)
        {
            return _masterInventoryRepository.GetMachineryTypeByID(TypeID);
        }
        public List<INV_MACHINERY_TYPE> GetAllMachneryType()
        {
            return _masterInventoryRepository.GetAllMachneryType();
        }
        public void AddMachineryTYpe(INV_MACHINERY_TYPE machineryType, string transType)
        {
            _masterInventoryRepository.AddMachineryTYpe(machineryType, transType);
        }
        public void DeleteMachineyType(string TypeID)
        {
            _masterInventoryRepository.DeleteMachineyType(TypeID);
        }

        #endregion

        #region Machinery Info

        public INV_MACHINERY GetMachineryByID(string prodId)
        {
            return _masterInventoryRepository.GetMachineryByID(prodId);
        }

        public IQueryable<object> GetMachineryByTypeID(string strcategory)
        {
            return _masterInventoryRepository.GetMachineryByTypeID(strcategory);
        }

        public IQueryable<object> GetAllMachinery()
        {
            return _masterInventoryRepository.GetAllMachinery();
        }

        public void AddMachinery(INV_PRODUCT products, string TranType)
        {
            _masterInventoryRepository.AddMachinery(products, TranType);
        }

        public void DeleteMachinery(string prodId)
        {
            _masterInventoryRepository.DeleteMachinery(prodId);
        }

        #endregion

        #region Measurement Unit

        public INV_UNIT_OF_MEASUREMENT GetUnitOfMeasurmentTById(string catId)
        {
            return _masterInventoryRepository.GetUnitOfMeasurmentTById(catId);
        }

        public List<INV_UNIT_OF_MEASUREMENT> GetAllUNIT_OF_MEASUREMENT()
        {
            return _masterInventoryRepository.GetAllUNIT_OF_MEASUREMENT();
        }

        public void AddUNIT_OF_MEASUREMENT(INV_UNIT_OF_MEASUREMENT untiOfMeasurment, string TranType)
        {
            _masterInventoryRepository.AddUNIT_OF_MEASUREMENT(untiOfMeasurment, TranType);
        }

        public void DeleteUNIT_OF_MEASUREMENT(string catId)
        {
            _masterInventoryRepository.DeleteUNIT_OF_MEASUREMENT(catId);
        }
        #endregion

        #region Factory

        public INV_FACTORY GetInvFactoryById(string catId)
        {
            return _masterInventoryRepository.GetInvFactoryById(catId);
        }
        public List<INV_FACTORY> GetAllInvFactory()
        {
            return _masterInventoryRepository.GetAllInvFactory();
        }
        public void AddInvFactory(INV_FACTORY iNV_FACTORY, string TranType)
        {
            _masterInventoryRepository.AddInvFactory(iNV_FACTORY, TranType);
        }
        public void DeleteInvFactory(string FactoryId)
        {
            _masterInventoryRepository.DeleteInvFactory(FactoryId);
        }
        #endregion

        #region Godown
        public IQueryable<object> GetInvGodownByFct()
        {
            return _masterInventoryRepository.GetInvGodownByFct();
        }

        public INV_GODOWN GetInvGodownById(string GodownID)
        {
            return _masterInventoryRepository.GetInvGodownById(GodownID);
        }

        public List<INV_GODOWN> GetAllInvGodown()
        {
            return _masterInventoryRepository.GetAllInvGodown();
        }

        public void AddInvGodown(INV_GODOWN inv_Godown, string TranType)
        {
            _masterInventoryRepository.AddInvGodown(inv_Godown, TranType);
        }

        public void DeleteInvGodown(string FactoryId)
        {
            _masterInventoryRepository.DeleteInvFactory(FactoryId);
        }

        #endregion

        #region Factory Godown

        public List<INV_GODOWN> GetAllInvGodownForFCTMapping()
        {
            try
            {
                return _masterInventoryRepository.GetAllInvGodownForFCTMapping();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public IQueryable<object> GetInvFactoryGodown()
        {
            try
            {
                return _masterInventoryRepository.GetInvFactoryGodown();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public INV_FACTORY_GODOWN GetInvFactoryGodownById(string FactoryGodownID)
        {
            try
            {
                return _masterInventoryRepository.GetInvFactoryGodownById(FactoryGodownID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddInvFactoryGodown(INV_FACTORY_GODOWN inv_FactoryGodown, string TranType)
        {
            try
            {
                _masterInventoryRepository.AddInvFactoryGodown(inv_FactoryGodown, TranType);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteInvFactoryGodown(string GodownFactoryId)
        {
            try
            {
                _masterInventoryRepository.DeleteInvFactoryGodown(GodownFactoryId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Core Bank
        public List<CORE_BANK> GetAllCoreBank()
        {
            try
            {
                return _masterInventoryRepository.GetAllCoreBank();
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public CORE_BANK GetCoreBankById(string CoreBankID)
        {
            try
            {
                return _masterInventoryRepository.GetCoreBankById(CoreBankID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void AddCoreBankinfo(CORE_BANK core_BANK, string TranType)
        {
            try
            {
                _masterInventoryRepository.AddCoreBankinfo(core_BANK, TranType);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void DeleteCoreBankInfo(string CoreBankID)
        {
            try
            {
                _masterInventoryRepository.DeleteCoreBankInfo(CoreBankID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        } 
        #endregion

        #region Branch Info

        public IQueryable<object> GetAllBranchDeatials()
        {
            try
            {
                return _masterInventoryRepository.GetAllBranchDeatils();
            }
            catch (Exception ex)
            {
                
                throw ex;
            } 
        }
        public List<CORE_BANK_BRANCH> GetAllCoreBranch()
        {
            try
            {
                return _masterInventoryRepository.GetAllCoreBranch();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public CORE_BANK_BRANCH GetCoreBranchById(string CoreBranchID)
        {
            try
            {
                return _masterInventoryRepository.GetCoreBranchById(CoreBranchID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void AddCoreBranchInfo(CORE_BANK_BRANCH core_Branch, string TranType)
        {
            try
            {
                _masterInventoryRepository.AddCoreBranchInfo(core_Branch, TranType);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void DeleteCoreBranchInfo(string CoreBranchID)
        {
            try
            {
                _masterInventoryRepository.DeleteCoreBranchInfo(CoreBranchID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        } 
        #endregion

        #region Account Info
        public IQueryable<object> GetACCOUNTINFO()
        {
           return _masterInventoryRepository.GetACCOUNTINFO();
        }
        public List<CORE_BANK_ACCOUNTS> GetAllCoreAccount()
        {
            try
            {
                return _masterInventoryRepository.GetAllCoreAccount();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public CORE_BANK_ACCOUNTS GetCoreAccountById(string CoreAccountID)
        {
            try
            {
                return _masterInventoryRepository.GetCoreAccountById(CoreAccountID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void AddCoreAccountInfo(CORE_BANK_ACCOUNTS core_Account, string TranType)
        {
            try
            {
                _masterInventoryRepository.AddCoreAccountInfo(core_Account, TranType);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void DeleteAccountInfo(string core_ACCID)
        {
            try
            {
                _masterInventoryRepository.DeleteAccountInfo(core_ACCID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public List<CORE_BANK_ACCOUNTS> GetBankAccountByBankId(string bankId)
        {
            return _masterInventoryRepository.GetBankAccountByBankId(bankId);
        }

        #endregion

        #region Direct Sales Client List

        public List<PUR_VENDOR> GetAllDirectClientList()
        {
            return _masterInventoryRepository.GetAllDirectClientList();
        }
        #endregion

        #endregion

        #region Hannan  ///////             
        public List<INV_FACTORY> GetAllFactories()
        {
            return _masterInventoryRepository.GetAllFactories();
        }
        public List<INV_PRODUCT_CATEGORY> GetProductCategoryByParentId(string ParentId)
        {
            return _masterInventoryRepository.GetProductCategoryByParentId(ParentId);
        }
        public IQueryable<object> GetProductCategoryByParentIdWithAlias(string ParentId)
        {
            return _masterInventoryRepository.GetProductCategoryByParentIdWithAlias(ParentId);
        }
        public IQueryable<object> GetProductByGenericId(string genericId)
        {
            return _masterInventoryRepository.GetProductByGenericId(genericId);
        }
        public IQueryable<object> GetAllCategoryWithGenericName()
        {
            return _masterInventoryRepository.GetAllCategoryWithGenericName();
        }
        public IQueryable<object> GetAllPaddyRice()
        {
            return _masterInventoryRepository.GetAllPaddyRice();
        }
        public IQueryable<object> GetMasterInventoryData(string TransType)
        {
            return _masterInventoryRepository.GetMasterInventoryData(TransType);
        }
        #region LoanType ///
        public List<INV_LOAN_TYPE> GetAllLoanTypes()
        {
            return _masterInventoryRepository.GetAllLoanTypes();
        }
        public List<CORE_BANK_BRANCH> GetBankBranchByBankId(string bankId)
        {
            return _masterInventoryRepository.GetBankBranchByBankId(bankId);
        }
        public void AddLoanDetails(INV_LOAN_DETAILS objLD, string OperationType, string loggedUserId)
        {
            _masterInventoryRepository.AddLoanDetails(objLD, OperationType, loggedUserId);
        }

        public IQueryable<object> GetLoanDetails()
        {
            return _masterInventoryRepository.GetLoanDetails();
        }
        public INV_LOAN_DETAILS GetLoanDetailsByID(string strLoanID)
        {
            return _masterInventoryRepository.GetLoanDetailsByID(strLoanID);
        }
        public List<INV_LOAN_DETAILS> GetAllLoans()
        {
            return _masterInventoryRepository.GetAllLoans();
        }
        #endregion

        public decimal GetCurrentStock(int GodownId, int ProductId)
        {
            return _masterInventoryRepository.GetCurrentStock(GodownId, ProductId);
        }
        public decimal GetSIRateByUnitId(int UnitId)
        {
            return _masterInventoryRepository.GetSIRateByUnitId(UnitId);
        }
        public decimal GetProductPriceById(string ProductId)
        {
            return _masterInventoryRepository.GetProductPriceById(ProductId);
        }
        public List<INV_GODOWN> GetOnlyFactoryGodowns()
        {
            return _masterInventoryRepository.GetOnlyFactoryGodowns();
        }
        public void UpdateSalesPaperStatus(string SalesId, string Status)
        {
             _masterInventoryRepository.UpdateSalesPaperStatus(SalesId, Status);
        }
        #endregion

       
    }
}
