﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using System.Collections;

namespace rBKK.Core.Inventory
{
    public interface IMasterInventoryServise
    {
        #region Mostaq  ////
        #region Product Category
        INV_PRODUCT_CATEGORY GetCategoryByID(string catId);
        List<INV_PRODUCT_CATEGORY> GetAllCategory();
        void AddProductType(INV_PRODUCT_CATEGORY productCategory, string TranType);
        void DeleteProductType(string catId);
        #endregion

        #region Peoduct Info

        INV_PRODUCT GetProductByID(string prodId);
        IQueryable<object> GetProductByCatID(string strcategory);
        IQueryable<object> GetAllProduct();
        void AddProduct(INV_PRODUCT products, string TranType);
        void DeleteProduct(string prodId);
        void UpdateProductPrice(List<INV_PRODUCT> prodInfos);
        #endregion

        #region Machieny Type
        INV_MACHINERY_TYPE GetMachineryTypeByID(string TypeID);
        List<INV_MACHINERY_TYPE> GetAllMachneryType();
        void AddMachineryTYpe(INV_MACHINERY_TYPE machineryType, string transType);
        void DeleteMachineyType(string TypeID);
        #endregion

        #region Machine Info

        INV_MACHINERY GetMachineryByID(string prodId);
        IQueryable<object> GetMachineryByTypeID(string strcategory);
        IQueryable<object> GetAllMachinery();
        void AddMachinery(INV_PRODUCT products, string TranType);
        void DeleteMachinery(string prodId);

        #endregion

        #region Measurment Unit

        INV_UNIT_OF_MEASUREMENT GetUnitOfMeasurmentTById(string catId);
        List<INV_UNIT_OF_MEASUREMENT> GetAllUNIT_OF_MEASUREMENT();
        void AddUNIT_OF_MEASUREMENT(INV_UNIT_OF_MEASUREMENT untiOfMeasurment, string TranType);
        void DeleteUNIT_OF_MEASUREMENT(string catId);

        #endregion

        #region Factory Info

        INV_FACTORY GetInvFactoryById(string catId);
        List<INV_FACTORY> GetAllInvFactory();
        void AddInvFactory(INV_FACTORY iNV_FACTORY, string TranType);
        void DeleteInvFactory(string FactoryId);

        #endregion

        #region Godown Info

        IQueryable<object> GetInvGodownByFct();
        INV_GODOWN GetInvGodownById(string GodownID);
        List<INV_GODOWN> GetAllInvGodown();
        void AddInvGodown(INV_GODOWN inv_Godown, string TranType);
        void DeleteInvGodown(string FactoryId);

        #endregion

        #region Factory Godown Mapping

        List<INV_GODOWN> GetAllInvGodownForFCTMapping();
        IQueryable<object> GetInvFactoryGodown();
        INV_FACTORY_GODOWN GetInvFactoryGodownById(string FactoryGodownID);
        void AddInvFactoryGodown(INV_FACTORY_GODOWN inv_FactoryGodown, string TranType);
        void DeleteInvFactoryGodown(string GodownFactoryId);

        #endregion

        #region Bank Info

        List<CORE_BANK> GetAllCoreBank();
        CORE_BANK GetCoreBankById(string CoreBankID);
        void AddCoreBankinfo(CORE_BANK core_BANK, string TranType);
        void DeleteCoreBankInfo(string CoreBankID);

        #endregion

        #region Branch Info
        IQueryable<object> GetAllBranchDeatials();
        List<CORE_BANK_BRANCH> GetAllCoreBranch();
        CORE_BANK_BRANCH GetCoreBranchById(string CoreBranchID);
        void AddCoreBranchInfo(CORE_BANK_BRANCH core_Branch, string TranType);
        void DeleteCoreBranchInfo(string CoreBranchID);

        #endregion

        #region Account Info 
        IQueryable<object> GetACCOUNTINFO();
        List<CORE_BANK_ACCOUNTS> GetAllCoreAccount();
        CORE_BANK_ACCOUNTS GetCoreAccountById(string CoreAccountID);
        void AddCoreAccountInfo(CORE_BANK_ACCOUNTS core_Account, string TranType);
        void DeleteAccountInfo(string core_ACCID);
        List<CORE_BANK_ACCOUNTS> GetBankAccountByBankId(string bankId);
        #endregion

        #region Direct Sales Client List///
        List<PUR_VENDOR> GetAllDirectClientList();
        #endregion

        #endregion

        #region Hannan ////           
        List<INV_FACTORY> GetAllFactories();
        List<INV_PRODUCT_CATEGORY> GetProductCategoryByParentId(string ParentId);
        IQueryable<object> GetProductCategoryByParentIdWithAlias(string ParentId);
        IQueryable<object> GetProductByGenericId(string genericId);
        IQueryable<object> GetAllCategoryWithGenericName();
        IQueryable<object> GetAllPaddyRice();
        IQueryable<object> GetMasterInventoryData(string TransType);
        #region LoanType ///  
        List<INV_LOAN_TYPE> GetAllLoanTypes();
        List<CORE_BANK_BRANCH> GetBankBranchByBankId(string bankId);
        void AddLoanDetails(INV_LOAN_DETAILS objLD, string OperationType, string loggedUserId);
        IQueryable<object> GetLoanDetails();
        INV_LOAN_DETAILS GetLoanDetailsByID(string strLoanID);
        List<INV_LOAN_DETAILS> GetAllLoans();
        #endregion
        decimal GetCurrentStock(int GodownId, int ProductId);
        decimal GetSIRateByUnitId(int UnitId);
        decimal GetProductPriceById(string ProductId);
        List<INV_GODOWN> GetOnlyFactoryGodowns();
        void UpdateSalesPaperStatus(string SalesId, string Status);
        #endregion

    }
}
