﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using Ninject;
using rBKK.Infrastructure.Procurement;
using System.Collections;
using System.Data;

namespace rBKK.Core.Procurement
{
    public class MasterProcurementService : IMasterProcurementService
    {
        [Inject]
        public IMasterProcurementRepository _masterProcurementRepository { get; set; }

        #region Hannan  ///////    
        public List<PUR_REQUISITION_TYPE> GetPurchaseType()
        {
            try
            {
                return _masterProcurementRepository.GetPurchaseType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PUR_PADDY_PURCHASE_TYPE> GetPaddyPurchaseType()
        {
            try
            {
                return _masterProcurementRepository.GetPaddyPurchaseType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PUR_VENDOR> GetVendorByTypeId(string VendorTypeId)
        {
            try
            {
                return _masterProcurementRepository.GetVendorByTypeId(VendorTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IQueryable<object> GetAllVendorTypes()
        {
            try
            {
                return _masterProcurementRepository.GetAllVendorTypes();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<PUR_CONSUMPTION_TYPE> GetAllConsumptionTypes()
        {
            try
            {
                return _masterProcurementRepository.GetAllConsumptionTypes();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SavePaddyRequisition()
        {
            try
            {
                _masterProcurementRepository.SavePaddyRequisition();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string SavePaddyRequisition(PUR_PURCHASE_REQUISITION req, List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails, List<PUR_PURCHASE_REQUISITION_BOSTA> lbag)
        {
            try
            {
                return _masterProcurementRepository.SavePaddyRequisition(req, lreqDetails, lbag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FN_PUR_GET_REQUISITION_LIST_Result> GetRequisitionsList()
        {
            try
            {
                return _masterProcurementRepository.GetRequisitionsList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Hashtable LoadPurchaseRequisitionOrderDetails(string RequisitionOrderID)
        {
            try
            {
                return _masterProcurementRepository.LoadPurchaseRequisitionOrderDetails(RequisitionOrderID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PUR_VENDOR_TYPE> GetVendorTypeByGenericId(string requisitionType)
        {
            try
            {
                return _masterProcurementRepository.GetVendorTypeByGenericId(requisitionType);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SaveProductionData(INV_INVENTORY_MASTER invMaster, List<INV_INVENTORY_DETAILS> invDetailsFrom, List<INV_INVENTORY_DETAILS> invDetailsTo, string loggedUserID)
        {
             _masterProcurementRepository.SaveProductionData(invMaster, invDetailsFrom, invDetailsTo, loggedUserID);
        }
        #endregion

        #region Mostafiz /////
        #region Vendor Type
        public PUR_VENDOR_TYPE GetVendorTypeByID(string VendorTypeId)
        {
            try
            {
                return _masterProcurementRepository.GetVendorTypeByID(VendorTypeId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<PUR_VENDOR_TYPE> GetAllVendorType()
        {
            try
            {
                return _masterProcurementRepository.GetAllVendorType();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
       
        public void AddVendorType(PUR_VENDOR_TYPE vendorType, string TransType)
        {
            try
            {
                _masterProcurementRepository.AddVendorType(vendorType, TransType);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteVendorType(string strVendorTypeId)
        {
            try
            {
                _masterProcurementRepository.DeleteVendorType(strVendorTypeId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Vendor

        public PUR_VENDOR GetVendorByID(string VendorTypeId)
        {
            try
            {
                return _masterProcurementRepository.GetVendorByID(VendorTypeId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<PUR_VENDOR> GetAllVendor()
        {
            try
            {
                return _masterProcurementRepository.GetAllVendor();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddVendorType(PUR_VENDOR vendor, string TransType)
        {
            try
            {
                _masterProcurementRepository.AddVendorType(vendor, TransType);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteVendor(string strVendorId)
        {
            try
            {
                _masterProcurementRepository.DeleteVendor(strVendorId);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        public string SavePaddyPurchaseOrder(PUR_PURCHASE_ORDER ord, List<PUR_PURCHASE_ORDER_DETAILS> lordDetails,bool bReceivedFinish)
        {
            try
            {
                return _masterProcurementRepository.SavePaddyPurchaseOrder(ord, lordDetails, bReceivedFinish);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Reports//Mostfiz

        public List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetRequsionReportsData(string strRequsionId)
        {
            return _masterProcurementRepository.GetRequsionReportsData(strRequsionId);
        }
        public List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequisionOrderReportsData(string intOrderId)
        {
            return _masterProcurementRepository.GetRequisionOrderReportsData(intOrderId);
        }

        public List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequsionReportsData(int rType, DateTime dtLDate, DateTime dtUDate)
        {
            return _masterProcurementRepository.GetRequisionOrderReportsData(rType, dtLDate,dtUDate);
        }

        public List<FN_RPT_INV_RICE_PRODUCTIONS_Result> GetProductionReportsData(string intOrderId)
        {
            return _masterProcurementRepository.GetProductionReportsData(intOrderId);
        }

        public List<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result> GetPSTData(int typeId, DateTime dtLDate, DateTime dtUDate)
        {
            return _masterProcurementRepository.GetPSTData(typeId,dtLDate, dtUDate);
        }

        public List<FN_RPT_INV_CURRENT_STOCK_Result> GetCurrentStock(int stockId)
        {
            return _masterProcurementRepository.GetCurrentStock(stockId);
        }

        #endregion

        public List<FN_PUR_GET_REQUISITION_Partial_Received_LIST_Result> Get_REQUISITION_PARTIAL_RECEIVED_LIST(long intRequisionId)
        {
            try
            {
                return _masterProcurementRepository.Get_REQUISITION_PARTIAL_RECEIVED_LIST(intRequisionId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public void SavePartialReceivedFinish(string strID)
        {
            try
            {
                _masterProcurementRepository.SavePartialReceivedFinish(strID);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }


        public List<FN_PUR_GET_PURCHASEORDERLIST_LIST_Result> GetPurchaseOrderList()
        {
            return _masterProcurementRepository.GetPurchaseOrderList();
        }


        public List<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetDateWiseRequision(DateTime dtLDate, DateTime dtUDate)
        {
            return _masterProcurementRepository.GetDateWiseRequision(dtLDate, dtUDate);
        }
    }
}
