﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using System.Collections;
using System.Data;

namespace rBKK.Core.Procurement
{
    public interface IMasterProcurementService
    {
        #region Hannan ///////////        
        List<PUR_REQUISITION_TYPE> GetPurchaseType();
        List<PUR_PADDY_PURCHASE_TYPE> GetPaddyPurchaseType();
        List<PUR_VENDOR> GetVendorByTypeId(string VendorTypeId);
        List<PUR_CONSUMPTION_TYPE> GetAllConsumptionTypes();
        void SavePaddyRequisition();
        string SavePaddyRequisition(PUR_PURCHASE_REQUISITION req, List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails, List<PUR_PURCHASE_REQUISITION_BOSTA> lbag);
        List<FN_PUR_GET_REQUISITION_LIST_Result> GetRequisitionsList();
        Hashtable LoadPurchaseRequisitionOrderDetails(string RequisitionOrderID);
        List<PUR_VENDOR_TYPE> GetVendorTypeByGenericId(string requisitionType);
        void SaveProductionData(INV_INVENTORY_MASTER invMaster, List<INV_INVENTORY_DETAILS> invDetailsFrom, List<INV_INVENTORY_DETAILS> invDetailsTo, string loggedUserID);
        List<FN_PUR_GET_PURCHASEORDERLIST_LIST_Result> GetPurchaseOrderList();

        #endregion
        #region Mostafiz /////
        #region Vendor Type
        PUR_VENDOR_TYPE GetVendorTypeByID(string VendorTypeId);

        List<PUR_VENDOR_TYPE> GetAllVendorType();
        IQueryable<object> GetAllVendorTypes();

        void AddVendorType(PUR_VENDOR_TYPE vendorType, string TransType);

        void DeleteVendorType(string strVendorTypeId);
        #endregion

        #region Vendor
        PUR_VENDOR GetVendorByID(string VendorTypeId);

        List<PUR_VENDOR> GetAllVendor();

        void AddVendorType(PUR_VENDOR vendor, string TransType);

        void DeleteVendor(string strVendorId);
        #endregion
        string SavePaddyPurchaseOrder(PUR_PURCHASE_ORDER ord, List<PUR_PURCHASE_ORDER_DETAILS> lordDetails, bool bFinishReceived);

        #region Partial Requision Received
        List<FN_PUR_GET_REQUISITION_Partial_Received_LIST_Result> Get_REQUISITION_PARTIAL_RECEIVED_LIST(long intRequisionId); 
        #endregion

        #endregion

        #region Reports ///Mostafiz
        List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetRequsionReportsData(string strRequsionId);
        List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequisionOrderReportsData(string intOrderId);
        List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequsionReportsData(int rType, DateTime dtLDate, DateTime dtUDate);//Date Wise Purchase
        List<FN_RPT_INV_RICE_PRODUCTIONS_Result> GetProductionReportsData(string intOrderId);
        List<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result> GetPSTData(int typeId, DateTime dtLDate, DateTime dtUDate);
        List<FN_RPT_INV_CURRENT_STOCK_Result> GetCurrentStock(int stockId);
        List<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetDateWiseRequision(DateTime dtLDate, DateTime dtUDate);

        #endregion
        
        void SavePartialReceivedFinish(string strID);

       
    }
}
