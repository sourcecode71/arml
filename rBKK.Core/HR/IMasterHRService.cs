﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using System.Collections;

namespace rBKK.Core.HR
{
    public interface IMasterHRService
    {
        List<CORE_RESULT> GetAllResults();

        List<CORE_GRADE> GetAllGrades();
        CORE_GRADE GetGrade(int gradeId);
        string AddGrade(string transactionType, CORE_GRADE new_grade);
        string DeleteGrade(int gradeId);

        List<HR_EMPLOYEE> GetAllEmployees();
        List<HR_EMPLOYEE> GetEmployeesByStatus(bool isPermanent);
        List<HR_EMPLOYEE> GetSearchedEmployees(string employeeId, string employeeName, string designation, string contactNo);
        HR_EMPLOYEE GetEmployee(Int64 employeeId);
        Int64 AddEmployee(HR_EMPLOYEE employee);
        Int64 AddEmployee(string transactionType, HR_EMPLOYEE new_employee, HR_EMPLOYEE_COMPANY_INFO new_employee_Company_Info, HR_EDUCATION new_education, HR_EXPERIENCE new_experience);
        void DeleteEmployee(Int64 employeeId);

        List<HR_LEAVE_TYPE> GetAllLeaveTypes();
        HR_LEAVE_TYPE GetLeaveType(int leaveTypeId);
        string AddLeaveType(string transactionType, HR_LEAVE_TYPE new_leave_type);
        string DeleteLeaveType(int leaveTypeId);

        List<HR_LEAVE_APPLICATION> GetAllLeaveApplications();
        HR_LEAVE_APPLICATION GetLeaveApplication(int leaveApplicationId);
        List<HR_LEAVE_APPLICATION> GetLeaveApplicationByEmployeeId(Int64 employeeId);
        List<HR_LEAVE_APPLICATION> GetLeaveApplicationByLeaveStatus(int leaveStatus);
        List<HR_LEAVE_APPLICATION> GetSearchedEmployeesWhoAreOnLeave(string employeeId, string employeeName, string fromDate, string toDate, int leaveStatus);
        string AddLeaveApplication(string transactionType, HR_LEAVE_APPLICATION new_leave_application);
        void DeleteLeaveApplication(int leaveApplicationId);

        List<HR_EMPLOYEE_LEAVE_STATUS> GetLeaveStatusByEmployee(Int64 employeeId);

        List<HR_EMPLOYEE_ATTENDANCE> GetAllEmployeeAttendance();
        List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByDate(string attendanceDate);
        List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByDateRange(string fromDate, string toDate);
        List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByEmployeeIdByDateRange(Int64 employeeId, string fromDate, string toDate);
        void AddEmployeeAttendance(List<HR_EMPLOYEE_ATTENDANCE> new_employee_attendances);
    }
}
