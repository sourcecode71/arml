﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using Ninject;
using System.Collections;
using rBKK.Infrastructure.HR;

namespace rBKK.Core.HR
{
    public class LeaveReports
    {
        public Int64 EmployeeId { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public Int32 LeaveApplicationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int LeaveStatus { get; set; }
    }
}
