﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using Ninject;
using System.Collections;
using rBKK.Infrastructure.HR;

namespace rBKK.Core.HR
{
    public class MasterHRService : IMasterHRService
    {
        [Inject]
        public IMasterHRRepository _masterHRRepository { get; set; }

        public List<CORE_RESULT> GetAllResults()
        {
            try
            {
                return _masterHRRepository.GetAllResults();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CORE_GRADE> GetAllGrades()
        {
            try
            {
                return _masterHRRepository.GetAllGrades();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CORE_GRADE GetGrade(int gradeId)
        {
            try
            {
                return _masterHRRepository.GetGrade(gradeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string AddGrade(string transactionType, CORE_GRADE new_grade)
        {
            try
            {
                return _masterHRRepository.AddGrade(transactionType, new_grade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteGrade(int gradeId)
        {
            try
            {
                return _masterHRRepository.DeleteGrade(gradeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HR_EMPLOYEE> GetAllEmployees()
        {
            try
            {
                return _masterHRRepository.GetAllEmployees();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE> GetEmployeesByStatus(bool isPermanent)
        {
            try
            {
                return _masterHRRepository.GetEmployeesByStatus(isPermanent);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE> GetSearchedEmployees(string employeeId, string employeeName, string designation, string contactNo)
        {
            try
            {
                return _masterHRRepository.GetSearchedEmployees(employeeId, employeeName, designation, contactNo);
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }
        public HR_EMPLOYEE GetEmployee(Int64 employeeId)
        {
            try
            {
                return _masterHRRepository.GetEmployee(employeeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int64 AddEmployee(HR_EMPLOYEE employee)
        {
            try
            {
                return _masterHRRepository.AddEmployee(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int64 AddEmployee(string transactionType, HR_EMPLOYEE new_employee, HR_EMPLOYEE_COMPANY_INFO new_employee_Company_Info, HR_EDUCATION new_education, HR_EXPERIENCE new_experience)
        {
            try
            {
                return _masterHRRepository.AddEmployee(transactionType, new_employee, new_employee_Company_Info, new_education, new_experience);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteEmployee(Int64 employeeId)
        {
            try
            {
                _masterHRRepository.DeleteEmployee(employeeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HR_LEAVE_TYPE> GetAllLeaveTypes()
        {
            try
            {
                return _masterHRRepository.GetAllLeaveTypes();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HR_LEAVE_TYPE GetLeaveType(int leaveTypeId)
        {
            try
            {
                return _masterHRRepository.GetLeaveType(leaveTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string AddLeaveType(string transactionType, HR_LEAVE_TYPE new_leave_type)
        {
            try
            {
                return _masterHRRepository.AddLeaveType(transactionType, new_leave_type);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DeleteLeaveType(int leaveTypeId)
        {
            try
            {
                return _masterHRRepository.DeleteLeaveType(leaveTypeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<HR_LEAVE_APPLICATION> GetAllLeaveApplications()
        {
            try
            {
                return _masterHRRepository.GetAllLeaveApplications();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HR_LEAVE_APPLICATION GetLeaveApplication(int leaveApplicationId)
        {
            try
            {
                return _masterHRRepository.GetLeaveApplication(leaveApplicationId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_LEAVE_APPLICATION> GetLeaveApplicationByEmployeeId(Int64 employeeId)
        {
            try
            {
                return _masterHRRepository.GetLeaveApplicationByEmployeeId(employeeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_LEAVE_APPLICATION> GetLeaveApplicationByLeaveStatus(int leaveStatus)
        {
            try
            {
                return _masterHRRepository.GetLeaveApplicationByLeaveStatus(leaveStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_LEAVE_APPLICATION> GetSearchedEmployeesWhoAreOnLeave(string employeeId, string employeeName, string fromDate, string toDate, int leaveStatus)
        {
            try
            {
                return _masterHRRepository.GetSearchedEmployeesWhoAreOnLeave(employeeId, employeeName, fromDate, toDate, leaveStatus);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string AddLeaveApplication(string transactionType, HR_LEAVE_APPLICATION new_leave_application)
        {
            try
            {
                return _masterHRRepository.AddLeaveApplication(transactionType, new_leave_application);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteLeaveApplication(int leaveApplicationId)
        {
            try
            {
                _masterHRRepository.DeleteLeaveApplication(leaveApplicationId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HR_EMPLOYEE_LEAVE_STATUS> GetLeaveStatusByEmployee(Int64 employeeId) 
        {
            try
            {
                return _masterHRRepository.GetLeaveStatusByEmployee(employeeId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE_ATTENDANCE> GetAllEmployeeAttendance()
        {
            try
            {
                return _masterHRRepository.GetAllEmployeeAttendance();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByDate(string attendanceDate)
        {
            try
            {
                return _masterHRRepository.GetEmployeeAttendanceByDate(attendanceDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByDateRange(string fromDate, string toDate)
        {
            try
            {
                return _masterHRRepository.GetEmployeeAttendanceByDateRange(fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<HR_EMPLOYEE_ATTENDANCE> GetEmployeeAttendanceByEmployeeIdByDateRange(Int64 employeeId, string fromDate, string toDate)
        {
            try
            {
                return _masterHRRepository.GetEmployeeAttendanceByEmployeeIdByDateRange(employeeId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddEmployeeAttendance(List<HR_EMPLOYEE_ATTENDANCE> new_employee_attendances)
        {
            try
            {
                _masterHRRepository.AddEmployeeAttendance(new_employee_attendances);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
