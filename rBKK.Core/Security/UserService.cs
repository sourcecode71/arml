﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using Ninject;
using rBKK.Infrastructure.Security;

namespace rBKK.Core.Security
{
    public class UserService : IUserService
    {
        [Inject]
        public IUserRepository _userRepository { get; set; }

        public List<SEC_USER> GetAllUsers()
        {
            return _userRepository.GetAllUsers();
        }
        public void AddUser(SEC_USER objSecUser, string setUser)
        {
            _userRepository.AddUser(objSecUser, setUser);
        }

        #region Mostaq  ///   
        public SEC_USER GetUserByID(string strUserId)
        {
            return _userRepository.GetUserByID(strUserId);
        }
        #endregion

        #region Han  ///////
        public SEC_USER GetUserInfoById(string UserId)
        {
            return _userRepository.GetUserInfoById(UserId);
        }
        public List<COM_MODULE> GetAllModules()
        {
            return _userRepository.GetAllModules();
        }
        public List<SEC_GROUP> GetAllUserGroups()
        {
            return _userRepository.GetAllUserGroups();
        }
        public string CheckUserAuthentication(string UserId, string Password)
        {
            return _userRepository.CheckUserAuthentication(UserId, Password);
        }
        public string CheckUserPagePermission(string UserId, string PageId)
        {
            return _userRepository.CheckUserPagePermission(UserId, PageId);
        }
        public void SaveUserSession(string strGuid, string strUserId, string strPassword, string strHostIP)
        {
             _userRepository.SaveUserSession(strGuid, strUserId, strPassword, strHostIP);
        }
        public List<FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID_Result> GetGroupPermissionByUserId(string UserId)
        {
            return _userRepository.GetGroupPermissionByUserId(UserId);
        }
        public void AddUser(SEC_USER objUser, List<SEC_USER_GROUP> lstUserGroup, string OperationType, string SetUser)
        {
            _userRepository.AddUser(objUser, lstUserGroup, OperationType, SetUser);
        }
        public List<FN_SEC_GET_OBJECTS_PERMISSION_BY_GROUP_ID_Result> GetPageObjectsByUserGroupId(string groupID)
        {
            return _userRepository.GetPageObjectsByUserGroupId(groupID);
        }
        public void AddGroupObjectsPermission(List<SEC_OBJECTS_GROUP_PERMISSION> lstGroupPermission, string groupId, string UserId)
        {
            _userRepository.AddGroupObjectsPermission(lstGroupPermission, groupId, UserId);
        }
        public void AddUserPermissionsOverride(List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> lstUserPermission, string UserId)
        {
            _userRepository.AddUserPermissionsOverride(lstUserPermission, UserId);
        }
        public List<FN_SEC_GET_USER_OBJECTS_PERMISSION_BY_USER_ID_Result> GetPageObjectsByUserId(string UserId)
        {
            return _userRepository.GetPageObjectsByUserId(UserId);
        }
        public string GetGroupsNameByUserId(string UserId)
        {
            return _userRepository.GetGroupsNameByUserId(UserId);
        }
        #endregion

    }
}
