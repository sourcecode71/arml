﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Security;
using Resources;
using rBKK.Entities;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Security
{
    public partial class GroupWisePagePermission : System.Web.UI.Page
    {
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.GroupWisePermission).ToString());

            if (!IsPostBack)
            {
                try
                {
                    lblMessage.Visible = false;
                    this.FillUserGroupsList();
                }
                catch (Exception ex)
                {
                    ShowMessage(lblMessage, ex.Message, MessageType.Error);
                }
            }
        }
        private void ShowMessage(Label lblErrorMsg, string msg, MessageType msgType)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(msg, msgType);
            //lblErrorMsg.Visible = true;
            //lblErrorMsg.Text = msg;
            //lblErrorMsg.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
            //AppHelper.Logger.Warn("AdminAreaWarning. UserName: " + _user.Username + " Error: " + msg);
        }
        private void FillUserGroupsList()
        {
            ddlUserGroup.DataSource = _userService.GetAllUserGroups();
            ddlUserGroup.DataTextField = "GROUP_NAME";
            ddlUserGroup.DataValueField = "GROUP_ID";
            ddlUserGroup.DataBind();
            ddlUserGroup.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, String.Empty));
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string setUser =HttpContext.Current.Session["USER_ID"].ToString();

                List<SEC_OBJECTS_GROUP_PERMISSION> lstGroupPermission = this.BuildGroupPermissionObjectPermissions(ddlUserGroup.SelectedValue.ToString(), setUser);
                _userService.AddGroupObjectsPermission(lstGroupPermission, ddlUserGroup.SelectedValue.ToString(), setUser);

                ShowMessage(lblMessage,Localization.RECORD_SAVED_SUCCESS, MessageType.Info);
            }
            catch (Exception ex)
            {
                ShowMessage(lblMessage, ex.Message, MessageType.Error);
            }
        }

        private List<SEC_OBJECTS_GROUP_PERMISSION> BuildGroupPermissionObjectPermissions(string groupId, string setUser)
        {
            try
            {
                List<SEC_OBJECTS_GROUP_PERMISSION> lstUserGroup = new List<SEC_OBJECTS_GROUP_PERMISSION>();
                SEC_OBJECTS_GROUP_PERMISSION objGroupObjects;
                foreach (GridViewRow row in gvGroupObjects.Rows)
                {
                    CheckBox chkView = (CheckBox)row.FindControl("chkView");
                    CheckBox chkAdd = (CheckBox)row.FindControl("chkAdd");
                    CheckBox chkEdit = (CheckBox)row.FindControl("chkEdit");
                    CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");
                    CheckBox chkPrint = (CheckBox)row.FindControl("chkPrint");

                    if ((chkView != null && chkAdd != null && chkEdit != null && chkDelete != null && chkPrint != null) &&
                        (chkView.Checked || chkAdd.Checked || chkEdit.Checked || chkDelete.Checked || chkPrint.Checked)
                        )
                    {
                        objGroupObjects = new SEC_OBJECTS_GROUP_PERMISSION();
                        objGroupObjects.OBJECT_ID = row.Cells[0].Text.ToString();
                        objGroupObjects.GROUP_ID = groupId;
                        objGroupObjects.SET_USER = setUser;
                        objGroupObjects.SET_DATE = DateTime.UtcNow;
                        objGroupObjects.VIEW_BIT = chkView.Checked;
                        objGroupObjects.ADD_BIT = chkAdd.Checked;
                        objGroupObjects.EDIT_BIT = chkEdit.Checked;
                        objGroupObjects.DELETE_BIT = chkDelete.Checked;
                        objGroupObjects.PRINT_BIT = chkPrint.Checked;

                        lstUserGroup.Add(objGroupObjects);
                    }
                }

                return lstUserGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
        }

        protected void ddlUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Visible = false;
                this.GetPageObjectsByUserGroupId(ddlUserGroup.SelectedValue.ToString());
            }
            catch (Exception ex)
            {
                ShowMessage(lblMessage, ex.Message, MessageType.Error);
            }
        }

        private void GetPageObjectsByUserGroupId(string groupID)
        {
            var query= _userService.GetPageObjectsByUserGroupId(groupID);
            gvGroupObjects.DataSource = query;
            gvGroupObjects.DataBind();
        }
    }
}