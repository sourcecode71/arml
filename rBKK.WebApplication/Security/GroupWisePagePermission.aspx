﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GroupWisePagePermission.aspx.cs" Inherits="rBKK.WebApplication.Security.GroupWisePagePermission" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div class="PageDetial">
        <fieldset class="GroupObjects">
            <legend class="lgView">Group wise permission </legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay" width="700px">
                    <tr>
                    <td colspan="3">
                        <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                            style="text-align: center" Width="461px"></asp:Label>
                       </td>
                    </tr>
                            <tr>
                                <td>
                                   <span>user Group name</span></td>
                                <td>
                                    <asp:DropDownList ID="ddlUserGroup" runat="server" Width="230px" 
                                        onselectedindexchanged="ddlUserGroup_SelectedIndexChanged" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td class="style1">
                                    &nbsp;</td>
                        </tr>
                   
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gvGroupObjects" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                                    style="font-size: 9pt" Width="100%">
                                    <HeaderStyle CssClass="dataScheduleHeader"/>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="OBJECT_ID" HeaderText="PageID">
                                        <HeaderStyle CssClass="HiddenGridColumn" />
                                        <ItemStyle CssClass="HiddenGridColumn" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODULE_NAME" HeaderText="Module" />
                                        <asp:BoundField DataField="OBJECT_NAME" HeaderText="Page Name" />
                                        <asp:TemplateField HeaderText="View">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkView" runat="server" 
                                                    Checked='<%# Convert.ToBoolean(Eval("VIEW_BIT")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Add">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAdd" runat="server" 
                                                    Checked='<%# Convert.ToBoolean(Eval("ADD_BIT")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkEdit" runat="server" 
                                                    Checked='<%# Convert.ToBoolean(Eval("EDIT_BIT")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" 
                                                    Checked='<%# Convert.ToBoolean(Eval("DELETE_BIT")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Print">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkPrint" runat="server" 
                                                    Checked='<%# Convert.ToBoolean(Eval("PRINT_BIT")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td class="style1">
                                &nbsp;</td>
                        </tr>
                   
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                            </td>
                            <td class="style1">
                                &nbsp;</td>
                        </tr>
                   
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>
</asp:Content>
