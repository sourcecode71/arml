﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="rBKK.WebApplication.Security.UserInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <style type="text/css">
    .water
    {
         font-family: Tahoma, Arial, sans-serif;
         color:gray;
    }
    </style>
    <script type="text/javascript">
        $(function () {

            $(".water").each(function () {
                $tb = $(this);
                if ($tb.val() != this.title) {
                    $tb.removeClass("water");
                }
            });

            $(".water").focus(function () {
                $tb = $(this);
                if ($tb.val() == this.title) {
                    $tb.val("");
                    $tb.removeClass("water");
                }
            });

            $(".water").blur(function () {
                $tb = $(this);
                if ($.trim($tb.val()) == "") {
                    $tb.val(this.title);
                    $tb.addClass("water");
                }
            });
        });       
 
    </script>
<%--    <script type="text/javascript">
        jQuery(document).ready(function () {


            jQuery("#aspnetForm33").validationEngine();


        });
     </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="detailPageMiddle"> 
       <fieldset class="GroupObjects">
                                    <legend class="lgView"> User information </legend>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                          
                                            <table class="bgdisplay" width="700px">
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                                                            style="text-align: center" Width="461px"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                        <span>ID</span>&nbsp;&nbsp;</td>
                                                    <td>
                                                        <asp:TextBox ID="txtId" runat="server" 
                                                            Width="226px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="txtId" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td style="width:300px" rowspan="9">
                                                        <fieldset>
                                                            <legend class="lgView">Group Wise Permission </legend>
                                                            <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="False" 
                                                                CellPadding="4" DataKeyNames="GROUP_ID" ForeColor="#333333" GridLines="None" 
                                                                style="font-size: 9pt" Width="100%">
                                                                <HeaderStyle CssClass="dataScheduleHeader" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="GROUP_ID" HeaderText="GroupId" >
                                                                    <HeaderStyle CssClass="HiddenGridColumn" />
                                                                    <ItemStyle CssClass="HiddenGridColumn" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="GROUP_NAME" HeaderText="Name" />
                                                                    <asp:TemplateField HeaderText="Y/N">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkActivation" runat="server" 
                                                                                Checked='<%# Convert.ToBoolean(Eval("ACTIVE_YN").ToString().Equals("1")) %>' />
                                                                        </ItemTemplate>
                                                                        <ItemStyle Width="50px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <EditRowStyle BackColor="#2461BF" />
                                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                <RowStyle BackColor="#EFF3FB" />
                                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                            </asp:GridView>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                        <span>Password </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtPassword" runat="server" 
                                                            Width="226px" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtPassword" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                        <span>Confirm Password </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtConfirmPassword" runat="server" Width="226px" 
                                                            TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="txtConfirmPassword" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                        <span>Title </span>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtTitle" runat="server" 
                                                            Width="226px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                                            ControlToValidate="txtTitle" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                       <span>Frist Name</span> </td>
                                                    <td>
                                                        <asp:TextBox ID="txtFristName" runat="server" Width="226px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                            ControlToValidate="txtFristName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                       <span>Last Name </span> </td>
                                                    <td >
                                                        <asp:TextBox ID="txtLastName" runat="server" Width="226px" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                            ControlToValidate="txtLastName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                        <span>e-mail </span> </td>
                                                    <td >
                                                        <asp:TextBox ID="txtemail" runat="server" Width="226px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                    <span>Phone</span>
                                                    </td>
                                                    <td >
                                                        <asp:TextBox ID="txtPhone" runat="server" Width="226px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Active (Yes/No)</span>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkActiveYN" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                    <td >
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
</div>

<div class="detailPageMiddle">

 <fieldset class="GroupObjects">
 <legend class="lgView"> User list </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      
         <ContentTemplate>
             <asp:GridView ID="gvUser" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" DataKeyNames="USER_ID" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="ID">
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("USER_ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="TITLE" HeaderText="TITLE"/>
                     <asp:BoundField DataField="FIRST_NAME" HeaderText="FRIST NAME" />
                     <asp:BoundField DataField="LAST_NAME" HeaderText="LAST NAME" />
                     <asp:BoundField DataField="EMAIL" HeaderText="e-MAIL" />
                     <asp:BoundField DataField="CELL_PHONE" HeaderText="PHONE" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>



 

</asp:Content>
