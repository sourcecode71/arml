﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Core.Security;
using rBKK.Entities;
using Ninject;
using rBKK.WebApplication.Common;
using Resources;

namespace rBKK.WebApplication.Security
{
    public partial class UserInfo : System.Web.UI.Page
    {

        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddUser).ToString());

            if (!IsPostBack)
            {
                try
                {
                    this.GetUsersInfo(string.Empty);
                    this.GetGroupPermissionByUserId("0000");
                    ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
                }
                catch (Exception ex)
                {
                    ShowMessage(lblMessage, ex.Message, MessageType.Error);
                    //lblMessage.Text = ex.Message;
                } 
            }
        }

        private void ShowMessage(Label lblErrorMsg, string msg, MessageType msgType)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(msg, msgType);
            //lblErrorMsg.Visible = true;
            //lblErrorMsg.Text = msg;
            //lblErrorMsg.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
            //AppHelper.Logger.Warn("AdminAreaWarning. UserName: " + _user.Username + " Error: " + msg);
        }
        private void GetGroupPermissionByUserId(string UserId)
        {
            try
            {
                List<FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID_Result> results = _userService.GetGroupPermissionByUserId(UserId);
                gvCategory.DataSource = results;
                gvCategory.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void GetUsersInfo(string strquery)
        {
            try
            {
                List<SEC_USER> users = _userService.GetAllUsers();
                gvUser.DataSource = users;
                gvUser.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtConfirmPassword.Text != txtPassword.Text)
                {
                    ShowMessage(lblMessage, Localization.CONFIRM_PASSWORD_MISMATCH, MessageType.Error);
                    //lblMessage.Text = "Confirm password mismatch. Please check...";
                    return;
                }
                string UserId = HttpContext.Current.Session["USER_ID"].ToString();

                SEC_USER objUser = new SEC_USER();
                objUser.USER_ID = txtId.Text.Trim();
                objUser.TITLE = txtTitle.Text.Trim();
                objUser.FIRST_NAME = txtFristName.Text.Trim();
                objUser.LAST_NAME = txtLastName.Text.Trim();
                objUser.CELL_PHONE = txtPhone.Text.Trim();
                objUser.EMAIL = txtemail.Text.Trim();
                objUser.RAW_PASSWORD = txtPassword.Text.Trim();
                objUser.ENCRIPTED_PASSWORD = txtPassword.Text.Trim();
                objUser.ACTIVE_YN = chkActiveYN.Checked;
                objUser.SET_USER = UserId;
                objUser.SET_DATE = DateTime.Now;
                List<SEC_USER_GROUP> lstUserGroup = this.BuildUserGroupPermissionObject(UserId);
                if (btnSave.Text == "Save")
                    _userService.AddUser(objUser, lstUserGroup, Common.OperationType.SAVE.ToString(), objUser.SET_USER);
                else if (btnSave.Text == "Update")
                    _userService.AddUser(objUser, lstUserGroup, Common.OperationType.UPDATE.ToString(), objUser.SET_USER);
                
                ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS , MessageType.Info);
                this.ClearCoontrol();
                this.GetUsersInfo(string.Empty);
                this.GetGroupPermissionByUserId("0000");
            }
            catch (Exception ex)
            {
                ShowMessage(lblMessage, ex.Message, MessageType.Error);
                //lblMessage.Text = ex.Message;
            }
        }

        private void ClearCoontrol()
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
            btnSave.Text = "Save";
            txtId.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtFristName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtemail.Text = string.Empty;
            txtPhone.Text = string.Empty;
            chkActiveYN.Checked = false;
        }

        private List<SEC_USER_GROUP> BuildUserGroupPermissionObject(string UserId)
        {
            List<SEC_USER_GROUP> lstUserGroup = new List<SEC_USER_GROUP>();
            SEC_USER_GROUP objUserGroup;
            foreach (GridViewRow row in gvCategory.Rows)
            {
                CheckBox chk = (CheckBox)row.FindControl("chkActivation");
                if (chk != null && chk.Checked)
                {
                    objUserGroup = new SEC_USER_GROUP();
                    objUserGroup.GROUP_ID = row.Cells[0].Text.ToString();
                    objUserGroup.USER_ID = txtId.Text.Trim();
                    objUserGroup.SET_USER = UserId;
                    objUserGroup.SET_DATE = DateTime.UtcNow;
                    lstUserGroup.Add(objUserGroup);
                }
            }

            return lstUserGroup;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ClearCoontrol();
        }

       

        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                SEC_USER _user = new SEC_USER();
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string struserID = Convert.ToString(((Label)gvUser.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                SEC_USER users = _userService.GetUserByID(struserID);
                this.GetGroupPermissionByUserId(struserID);
                txtId.Text = users.USER_ID;
                txtFristName.Text = users.FIRST_NAME;
                txtLastName.Text = users.LAST_NAME;
                txtTitle.Text = users.TITLE;
                txtPhone.Text = users.CELL_PHONE;
                txtemail.Text = users.EMAIL;
                chkActiveYN.Checked = users.ACTIVE_YN;
                txtPassword.Text = users.RAW_PASSWORD;
                txtConfirmPassword.Text = users.RAW_PASSWORD;
                btnSave.Text = "Update";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

       
    }
}