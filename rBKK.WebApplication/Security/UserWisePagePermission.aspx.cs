﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Security;
using rBKK.Entities;
using rBKK.WebApplication.Common;
using Resources;

namespace rBKK.WebApplication.Security
{
    public partial class UserWisePagePermission : System.Web.UI.Page
    {
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.UserWisepermission).ToString());
            if (!IsPostBack)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
                lblMessage.Visible = false;
            }
        }
        private void ShowMessage(Label lblErrorMsg, string msg, MessageType msgType)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(msg, msgType);
            //lblErrorMsg.Visible = true;
            //lblErrorMsg.Text = msg;
            //lblErrorMsg.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
            //AppHelper.Logger.Warn("AdminAreaWarning. UserName: " + _user.Username + " Error: " + msg);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string UserId = txtUserId.Text.Trim();
                string setUser=HttpContext.Current.Session["USER_ID"].ToString();
                List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> lstGroupPermission = this.BuildUserPermissions(setUser);
                _userService.AddUserPermissionsOverride(lstGroupPermission, UserId);
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
            }
            catch (Exception ex)
            {
                //ShowMessage(lblMessage, ex.Message, MessageType.Error);
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        private List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> BuildUserPermissions(string setUser)
        {
            try
            {
                List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> lstUserGroup = new List<SEC_OBJECT_USER_PERMISSION_OVERRIDE>();
                SEC_OBJECT_USER_PERMISSION_OVERRIDE objGroupObjects;
                foreach (GridViewRow row in gvUserObjects.Rows)
                {
                    CheckBox chkView = (CheckBox)row.FindControl("chkView");
                    CheckBox chkAdd = (CheckBox)row.FindControl("chkAdd");
                    CheckBox chkEdit = (CheckBox)row.FindControl("chkEdit");
                    CheckBox chkDelete = (CheckBox)row.FindControl("chkDelete");
                    CheckBox chkPrint = (CheckBox)row.FindControl("chkPrint");

                    if ((chkView != null && chkAdd != null && chkEdit != null && chkDelete != null && chkPrint != null) &&
                        (chkView.Checked || chkAdd.Checked || chkEdit.Checked || chkDelete.Checked || chkPrint.Checked)
                        )
                    {
                        objGroupObjects = new SEC_OBJECT_USER_PERMISSION_OVERRIDE();
                        objGroupObjects.OBJECT_ID = row.Cells[0].Text.ToString();
                        objGroupObjects.USER_ID = txtUserId.Text.Trim();
                        objGroupObjects.SET_USER = setUser;
                        objGroupObjects.SET_DATE = DateTime.Now;
                        objGroupObjects.VIEW_BIT = chkView.Checked;
                        objGroupObjects.ADD_BIT = chkAdd.Checked;
                        objGroupObjects.EDIT_BIT = chkEdit.Checked;
                        objGroupObjects.DELETE_BIT = chkDelete.Checked;
                        objGroupObjects.PRINT_BIT = chkPrint.Checked;

                        lstUserGroup.Add(objGroupObjects);
                    }
                }

                return lstUserGroup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetPageObjectsByUserId(string UserId)
        {
            var query = _userService.GetPageObjectsByUserId(UserId);
            gvUserObjects.DataSource = query;
            gvUserObjects.DataBind();
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
            lblMessage.Visible = false;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Visible = false;
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
                SEC_USER ou = _userService.GetUserInfoById(txtUserId.Text.Trim().ToString());
                string gn = _userService.GetGroupsNameByUserId(txtUserId.Text.Trim().ToString());
                lblName.Text = ou.FIRST_NAME + " " + ou.LAST_NAME;
                lblMemberGroups.Text = gn;
                this.GetPageObjectsByUserId(txtUserId.Text.Trim().ToString());
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}