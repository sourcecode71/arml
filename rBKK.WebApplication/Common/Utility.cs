﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rBKK.WebApplication.Common
{
    /*
     *--------- Module------------
        11	Human Resource
        12	Procurement
        13	Inventory
        14	Sales
        15	Finance
        16	Administrator
     *  17  Security
  
     * 
     * Procurement
     * 
12001	Add Vendor Type
12002	Add Vendor
12003	Generate Purchase Order
12004	Product Procurement
     * 
     * Inventory
     * 
13001	Add Product Type
13002	Add Product
13003	Add Factory
13004	Add Godown
13005	Machinery Type
13006	Factory Godown Mapping
13007	Unit of Measurement
13008	Add Bank
13009	Add Bank Branch
13010	Create Bank Accounts
13011	Create Loan
13012	Rice Production
13013	Receive Finish Goods
     * * 
     * Security
     * 
17001	Add User
17002	Group wise permission
17003	User wise permission     
     
     */
    public enum PagesName
    {
        //Procurement
        AddVendorType = 12001,
        AddVendor = 12002,
        GeneratePurchaseOrder = 12003,
        ProductProcurement = 12004,
        // Inventory
        AddProductType = 13001,
        AddProduct = 13002,
        AddFactory = 13003,
        AddGodown = 13004,
        MachineryType = 13005,
        FactoryGodownMapping = 13006,
        UnitOfMeasurement = 13007,
        AddBank=13008,
        AddBankBranch=13009,
        CreateBankAccounts = 13010,
        CreateLoan = 13011,
        RiceProduction = 13012,
        ReceiveFinishGoods = 13013,
        GodownTransfer = 13014,
        PledgeTransfer = 13015,
        RiceSales = 13016,

        SalesPaperAttachment=14001,

        AddUser = 17001,
        GroupWisePermission = 17002,
        UserWisepermission = 17003
    }


    public enum MessageType { Error, Info }
    public enum OperationType { SAVE, UPDATE }
    public enum SessionVariable
    {
        INV_GODOWN = 1,
        UNIT_OF_MEASUREMENT_ID,


        // Han //
        CONSUMPTION_TYPE,
        PRODUCT_LIST,
        MACHINE_TYPE,
        PUR_PADDY,
        INV_FACTORY

    }

    //// Hannan /////
    /*
   1	Puddy
   3	Rice
   4	Fuel
   6	Machine
   8	Construction
   9	Others
    */
    public enum PurchaseType
    {
        PADDY = 1,
        PRODUCTION=2,
        RICE=3,
        FUEL = 4,
        GODOWN_TRANSFER=5,
        MACHINE = 6,
        PLEDGE_TRANSFER=7,
        CONSTRUCTION=8,
        OTHERS=9,
        SALES=10,
        Received_FG=11
    }



    public enum ddlname
    {
        Godown, MEASUREMENTUNIT
    }

    public enum EnumLeaveStatus
    {
        Created = 1,
        Approved = 2
    }


    public class Utility
    {
    }
}