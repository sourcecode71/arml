//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "10.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Localization {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Localization() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Localization", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete {0} ?.
        /// </summary>
        internal static string CONFIRM_DELETE_RECORD {
            get {
                return ResourceManager.GetString("CONFIRM_DELETE_RECORD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm password mismatch. Please check....
        /// </summary>
        internal static string CONFIRM_PASSWORD_MISMATCH {
            get {
                return ResourceManager.GetString("CONFIRM_PASSWORD_MISMATCH", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string EMPTY_MESSAGE {
            get {
                return ResourceManager.GetString("EMPTY_MESSAGE", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User ID or Password incorrect. Please check....
        /// </summary>
        internal static string LOGIN_ERROR_MSG {
            get {
                return ResourceManager.GetString("LOGIN_ERROR_MSG", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No data found.
        /// </summary>
        internal static string NO_DATA_FOUND {
            get {
                return ResourceManager.GetString("NO_DATA_FOUND", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record has been deleted successfully..
        /// </summary>
        internal static string RECORD_DELETED_SUCCESS {
            get {
                return ResourceManager.GetString("RECORD_DELETED_SUCCESS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record not found. Please check....
        /// </summary>
        internal static string RECORD_NOT_FOUND {
            get {
                return ResourceManager.GetString("RECORD_NOT_FOUND", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record has been saved successfully..
        /// </summary>
        internal static string RECORD_SAVED_SUCCESS {
            get {
                return ResourceManager.GetString("RECORD_SAVED_SUCCESS", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;&lt; Select &gt;&gt;.
        /// </summary>
        internal static string SELECT_DDL_ITEMS {
            get {
                return ResourceManager.GetString("SELECT_DDL_ITEMS", resourceCulture);
            }
        }
    }
}
