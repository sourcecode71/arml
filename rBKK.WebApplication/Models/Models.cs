﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rBKK.WebApplication.Models
{
    public interface IModels
    {
        string GetName();
    }

    public class ModelsA : IModels
    {
        public string GetName()
        {
            return "Model A";
        }
    }

    public class ModelsB : IModels
    {
        public string GetName()
        {
            return "Model B";
        }
    }
}