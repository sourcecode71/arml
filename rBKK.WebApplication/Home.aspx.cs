﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.WebApplication.Models;
using rBKK.Core.Security;
using Resources;

namespace rBKK.WebApplication
{
    public partial class Home : System.Web.UI.Page
    {
        [Inject]
        public IModels model { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsCallback)
                {
                    //HttpContext.Current.Session.Add("USER_ID", "");
                    HttpContext.Current.Session.Add("MODULE_ID", "00");
                    HttpContext.Current.Session.Add("PAGE_ID", "10000");
                }
                // Label1.Text = model.GetName();    
                //var query = _userService.GetAllUsers();
                //GridView1.DataSource = query;
                //GridView1.DataBind();
                //((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
            }
            catch //(Exception)
            {
                Response.Redirect("~/Login.aspx", false);
            }
        }

       
    }
}
