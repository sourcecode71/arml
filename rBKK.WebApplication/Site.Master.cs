﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.WebApplication.Common;
using Resources;
using Ninject;
using rBKK.Core.Security;
using rBKK.Entities;

namespace rBKK.WebApplication
{
    public partial class Site : System.Web.UI.MasterPage
    {
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.CheckPageAuthentication();

                CreateMenuControl();
                SetupNavBar();

                if (!Page.IsPostBack)
                {
                    //CreateMenuControl();
                    //SetupNavBar();
                    this.FillModulesList();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageType.Error);
            }
        }

        private void CheckPageAuthentication()
        {
            try
            {
                string UserId = HttpContext.Current.Session["USER_ID"].ToString();
                SEC_USER objSecUser =(SEC_USER) HttpContext.Current.Session["SEC_USER"];
                lblLoggedUserName.Text = "Logged as: " + objSecUser.FIRST_NAME + " " + objSecUser.LAST_NAME;

                //string PageId = HttpContext.Current.Session["PAGE_ID"].ToString();
                //if (_userService.CheckUserPagePermission(UserId, PageId).ToString() == "0")
                //{
                //    Response.Redirect("~/NoPermission.htm", false);
                //}
            }
            catch //()
            {
                Response.Redirect("~/Login.aspx", false);
            }
        }
        public void CheckPageAuthentication(string PageId)
        {
            try
            {
                string UserId = HttpContext.Current.Session["USER_ID"].ToString();
                if (_userService.CheckUserPagePermission(UserId, PageId).ToString() == "0")
                {
                    Response.Redirect("~/NoPermission.aspx", false);
                }
            }
            catch //()
            {
                Response.Redirect("~/Login.aspx", false);
            }
        }

        private void FillModulesList()
        {
            ddlModule.DataSource = _userService.GetAllModules();
            ddlModule.DataTextField = "MODULE_NAME";
            ddlModule.DataValueField = "MODULE_ID";
            ddlModule.DataBind();
            ddlModule.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "00"));
            ddlModule.SelectedValue = HttpContext.Current.Session["MODULE_ID"].ToString();
        }
        //protected override void OnPreRender(EventArgs e)
        //{
        //    base.OnPreRender(e);

        //    // moved out of !ispostback because now applications menu is loaded dynamically.
        //    CreateMenuControl();
        //}
        private MenuItem SearchItemByURL(MenuItemCollection items, Uri uri)
        {
            foreach (MenuItem item in items)
            {
                MenuItem childItem = SearchItemByURL(item.ChildItems, uri);
                if (childItem != null)
                    return childItem;

                if (!String.IsNullOrEmpty(item.NavigateUrl) && VirtualPathUtility.ToAbsolute(item.NavigateUrl) == Request.Url.AbsolutePath)
                {
                    return item;
                }
            }
            return null;
        }
        private void SetupNavBar()
        {
            try
            {
                LabelPageTitle.Text = Page.Title;
                MenuItem item = SearchItemByURL(mnuMainMenu.Items, Request.Url);
                List<MenuItem> path = new List<MenuItem>();
                do
                {
                    path.Add(item);
                    item = item.Parent;
                }
                while (item != null);
                //path.Add(new MenuItem("Home", "Home", String.Empty, "~/"));
                path.Reverse();
                RepeaterNavPath.DataSource = path;
                RepeaterNavPath.DataBind();
            }
            catch { }
        }
        private void CreateMenuControl()
        {

            mnuMainMenu.DataSource = GetSiteMapDataSource();
            mnuMainMenu.DataBind();
        }
        private SiteMapDataSource GetSiteMapDataSource()
        {
            /* Modules
           11	Human Resource
           12	Procurement
           13	Inventory
           14	Sales
           15	Finance
           16	Administrator
           17	Security
            * */

            //XmlSiteMapProvider xmlSiteMap = new XmlSiteMapProvider();
            //System.Collections.Specialized.NameValueCollection myCollection = new System.Collections.Specialized.NameValueCollection(1);
            //myCollection.Add("tmp", "StudentSiteMap.sitemap");
            //xmlSiteMap.Initialize("provider", myCollection);
            //xmlSiteMap.BuildSiteMap();

            try
            {
                string strGroupId = HttpContext.Current.Session["MODULE_ID"].ToString();
                if (strGroupId == "11") //HR
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "HR";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "12") //Procurement
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Procurement";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "13") //Inventory
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Inventory";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "14") //Sales
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Sales";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "15") //Finance
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Finance";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "16") //Administrator
                {

                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Administrator";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }
                else if (strGroupId == "17")//Security
                {
                    SiteMapDataSource siteMap = new SiteMapDataSource();
                    siteMap.SiteMapProvider = "Security";
                    siteMap.ShowStartingNode = false;
                    return siteMap;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ShowMessage(string msg, MessageType msgType)
        {
            lblMasterMessage.Visible = true;
            lblMasterMessage.Text = msg;
            lblMasterMessage.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
            //AppHelper.Logger.Warn("AdminAreaWarning. UserName: " + _user.Username + " Error: " + msg);
        }


        protected void mnuMainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {

        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("~/Login.aspx", true);
        }

        protected void ddlModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* Modules
            11	Human Resource
            12	Procurement
            13	Inventory
            14	Sales
            15	Finance
            16	Administrator
            17	Security
             * */

            HttpContext.Current.Session.Add("MODULE_ID", ddlModule.SelectedValue.ToString());

            if (ddlModule.SelectedValue == "11")//HR
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "HR";
                siteMap.ShowStartingNode = false;
                Session["group"] = "HR";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "12")//Procurement
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Procurement";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Procurement";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "13")//Inventory
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Inventory";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Inventory";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "14")//Sales
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Security";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Security";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "15")//Finance
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Finance";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Finance";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "16")
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Administrator";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Administrator";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            else if (ddlModule.SelectedValue == "17")//Security
            {
                SiteMapDataSource siteMap = new SiteMapDataSource();
                siteMap.SiteMapProvider = "Security";
                siteMap.ShowStartingNode = false;
                Session["group"] = "Security";
                mnuMainMenu.DataSource = siteMap;
                mnuMainMenu.DataBind();
            }
            
            Response.Redirect("~/MenuHome.aspx", true);
        }
    }
}