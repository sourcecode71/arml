﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using rBKK.Core;
using rBKK.Entities;
using Ninject;
using Ninject.Web;

using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.Infrastructure.Procurement;
using rBKK.Infrastructure.Inventory;

namespace rBKK.WebApplication.WebServices
{
    /// <summary>
    /// Summary description for CommonWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CommonWebService : Ninject.Web.WebServiceBase //System.Web.Services.WebService
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<string> GetAutoCompleteData(string username)
        {
            List<string> result = new List<string>();
            List<PUR_VENDOR> vendor = _masterProcurementService.GetVendorByTypeId("2");
            foreach (var itm in vendor.ToList())
                result.Add(string.Format("{0}/{1}", itm.VENDOR_NAME.ToString(), itm.VENDOR_ID.ToString()));

            return result;
            //using (SqlConnection con = new SqlConnection("Data Source=SureshDasari;Integrated Security=true;Initial Catalog=MySampleDB"))
            //{
            //    using (SqlCommand cmd = new SqlCommand("select DISTINCT UserName,UserId from UserInformation where UserName LIKE '%'+@SearchText+'%'", con))
            //    {
            //        con.Open();
            //        cmd.Parameters.AddWithValue("@SearchText", username);
            //        SqlDataReader dr = cmd.ExecuteReader();
            //        while (dr.Read())
            //        {
            //            result.Add(string.Format("{0}/{1}", dr["UserName"], dr["UserId"]));
            //        }
            //        return result;
            //    }
            //}
        }


        [WebMethod]
        public List<string> GetAutoCompleteVendorData(string username)
        {
            //MasterProcurementRepository _masterProcurementService1 = new MasterProcurementRepository();
            List<string> result = new List<string>();
            List<PUR_VENDOR> vendor = _masterProcurementService.GetAllVendor();
            var Vendorname = from item in vendor
                             where item.VENDOR_NAME.ToLower().StartsWith(username.ToLower())
                             select item.VENDOR_NAME;
            foreach (var vName in Vendorname)
            {
                result.Add(vName.ToString());
            }

            return result;

        }
        [WebMethod]
        public List<string> GetAutoCompleteAccData(string username)
        {
            //MasterInventoryRepository _masterProcurementService1 = new MasterInventoryRepository();
            List<string> result = new List<string>();
            List<CORE_BANK_ACCOUNTS> account = _masterInventoryServise.GetAllCoreAccount();
            var Account = from item in account
                          where item.ACCOUNT_NO.ToLower().StartsWith(username.ToLower())
                          select item.ACCOUNT_NO;
            foreach (var vName in Account)
            {
                result.Add(vName.ToString());
            }

            return result;
        }


        [WebMethod]
        public decimal CompareCurrentStock(string GodownId, string ProductId, string UnitId, string Quantity)
        {
            int gid=Convert.ToInt32(GodownId);
            int pid = Convert.ToInt32(ProductId);
            int uid = Convert.ToInt32(UnitId);
            decimal qty=Convert.ToDecimal(Quantity);
            decimal currentStock=_masterInventoryServise.GetCurrentStock(gid, pid);
            decimal siRate = _masterInventoryServise.GetSIRateByUnitId(uid);

            return qty * siRate <= currentStock ? 0 : currentStock;
        }

        [WebMethod]
        public List<string> GetAutoCompleteDirectClientList(string username)
        {
            //MasterInventoryRepository _masterProcurementService1 = new MasterInventoryRepository();
            List<string> result = new List<string>();
            List<PUR_VENDOR> account = _masterInventoryServise.GetAllDirectClientList();
            var Account = from item in account
                          where item.VENDOR_NAME.ToLower().StartsWith(username.ToLower())
                          select item.VENDOR_NAME;
            foreach (var vName in Account)
            {
                result.Add(vName.ToString());
            }

            return result;
        }

    }
}

