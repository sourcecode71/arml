﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="rBKK.WebApplication.WebServices.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<%--

http://stackoverflow.com/questions/4647259/asp-net-textbox-with-jquery-autocomplete?rq=1


<head runat="server">
    <title></title>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        SearchText();
    });
    function SearchText() {
        $(".autosuggest").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "/WebServices/CommonWebService.asmx/GetAutoCompleteData",
                    data: "{'username':'" + document.getElementById('txtSearch').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('/')[0],
                                val: item.split('/')[1]
                            }
                        }));
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            select: function (event, ui) {
                $('#lblUserId').text(ui.item.val);
            }
        });
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
     <div class="demo">
        <asp:HiddenField ID="hdnId" runat="server" />
        <div class="ui-widget">
        <label for="tbAuto">Enter UserName: </label>
        <input type="text" id="txtSearch" class="autosuggest" />
        </div>
        <div>&nbsp;</div>
        <div>
            Selected UserId:<b><label id="lblUserId" /></b>
       </div>
    </div>
    </form>
</body>
--%>

<head  runat="server">
    <meta charset="utf-8" />
    <title>jQuery UI Autocomplete - Default functionality</title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script>
        $(function () {
            var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
            $("#tags").autocomplete({
                source: availableTags
            });
        });
    </script>

   <script type="text/javascript">
    var $local_source = [
    {
    value: 1,
    label: "c++"
    }, {
    value: 2,
    label: "java"
    }, {
    value: 3,
    label: "php"
    }, {
    value: 4,
    label: "coldfusion"
    }, {
    value: 5,
    label: "javascript"
    }, {
    value: 6,
    label: "asp"
    }, {
    value: 7,
    label: "ruby"
    }
];
    $(document).ready(function () {
        $("#txtAllowSearch").autocomplete({
            source: $local_source,
            focus: function (event, ui) {
                $("#txtAllowSearch").val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $("#txtAllowSearch").val(ui.item.label);
                $("#hiddenAllowSearch").val(ui.item.value);
                return false;
            }
        });
        $('#button').click(function () {
            alert($("#hiddenAllowSearch").val());
        });
    });
</script>


</head>
<body>
 
    <form id="form1" runat="server">
 
<div class="ui-widget">
    <label for="tags">Tags: </label>
    <input id="tags1" />
    <asp:TextBox ID="txtAllowSearch" runat="server"></asp:TextBox>
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
</div>
    </form>

</html>
