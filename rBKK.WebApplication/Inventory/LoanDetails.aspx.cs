﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using Resources;
using rBKK.WebApplication.Common;
using System.Collections;

namespace rBKK.WebApplication.Inventory
{
    public partial class LoanDetails : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               // ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.CreateLoan).ToString());

                if (!Page.IsPostBack)
                {
                    this.FillLoanTypeDropDownList();
                    this.FillBankDropDownList();
                    this.FillLoadDetailsGrid();
                    this.FillBankAccountDropDownList(string.Empty);
                }

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void FillLoadDetailsGrid()
        {
            gvLoanDetails.DataSource = _masterInventoryServise.GetLoanDetails();
            gvLoanDetails.DataBind();
        }
        
        private void FillLoanTypeDropDownList()
        {
            try
            {
                ddlLoanType.DataSource = _masterInventoryServise.GetAllLoanTypes();
                ddlLoanType.DataTextField = "LOAN_TYPE";
                ddlLoanType.DataValueField = "LOAN_TYPE_ID";
                ddlLoanType.DataBind();
                ddlLoanType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillBankDropDownList()
        {
            try
            {
                ddlBank.DataSource = _masterInventoryServise.GetAllCoreBank();
                ddlBank.DataTextField = "BANK_NAME";
                ddlBank.DataValueField = "BANK_ID";
                ddlBank.DataBind();
                ddlBank.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillBankAccountDropDownList(string bankId)
        {
            try
            {
                ddlAccount.DataSource = _masterInventoryServise.GetBankAccountByBankId(bankId);
                ddlAccount.DataTextField = "ACCOUNT_NO";
                ddlAccount.DataValueField = "ACCOUNT_NO";
                ddlAccount.DataBind();
                ddlAccount.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillBankBranchDropDownList(string bankId)
        {
            try
            {
                //ddlBranch.DataSource = _masterInventoryServise.GetBankBranchByBankId(bankId);
                //ddlBranch.DataTextField = "BRANCH_NAME";
                //ddlBranch.DataValueField = "BRANCH_ID";
                //ddlBranch.DataBind();
                //ddlBranch.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS,"0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserId = HttpContext.Current.Session["USER_ID"].ToString();
                INV_LOAN_DETAILS objLD = new INV_LOAN_DETAILS();

                objLD.LOAN_CODE = txtCode.Text.Trim();
                objLD.LOAN_TYPE_ID = ddlLoanType.SelectedValue.ToString();
                objLD.ACCOUNT_NO = ddlAccount.SelectedValue.ToString ();
                objLD.CREDIT_LIMIT = Convert.ToDecimal(txtCreditLimit.Text.Trim());
                objLD.INTEREST_RATE = Convert.ToDecimal(txtInterestRate.Text.Trim());
                objLD.EFFECTIVE_DATE = DateTime.ParseExact(txtEffectiveDate.Text.Trim(), "dd/MM/yyyy", null);
                objLD.EXPIRY_DATE = DateTime.ParseExact(txtEffectiveDate.Text.Trim(), "dd/MM/yyyy", null);
                objLD.REMARKS = txtRemarks.Text.Trim();
                objLD.SET_USER = loggedUserId;
                objLD.SET_DATE = DateTime.Now;


                if (btnSave.Text == "Save")
                {
                    _masterInventoryServise.AddLoanDetails(objLD, OperationType.SAVE.ToString(), loggedUserId);
                }
                else if (btnSave.Text == "Update")
                {
                    objLD.LOAN_ID = Convert.ToInt32(hiID.Value.ToString());
                    _masterInventoryServise.AddLoanDetails(objLD, OperationType.UPDATE.ToString(), loggedUserId);
                }

                this.ClearAllControls();
                this.FillLoadDetailsGrid();
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void ClearAllControls()
        {
            txtCode.Text = string.Empty;
            txtCreditLimit.Text = string.Empty;
            txtInterestRate.Text = string.Empty;
            txtEffectiveDate.Text = string.Empty;
            txtExpireDate.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            //txtAcountNo.Text = string.Empty;
            //lblAccountName.Text = string.Empty;
            btnSave.Text = "Save";
            ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.ClearAllControls();
        }

        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strLoanID = Convert.ToString(((Label)gvLoanDetails.Rows[rowINdx].Cells[0].FindControl("lblLoanId")).Text.ToString());
                hiID.Value = strLoanID;
                this.FillAllControls(strLoanID);
                btnSave.Text = "Update";
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void FillAllControls(string strLoanID)
        {
            INV_LOAN_DETAILS lstLD = _masterInventoryServise.GetLoanDetailsByID(strLoanID);
            ddlLoanType.SelectedValue = lstLD.LOAN_TYPE_ID;
            ddlAccount.SelectedValue = lstLD.ACCOUNT_NO;
            txtCode.Text = lstLD.LOAN_CODE;
            //txtAcountNo.Text = lstLD.ACCOUNT_NO.ToString();
            //lblAccountName.Text = lstLD.ACCOUNT_HOLDER.ToString();
            txtCreditLimit.Text = lstLD.CREDIT_LIMIT.Value.ToString("0.00");
            txtInterestRate.Text = lstLD.INTEREST_RATE.Value.ToString("0.00");
            txtEffectiveDate.Text = lstLD.EFFECTIVE_DATE.Value.ToString("dd/MM/yyyy");
            txtExpireDate.Text = lstLD.EXPIRY_DATE.Value.ToString("dd/MM/yyyy");
            txtRemarks.Text = lstLD.REMARKS.ToString();
        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {
           // this.FillBankBranchDropDownList(ddlBank.SelectedValue.ToString());
            this.FillBankAccountDropDownList(ddlBank.SelectedValue.ToString());
        }

     
       
    }
}