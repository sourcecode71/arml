﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Godown.aspx.cs" Inherits="rBKK.WebApplication.Inventory.Godown" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<div class="MiniPage"> 
       <fieldset class="flView">
                                    <legend class="lgView"> Godown info </legend>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table class="bgdisplay">
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:HiddenField ID="hiID" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:90px">
                                                       <span>Name </span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtName" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtName" ErrorMessage="*" ForeColor="#FF0066"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:90px">
                                                       <span>  Adress</span></td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtAdress" runat="server" Width="226px" SkinID="normalTextBox" 
                                                            TextMode="MultiLine"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="txtAdress" ErrorMessage="*" ForeColor="#FF0066"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:90px">
                                                      <span>Capacity </span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtCapacity" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td >
                                                       <span>Unit </span> </td>
                                                    <td >
                                                        <asp:DropDownList ID="ddlUnit" runat="server" Width="230px" SkinID="normalDropDownList">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td >
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
</div>

<div class="GodownDetial">

 <fieldset >
 <legend class="lgView"> Godown list </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvGodown" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="GODOWN_ID">
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("GODOWN_ID") %>'></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("GODOWN_ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="NAME" HeaderText=" NAME" />
                     <asp:BoundField DataField="ADDRESS" HeaderText="Adress" />
                     <asp:BoundField DataField="Capacity" HeaderText="Capacity" />
                     <asp:BoundField DataField="UNIT_OF_MEASUREMENT" HeaderText="Unit" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>

</asp:Content>
