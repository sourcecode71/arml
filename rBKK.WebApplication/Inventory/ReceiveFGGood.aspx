﻿  <%@Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
  CodeBehind="ReceiveFGGood.aspx.cs" Inherits="rBKK.WebApplication.Inventory.ReceiveFGGood" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <script type="text/javascript">
         function DatePicker() {
             $(function () {
                 $("#<%=txtDate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });
             });
         }
         function pageLoad() {

             if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                 DatePicker();

             }
         }
         DatePicker();

         function toggleSelectionGrid(source)
          {
             var isChecked = source.checked;
             $("#<%=gvProduction.ClientID %> input[id*='rbtnSelect']").each(function (index) 
             {
                 $(this).attr('checked', false);
                
             });
             source.checked = isChecked;
         }

         function filter(phrase, _id) {
             var words = phrase.value.toLowerCase().split(" ");
             var table = document.getElementById(_id);
             var ele;
             for (var r = 0; r < table.rows.length; r++) {
                 ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
                 var displayStyle = 'none';
                 for (var i = 0; i < words.length; i++) {
                     if (ele.toLowerCase().indexOf(words[i]) >= 0)
                         displayStyle = '';
                     else {
                         displayStyle = 'none';
                         break;
                     }
                 }
                 table.rows[r].style.display = displayStyle;
             }
         }
         function GotFocus(txtSearch) {
             var text = "Search...";
             var words = txtSearch.value;
             if (words == text)
                 txtSearch.value = '';
         }

         function LostFocus(txtSearch) {
             var text = "Search...";
             var words = txtSearch.value.trim();
             if (words == '')
                 txtSearch.value = text;
         }

         String.prototype.trim = function () {
             return this.replace(/^\s+|\s+$/g, "");
         }
     </script>
     <style type="text/css">
         .style1
         {
             width: 154px;
         }
         .style2
         {
             width: 166px;
         }
     </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="PageDetial"> 
                                 <fieldset class="flDeatialView" style="width:98%">
                                    <legend class="lgView"> Finish goods Receive </legend>
          

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table class="bgdisplay" width="100%">
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:HiddenField ID="hiID" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Panel ID="Panel1" runat="server" Height="140px" ScrollBars="Vertical">
                                                            <asp:GridView ID="gvProduction" runat="server" AutoGenerateColumns="False" 
                                                                GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="dataScheduleHeader" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="	Challan">
                                                                        <ItemTemplate>
                                                                            <asp:RadioButton ID="rbtnSelect" runat="server" AutoPostBack="True" 
                                                                                  onclick="toggleSelectionGrid(this);"  oncheckedchanged="rbtnSelect_CheckedChanged" />
                                                                            <asp:Label ID="lblCHALLAN_NO" runat="server" Text='<%# Bind("CHALLAN_NO") %>'></asp:Label>
                                                                            <asp:HiddenField ID="hfInvId" runat="server" 
                                                                                Value='<%# Bind("INV_MASTER_ID") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="DATE">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label3" runat="server" 
                                                                                Text='<%# Eval("CHALLAN_DATE" , "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Godown ">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("GODOWNNAME") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("GODOWNNAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Factory ">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FACTORYNAME") %>'></asp:Label>
                                                                            <asp:HiddenField ID="hfFactory" runat="server" 
                                                                                Value='<%# Bind("GODOWN_ID") %>' />
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FACTORYNAME") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Product">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Quantity">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Unit">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox5" runat="server" 
                                                                                Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Rate">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("RATE") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("RATE") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#006600" 
                                                            Text="Partial Received Finish Goods"></asp:Label>
                                                    </td>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:Panel ID="Panel3" runat="server" Height="50px" ScrollBars="Vertical" BorderWidth="1px" BorderColor="Green" BorderStyle="Solid">
                                                            <asp:GridView ID="gvPartialGoods" runat="server" AutoGenerateColumns="False" 
                                                                GridLines="None" Width="100%" EmptyDataText="No Record Found">
                                                                <HeaderStyle CssClass="dataScheduleHeader" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="	Challan">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblCHALLAN_NO0" runat="server" Text='<%# Bind("CHALLAN_NO") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="DATE">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label8" runat="server" 
                                                                                Text='<%# Eval("CHALLAN_DATE" , "{0:dd/MM/yyyy}")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Product">
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label11" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Quantity">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label12" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Unit">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label13" runat="server" 
                                                                                Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate>
                                                                            <asp:TextBox ID="TextBox11" runat="server" 
                                                                                Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:TextBox>
                                                                        </EditItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2">
                                                        <span>Issue no </span>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtChallanNo" runat="server" SkinID="normalTextBox" 
                                                            Width="200px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="txtChallanNo" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2">
                                                        <span>Date</span></td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtDate" runat="server" SkinID="normalTextBox" Width="100px"></asp:TextBox>
                                                        <span class="style1">(DD/MM/YYYY)</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="style2">
                                                        <span>Comments</span></td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtComments" runat="server" SkinID="normalTextBox" 
                                                            TextMode="MultiLine" Width="205px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:GridView ID="gvFinishGood" runat="server" AutoGenerateColumns="False" 
                                                            CellPadding="4" ForeColor="#333333" GridLines="None" 
                                                            onrowdatabound="gvPreOrder_RowDataBound" style="font-size: 9pt" 
                                                            Width="100%">
                                                            <HeaderStyle CssClass="dataScheduleHeader" />
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                               <asp:TemplateField HeaderText="Godown">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlFGGodown" runat="server" 
                                                                            SkinID="normalGridDropDownList" Width="100%">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hfFGGodown" runat="server" 
                                                                            Value='<%# Bind("GODOWN_ID") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="120px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Product">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlProduct" runat="server" 
                                                                            SkinID="normalGridDropDownList" Width="100%">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hfProduct" runat="server" 
                                                                            Value='<%# Bind("PRODUCT_ID") %>' />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="120px" />
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Unit">
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList ID="ddlUnit" runat="server" SkinID="normalGridDropDownList" 
                                                                            Width="90px">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hfMeasurementUnit" runat="server" 
                                                                            Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Quantity">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtQuantity" runat="server" SkinID="normalGridTextBox" 
                                                                            Text='<%# Bind("QUANTITY") %>' Width="81px"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="120px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Rate">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRate" runat="server" Width="80px" 
                                                                            Text='<%# Bind("Rate") %>' SkinID="normalGridTextBox"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="By Products Rice Bran">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRiceBran" runat="server" Width="50px" 
                                                                            Text='<%# Bind("BY_PRODUCT_RICE_BRAN_QTY") %>' SkinID="normalGridTextBox"></asp:TextBox>
                                                                     <asp:DropDownList ID="ddlUnitBran" runat="server" SkinID="normalGridDropDownList" 
                                                                            Width="90px">
                                                                        </asp:DropDownList>
                                                                      <asp:HiddenField ID="hfUnitBran" runat="server" 
                                                                            Value='<%# Bind("BRAN_MEASUREMENT_UNIT_ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="By Products Rice Husk">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtRiceHusk" runat="server" Width="50px" 
                                                                            Text='<%# Bind("BY_PRODUCT_RICE_HUSK_QTY") %>' SkinID="normalGridTextBox"></asp:TextBox>
                                                                     <asp:DropDownList ID="ddlRiceHuskUnit" runat="server" SkinID="normalGridDropDownList" 
                                                                            Width="90px">
                                                                        </asp:DropDownList>
                                                                      <asp:HiddenField ID="hfUnitHusk" runat="server" 
                                                                            Value='<%# Bind("HUSK_MEASUREMENT_UNIT_ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="ImageButton1" runat="server" 
                                                                            ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="imgDelete_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EditRowStyle BackColor="#2461BF" />
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#EFF3FB" />
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                        </asp:GridView>
                                                        <asp:ImageButton ID="ibtnAddMore" runat="server" Height="20px" 
                                                            ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                                            onclick="ibtnAddMore_Click" Width="21px" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">                                                        
                                                        <asp:CheckBox ID="chkFinishProduction" runat="server" 
                                                            Text="Finished Production" TextAlign="Left" />
                                                    </td>
                                                    <td align="left">
                                                        &nbsp 
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                    </td>
                                                    <td align="center" colspan="2">
                                                        &nbsp;</td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
</div>

<div class="PageDetial">

 <fieldset class="flDeatialView" style="width:98%">
 <legend class="lgView"> Finish goods list </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Vertical">
                     
             <input id="htxtSearch" onkeyup="filter(this, 'ctl00_MainContent_gvReceiveFGGood', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none; width: 143px;" />

                 <asp:GridView ID="gvReceiveFGGood" runat="server" AutoGenerateColumns="False" 
                     CellPadding="4" ForeColor="#333333" GridLines="None" style="font-size: 9pt" 
                     Width="100%" EmptyDataText="No Record Found">
                     <HeaderStyle CssClass="dataScheduleHeader" />
                     <AlternatingRowStyle BackColor="White" />
                     <Columns>
                       <asp:TemplateField HeaderText="ID">
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("INV_MASTER_ID") %>'></asp:Label>
                         </ItemTemplate>
                        
                     </asp:TemplateField>
                         <asp:TemplateField HeaderText="Issue No">
                             <ItemTemplate>
                                 <asp:Label ID="lblChallan" runat="server" Text='<%# Bind("CHALLAN_NO") %>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:TemplateField HeaderText="Date">
                             <ItemTemplate>
                                 <asp:Label ID="Label4" runat="server" 
                                     Text='<%# Bind("CHALLAN_DATE", "{0:dd/MM/yyyy}")%>'></asp:Label>
                             </ItemTemplate>
                         </asp:TemplateField>
                         <asp:BoundField DataField="PRODUCT_NAME" HeaderText="Name" />
                         <asp:BoundField DataField="QUANTITY" HeaderText="Quantity" />
                         <asp:BoundField DataField="UNIT_OF_MEASUREMENT" HeaderText="Unit" />
                         <asp:BoundField DataField="RATE" HeaderText="Rate" />
                         <asp:BoundField DataField="FACTORYNAME" HeaderText="Factory" />
                         <asp:BoundField DataField="GODOWNNAME" HeaderText="Godown" />
                         <asp:TemplateField ShowHeader="False" Visible="False">
                             <ItemTemplate>
                                 <asp:ImageButton ID="imgEdit" runat="server" 
                                     ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" />
                             </ItemTemplate>
                             <ItemStyle Width="15px" />
                         </asp:TemplateField>
                         <asp:TemplateField ShowHeader="False">
                             <ItemTemplate>
                                 <asp:ImageButton ID="imgDelete" runat="server" 
                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="DeleteFG"  CausesValidation="false" />
                             </ItemTemplate>
                             <ItemStyle Width="15px" />
                         </asp:TemplateField>
                         <asp:TemplateField>
                             <ItemTemplate>
                                 <asp:ImageButton ID="ImageButton2" runat="server" Height="16px" 
                                     ImageUrl="~/App_Themes/Green/Images/print-button.png" 
                                     onclick="ImageButton2_Click" Width="16px" CausesValidation="false" />
                             </ItemTemplate>
                         </asp:TemplateField>
                     </Columns>
                     <EditRowStyle BackColor="#2461BF" />
                     <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                     <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                     <RowStyle BackColor="#EFF3FB" />
                     <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                     <SortedAscendingCellStyle BackColor="#F5F7FB" />
                     <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                     <SortedDescendingCellStyle BackColor="#E9EBEF" />
                     <SortedDescendingHeaderStyle BackColor="#4870BE" />
                 </asp:GridView>
             </asp:Panel>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
