﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LoanDetails.aspx.cs" Inherits="rBKK.WebApplication.Inventory.LoanDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });

        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        //url: "http://localhost:4345/WebServices/CommonWebService.asmx/GetAutoCompleteAccData",
                        url: "../WebServices/CommonWebService.asmx/GetAutoCompleteAccData",


                        data: "{'username':'" + document.getElementById('<%=txtCreditLimit.ClientID%>').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <div class="MiniPage">
        <fieldset class="flView">
            <legend class="lgView"> loan Info </legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hiID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                                      <span> Loan Type</span>   </td>
                            <td style="width: 315px">
                                <asp:DropDownList ID="ddlLoanType" runat="server" Width="235px" 
                                    SkinID="normalDropDownList">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="ddlLoanType" ErrorMessage="*" ForeColor="Red" 
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                             <span>Loan Code</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtCode" runat="server" Width="230px" SkinID="normalTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txtCode" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span>Bank </span></td>
                            <td style="width: 315px">
                                <asp:DropDownList ID="ddlBank" runat="server" AutoPostBack="True" Height="21px" 
                                    onselectedindexchanged="ddlBank_SelectedIndexChanged" 
                                    SkinID="normalDropDownList" Width="235px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                <span>Account No</span></td>
                            <td style="width: 315px">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlAccount" runat="server" Height="21px" 
                                            SkinID="normalDropDownList" Width="235px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span>Creadit Limit</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtCreditLimit" runat="server" SkinID="normalTextBox" 
                                    Width="230px" class="autosuggest"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="txtCreditLimit" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span>Interest Rate</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtInterestRate" runat="server" SkinID="normalTextBox" 
                                    Width="230px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="txtInterestRate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span>Effective Date</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtEffectiveDate" runat="server" SkinID="normalTextBox" 
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span>Expiry Date</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtExpireDate" runat="server" SkinID="normalTextBox" 
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                <span>remarks</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtRemarks" runat="server" SkinID="normalTextBox" 
                                    TextMode="MultiLine" Width="230px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                        </tr>
                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>

 <div class="PageDetial" align="center">
 <fieldset class="flDeatialView">
 <legend class="lgView"> loan List</legend> 
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>
             <asp:GridView ID="gvLoanDetails" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="LOAN_ID">
                         <ItemTemplate>
                             <asp:Label ID="lblLoanId" runat="server" Text='<%# Bind("LOAN_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:BoundField DataField="LOAN_CODE" HeaderText="Loan Code" />
                     <asp:BoundField DataField="LOAN_TYPE" HeaderText="Loan Type" />
                     <asp:BoundField HeaderText="Account No" DataField="ACCOUNT_NO"  />
                     <asp:BoundField HeaderText="Account Info" DataField="ACCOUNT_HOLDER"  />
                     <asp:BoundField DataField="CREDIT_LIMIT" HeaderText="Credit Limit" 
                         DataFormatString="{0:f2}"  >
                     </asp:BoundField>
                     <asp:BoundField DataField="INTEREST_RATE" HeaderText="Interest Rate"  
                         DataFormatString="{0:f2}" >
                     </asp:BoundField>
                     <asp:TemplateField HeaderText="Effective Date">
                         <ItemTemplate>
                             <asp:Label ID="Label3" runat="server" Text='<%# Eval("EFFECTIVE_DATE", "{0:dd/MM/yyyy}") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Expiry Date">
                         <ItemTemplate>
                             <asp:Label ID="Label2" runat="server" Text='<%# Eval("EXPIRY_DATE", "{0:dd/MM/yyyy}") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
    </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>

</asp:Content>
