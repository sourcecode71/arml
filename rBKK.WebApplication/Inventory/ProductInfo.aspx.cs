﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class ProductInfo : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddProduct).ToString());

                if (!IsPostBack)
                {
                    this.PagedataBound();
                }
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        private void ResetAll()
        {
            txtName.Text = "";
            ddlSiUnit.SelectedIndex = 0;
            btnSave.Text = "Save";
        }
        private void PagedataBound()
        {
            this.BindDropdownlist(ddlCategory, "Category");
            this.BindDropdownlist(ddlSiUnit, "Unit");
        }
        private void BindDropdownlist(DropDownList ddl, string ddlType)
        {
            try
            {
                ddl.Items.Clear();
                bool selectDefault = true;
                switch (ddlType)
                {
                    case "Category":
                        {
                            var dATA = (_masterInventoryServise.GetAllCategory()).ToList();
                            ddl.DataValueField = "CATEGORY_ID";
                            ddl.DataTextField = "CATEGORY_NAME";
                            ddl.DataSource = dATA;
                            break;
                        }

                    case "Unit":
                        {
                            var dATA = (_masterInventoryServise.GetAllUNIT_OF_MEASUREMENT()).ToList();
                            ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                            ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                            ddl.DataSource = dATA;
                            break;
                        }
                }

                if (selectDefault)
                {
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                    ddl.SelectedValue = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindGridView(GridView gv, string gvType)
        {
            try
            {
                switch (gvType)
                {
                    case "ProdList":
                        {
                            string strcategory = ddlCategory.SelectedValue;
                            var dATA = (_masterInventoryServise.GetProductByCatID(strcategory)).ToList();
                            gv.DataSource = dATA;
                            gv.DataBind();
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string TranType;
                INV_PRODUCT iNV_PRODUCT = new INV_PRODUCT();
                string loggedUserId = HttpContext.Current.Session["USER_ID"].ToString();

                if (btnSave.Text == "Save")
                {
                    TranType = "Save";
                    iNV_PRODUCT.PRODUCT_NAME = txtName.Text;
                    iNV_PRODUCT.CATEGORY_ID = Convert.ToInt16(ddlCategory.SelectedValue);
                    iNV_PRODUCT.COMPANY_ID = "1";
                    iNV_PRODUCT.SIUnitId = Convert.ToInt32(ddlSiUnit.SelectedValue);
                    iNV_PRODUCT.SET_USER = loggedUserId;
                    iNV_PRODUCT.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddProduct(iNV_PRODUCT, TranType);

                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    iNV_PRODUCT.PRODUCT_ID = Convert.ToInt32(hiID.Value);
                    iNV_PRODUCT.CATEGORY_ID = Convert.ToInt16(ddlCategory.SelectedValue);
                    iNV_PRODUCT.PRODUCT_NAME = txtName.Text;
                    iNV_PRODUCT.SIUnitId = Convert.ToInt32(ddlSiUnit.SelectedValue);
                    _masterInventoryServise.AddProduct(iNV_PRODUCT, TranType);
                    btnSave.Text = "Save";
                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                this.BindGridView(gvProduct, "ProdList");
                this.ResetAll();
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlCategory.SelectedIndex = 0;
            this.ResetAll();
        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvProduct.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                INV_PRODUCT product = _masterInventoryServise.GetProductByID(strCatID);
                txtName.Text = product.PRODUCT_NAME;
                ddlSiUnit.SelectedValue = product.SIUnitId.ToString();
                btnSave.Text = "Update";
                hiID.Value = strCatID;
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvProduct.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteProduct(strCatID);
                this.BindGridView(gvProduct, "ProdList");
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.BindGridView(gvProduct, "ProdList");
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}