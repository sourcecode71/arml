﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using  Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class FactoryInfo : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

       protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddFactory).ToString());

                if (!IsPostBack)
                {
                    this.GetPageData(); 
                }
            }
            catch (Exception ex)
            {
                
               ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
       private void GetPageData()
       {

           try
           {
               List<INV_FACTORY> factoryInfo = _masterInventoryServise.GetAllInvFactory();
               gvFactory.DataSource = factoryInfo;
               gvFactory.DataBind();
           }
           catch (Exception ex)
           {
               
               throw ex;
           }

       }
       protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                INV_FACTORY invFactory = new INV_FACTORY();
                string TranType;
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();
                if (btnSave.Text == "Save")
                {
                   
                    TranType = "Save";
                    invFactory.NAME = txtname.Text.Trim();
                    invFactory.ADDRESS = txtAddress.Text.Trim();
                    invFactory.COMPANY_ID="1";
                    invFactory.SET_USER = loggedUserID;
                    invFactory.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddInvFactory(invFactory, TranType);
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    invFactory.FACTORY_ID = Convert.ToInt32(hfFactoryID.Value);
                    invFactory.NAME = txtname.Text;
                    invFactory.ADDRESS = txtAddress.Text;
                    _masterInventoryServise.AddInvFactory(invFactory, TranType);
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                txtAddress.Text = string.Empty;
                txtname.Text = string.Empty;
                this.GetPageData();

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
       protected void btnReset_Click(object sender, EventArgs e)
        {
            txtAddress.Text = string.Empty;
            txtname.Text = string.Empty;
            btnSave.Text = "Save";
        }
       protected void img_editFactory(object sender, ImageClickEventArgs e)

        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                INV_FACTORY invFactory = _masterInventoryServise.GetInvFactoryById(strID);
                txtname.Text = invFactory.NAME;
                txtAddress.Text = invFactory.ADDRESS;
                btnSave.Text = "Update";
                hfFactoryID.Value = strID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
       protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteInvFactory(strID);
                this.GetPageData();
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }

     

       

    }
}