﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using rBKK.Entities;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class UnitOfMeasurement : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.UnitOfMeasurement).ToString());

                if (!Page.IsPostBack)
                {
                    this.GetMeasurementUnit();
                }

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        private void GetMeasurementUnit()
        {
            try
            {
                List<INV_UNIT_OF_MEASUREMENT> invMeasurement = _masterInventoryServise.GetAllUNIT_OF_MEASUREMENT();
                gvUnit.DataSource = invMeasurement;
                gvUnit.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                INV_UNIT_OF_MEASUREMENT unitOfMeasurement = new INV_UNIT_OF_MEASUREMENT();
                string loggedUserId = HttpContext.Current.Session["USER_ID"].ToString();

                string TranType;
                if (btnSave.Text == "Save")
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Save";
                    unitOfMeasurement.UNIT_OF_MEASUREMENT = txtUnit.Text;
                    unitOfMeasurement.SI_UNIT = txtSIUnit.Text;
                    unitOfMeasurement.SI_UNIT_CONVERSION_RATE =Convert.ToDecimal(txtConvertionRate.Text);
                    unitOfMeasurement.SET_DATE = DateTime.Now;
                    unitOfMeasurement.COMPANY_ID = "1";
                    unitOfMeasurement.SET_USER = loggedUserId;
                    txtUnit.Text = "";
                    txtSIUnit.Text = "";
                    txtConvertionRate.Text = "";
                    _masterInventoryServise.AddUNIT_OF_MEASUREMENT(unitOfMeasurement, TranType);
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    unitOfMeasurement.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(hiID.Value);
                    unitOfMeasurement.UNIT_OF_MEASUREMENT = txtUnit.Text;
                    unitOfMeasurement.SI_UNIT = txtSIUnit.Text;
                    unitOfMeasurement.SI_UNIT_CONVERSION_RATE = Convert.ToDecimal(txtConvertionRate.Text);
                    _masterInventoryServise.AddUNIT_OF_MEASUREMENT(unitOfMeasurement, TranType);
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                txtUnit.Text = string.Empty;
                 this.GetMeasurementUnit();

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtUnit.Text = "";
            txtSIUnit.Text = "";
            txtConvertionRate.Text = "";
            btnSave.Text = "Save";

        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvUnit.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                INV_UNIT_OF_MEASUREMENT category = _masterInventoryServise.GetUnitOfMeasurmentTById(strCatID);
                txtUnit.Text = category.UNIT_OF_MEASUREMENT;
                txtSIUnit.Text = category.SI_UNIT;
                txtConvertionRate.Text = category.SI_UNIT_CONVERSION_RATE.ToString();
                btnSave.Text = "Update";
                hiID.Value = strCatID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvUnit.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteUNIT_OF_MEASUREMENT(strCatID);
                this.GetMeasurementUnit();
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}