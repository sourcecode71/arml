﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BranchDetails.aspx.cs" Inherits="rBKK.WebApplication.Inventory.BranchDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
    <style type="text/css">
        .style1
        {
            width: 93px;
        }
        .style2
        {
            width: 94px;
        }
        .style3
        {
            width: 95px;
        }
        .style4
        {
            width: 153px;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
<div class="MiniPage"> 
       <fieldset class="flView">
                                    <legend class="lgView"> Branch info </legend>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table class="bgdisplay">
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:HiddenField ID="hfID" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                       <span>Bank</span></td>
                                                    <td style="width: 315px">
                                                        <asp:DropDownList ID="ddlBank" runat="server" SkinID="normalDropDownList" 
                                                            Width="230px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 13px">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="ddlBank" ErrorMessage="*" ForeColor="#CC0000" 
                                                            InitialValue="0"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                        <span>Branch Name </span>
                                                    </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtBranchName" runat="server" SkinID="normalTextBox" 
                                                            Width="226px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtBranchName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                       <span>  Address</span></td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtAdress" runat="server" Width="226px" SkinID="TextArea" 
                                                            TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                      <span>Contact Person</span></td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtContact" runat="server" Width="226px" 
                                                            SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                        <span>Phone</span></td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtPhone" runat="server" SkinID="normalTextBox" 
                                                            Width="226px"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style4">
                                                      <span>Fax</span></td>
                                                    <td class="style2">
                                                        <asp:TextBox ID="txtFax" runat="server" SkinID="normalTextBox" 
                                                            Width="226px"></asp:TextBox>
                                                    </td>
                                                    <td class="style3">
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td class="style4" >
                                                       <span>Email </span> </td>
                                                    <td >
                                                        <asp:TextBox ID="txtEmail" runat="server" SkinID="normalTextBox" 
                                                            Width="226px"></asp:TextBox>
                                                    </td>
                                                    <td >
                                                        </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
</div>

<div class="Detailpage" style="width:100%">

 <fieldset >
 <legend class="lgView"> Branch list </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvBranch" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="BRANCH_ID">
                        
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("BRANCH_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Bank">
                        
                         <ItemTemplate>
                             <asp:Label ID="lblBankId" runat="server" Text='<%# Bind("BANK_NAME") %>'></asp:Label>
                             <asp:HiddenField ID="hfBankId" runat="server" Value='<%# Bind("BANK_ID") %>' />
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Contact">
                        
                         <ItemTemplate>
                             <asp:Label ID="lblContact" runat="server" Text='<%# Bind("CONTACT_PERSON") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Phone">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("PHONE").ToString().Length<13?Eval("PHONE").ToString():Eval("PHONE").ToString().Substring(0,13)+"..." %>'></asp:Label>
                             <asp:HiddenField ID="hfPHONE" runat="server" Value='<%# Bind("PHONE") %>' />

                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Fax">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblFax" runat="server" Text='<%# Bind("FAX") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Email">
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EMAIL") %>'></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("EMAIL") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Branch">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblBranch" runat="server" Text='<%# Eval("BRANCH_NAME") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Address">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("ADDRESS").ToString().Length<25? Eval("ADDRESS").ToString(): Eval("ADDRESS").ToString().Substring(0,25)+"..." %>'></asp:Label>
                             <asp:HiddenField ID="hfAddress" runat="server" Value='<%# Bind("ADDRESS") %>' />
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
