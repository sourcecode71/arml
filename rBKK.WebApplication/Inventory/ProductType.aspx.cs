﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Entities;
using Resources;
using rBKK.Core.Security;
using rBKK.WebApplication.Common;
using rBKK.Core.Procurement;

namespace rBKK.WebApplication.Inventory
{
    public partial class ProductType : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementServise { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddProductType).ToString());

                if (!Page.IsPostBack)
                {
                    this.GetProductCategory();
                    this.FillPurchaseTypeDropDownList();
                }

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        private void FillPurchaseTypeDropDownList()
        {
            try
            {
                List<string> Ids = new List<string> { "1", "3", "4", "6"};
                List<PUR_REQUISITION_TYPE> lst = _masterProcurementServise.GetPurchaseType();

                ddlReqisitionType.DataSource = lst.Where(d => Ids.Contains(d.REQUISITION_TYPE_ID)); 
                ddlReqisitionType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlReqisitionType.DataValueField = "REQUISITION_TYPE_ID";
                ddlReqisitionType.DataBind();
                ddlReqisitionType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void GetProductCategory()
        {
            try
            {
                //List<INV_PRODUCT_CATEGORY> invProductCategory = _masterInventoryServise.GetAllCategory();
                IQueryable<object> invProductCategory = _masterInventoryServise.GetAllCategoryWithGenericName();
                gvCategory.DataSource = invProductCategory;
                gvCategory.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void CheckPageAuthentication(string PageId)
        {
            try
            {
                string UserId = HttpContext.Current.Session["USER_ID"].ToString();
                if (_userService.CheckUserPagePermission(UserId, PageId).ToString() == "0")
                {
                    Response.Redirect("~/NoPermission.aspx", false);
                }
            }
            catch //()
            {
                Response.Redirect("~/Login.aspx", false);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                INV_PRODUCT_CATEGORY productCategory = new INV_PRODUCT_CATEGORY();
                string loggedUserId = HttpContext.Current.Session["USER_ID"].ToString();
                string TranType;
                if (btnSave.Text == "Save")
                {
                    TranType = "Save";
                    productCategory.CATEGORY_NAME = txtCategoryName.Text;
                    productCategory.PARRENT_CAT_ID = ddlReqisitionType.SelectedValue;
                    productCategory.SET_USER = loggedUserId;
                    productCategory.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddProductType(productCategory, TranType);
                }
                else
                {
                    TranType = "Update";
                    productCategory.CATEGORY_ID = Convert.ToInt32(hiCatgoryID.Value);
                    productCategory.CATEGORY_NAME = txtCategoryName.Text;
                    productCategory.SET_USER = loggedUserId;
                    _masterInventoryServise.AddProductType(productCategory, TranType);
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                txtCategoryName.Text = string.Empty;
                ddlReqisitionType.Enabled = true;
                ddlReqisitionType.SelectedValue = "0";
                this.GetProductCategory();

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                string strGenericTypeName = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[2].FindControl("lblGenericTypeId")).Text.ToString());
                INV_PRODUCT_CATEGORY category = _masterInventoryServise.GetCategoryByID(strCatID);
                txtCategoryName.Text = category.CATEGORY_NAME;
                ddlReqisitionType.SelectedValue = strGenericTypeName;
                ddlReqisitionType.Enabled = false;
                btnSave.Text = "Update";
                hiCatgoryID.Value = strCatID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                 ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteProductType(strCatID);
                this.GetProductCategory();
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlReqisitionType.Enabled = true;
            ddlReqisitionType.SelectedValue = "0";
            btnSave.Text = "Save";
            txtCategoryName.Text = "";
        }
    }
}