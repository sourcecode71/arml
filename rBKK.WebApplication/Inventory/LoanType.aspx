﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LoanType.aspx.cs" Inherits="rBKK.WebApplication.Inventory.LoanType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<div class="MiniPage">

       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <fieldset class="flView">
            <legend class="lgView"> Factroy Info </legend>

           
                
                    <table class="bgdisplay">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hfFactoryID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                       <span> Company</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtname0" runat="server" SkinID="normalTextBox" Width="226px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                <span>Loan Type </span>
                            </td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtname" runat="server" SkinID="normalTextBox" Width="226px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                        </tr>
                    </table>
                 
           

        </fieldset>
    </ContentTemplate>
    </asp:UpdatePanel>

    </div>
   

 <div class="MiniDisplay">

 <fieldset class="flView">
 <legend class="lgView"> Factroy List</legend>
 
      <asp:UpdatePanel ID="upDisplay" runat="server">
          <ContentTemplate>
              <asp:GridView ID="gvLoan" runat="server" AutoGenerateColumns="False" 
                  CellPadding="4" ForeColor="#333333" GridLines="None" style="font-size: 9pt" 
                  Width="100%">
                  <HeaderStyle CssClass="dataScheduleHeader" />
                  <AlternatingRowStyle BackColor="White" />
                  <Columns>
                      <asp:TemplateField ShowHeader="False">
                          <ItemTemplate>
                              <asp:ImageButton ID="imgEdit" runat="server" 
                                  ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_editFactory" 
                                  CausesValidation="False" />
                          </ItemTemplate>
                          <ItemStyle Width="15px" />
                      </asp:TemplateField>
                      <asp:TemplateField ShowHeader="False">
                          <ItemTemplate>
                              <asp:ImageButton ID="imgDelete" runat="server" 
                                  ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="imgDelete_Click" 
                                  CausesValidation="False" />
                          </ItemTemplate>
                          <ItemStyle Width="15px" />
                      </asp:TemplateField>
                  </Columns>
                  <EditRowStyle BackColor="#2461BF" />
                  <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                  <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                  <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                  <RowStyle BackColor="#EFF3FB" />
                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                  <SortedAscendingCellStyle BackColor="#F5F7FB" />
                  <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                  <SortedDescendingCellStyle BackColor="#E9EBEF" />
                  <SortedDescendingHeaderStyle BackColor="#4870BE" />
              </asp:GridView>
          </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>

</asp:Content>
