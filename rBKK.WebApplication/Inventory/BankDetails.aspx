﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BankDetails.aspx.cs" Inherits="rBKK.WebApplication.Inventory.BankDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <div class="MiniPage">
        <fieldset class="flView">
            <legend class="lgView"> Bank Deatils</legend>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
          <table class="bgdisplay">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hfID" runat="server" />
                                <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                                    style="text-align: center" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                             <span>Name </span>   </td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtname" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtname" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                <span>Short Name </span>
                            </td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtShortName" runat="server" SkinID="normalTextBox" style="text-transform:uppercase;" 
                                    Width="226px" MaxLength="8"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="txtShortName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                               <span> Web Address </span>  </td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtwebAddress" runat="server" SkinID="normalTextBox" 
                                    Width="226px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txtwebAddress" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                <span> Address</span></td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtAddress" runat="server" SkinID="normalTextBox" 
                                    TextMode="MultiLine" Width="226px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtAddress" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                        </tr>
                    </table>
      

           
                
              
              </ContentTemplate>
    </asp:UpdatePanel>   
           

        </fieldset>
    

    </div>

  <div class="DetailPage" align="center" >

 <fieldset class="flDeatialView">
 <legend class="lgView"> Bank list</legend>
 
      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
          <ContentTemplate>
              <asp:GridView ID="gvBank" runat="server" AutoGenerateColumns="False" 
                  CellPadding="4" ForeColor="#333333" GridLines="None" style="font-size: 9pt" 
                  Width="100%">
                  <HeaderStyle CssClass="dataScheduleHeader" />
                  <AlternatingRowStyle BackColor="White" />
                  <Columns>
                      <asp:TemplateField HeaderText="ID">
                          <ItemTemplate>
                              <asp:Label ID="lblId" runat="server" Text='<%# Bind("BANK_ID") %>'></asp:Label>
                          </ItemTemplate>
                          <HeaderStyle CssClass="HiddenGridColumn" />
                          <ItemStyle CssClass="HiddenGridColumn" />
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Symbolic Name">
                         
                          <ItemTemplate>
                              <asp:Label ID="lblShortName" runat="server" Text='<%# Bind("SYMBOLIC_NAME") %>'></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>

                      <asp:TemplateField HeaderText="Bank Name">
                         
                          <ItemTemplate>
                              <asp:Label ID="lblname" runat="server" Text='<%# Bind("BANK_NAME") %>'></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Web">
                         
                          <ItemTemplate>
                              <asp:Label ID="lblWebAddress" runat="server" Text='<%# Bind("WEB_ADDRESS") %>'></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField HeaderText="Address">
                         
                          <ItemTemplate>
                              <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                          </ItemTemplate>
                      </asp:TemplateField>
                      <asp:TemplateField ShowHeader="False">
                          <ItemTemplate>
                              <asp:ImageButton ID="imgEdit" runat="server" 
                                  ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_editFactory" 
                                  CausesValidation="False" />
                          </ItemTemplate>
                          <ItemStyle Width="15px" />
                      </asp:TemplateField>
                      <asp:TemplateField ShowHeader="False">
                          <ItemTemplate>
                              <asp:ImageButton ID="imgDelete" runat="server" 
                                  ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="imgDelete_Click" 
                                  CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                          </ItemTemplate>
                          <ItemStyle Width="15px" />
                      </asp:TemplateField>
                  </Columns>
                  <EditRowStyle BackColor="#2461BF" />
                  <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                  <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                  <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                  <RowStyle BackColor="#EFF3FB" />
                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                  <SortedAscendingCellStyle BackColor="#F5F7FB" />
                  <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                  <SortedDescendingCellStyle BackColor="#E9EBEF" />
                  <SortedDescendingHeaderStyle BackColor="#4870BE" />
              </asp:GridView>
          </ContentTemplate>
      </asp:UpdatePanel>
    
    
 </fieldset>

</div>

</asp:Content>
