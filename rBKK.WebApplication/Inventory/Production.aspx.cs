﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.WebApplication.Common;
using rBKK.Core.Inventory;
using Ninject;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;
using System.Data;
using rBKK.WebApplication.Reports;

namespace rBKK.WebApplication.Inventory
{
    public partial class Production : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtDirectClientList.Visible = false;
                string TransType = Request.QueryString["TransType"].ToString();
                if (TransType != ((int)(PurchaseType.SALES)).ToString() && TransType != ((int)(PurchaseType.GODOWN_TRANSFER)).ToString()
                    && TransType != ((int)(PurchaseType.PLEDGE_TRANSFER)).ToString() && TransType != ((int)(PurchaseType.PRODUCTION)).ToString())
                {
                    Response.Redirect("~/MenuHome.aspx", true);
                }
                if (TransType == ((int)(PurchaseType.PRODUCTION)).ToString())
                {
                    ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.RiceProduction).ToString());
                    gvProduct.Width = new Unit("65%");
                }
                else if (TransType == ((int)(PurchaseType.GODOWN_TRANSFER)).ToString())
                {
                    ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.GodownTransfer).ToString());
                    gvProduct.Width = new Unit("75%");
                }
                else if (TransType == ((int)(PurchaseType.PLEDGE_TRANSFER)).ToString())
                {
                    ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.PledgeTransfer).ToString());
                    gvProduct.Width = new Unit("90%");
                   
                }
                else if (TransType == ((int)(PurchaseType.SALES)).ToString())
                {
                    ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.RiceSales).ToString());
                }

                if (!Page.IsPostBack)
                {
                    this.FillTransactionTypeDropDownList();
                    ddlReqisitionType.SelectedValue = TransType;
                    this.FillBankDropDownList();
                    this.FillChallanListGridView(TransType);
                    this.LoadNewProductGridView();
                    this.SetNullToSessionVariables();
                    txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    txtDate.Attributes.Add("readonly", "readonly");
                    this.LoadNewSalesGridView();
                    divProductions.Visible = false;
                    divSales.Visible = false;

                    ddlReqisitionType_SelectedIndexChanged(ddlReqisitionType, null);
                }

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void FillBankDropDownList()
        {
            try
            {
                ddlBank.DataSource = _masterInventoryServise.GetAllCoreBank();
                ddlBank.DataTextField = "BANK_NAME";
                ddlBank.DataValueField = "BANK_ID";
                ddlBank.DataBind();
                ddlBank.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void FillChallanListGridView(string TransType)
        {
            IQueryable<object> lst = _masterInventoryServise.GetMasterInventoryData(TransType);
            gvChallanList.DataSource = lst;
            gvChallanList.DataBind();
        }
        private void SetNullToSessionVariables()
        {
            Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = null;
            Session[SessionVariable.INV_GODOWN.ToString()] = null;
            Session[SessionVariable.MACHINE_TYPE.ToString()] = null;
            Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            Session[SessionVariable.PUR_PADDY.ToString()] = null;
            Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = null;
        }
        private void FillTransactionTypeDropDownList()
        {
            try
            {
                List<PUR_REQUISITION_TYPE> lst = _masterProcurementService.GetPurchaseType();

                foreach (PUR_REQUISITION_TYPE itm in lst.ToList())
                {
                    if (itm.SPECIAL_NODE_YN == "0")
                        lst.Remove(itm);
                }

                ddlReqisitionType.DataSource = lst;
                ddlReqisitionType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlReqisitionType.DataValueField = "REQUISITION_TYPE_ID";
                ddlReqisitionType.DataBind();
                ddlReqisitionType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillLoanDropDownList()
        {
            try
            {
                List<INV_LOAN_DETAILS> lst = _masterInventoryServise.GetAllLoans();
                lst.Where(d => d.ALREADY_USED_YN == "0");
                
                ddlLoan.DataSource = lst;
                ddlLoan.DataTextField = "ACCOUNT_NO";
                ddlLoan.DataValueField = "LOAN_ID";
                ddlLoan.DataBind();
                ddlLoan.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProductByGenericId(string genericId = "1", DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetAllPaddyRice();
                    HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillMeasurementUnisDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] != null)
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = (List<INV_UNIT_OF_MEASUREMENT>)HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = _masterInventoryServise.GetAllUNIT_OF_MEASUREMENT();
                    HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillGodownDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.INV_GODOWN.ToString()] != null)
                {
                    List<INV_GODOWN> lst = (List<INV_GODOWN>)HttpContext.Current.Session[SessionVariable.INV_GODOWN.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "NAME";
                    ddl.DataValueField = "GODOWN_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_GODOWN> lst = _masterInventoryServise.GetAllInvGodown();
                    HttpContext.Current.Session[SessionVariable.INV_GODOWN.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "NAME";
                    ddl.DataValueField = "GODOWN_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillFactoryGodownDropDownList(DropDownList ddl)
        {
            try
            {
                List<INV_GODOWN> lst = _masterInventoryServise.GetOnlyFactoryGodowns();
                ddl.DataSource = lst;
                ddl.DataTextField = "NAME";
                ddl.DataValueField = "GODOWN_ID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));

                //if (HttpContext.Current.Session[SessionVariable.INV_FACTORY.ToString()] != null)
                //{
                //    List<INV_GODOWN> lst = (List<INV_GODOWN>)HttpContext.Current.Session[SessionVariable.INV_FACTORY.ToString()];
                //    ddl.DataSource = lst;
                //    ddl.DataTextField = "NAME";
                //    ddl.DataValueField = "FACTORY_ID";
                //    ddl.DataBind();
                //    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                //}
                //else
                //{
                //    List<INV_GODOWN> lst = _masterInventoryServise.GetOnlyFactoryGodowns();
                //    HttpContext.Current.Session[SessionVariable.INV_FACTORY.ToString()] = lst;
                //    ddl.DataSource = lst;
                //    ddl.DataTextField = "NAME";
                //    ddl.DataValueField = "FACTORY_ID";
                //    ddl.DataBind();
                //    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadNewProductGridView()
        {
            DataTable newline = new DataTable("Product");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("GODOWN_ID2", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));

            //if (gvProduct.Rows.Count == 0)
            //{
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "0" });
            //}

            gvProduct.DataSource = newline;
            gvProduct.DataBind();
        }
        private void LoadNewSalesGridView()
        {
            DataTable newline = new DataTable("ProductSales");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            //if (gvSales.Rows.Count == 0)
            //{
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "0", "0" });
            //}

            gvSales.DataSource = newline;
            gvSales.DataBind();
        }
        private List<INV_INVENTORY_DETAILS> BuiltInventoryDetailsObjectsFromSales(string loggedUserID)
        {
            List<INV_INVENTORY_DETAILS> lst = new List<INV_INVENTORY_DETAILS>();
            INV_INVENTORY_DETAILS prodFrom;//
            foreach (GridViewRow row in gvSales.Rows)
            {
                if (((DropDownList)row.FindControl("ddlGodown")).SelectedValue.ToString() != "0"
                    && ((DropDownList)row.FindControl("ddlProductSales")).SelectedValue.ToString() != "0"
                    && ((DropDownList)row.FindControl("ddlUnitSales")).SelectedValue.ToString() != "0"
                    && Convert.ToDecimal(((TextBox)row.FindControl("txtQuantitySales")).Text.ToString().Trim())>0
                    )
                {
                    prodFrom = new INV_INVENTORY_DETAILS();

                    prodFrom.GODOWN_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlGodown")).SelectedValue.ToString().Trim());
                    prodFrom.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProductSales")).SelectedValue.ToString().Trim());
                    prodFrom.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantitySales")).Text.ToString().Trim());
                    prodFrom.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnitSales")).SelectedValue.ToString().Trim());
                    prodFrom.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRateSales")).Text.ToString().Trim());
                    prodFrom.DISCOUNT_AMOUNT = Convert.ToDecimal(((TextBox)row.FindControl("txtDiscunt")).Text.ToString().Trim());
                    prodFrom.DISCOUNT_PC = Convert.ToDecimal(((TextBox)row.FindControl("txtPC")).Text.ToString().Trim());
                    prodFrom.FROM_TO_PLEDGE_ID = "1";
                    prodFrom.MULTIPLICATION_FACTOR = -1;
                    prodFrom.SET_USER = loggedUserID;
                    prodFrom.SET_DATE = DateTime.Now;

                    lst.Add(prodFrom);
                }
            }
            return lst;
        }
        private List<INV_INVENTORY_DETAILS> BuiltInventoryDetailsObjectsFrom(string loggedUserID)
        {
            List<INV_INVENTORY_DETAILS> lst = new List<INV_INVENTORY_DETAILS>();
            INV_INVENTORY_DETAILS prodFrom;//
            int facGodown = Convert.ToInt32(ddlBank.SelectedValue.ToString());

            foreach (GridViewRow row in gvProduct.Rows)
            {
                if (((DropDownList)row.FindControl("ddlFromGodown")).SelectedValue.ToString() != "0")
                {
                    prodFrom = new INV_INVENTORY_DETAILS();

                    prodFrom.GODOWN_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlFromGodown")).SelectedValue.ToString().Trim());

                    if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PRODUCTION)).ToString())
                        prodFrom.GODOWN_ID2 = facGodown;// Convert.ToInt32(((DropDownList)row.FindControl("ddlToGodown")).SelectedValue.ToString().Trim());
                    else
                        prodFrom.GODOWN_ID2 = Convert.ToInt32(((DropDownList)row.FindControl("ddlToGodown")).SelectedValue.ToString().Trim());


                    prodFrom.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                    prodFrom.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                    prodFrom.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                    prodFrom.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                    prodFrom.FROM_TO_PLEDGE_ID = "1";
                    prodFrom.MULTIPLICATION_FACTOR = -1;
                    prodFrom.SET_USER = loggedUserID;
                    prodFrom.SET_DATE = DateTime.Now;

                    lst.Add(prodFrom);
                }
            }
            return lst;
        }
        private List<INV_INVENTORY_DETAILS> BuiltInventoryDetailsObjectsTo(string loggedUserID)
        {
            List<INV_INVENTORY_DETAILS> lst = new List<INV_INVENTORY_DETAILS>();
            INV_INVENTORY_DETAILS prodTo;//
            int facGodown =Convert.ToInt32( ddlBank.SelectedValue.ToString());

            if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PRODUCTION)).ToString())
            {
                foreach (GridViewRow row in gvProduct.Rows)
                {
                    if (ddlBank.SelectedValue.ToString() != "0")
                    {
                        prodTo = new INV_INVENTORY_DETAILS();

                        prodTo.GODOWN_ID2 = Convert.ToInt32(((DropDownList)row.FindControl("ddlFromGodown")).SelectedValue.ToString().Trim());
                        prodTo.GODOWN_ID = facGodown;// Convert.ToInt32(((DropDownList)row.FindControl("ddlToGodown")).SelectedValue.ToString().Trim());
                        prodTo.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                        prodTo.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                        prodTo.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                        prodTo.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                        prodTo.FROM_TO_PLEDGE_ID = "2";
                        prodTo.MULTIPLICATION_FACTOR = 1;
                        prodTo.SET_USER = loggedUserID;
                        prodTo.SET_DATE = DateTime.Now;

                        lst.Add(prodTo);
                    }
                }
            }
            else
            {
                foreach (GridViewRow row in gvProduct.Rows)
                {
                    if (((DropDownList)row.FindControl("ddlToGodown")).SelectedValue.ToString() != "0")
                    {
                        prodTo = new INV_INVENTORY_DETAILS();

                        prodTo.GODOWN_ID2 = Convert.ToInt32(((DropDownList)row.FindControl("ddlFromGodown")).SelectedValue.ToString().Trim());
                        prodTo.GODOWN_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlToGodown")).SelectedValue.ToString().Trim());
                        prodTo.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                        prodTo.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                        prodTo.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                        prodTo.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                        prodTo.FROM_TO_PLEDGE_ID = "2";
                        prodTo.MULTIPLICATION_FACTOR = 1;
                        prodTo.SET_USER = loggedUserID;
                        prodTo.SET_DATE = DateTime.Now;

                        lst.Add(prodTo);
                    }
                }
            }
            return lst;
        }
        private void ClearControls()
        {
            txtChallanNo.Text = string.Empty;
            //txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtRemarks.Text = string.Empty;
            //ddlLoan.SelectedValue = "0";
            rbtnCashCredit.SelectedValue = "1";
            this.LoadNewProductGridView();
            this.LoadNewSalesGridView();
        }
        private void GetLoanDetailsById(string LoanId)
        {
            INV_LOAN_DETAILS ln = _masterInventoryServise.GetLoanDetailsByID(LoanId);
            divLoanInfo.Controls.Add(new LiteralControl("Loan Holder: " + ln.ACCOUNT_HOLDER)); //Add some test text
            divLoanInfo.Controls.Add(new LiteralControl("<br/>")); //Add line break
            divLoanInfo.Controls.Add(new LiteralControl("Credit Limit: " + ln.CREDIT_LIMIT.Value.ToString("0.00"))); //Add some test text
            divLoanInfo.Controls.Add(new LiteralControl("<br/>")); //Add line break
            divLoanInfo.Controls.Add(new LiteralControl("Interest Rate: " + ln.INTEREST_RATE.Value.ToString("0.00"))); //Add some test text

            //divLoanInfo.InnerText = "Loan Holder: " + ln.ACCOUNT_HOLDER + "\r\n Credit Limit: " + ln.CREDIT_LIMIT + "\r\nInterest Rate: " + ln.INTEREST_RATE;
        }
        private void FillSalesTypeDropDownList(string requisitionType)
        {
            try
            {
                //if (requisitionType == "0") return;
                //ddlLoan.DataSource = null;
                //ddlLoan.DataBind();

                ddlLoan.DataSource = _masterProcurementService.GetVendorTypeByGenericId(requisitionType);
                ddlLoan.DataTextField = "VENDOR_TYPE";
                ddlLoan.DataValueField = "VENDOR_TYPE_ID";
                ddlLoan.DataBind();
                ddlLoan.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<PUR_VENDOR> GetVendorListByCategoryId(string VendorType)
        {
            return _masterProcurementService.GetVendorByTypeId(VendorType);
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                if (!this.ValidateInputedData())
                    return;

                INV_INVENTORY_MASTER invMaster = new INV_INVENTORY_MASTER();
                invMaster.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue;
                invMaster.CHALLAN_NO = txtChallanNo.Text.Trim();
                invMaster.CHALLAN_DATE = DateTime.ParseExact(txtDate.Text.Trim(), "dd/MM/yyyy", null);
                if (ddlReqisitionType.SelectedValue ==((int) (PurchaseType.PLEDGE_TRANSFER)).ToString())
                    invMaster.LOAN_ID = Convert.ToInt32(ddlLoan.SelectedValue);
                if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.SALES)).ToString())
                {
                    invMaster.SALES_ON_CASH_CREDIT = rbtnCashCredit.SelectedValue.ToString();
                    invMaster.SALES_TK =Convert.ToDecimal( txtCashAmount.Text.Trim());
                    invMaster.PADDY_PURCHASE_TYPE_ID = Convert.ToInt32(ddlLoan.SelectedValue);
                    invMaster.VENDOR_ID = Convert.ToInt32(ddlSalesClient.SelectedValue);
                    //invMaster.PAYMENT_MODE_ID=""
                }
                invMaster.REMARKS = txtRemarks.Text.Trim();
                invMaster.TRANS_COMPLETE_YN = "0";
                invMaster.SET_DATE = DateTime.Now;
                invMaster.SET_USER = loggedUserID;
                List<INV_INVENTORY_DETAILS> invDetailsFrom=new List<INV_INVENTORY_DETAILS> ();
                List<INV_INVENTORY_DETAILS> invDetailsTo=new List<INV_INVENTORY_DETAILS> ();

                if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.SALES)).ToString())
                {
                    invDetailsFrom = this.BuiltInventoryDetailsObjectsFromSales(loggedUserID);
                }
                else
                {
                    invDetailsFrom = this.BuiltInventoryDetailsObjectsFrom(loggedUserID);
                    invDetailsTo = this.BuiltInventoryDetailsObjectsTo(loggedUserID);
                }

                if (invDetailsFrom.Count() <= 0)
                {
                    ((rBKK.WebApplication.Site)Master).ShowMessage("No record found in grid. Please check...", Common.MessageType.Error);
                    return;
                }

                _masterProcurementService.SaveProductionData(invMaster, invDetailsFrom, invDetailsTo, loggedUserID);
                this.ClearControls();
                string TransType = Request.QueryString["TransType"].ToString();
                this.FillChallanListGridView(TransType);
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private bool ValidateInputedData()
        {
            string msg = string.Empty;

            if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PRODUCTION)).ToString())
            {
                if (string.IsNullOrEmpty(txtChallanNo.Text.Trim()))
                    msg += "Enter Issue No.\n";
                if (ddlBank.SelectedValue.ToString() == "0")
                    msg += "Select Factory-Godown.";
            }
            else if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.GODOWN_TRANSFER)).ToString())
            { }
            else if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PLEDGE_TRANSFER)).ToString())
            { }
            else if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.SALES)).ToString())
            { }

            if (!string.IsNullOrEmpty(msg))
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(msg, Common.MessageType.Error);
                return false;
            }
            return true;
        }

        
        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.ClearControls();
            ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);
        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {

        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvProduct.Rows.Count == 1) return;

            DataTable newline = new DataTable("Product");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("GODOWN_ID2", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));

            if (gvProduct.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "" });
            }

            foreach (GridViewRow gvrow in gvProduct.Rows)
            {
                DropDownList ddlFromGodown = (DropDownList)gvrow.FindControl("ddlFromGodown");
                DropDownList ddlToGodown = (DropDownList)gvrow.FindControl("ddlToGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                newline.Rows.Add(new String[] { ddlFromGodown.SelectedValue, ddlToGodown.SelectedValue
                    , ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text});
            }
            newline.Rows[rowINdx].Delete();

            gvProduct.DataSource = newline;
            gvProduct.DataBind();
        }
        protected void ibtnAddMore_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Product");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("GODOWN_ID2", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));

            if (gvProduct.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "0" });
            }

            foreach (GridViewRow gvrow in gvProduct.Rows)
            {
                DropDownList ddlFromGodown = (DropDownList)gvrow.FindControl("ddlFromGodown");
                DropDownList ddlToGodown = (DropDownList)gvrow.FindControl("ddlToGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                newline.Rows.Add(new String[] { ddlFromGodown.SelectedValue, ddlToGodown.SelectedValue
                    , ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text});
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "0" });

            gvProduct.DataSource = newline;
            gvProduct.DataBind();
        }
        protected void gvProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region paddy & rice  //
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                DropDownList ddlFromGodown = (DropDownList)e.Row.FindControl("ddlFromGodown");
                this.FillGodownDropDownList(ddlFromGodown);
                string strGodownId = ((HiddenField)e.Row.FindControl("dhfFromGodown")).Value;
                ddlFromGodown.SelectedValue = strGodownId;

                //e.Row.Cells[1].CssClass = "HiddenGridColumn";//1=2nd column (godown Type)
                DropDownList ddlToGodown = (DropDownList)e.Row.FindControl("ddlToGodown");
                this.FillGodownDropDownList(ddlToGodown);
                string strGodownId2 = ((HiddenField)e.Row.FindControl("hfToGodown")).Value;
                ddlToGodown.SelectedValue = strGodownId2;

                DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProduct");
                string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                this.FillProductByGenericId("1", ddlProduct);
                ddlProduct.SelectedValue = strhfProduct;

                DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                this.FillMeasurementUnisDropDownList(ddlUnit);
                string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                ddlUnit.SelectedValue = strhfMeasurementUnit;

                TextBox txtQuantity = (TextBox)e.Row.FindControl("txtQuantity");

                ddlFromGodown.Attributes.Add("onchange", "DropDownListChange('" + ddlFromGodown.ClientID + "','" + txtQuantity.ClientID + "','0')");
                ddlProduct.Attributes.Add("onchange", "DropDownListChange('" + ddlProduct.ClientID + "','" + txtQuantity.ClientID + "','2')");
                ddlUnit.Attributes.Add("onchange", "DropDownListChange('" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "','0')");

                txtQuantity.Attributes.Add("onblur", "return CompareCurrentStock('" + ddlFromGodown.ClientID + "','" + ddlProduct.ClientID + "','" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "')");


                //TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                //TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                //txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                //txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
            }
            string TransType = Request.QueryString["TransType"].ToString();
            if (TransType == ((int)(PurchaseType.PRODUCTION)).ToString()
                || (TransType == ((int)(PurchaseType.GODOWN_TRANSFER)).ToString()))
            {
                e.Row.Cells[5].CssClass = "HiddenGridColumn"; //Rate column

                if(TransType == ((int)(PurchaseType.PRODUCTION)).ToString())
                    e.Row.Cells[1].CssClass = "HiddenGridColumn"; //To-Godown

            }

            //else if (e.Row.RowType == DataControlRowType.Header)
            //{
            //    //e.Row.Cells[0].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
            //}
            #endregion
        }

        protected void ddlReqisitionType_SelectedIndexChanged(object sender, EventArgs e)
        {

            lblType.Visible = false;
            ddlLoan.Visible = false;
            lblClient.Visible = false;
            ddlSalesClient.Visible = false;
            lblCashCredit.Visible = false;
            rbtnCashCredit.Visible = false;
            ddlLoan.Enabled = false;
            txtCashAmount.Visible = false;
            lblBank.Visible = false;
            ddlBank.Visible = false;

            if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PRODUCTION)).ToString())
            {
                ddlBank.Visible = true;
                lblBank.Visible = true;
                lblBank.Text = "Factory Godown";
                ddlBank.AutoPostBack = false;

                this.FillFactoryGodownDropDownList(ddlBank);               

                divProductions.Visible = true;
                divSales.Visible = false;
            }
            else if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.PLEDGE_TRANSFER)).ToString())
            {
                lblType.Visible = true;
                ddlLoan.Visible = true;
                ddlLoan.Enabled = true;
                divProductions.Visible = true;
                divSales.Visible = false;
                ddlBank.Visible = true;
                lblBank.Visible = true;
                lblType.Text = "Loan Account";
                this.FillLoanDropDownList();
            }
            //else
            //{
            //    ddlLoan.SelectedValue = "0";
            //    ddlLoan.Enabled = false;
            //}
            else if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.SALES)).ToString())
            {
                lblType.Visible = true;
                ddlLoan.Visible = true;
                txtDirectClientList.Visible = false;
                divProductions.Visible = false;
                divSales.Visible = true;
                lblClient.Visible = true;
                ddlSalesClient.Visible = true;
                ddlLoan.Enabled = true;
                lblCashCredit.Visible = true;
                rbtnCashCredit.Visible = true;
                txtCashAmount.Visible = true;
                lblType.Text = "Sales Type";
                this.FillSalesTypeDropDownList(ddlReqisitionType.SelectedValue);
            }
            else
            {
                divProductions.Visible = true;
                divSales.Visible = false;
            }

        }

       

        protected void ddlLoan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReqisitionType.SelectedValue == ((int)(PurchaseType.SALES)).ToString())
            {
                string strSalesType = ddlLoan.SelectedValue.ToString();
                if (strSalesType == "17")
                {
                    this.FillSalesClientDropDownList(ddlLoan.SelectedValue.ToString());
                    txtDirectClientList.Visible = false;
                    ddlSalesClient.Visible = true;
                }
                else
                {
                    txtDirectClientList.Visible = true;
                    ddlSalesClient.Visible = false;
                }
            }
            else
            {
                this.GetLoanDetailsById(ddlLoan.SelectedValue.ToString());
            }

        }

        private void FillSalesClientDropDownList(string vendorType)
        {
            ddlSalesClient.DataSource = this.GetVendorListByCategoryId(vendorType);
            ddlSalesClient.DataTextField = "VENDOR_NAME";
            ddlSalesClient.DataValueField = "VENDOR_ID";
            ddlSalesClient.DataBind();
            ddlSalesClient.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
        }


        protected void ibtnAddMoreSales_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("ProductSales");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            //if (gvProduct.Rows.Count == 0)
            //{
            //    for (int i = 0; i < 2; i++)
            //        newline.Rows.Add(new String[] { "", "", "", "", "", "", "" });
            //}

            foreach (GridViewRow gvrow in gvSales.Rows)
            {
                DropDownList ddlGodown = (DropDownList)gvrow.FindControl("ddlGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProductSales");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantitySales");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnitSales");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRateSales");
                TextBox txtAmt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPc = (TextBox)gvrow.FindControl("txtPC");
                newline.Rows.Add(new String[] { ddlGodown.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text
                    , ddlUnit.SelectedValue.ToString(), txtRate.Text, txtAmt.Text, txtPc.Text});
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "0", "0"});

            gvSales.DataSource = newline;
            gvSales.DataBind();
        }

        protected void gvSales_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            #region paddy & rice  //
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlGodown");
                this.FillGodownDropDownList(ddlGodown);
                string strGodownId = ((HiddenField)e.Row.FindControl("dhfGodown")).Value;
                ddlGodown.SelectedValue = strGodownId;

                DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProductSales");
                string strhfProduct = ((HiddenField)e.Row.FindControl("hfProductSales")).Value;
                this.FillProductByGenericId("1", ddlProduct);
                ddlProduct.SelectedValue = strhfProduct;

                DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnitSales");
                this.FillMeasurementUnisDropDownList(ddlUnit);
                string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnitSales")).Value;
                ddlUnit.SelectedValue = strhfMeasurementUnit;

                TextBox txtQuantity = (TextBox)e.Row.FindControl("txtQuantitySales");
                TextBox txtRateSales = (TextBox)e.Row.FindControl("txtRateSales");

                ddlGodown.Attributes.Add("onchange", "DropDownListChange('" + ddlGodown.ClientID + "','" + txtQuantity.ClientID + "','" + txtRateSales.ClientID + "','0')");
                ddlProduct.Attributes.Add("onchange", "DropDownListChange('" + ddlProduct.ClientID + "','" + txtQuantity.ClientID + "','" + txtRateSales.ClientID + "','2')");
                ddlUnit.Attributes.Add("onchange", "DropDownListChange('" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "','" + txtRateSales.ClientID + "','0')");

                txtQuantity.Attributes.Add("onblur", "return CompareCurrentStock('" + ddlGodown.ClientID + "','" + ddlProduct.ClientID + "','" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "')");
                txtRateSales.Attributes.Add("readonly", "readonly");

                TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
            }
            #endregion
        }

        protected void ibtnDeleteSales_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvSales.Rows.Count == 1) return;

            DataTable newline = new DataTable("ProductSales");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            //if (gvProduct.Rows.Count == 0)
            //{
            //    for (int i = 0; i < 2; i++)
            //        newline.Rows.Add(new String[] { "", "", "", "", "", "", "" });
            //}

            foreach (GridViewRow gvrow in gvSales.Rows)
            {
                DropDownList ddlGodown = (DropDownList)gvrow.FindControl("ddlGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProductSales");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantitySales");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnitSales");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRateSales");
                TextBox txtAmt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPc = (TextBox)gvrow.FindControl("txtPC");
                newline.Rows.Add(new String[] { ddlGodown.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text
                    , ddlUnit.SelectedValue.ToString(), txtRate.Text, txtAmt.Text, txtPc.Text});
            }
            newline.Rows[rowINdx].Delete();

            gvSales.DataSource = newline;
            gvSales.DataBind();
        }

        protected void ImageButton2_Click1(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvChallanList.Rows[rowINdx].Cells[0].FindControl("lblID")).Text.ToString());

                string TypeID = Convert.ToString(((Label)gvChallanList.Rows[rowINdx].Cells[0].FindControl("lblType")).Text.ToString());
                 List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dtAttFrom = _masterProcurementService.GetProductionReportsData(strID);
                    SessionManager.GetRPTSalesDataTable = dtAttFrom;
                
                if (TypeID == "Production")
                {
                   
                    ShowReport(this, this.GetType(), "ProductionReports.rdlc", "INVSales", dtAttFrom);
                }

                else if(TypeID=="Sales")
                {
                    ShowReport(this, this.GetType(), "RiceSalesReports.rdlc", "INVSales", dtAttFrom);
                }

                else
                    ShowReport(this, this.GetType(), "TransferReports.rdlc", "INVSales", dtAttFrom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(Control control, Type type, string sReportFileName, string sReportDataSourceName, List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dtAttFrom)
        {
            string script = string.Format(@"window.open('ProductionReportsViewer.aspx?ReportFileName={0}&&DataSourceName={1}','CommonReportViewer',
                                            'width=1000,height=650,resizable=1,scrollbars=1');", sReportFileName, sReportDataSourceName);
            ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), script, true);
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string bankId = ddlBank.SelectedValue;
                ddlLoan.DataSource = _masterInventoryServise.GetBankAccountByBankId(bankId);
                ddlLoan.DataTextField = "ACCOUNT_NO";
                ddlLoan.DataValueField = "ACCOUNT_NO";
                ddlLoan.DataBind();
                ddlLoan.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvChallanList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string TransType = Request.QueryString["TransType"].ToString();
            if (TransType != ((int)(PurchaseType.SALES)).ToString())
            {
                e.Row.Cells[5].CssClass = "HiddenGridColumn"; //Rate column
            }
        }

      

     
    }
}