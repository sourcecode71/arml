﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductType.aspx.cs" Inherits="rBKK.WebApplication.Inventory.ProductType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<title>Product Category </title>
    <script src="http://localhost:4345/Scripts/jquery-1.6.min.js" type="text/javascript"></script>
    <script src="http://localhost:4345/Scripts/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="http://localhost:4345/Scripts/jquery.validationEngine.js" type="text/javascript"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#aspnetForm').validationEngine();
        });
                    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="MiniPage">

        <fieldset class="flView">
            <legend class="lgView"> Product Category </legend>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>

                 

                
                    <table class="bgdisplay">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hiCatgoryID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                <span>Generic Type</span></td>
                            <td style="width: 315px">
                                <asp:DropDownList ID="ddlReqisitionType" runat="server" AutoPostBack="True" 
                                    SkinID="normalDropDownList" Width="230px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 13px">
                                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                <span>Category Name </span>
                            </td>
                            <td style="width: 315px">
                                <asp:TextBox ID="txtCategoryName" runat="server" SkinID="normalTextBox" 
                                    Width="226px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtCategoryName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                          OnClientClick="myValidation()"  CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                        </tr>
                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </fieldset>

    </div>

     <div class="MiniDisplay">

 <fieldset class="flView">
 <legend class="lgView"> Category List </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="Category ID">
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("CATEGORY_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="GenericID">
                         <ItemTemplate>
                             <asp:Label ID="lblGenericTypeId" runat="server" Text='<%# Bind("REQUISITION_TYPE_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:BoundField DataField="REQUISITION_TYPE_NAME" HeaderText="Generic Type" />
                     <asp:BoundField DataField="CATEGORY_NAME" HeaderText="name" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
    
</asp:Content>
