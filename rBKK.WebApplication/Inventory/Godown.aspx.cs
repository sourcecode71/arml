﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using rBKK.Entities;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class Godown : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddGodown).ToString());

                if (!IsPostBack)
                {
                    this.LoadUnit();
                    this.BindGridView();

                }
            }
            catch (Exception ex)
            {
                
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void BindGridView()
        {
            var dATA = (_masterInventoryServise.GetInvGodownByFct()).ToList();
            gvGodown.DataSource = dATA;
            gvGodown.DataBind();
        }

        private void LoadUnit()
        {
            try
            {
                var dATA = (_masterInventoryServise.GetAllUNIT_OF_MEASUREMENT()).ToList();
                ddlUnit.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                ddlUnit.DataTextField = "UNIT_OF_MEASUREMENT";
                ddlUnit.DataSource = dATA;
                ddlUnit.DataBind();
                ddlUnit.Items.Insert(0, new ListItem("<<<----Select---->>>", "0"));
                ddlUnit.SelectedValue = "0";
            }
            catch (Exception ex)
            {
                
                throw ex;
            } 
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string TranType;
                INV_GODOWN iNV_Godown = new INV_GODOWN();
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                if (btnSave.Text == "Save")
                {
                    TranType = "Save";
                    iNV_Godown.NAME = txtName.Text;
                    iNV_Godown.ADDRESS = txtAdress.Text;
                    iNV_Godown.CAPACITY =Convert.ToDecimal(txtCapacity.Text);
                    iNV_Godown.UNIT_OF_MEASUREMENT = Convert.ToInt16(ddlUnit.SelectedValue);
                   iNV_Godown.COMPANY_ID = "1";
                   iNV_Godown.SET_DATE = DateTime.Now;
                   iNV_Godown.SET_USER = loggedUserID;
                    _masterInventoryServise.AddInvGodown(iNV_Godown, TranType);
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    iNV_Godown.GODOWN_ID = Convert.ToInt32(hiID.Value);
                    iNV_Godown.UNIT_OF_MEASUREMENT = Convert.ToInt16(ddlUnit.SelectedValue);
                    iNV_Godown.NAME = txtName.Text;
                    iNV_Godown.ADDRESS = txtAdress.Text;
                    iNV_Godown.CAPACITY = Convert.ToDecimal(txtCapacity.Text);
                    _masterInventoryServise.AddInvGodown(iNV_Godown, TranType);
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                this.BindGridView();
                this.ResetAll();

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }

        private void ResetAll()
        {
            txtCapacity.Text = string.Empty;
            txtAdress.Text = string.Empty;
            txtName.Text = string.Empty;
            ddlUnit.SelectedIndex = 0;
            btnSave.Text = "Save";
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvGodown.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                INV_GODOWN godown = _masterInventoryServise.GetInvGodownById(strID);
                txtName.Text = godown.NAME;
                txtAdress.Text = godown.ADDRESS;
                txtCapacity.Text = godown.CAPACITY.ToString();
                ddlUnit.SelectedValue = godown.UNIT_OF_MEASUREMENT.ToString();
                btnSave.Text = "Update";
                hiID.Value = strID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvGodown.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteInvGodown(strID);
                this.BindGridView();

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}