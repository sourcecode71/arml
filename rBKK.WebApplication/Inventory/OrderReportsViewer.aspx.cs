﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;
using rBKK.WebApplication.Reports;
using Microsoft.Reporting.WebForms;

namespace rBKK.WebApplication.Procurement
{
    public partial class OrderReportsViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count >= 2)
                {
                    PreviewReport(Request.QueryString["ReportFileName"], Request.QueryString["DataSourceName"]);
                }
            }

        }

        private void PreviewReport(string sReportFileName, string reportDataSourceName)
        {
            try
            {
                List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dsReportDaSourceSet = SessionManager.GetRPTSalesDataTable;
                this.ShowReport("Reports/" + sReportFileName, reportDataSourceName, dsReportDaSourceSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dataSourceValue)
        {
            this.ReportViewer1.LocalReport.DataSources.Clear();
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.ReportViewer1.LocalReport.ReportPath = reportName;
            this.ReportViewer1.LocalReport.Refresh();
        }
    }

}
