﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MachineryInfo.aspx.cs" Inherits="rBKK.WebApplication.Inventory.MachineryInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="../Scripts/jquery-1.6.min.js" type="text/javascript"></script>    
    <script src="../Scripts/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.validationEngine.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   

<div class="MiniPage">

        <fieldset class="flView">
            <legend class="lgView"> Machinery Info </legend>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
             
                <ContentTemplate>

                <script type="text/javascript">
                    jQuery(document).ready(function () {

                        jQuery("#aspnetForm").validationEngine();

                    });
                 </script>

                    <table class="bgdisplay">
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                                                            style="text-align: center" Width="461px"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                                        &nbsp; &nbsp; Type  </td>
                            <td style="width: 315px" colspan="2">
                                <asp:DropDownList ID="ddlCategory" runat="server" Width="230px" CssClass="validate[required]">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 13px">
                                                        &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                &nbsp;&nbsp;&nbsp; Code&nbsp;</td>
                            <td style="width: 315px" colspan="2">
                                <asp:TextBox ID="txtUniqNumber" runat="server" Width="226px" CssClass="validate[required]" ></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                &nbsp; &nbsp; Name
                            </td>
                            <td style="width: 315px" colspan="2">
                                <asp:TextBox ID="txtName" runat="server" Width="226px" CssClass="validate[required]"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                &nbsp;&nbsp;&nbsp; Warinty Period&nbsp;</td>
                            <td style="width: 315px" colspan="2">
                                <asp:TextBox ID="txtName0" runat="server" Width="226px"></asp:TextBox>
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:150px">
                                &nbsp;</td>
                            <td style="width: 315px" colspan="2">
                                <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                            </td>
                            <td style="width: 13px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                                        &nbsp; &nbsp; &nbsp; &nbsp;
                                                    </td>
                            <td align="center">
                                &nbsp;</td>
                            <td align="center" colspan="2">
                                &nbsp;</td>
                        </tr>
                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </fieldset>

    </div>

<div class="MiniDisplay">

 <fieldset class="flView">
 <legend class="lgView"> Machinery List</legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:BoundField DataField="Category" HeaderText="Category" />
                     <asp:BoundField DataField="Product Name" HeaderText=" NAME" />
                     <asp:BoundField HeaderText="Warinty Period" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
