﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.WebApplication.Common;
using rBKK.Entities;
using Resources;

namespace rBKK.WebApplication.Inventory
{
    public partial class AccountDetails : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddBank).ToString());

                if (!IsPostBack)
                {
                    this.loadPageData();
                    this.LoadBranchInfo();
                }
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }

        private void loadPageData()
        {
            try
            {
                var dATA = (_masterInventoryServise.GetACCOUNTINFO()).ToList();
                gvFactory.DataSource = dATA;
                gvFactory.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void LoadBranchInfo()
        {
            try
            {
                 var dATA = (_masterInventoryServise.GetAllBranchDeatials()).ToList();
                 ddlBranch.DataSource = dATA;
                 ddlBranch.DataValueField = "BRANCH_ID";
                 ddlBranch.DataTextField = "BANK_BRANCH_NAME";
                 //ddlBranch.DataTextField = "BRANCH_NAME";
                 ddlBranch.DataBind();
                 ddlBranch.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        private void ResetAll()
        {
            try
            {
                txtAccount.Text = string.Empty;
                txtAccountHolder.Text = string.Empty;
                txtAccountOpenDate.Text = string.Empty;
                ddlBranch.SelectedIndex = 0;
                btnSave.Text = "Save";
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                CORE_BANK_ACCOUNTS core_Acoount = new CORE_BANK_ACCOUNTS();
                if (btnSave.Text == "Save")
                {
                   
                    core_Acoount.ACCOUNT_NO = txtAccount.Text;
                    core_Acoount.ACCOUNT_HOLDER =txtAccountHolder.Text;
                    core_Acoount.ACC_OPEN_DATE = DateTime.ParseExact(txtAccountOpenDate.Text, "dd/MM/yyyy", null);
                    core_Acoount.BRANCH_ID =Convert.ToInt32(ddlBranch.SelectedValue);
                    core_Acoount.SET_USER = loggedUserID;
                    core_Acoount.SET_DATE = DateTime.Now;

                    _masterInventoryServise.AddCoreAccountInfo(core_Acoount, OperationType.SAVE.ToString());
                }
                else
                {
                    core_Acoount.ACCOUNT_NO = txtAccount.Text;
                    core_Acoount.ACCOUNT_HOLDER = txtAccountHolder.Text;
                    core_Acoount.ACC_OPEN_DATE = DateTime.ParseExact(txtAccountOpenDate.Text, "dd/MM/yyyy", null);
                    core_Acoount.BRANCH_ID = Convert.ToInt32(ddlBranch.SelectedValue);
                    core_Acoount.SET_USER = loggedUserID;
                    core_Acoount.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddCoreAccountInfo(core_Acoount, OperationType.UPDATE.ToString());
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);

                this.loadPageData();
                this.ResetAll();

            }
            catch (Exception ex)
            {

            }
        }


        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.ResetAll();
        }

        protected void img_editFactory(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblAccount")).Text.ToString());
                txtAccount.Text = ((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblAccount")).Text.ToString();
                txtAccountHolder.Text = ((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblAccountHolder")).Text.ToString();
                txtAccountOpenDate.Text = ((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblOpenDate")).Text.ToString();
                ddlBranch.SelectedValue = ((HiddenField)gvFactory.Rows[rowINdx].Cells[0].FindControl("hfBankId")).Value.ToString();

                hfID.Value = strID; 
                btnSave.Text = "Update";
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvFactory.Rows[rowINdx].Cells[0].FindControl("lblAccount")).Text.ToString());
                _masterInventoryServise.DeleteAccountInfo(strCatID);
                this.loadPageData();
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_DELETED_SUCCESS, Common.MessageType.Info);
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}