﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.Entities;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using Ninject;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class MachineryType : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.MachineryType).ToString());

                if (!IsPostBack)
                {
                   this.GetMachineryType();
                }
            }
            catch (Exception ex)
            {
           ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }

        private void GetMachineryType()
        {
            try
            {
                List<INV_MACHINERY_TYPE> invProductCategory = _masterInventoryServise.GetAllMachneryType();
                gvType.DataSource = invProductCategory;
                gvType.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                INV_MACHINERY_TYPE machineType = new INV_MACHINERY_TYPE();
                string TranType;
                if (btnSave.Text == "Save")
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Save";
                    machineType.MACHINERY_TYPE = txtTypeName.Text;
                    machineType.COMPANY_ID ="1";
                    machineType.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddMachineryTYpe(machineType, TranType);
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    machineType.MACHINERY_TYPE_ID = Convert.ToInt32(hiMachineType.Value);
                    machineType.MACHINERY_TYPE = txtTypeName.Text;
                    _masterInventoryServise.AddMachineryTYpe(machineType, TranType);
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                txtTypeName.Text = string.Empty;
                this.GetMachineryType();

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {

        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvType.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                INV_MACHINERY_TYPE machinery = _masterInventoryServise.GetMachineryTypeByID(strCatID);
                txtTypeName.Text = machinery.MACHINERY_TYPE;
                btnSave.Text = "Update";
                hiMachineType.Value = strCatID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvType.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteMachineyType(strCatID);
                this.GetMachineryType();
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}