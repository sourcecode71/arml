﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.WebApplication.Common;
using Resources;
using System.Data;
using rBKK.Entities;
using rBKK.WebApplication.Reports;
using rBKK.Infrastructure.Inventory;

namespace rBKK.WebApplication.Inventory
{
    public partial class ReceiveFGGood : System.Web.UI.Page
    {
        [Inject]
        public IFinishGoodsService _finishGoodsService { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IFinishGoodsRepository _finishGoodsRepository { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddBank).ToString());
                if (!IsPostBack)
                {
                    this.LoadPageData();
                    this.LoadNewPreOrder();
                    this.LoadReceiveFinishGood();
                    txtDate.Attributes.Add("readonly", "readonly");
                    txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    this.SetNullToSessionVariables();
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }

          

        }

        private void SetNullToSessionVariables()
        {
            Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = null;
            Session[SessionVariable.INV_GODOWN.ToString()] = null;
            Session[SessionVariable.MACHINE_TYPE.ToString()] = null;
            Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            Session[SessionVariable.PUR_PADDY.ToString()] = null;
            Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = null;
        }
        private void LoadNewPreOrder()
        {
            DataTable newline = new DataTable("Order");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_BRAN_QTY", typeof(String));
            newline.Columns.Add("BRAN_MEASUREMENT_UNIT_ID", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_HUSK_QTY", typeof(String));
            newline.Columns.Add("HUSK_MEASUREMENT_UNIT_ID", typeof(String));

            if (gvFinishGood.Rows.Count == 0)
            {
                for (int i = 0; i <2; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "","","","" });
            }

            gvFinishGood.DataSource = newline;
            gvFinishGood.DataBind();
        }
        private void LoadPageData()
        {

            try
            {
                this.LoadProductionGrid();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        private void LoadProductionGrid()
        {
            try
            {
                var dATA = _finishGoodsService.GetAllProductionGoods(((int)PurchaseType.PRODUCTION).ToString());
                gvProduction.DataSource = dATA;
                gvProduction.DataBind();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        private void LoadReceiveFinishGood()
        {
            try
            {
                var dATA = _finishGoodsService.GetAllFinishGoods(((int)PurchaseType.PRODUCTION).ToString());
                gvReceiveFGGood.DataSource = dATA;
                gvReceiveFGGood.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private void ClearControls()
        {
            txtComments.Text = string.Empty;
            txtComments.Text = String.Empty;
            txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

        }
        private void FillUpDropDownlist(DropDownList ddl)
        {
            try
            {
                if (Session[SessionVariable.INV_GODOWN.ToString()] == null)
                {
                    var dATA = (_masterInventoryServise.GetAllInvGodown()).ToList();

                    Session[SessionVariable.INV_GODOWN.ToString()] = dATA;
                    ddl.DataValueField = "GODOWN_ID";
                    ddl.DataTextField = "NAME";
                    ddl.DataSource = dATA;
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {

                    ddl.DataValueField = "GODOWN_ID";
                    ddl.DataTextField = "NAME";
                    ddl.DataSource = Session[SessionVariable.INV_GODOWN.ToString()];
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void FillMeasurementUnisDropDownList(DropDownList ddl)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] != null)
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = (List<INV_UNIT_OF_MEASUREMENT>)HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = _masterInventoryServise.GetAllUNIT_OF_MEASUREMENT();
                    HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProduct(int ProdType = 1, DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetProductByGenericId(ProdType.ToString());
                    //IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                    HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                INV_INVENTORY_MASTER invMaster = new INV_INVENTORY_MASTER();
                invMaster.CHALLAN_NO = txtChallanNo.Text.Trim();
                invMaster.CHALLAN_DATE = DateTime.ParseExact(txtDate.Text.Trim(), "dd/MM/yyyy", null);
                invMaster.REMARKS = txtComments.Text;
                invMaster.REQUISITION_TYPE_ID = ((int)PurchaseType.Received_FG).ToString();
                invMaster.SET_DATE = DateTime.Now;
                invMaster.SET_USER = loggedUserID;
                invMaster.TRANS_COMPLETE_YN = "1";
                invMaster.INV_MASTER_Parent_ID =Convert.ToInt64( hiID.Value);
                if (chkFinishProduction.Checked)
                    invMaster.ReceiveYN = true;
                else
                    invMaster.ReceiveYN = false;
                invMaster.INV_MASTER_ID = Convert.ToInt32(hiID.Value);

                List<INV_INVENTORY_DETAILS> invDetailsFrom = this.BuiltInventoryDetailsObjectsFrom(loggedUserID);
                List<INV_INVENTORY_DETAILS> invDetailsTo = this.BuiltInventoryDetailsObjectsTo(loggedUserID);
                _masterProcurementService.SaveProductionData(invMaster, invDetailsFrom, invDetailsTo, loggedUserID);
                this.ClearControls();
                this.LoadReceiveFinishGood();
                gvFinishGood.DataSource = null;
                gvFinishGood.DataBind();
                gvPartialGoods.DataSource = null;
                gvPartialGoods.DataBind();
                this.LoadNewPreOrder();
                this.LoadProductionGrid();
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        private List<INV_INVENTORY_DETAILS> BuiltInventoryDetailsObjectsFrom(string loggedUserID)
        {
           
            List<INV_INVENTORY_DETAILS> lst = new List<INV_INVENTORY_DETAILS>();
            INV_INVENTORY_DETAILS Fgprod;
            foreach (GridViewRow row in gvProduction.Rows)
            {
                if (((RadioButton)row.FindControl("rbtnSelect")).Checked)
                {
                   
                   hiID.Value = ((HiddenField)row.FindControl("hfFactory")).Value;
                }
 
            }
            foreach (GridViewRow row in gvFinishGood.Rows)
            {
                if (((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString() != "0")
                {
                    Fgprod = new INV_INVENTORY_DETAILS();
                    Fgprod.GODOWN_ID =Convert.ToInt32( hiID.Value);
                    Fgprod.GODOWN_ID2 = Convert.ToInt32(((DropDownList)row.FindControl("ddlFGGodown")).SelectedValue.ToString().Trim());
                    Fgprod.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                    Fgprod.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim()))
                        Fgprod.RATE = 0;
                    else
                    Fgprod.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                    Fgprod.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());

                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRiceBran")).Text.ToString().Trim()))
                        Fgprod.BY_PRODUCT_RICE_BRAN_QTY = 0;
                    else
                    Fgprod.BY_PRODUCT_RICE_BRAN_QTY = Convert.ToDecimal(((TextBox)row.FindControl("txtRiceBran")).Text.ToString().Trim());
                    Fgprod.HUSK_MEASUREMENT_UNIT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnitBran")).SelectedValue.ToString().Trim());
                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRiceHusk")).Text.ToString().Trim()))
                        Fgprod.BY_PRODUCT_RICE_HUSK_QTY = 0;
                    else
                    Fgprod.BY_PRODUCT_RICE_HUSK_QTY = Convert.ToDecimal(((TextBox)row.FindControl("txtRiceHusk")).Text.ToString().Trim());
                    Fgprod.HUSK_MEASUREMENT_UNIT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlRiceHuskUnit")).SelectedValue.ToString().Trim());
                    Fgprod.MULTIPLICATION_FACTOR =-1;
                    Fgprod.FROM_TO_PLEDGE_ID = "1";
                    Fgprod.SET_USER = loggedUserID;
                    Fgprod.SET_DATE = DateTime.Now;
                    lst.Add(Fgprod);
                }
            }
            return lst;
        }
        private List<INV_INVENTORY_DETAILS> BuiltInventoryDetailsObjectsTo(string loggedUserID)
        {
            List<INV_INVENTORY_DETAILS> lst = new List<INV_INVENTORY_DETAILS>();
            INV_INVENTORY_DETAILS Fgprod;
            foreach (GridViewRow row in gvProduction.Rows)
            {
                if (((RadioButton)row.FindControl("rbtnSelect")).Checked)
                {
                    hiID.Value = ((HiddenField)row.FindControl("hfFactory")).Value;

                }

            }
            foreach (GridViewRow row in gvFinishGood.Rows)
            {
               

                if (((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString() != "0")
                {
                    Fgprod = new INV_INVENTORY_DETAILS();
                    Fgprod.GODOWN_ID2 = Convert.ToInt32(hiID.Value);
                    Fgprod.GODOWN_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlFGGodown")).SelectedValue.ToString().Trim());
                    Fgprod.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                    Fgprod.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim()))
                        Fgprod.RATE = 0;
                    else
                    Fgprod.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                    Fgprod.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRiceBran")).Text.ToString().Trim()))
                        Fgprod.BY_PRODUCT_RICE_BRAN_QTY = 0;
                    else
                    Fgprod.BY_PRODUCT_RICE_BRAN_QTY = Convert.ToDecimal(((TextBox)row.FindControl("txtRiceBran")).Text.ToString().Trim());
                    Fgprod.HUSK_MEASUREMENT_UNIT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnitBran")).SelectedValue.ToString().Trim());
                    if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtRiceHusk")).Text.ToString().Trim()))
                        Fgprod.BY_PRODUCT_RICE_HUSK_QTY = 0;
                    else
                    Fgprod.BY_PRODUCT_RICE_HUSK_QTY = Convert.ToDecimal(((TextBox)row.FindControl("txtRiceHusk")).Text.ToString().Trim());
                    Fgprod.HUSK_MEASUREMENT_UNIT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlRiceHuskUnit")).SelectedValue.ToString().Trim());
                    Fgprod.MULTIPLICATION_FACTOR = 1;
                    Fgprod.FROM_TO_PLEDGE_ID = "2";
                    Fgprod.SET_USER = loggedUserID;
                    Fgprod.SET_DATE = DateTime.Now;

                    lst.Add(Fgprod);
                }
            }
            return lst;
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.ClearControls();
        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvFinishGood.Rows.Count == 1) return;

            DataTable newline = new DataTable("Order");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_BRAN_QTY", typeof(String));
            newline.Columns.Add("BRAN_MEASUREMENT_UNIT_ID", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_HUSK_QTY", typeof(String));
            newline.Columns.Add("HUSK_MEASUREMENT_UNIT_ID", typeof(String));

            if (gvFinishGood.Rows.Count == 0)
            {
                for (int i = 0; i <2; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "", "", "", "" });
            }

            foreach (GridViewRow gvrow in gvFinishGood.Rows)
            {
                DropDownList ddlGodown = (DropDownList)gvrow.FindControl("ddlFGGodown");
                DropDownList ddlProduct = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");

                TextBox txtRiceBran = (TextBox)gvrow.FindControl("txtRiceBran");
                DropDownList ddlUnitBran = (DropDownList)gvrow.FindControl("ddlUnitBran");
                TextBox txtRiceHusk = (TextBox)gvrow.FindControl("txtRiceHusk");
                DropDownList ddlRiceHuskUnit = (DropDownList)gvrow.FindControl("ddlRiceHuskUnit");

                newline.Rows.Add(new String[] { ddlGodown.SelectedValue, ddlProduct.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtRiceBran.Text, ddlUnitBran.SelectedValue, txtRiceHusk.Text, ddlRiceHuskUnit.SelectedValue });
            }
            newline.Rows[rowINdx].Delete();
            gvFinishGood.DataSource = newline;
            gvFinishGood.DataBind();

        }
        protected void gvPreOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlProduct");
                    string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                    this.FillProduct((int)PurchaseType.RICE, ddl);
                    ddl.SelectedValue = strhfProduct;

                    DropDownList ddlRiceUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                    this.FillMeasurementUnisDropDownList(ddlRiceUnit);
                    string strRiceUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                    ddlRiceUnit.SelectedValue = strRiceUnit;

                    DropDownList ddlHuskUnit = (DropDownList)e.Row.FindControl("ddlRiceHuskUnit");
                    this.FillMeasurementUnisDropDownList(ddlHuskUnit);
                    string strhfHuskUnit = ((HiddenField)e.Row.FindControl("hfUnitBran")).Value;
                    ddlHuskUnit.SelectedValue = strhfHuskUnit;

                    DropDownList ddlUnitBran = (DropDownList)e.Row.FindControl("ddlUnitBran");
                    this.FillMeasurementUnisDropDownList(ddlUnitBran);
                    string strhfUnitBran = ((HiddenField)e.Row.FindControl("hfUnitHusk")).Value;
                    ddlUnitBran.SelectedValue = strhfUnitBran;

                    DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlFGGodown");
                    this.FillUpDropDownlist(ddlGodown);
                    string strGodownId = ((HiddenField)e.Row.FindControl("hfFGGodown")).Value;
                    ddlGodown.SelectedValue = strGodownId;

                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        protected void ibtnAddMore_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Order");

            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_BRAN_QTY", typeof(String));
            newline.Columns.Add("BRAN_MEASUREMENT_UNIT_ID", typeof(String));
            newline.Columns.Add("BY_PRODUCT_RICE_HUSK_QTY", typeof(String));
            newline.Columns.Add("HUSK_MEASUREMENT_UNIT_ID", typeof(String));

            if (gvFinishGood.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "", "", "", "" });
            }

            foreach (GridViewRow gvrow in gvFinishGood.Rows)
            {
                DropDownList ddlGodown = (DropDownList)gvrow.FindControl("ddlFGGodown");
                DropDownList ddlProduct = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");

                TextBox txtRiceBran = (TextBox)gvrow.FindControl("txtRiceBran");
                DropDownList ddlUnitBran = (DropDownList)gvrow.FindControl("ddlUnitBran");
                TextBox txtRiceHusk = (TextBox)gvrow.FindControl("txtRiceHusk");
                DropDownList ddlRiceHuskUnit = (DropDownList)gvrow.FindControl("ddlRiceHuskUnit");

                newline.Rows.Add(new String[] { ddlGodown.SelectedValue, ddlProduct.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtRiceBran.Text, ddlUnitBran.SelectedValue, txtRiceHusk.Text, ddlRiceHuskUnit.SelectedValue });
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "", "", "", "" });

            gvFinishGood.DataSource = newline;
            gvFinishGood.DataBind();
        }
        protected void DeleteFG(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvReceiveFGGood.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteProduct(strCatID);
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {

        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvReceiveFGGood.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dtAttFrom = _masterProcurementService.GetProductionReportsData(strID);
                SessionManager.GetRPTSalesDataTable = dtAttFrom;
                ShowReport(this, this.GetType(), "RiceFGReceivedReports.rdlc", "INVSales", dtAttFrom);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(Control control, Type type, string sReportFileName, string sReportDataSourceName, List<FN_RPT_INV_RICE_PRODUCTIONS_Result> dtAttFrom)
        {
            string script = string.Format(@"window.open('OrderReportsViewer.aspx?ReportFileName={0}&&DataSourceName={1}','OrderReportsViewer',
                                            'width=1000,height=650,resizable=1,scrollbars=1');", sReportFileName, sReportDataSourceName);
            ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), script, true);
        }

       

        protected void rbtnSelect_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton imbtn = sender as RadioButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                if (((CheckBox)gvProduction.Rows[rowINdx].Cells[0].FindControl("rbtnSelect")).Checked)
                {
                    string strID = Convert.ToString(((HiddenField)gvProduction.Rows[rowINdx].Cells[0].FindControl("hfInvId")).Value.ToString());
                    List<INV_INVENTORY_DETAILS> dtAttFrom = _finishGoodsRepository.GetAllFinishGoodsReceivedByID(Convert.ToInt64(strID));
                    gvPartialGoods.DataSource = dtAttFrom;
                    gvPartialGoods.DataBind();
                    hiID.Value = strID.ToString();
                    SetFocus(((CheckBox)gvProduction.Rows[rowINdx].Cells[0].FindControl("rbtnSelect")));
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

      
    }
}