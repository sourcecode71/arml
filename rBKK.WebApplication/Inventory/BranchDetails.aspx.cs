﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.WebApplication.Common;
using rBKK.Entities;
using Resources;

namespace rBKK.WebApplication.Inventory
{
    public partial class BranchDetails : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddBankBranch).ToString());

                if (!IsPostBack)
                {
                    this.loadPageData();
                    this.LoadBankInfo();
                }
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }
        private void LoadBankInfo()
        {
            try
            {
                try
                {
                    var dATA = (_masterInventoryServise.GetAllCoreBank()).ToList();
                    ddlBank.DataSource = dATA;
                    ddlBank.DataValueField = "BANK_ID";
                    ddlBank.DataTextField = "BANK_NAME";
                    ddlBank.DataBind();
                    ddlBank.Items.Insert(0, new ListItem("<<<Select>>>", "0"));
                }
                catch (Exception ex)
                {

                    throw ex;
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        private void loadPageData()
        {
            try
            {
                var dATA = (_masterInventoryServise.GetAllBranchDeatials()).ToList();
                gvBranch.DataSource = dATA;
                gvBranch.DataBind();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void ResetAll()
        {
            try
            {
                txtAdress.Text = string.Empty;
                txtBranchName.Text = String.Empty;
                txtContact.Text = String.Empty;
                txtEmail.Text = string.Empty;
                txtFax.Text = string.Empty;
                txtPhone.Text = string.Empty;
                btnSave.Text = "Save";
                ddlBank.SelectedIndex = 0;
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);

            }

            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                CORE_BANK_BRANCH core_Branch = new CORE_BANK_BRANCH();
                if (btnSave.Text == "Save")
                {
                   
                    core_Branch.BRANCH_NAME = txtBranchName.Text;
                    core_Branch.BANK_ID = Convert.ToInt32(ddlBank.SelectedValue);
                    core_Branch.ADDRESS = txtAdress.Text;
                    core_Branch.CONTACT_PERSON = txtContact.Text;
                    core_Branch.PHONE = txtPhone.Text;
                    core_Branch.FAX = txtFax.Text;
                    core_Branch.EMAIL = txtEmail.Text;
                    core_Branch.SET_USER = loggedUserID;
                    core_Branch.SET_DATE = DateTime.Now;

                    _masterInventoryServise.AddCoreBranchInfo(core_Branch, OperationType.SAVE.ToString());
                }
                else
                {
                    core_Branch.BRANCH_ID = Convert.ToInt32(hfID.Value);
                    core_Branch.BRANCH_NAME = txtBranchName.Text;
                    core_Branch.BANK_ID = Convert.ToInt32(ddlBank.SelectedValue);
                    core_Branch.ADDRESS = txtAdress.Text;
                    core_Branch.CONTACT_PERSON = txtContact.Text;
                    core_Branch.PHONE = txtPhone.Text;
                    core_Branch.FAX = txtFax.Text;
                    core_Branch.EMAIL = txtEmail.Text;
                    _masterInventoryServise.AddCoreBranchInfo(core_Branch, OperationType.UPDATE.ToString());
                    btnSave.Text = "Save";

                }
                 ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);

                this.loadPageData();
                this.ResetAll();

            }
            catch (Exception ex)
            {

            }
        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblID")).Text.ToString());
                // CORE_BANK Bank = _masterInventoryServise.GetCoreBankById(strID);
                txtBranchName.Text = ((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblBranch")).Text.ToString();
                txtAdress.Text = ((HiddenField)gvBranch.Rows[rowINdx].Cells[0].FindControl("hfAddress")).Value.ToString();
                txtContact.Text = ((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblContact")).Text.ToString();
                txtEmail.Text = ((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblEmail")).Text.ToString();
                txtFax.Text = ((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblFax")).Text.ToString();
                txtPhone.Text = ((HiddenField)gvBranch.Rows[rowINdx].Cells[0].FindControl("hfPHONE")).Value.ToString();
                ddlBank.SelectedValue = ((HiddenField)gvBranch.Rows[rowINdx].Cells[0].FindControl("hfBankId")).Value.ToString();
                btnSave.Text = "Update";
                hfID.Value = strID;
                this.loadPageData();
                
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvBranch.Rows[rowINdx].Cells[0].FindControl("lblID")).Text.ToString());
                _masterInventoryServise.DeleteCoreBranchInfo(strCatID);
                this.loadPageData();

            }
            catch (Exception ex)
            {

                 ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
           this.ResetAll();
        }
    }
}