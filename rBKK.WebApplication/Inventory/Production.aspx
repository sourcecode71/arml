﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Production.aspx.cs" Inherits="rBKK.WebApplication.Inventory.Production" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
    function DatePicker() {
        $(function () {
            $("#<%=txtDate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });
        });
    }
    function pageLoad() {
        if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
            DatePicker();
        }
    }
    DatePicker();


    function DropDownListChange(ddl, txtbox, txtRate,productPrice) {
        var product = document.getElementById(ddl);
        var txt = document.getElementById(txtbox);
        var txtRates = document.getElementById(txtRate);

        if (productPrice == "2") {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "../WebServices/CommonWebService.asmx/GetProductPriceById",
                data: "{'ProductId':'" + product.options[product.selectedIndex].value + "'}",
                dataType: "json",
                success: function (data) {
                    txtRates.value = data.d;
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }
        txt.value = "0";
    }

    function CompareCurrentStock(godownId, productId, unitId, quantity) {

        var godown = document.getElementById(godownId);
        var product = document.getElementById(productId);
        var units = document.getElementById(unitId);
        var qty = document.getElementById(quantity);

        if (qty.value == "0")
            return;

        if (godown.options[godown.selectedIndex].value == "0") {
            alert("Please select a godown.")
            godown.focus();
            qty.value = "0";
            return;
        }
        if (product.options[product.selectedIndex].value == "0") {
            alert("Please select a product.")
            product.focus();
            qty.value = "0";
            return;
        }
        if (units.options[units.selectedIndex].value == "0") {
            alert("Please select a Unit.")
            units.focus();
            qty.value = "0";
            return;
        }
        

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "../WebServices/CommonWebService.asmx/CompareCurrentStock",
            data: "{'GodownId':'" + godown.options[godown.selectedIndex].value + "','ProductId':'" + product.options[product.selectedIndex].value + "','UnitId':'" + units.options[units.selectedIndex].value + "','Quantity':'" + qty.value + "'}",
            dataType: "json",
            success: function (data) {
                if (data.d != "0") {
                    alert(data.d);
                    qty.value = "0";
                    qty.focus();
                }
            },
            error: function (result) {
                alert("Error");
            }
        });

        //           alert("g: " + godown.options[godown.selectedIndex].value + " p: " + product.options[product.selectedIndex].value + " u: "
        //           + units.options[units.selectedIndex].value + " q: " + qty.value);

        return false;
    }


</script>
<script type="text/javascript">
    $(document).ready(function () {
        SearchText();
    });

    function SearchText() 
    {
        $(".autosuggest").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    //url: "http://localhost:4345/WebServices/CommonWebService.asmx/GetAutoCompleteAccData",
                    url: "../WebServices/CommonWebService.asmx/GetAutoCompleteDirectClientList",
                    data: "{'username':'" + document.getElementById('<%=txtDirectClientList.ClientID%>').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        response(data.d);
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
        });
    }
</script>
<script type="text/javascript">



    function DiscountCalculation(txt1, txt2) {
        var txt11 = document.getElementById(txt1);
        var txt22 = document.getElementById(txt2);
        if (txt11.value > 0)
            txt22.value = "0";

        return false;
    }
    function filter(phrase, _id) {
        var words = phrase.value.toLowerCase().split(" ");
        var table = document.getElementById(_id);
        var ele;
        for (var r = 0; r < table.rows.length; r++) {
            ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
            var displayStyle = 'none';
            for (var i = 0; i < words.length; i++) {
                if (ele.toLowerCase().indexOf(words[i]) >= 0)
                    displayStyle = '';
                else {
                    displayStyle = 'none';
                    break;
                }
            }
            table.rows[r].style.display = displayStyle;
        }
    }
    function GotFocus(txtSearch) {
        var text = "Search...";
        var words = txtSearch.value;
        if (words == text)
            txtSearch.value = '';
    }

    function LostFocus(txtSearch) {
        var text = "Search...";
        var words = txtSearch.value.trim();
        if (words == '')
            txtSearch.value = text;
    }

    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "");
    }
</script>

    <style type="text/css">
        .pad
        {
            padding-left:4px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="PageDetial"> 
    <fieldset class="flDeatialView" style="width:90%">
        <legend class="lgView"> Production info </legend>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table class="bgdisplay" width="100%">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="hiID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:150px" >
                            <span>Transaction Type </span></td>
                        <td style="width:200px" colspan="2">
                            <asp:DropDownList ID="ddlReqisitionType" runat="server" 
                                SkinID="normalDropDownList" Width="210px" AutoPostBack="True" 
                                onselectedindexchanged="ddlReqisitionType_SelectedIndexChanged" 
                                Enabled="False">
                            </asp:DropDownList>
                        </td>
                        <td valign="top" rowspan="8">
                            <div id="divLoanInfo" runat="server" style="width:382px; height:96px">
                            </div> 
                        </td>
                    </tr>
                    <tr>
                        <td style="width:150px">
                            <span>Issue no </span>
                        </td>
                        <td colspan="2" style="width:200px">
                            <asp:TextBox ID="txtChallanNo" runat="server" SkinID="normalTextBox" 
                                Width="205px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server" 
                                ControlToValidate="txtChallanNo" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <span> Date</span> </td>
                        <td style="width: 315px" colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                             <asp:TextBox ID="txtDate" runat="server" Width="100px" SkinID="normalTextBox"></asp:TextBox>
                            <span >
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server" 
                                ControlToValidate="txtDate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            (MM/DD/YYYY)</span>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                           
                            
                            </td>
                    </tr>
                    <tr>
                        <td >
                            <span>Comments</span></td>
                        <td style="width: 315px" colspan="2">
                            <asp:TextBox ID="txtRemarks" runat="server" SkinID="normalTextBox" 
                                TextMode="MultiLine" Width="205px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBank" runat="server" Text="Bank"></asp:Label>
                        </td>
                        <td colspan="2" style="width: 315px">
                            <asp:DropDownList ID="ddlBank" runat="server" AutoPostBack="True" Height="21px" 
                                onselectedindexchanged="ddlBank_SelectedIndexChanged" 
                                SkinID="normalDropDownList" Width="210px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblType" runat="server">Loan Account</asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlLoan" runat="server" AutoPostBack="True" 
                                Enabled="False" onselectedindexchanged="ddlLoan_SelectedIndexChanged" 
                                SkinID="normalDropDownList" Width="210px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblClient" runat="server" Text="Client List" Visible="False"></asp:Label>
                        </td>
                        <td colspan="2" >
                            <asp:DropDownList ID="ddlSalesClient" runat="server" 
                                SkinID="normalDropDownList" Visible="False" Width="210px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtDirectClientList" runat="server" SkinID="normalTextBox" 
                                Width="205px"  CssClass="autosuggest" Wrap="False" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblCashCredit" runat="server" Text="Cash/Credit" Visible="False"></asp:Label>
                        </td>
                        <td colspan="2" >
                            <asp:RadioButtonList ID="rbtnCashCredit" runat="server" 
                                RepeatDirection="Horizontal" RepeatLayout="Flow" Visible="False">
                                <asp:ListItem Selected="True" Value="1">Cash</asp:ListItem>
                                <asp:ListItem Value="0">Credit</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:TextBox ID="txtCashAmount" runat="server" MaxLength="11" 
                                SkinID="normalTextBox" Width="89px" >0</asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValid5" runat="server" 
                                                ControlToValidate="txtCashAmount" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValid133" runat="server" 
                                                    ControlToValidate="txtCashAmount" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                        <div id="divProductions" runat="server">
                            <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333" GridLines="None" 
                                onrowdatabound="gvProduct_RowDataBound" style="font-size: 9pt" 
                                Width="100%">
                                <HeaderStyle CssClass="dataScheduleHeader" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                <asp:TemplateField HeaderText="From Godown">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlFromGodown" runat="server" 
                                                SkinID="normalGridDropDownList" Width="100%">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="dhfFromGodown" runat="server" 
                                                Value='<%# Bind("GODOWN_ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Godown">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlToGodown" runat="server" 
                                                SkinID="normalGridDropDownList" Width="100%">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfToGodown" runat="server" 
                                                Value='<%# Bind("GODOWN_ID2") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlProduct" runat="server" 
                                                SkinID="normalGridDropDownList" Width="100%">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfProduct" runat="server" 
                                                Value='<%# Bind("PRODUCT_ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Unit">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlUnit" runat="server" SkinID="normalGridDropDownList" 
                                                Width="100px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfMeasurementUnit" runat="server" 
                                                Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQuantity" runat="server" SkinID="normalGridTextBox" 
                                                Text='<%# Bind("QUANTITY") %>' Width="81px" MaxLength="8"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" 
                                                    ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Rate">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRate" runat="server" Width="80px" 
                                                Text='<%# Bind("RATE") %>' SkinID="normalGridTextBox" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server" 
                                           ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red" 
                                            ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server" 
                                        ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" 
                                                ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                onclick="imgDelete_Click" CausesValidation="False" />
                                            <%--<asp:HiddenField ID="hfOrdID" runat="server" 
                                                Value='<%# Bind("INV_DETAILS_ID") %>' />--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:ImageButton ID="ibtnAddMore" runat="server" Height="20px" 
                                ImageUrl="~/App_Themes/Green/Images/addMore.jpg" onclick="ibtnAddMore_Click" 
                                Visible="true" Width="21px" CausesValidation="False" />
                        </div>
                        </td>

                    </tr>
                     <tr>
                        <td colspan="4">
                        <div id="divSales" runat="server">
                            <asp:GridView ID="gvSales" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333" GridLines="None" 
                                onrowdatabound="gvSales_RowDataBound" style="font-size: 9pt" 
                                Width="80%">
                                <HeaderStyle CssClass="dataScheduleHeader" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                <asp:TemplateField HeaderText="Godown">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlGodown" runat="server" 
                                                SkinID="normalGridDropDownList" Width="100%">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="dhfGodown" runat="server" 
                                                Value='<%# Bind("GODOWN_ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Product">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlProductSales" runat="server" 
                                                SkinID="normalGridDropDownList" Width="100%">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfProductSales" runat="server" 
                                                Value='<%# Bind("PRODUCT_ID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlUnitSales" runat="server" SkinID="normalGridDropDownList" 
                                                Width="100px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfMeasurementUnitSales" runat="server" 
                                                Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQuantitySales" runat="server" SkinID="normalGridTextBox" 
                                                Text='<%# Bind("QUANTITY") %>' Width="65px" MaxLength="8"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidators3" runat="server" 
                                                ControlToValidate="txtQuantitySales" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidators132" runat="server" 
                                                    ControlToValidate="txtQuantitySales" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Rate">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRateSales" runat="server" Width="65px" CssClass="pad"
                                                Text='<%# Bind("RATE") %>' SkinID="normalGridTextBox" MaxLength="10" 
                                                BackColor="#DEDEDE" BorderStyle="None"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Discount">
                                            <ItemTemplate>
                                            <div> 
                                                <asp:TextBox ID="txtDiscunt" runat="server" Width="50px" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("DISCOUNT_AMOUNT") %>' 
                                                    MaxLength="7"></asp:TextBox>TK.
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                                ControlToValidate="txtDiscunt" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" 
                                                    ControlToValidate="txtDiscunt" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    
                                                <asp:TextBox ID="txtPC" runat="server" Width="50px" SkinID="normalGridTextBox" 
                                                    Text='<%# Bind("DISCOUNT_PC") %>' MaxLength="7"></asp:TextBox> %
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidators5" runat="server" 
                                                ControlToValidate="txtPC" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidators133" runat="server" 
                                                    ControlToValidate="txtPC" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="HiddenGridColumn" />
                                            <ItemStyle CssClass="HiddenGridColumn" />
                                        </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnDeleteSales" runat="server" 
                                                ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                onclick="ibtnDeleteSales_Click" CausesValidation="False" />
                                            <%--<asp:HiddenField ID="hfOrdID" runat="server" 
                                                Value='<%# Bind("INV_DETAILS_ID") %>' />--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:ImageButton ID="ibtnAddMoreSales" runat="server" Height="20px" 
                                ImageUrl="~/App_Themes/Green/Images/addMore.jpg" onclick="ibtnAddMoreSales_Click" 
                                Visible="true" Width="21px" CausesValidation="False" />
                        </div>
                        </td>

                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                onclick="btnSave_Click" Text="Save" Width="70px" />
                            <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                        </td>
                        <td align="center" colspan="2">
                            &nbsp;</td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </fieldset>
</div>

<div class="PageDetial">

 <fieldset class="flDeatialView" style="width:90%" >
 <legend class="lgView"> Previous Transaction list </legend>
        
         <input id="htxtSearch" onkeyup="filter(this, 'ctl00_MainContent_gvChallanList', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none" />
            
         <asp:Panel runat="server" ID="pnlContainer" ScrollBars="Auto" Height="200px" Width="100%">
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <ContentTemplate>
             <asp:GridView ID="gvChallanList" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%" 
                     onrowdatabound="gvChallanList_RowDataBound">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="ID">
                        
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("INV_MASTER_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Transaction Type">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblType" runat="server" 
                                 Text='<%# Bind("REQUISITION_TYPE_NAME") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="CHALLAN_NO" HeaderText="Issue No" />
                     <asp:BoundField DataField="CHALLAN_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}"/>
                     <asp:BoundField DataField="REMARKS" HeaderText="Remarks" />
                    
                     <asp:BoundField DataField="SALES_PAPER_UPLODED_YN" 
                         HeaderText="Sales Paper Uploded">
                     <HeaderStyle Width="110px" />
                     <ItemStyle HorizontalAlign="Center" />
                     </asp:BoundField>
                    
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField>
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:ImageButton ID="ImageButton2" runat="server" Height="17px" 
                                 ImageUrl="~/App_Themes/Green/Images/print-button.png" 
                                 onclick="ImageButton2_Click1" CausesValidation="False" />
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
             </ContentTemplate>
             </asp:UpdatePanel>
             </asp:Panel>
 </fieldset>

</div>
</asp:Content>
