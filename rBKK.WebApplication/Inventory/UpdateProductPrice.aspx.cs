﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using Resources;
using rBKK.Entities;

namespace rBKK.WebApplication.Inventory
{
    public partial class UpdateProductPrice : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                 this.PagedataBound();
            }
        }
        private void PagedataBound()
        {
            this.BindDropdownlist(ddlCategory, "Category");
           
        }
        private void BindDropdownlist(DropDownList ddl, string ddlType)
        {
            try
            {
                ddl.Items.Clear();
                bool selectDefault = true;
                switch (ddlType)
                {
                    case "Category":
                        {
                            var dATA = (_masterInventoryServise.GetAllCategory()).ToList();
                            ddl.DataValueField = "CATEGORY_ID";
                            ddl.DataTextField = "CATEGORY_NAME";
                            ddl.DataSource = dATA;
                            break;
                        }
                
                }

                if (selectDefault)
                {
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                    ddl.SelectedValue = "0";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindGridView(GridView gv, string gvType)
        {
            try
            {
                switch (gvType)
                {
                    case "ProdList":
                        {
                            string strcategory = ddlCategory.SelectedValue;
                            var dATA = (_masterInventoryServise.GetProductByCatID(strcategory)).ToList();
                            gv.DataSource = dATA;
                            gv.DataBind();
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<INV_PRODUCT> prodInfos = new List<INV_PRODUCT>();
                prodInfos = this.GetProductInfo();
                _masterInventoryServise.UpdateProductPrice(prodInfos);
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }

        }

        private List<INV_PRODUCT> GetProductInfo()
        {
            try
            {
                List<INV_PRODUCT> prodInfos = new List<INV_PRODUCT>();

                foreach (GridViewRow gvr in gvProductPrice.Rows)
                {
                    INV_PRODUCT prodInfo = new INV_PRODUCT();

                    prodInfo.PRODUCT_ID =Convert.ToInt32( ((Label)gvr.FindControl("lblProdId")).Text.ToString());
                    prodInfo.UpdatePrice = Convert.ToDecimal(((TextBox)gvr.FindControl("txtRate")).Text.ToString());
                    prodInfos.Add(prodInfo);
                }

                return prodInfos;
               
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.BindGridView(gvProductPrice, "ProdList");
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}