﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FactoryGodownMapping.aspx.cs" Inherits="rBKK.WebApplication.Inventory.FactoryGodownMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="MiniPage">
        <fieldset class="flView">
            <legend class="lgView">Factory Godown Mapping </legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay">
                        <tr>
                            <td colspan="3">
                                <asp:HiddenField ID="hfID" runat="server" />
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="width:500px">
                                <span>Factory </span></td>
                            <td style="width:500px">
                                <asp:DropDownList ID="ddlFactory" runat="server" Width="250px" SkinID="normalDropDownList">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="ddlFactory" ErrorMessage="*" ForeColor="Red" 
                                    InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width:500px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                               <span>Godown</span></td>
                            <td>
                                <asp:DropDownList ID="ddlGodown" runat="server" Width="250px" SkinID="normalDropDownList">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="ddlGodown" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3">
                                &nbsp;&nbsp; &nbsp;
                                &nbsp;
                                <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                                &nbsp;
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>
 <div class="MiniDisplay">

 <fieldset class="flView">
 <legend class="lgView">Mapping List</legend> 
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
  <ContentTemplate>
             <asp:GridView ID="gvFactoryGodown" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="ID">
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server" 
                                 Text='<%# Bind("FACTORY_GODOWN_ID") %>'></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("FACTORY_GODOWN_ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="factoryname" HeaderText="Factory" />
                     <asp:BoundField DataField="godownname" HeaderText="Godown" />
                     <asp:TemplateField ShowHeader="False" Visible="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
        
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
