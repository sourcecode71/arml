﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Security;
using rBKK.Entities;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class FactoryGodownMapping : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.FactoryGodownMapping).ToString());

                if (!IsPostBack)
                {
                    this.LoadPageData();
                    this.GetGridData();
                }
            }
            catch (Exception ex)
            {
                
            ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }
        private void LoadPageData()
        {
            try
            {
                this.BindDropdownlist(ddlFactory, "Factory");
                this.BindDropdownlist(ddlGodown, "Godown");
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        private void GetGridData()
        {
            try
            {
                var factoryGodown = (_masterInventoryServise.GetInvFactoryGodown()).ToList();
                gvFactoryGodown.DataSource = factoryGodown;
                gvFactoryGodown.DataBind();

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);

            }
        }
        private void BindDropdownlist(DropDownList ddl, string ddlType)
        {
            try
            {


                ddl.Items.Clear();
                bool selectDefault = true;
                switch (ddlType)
                {
                    case "Factory":
                        {
                            var dATA = (_masterInventoryServise.GetAllFactories()).ToList();
                            ddl.DataValueField = "FACTORY_ID";
                            ddl.DataTextField = "NAME";
                            ddl.DataSource = dATA;
                            break;
                        }

                    case "Godown":
                        {
                            var dATA = (_masterInventoryServise.GetAllInvGodownForFCTMapping()).ToList();
                            ddl.DataValueField = "GODOWN_ID";
                            ddl.DataTextField = "NAME";
                            ddl.DataSource = dATA;
                            break;
                        }

                }

                if (selectDefault)
                {

                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("<<<Select>>>", "0"));
                    ddl.SelectedValue = "0";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                INV_FACTORY_GODOWN invFactoryGodown = new INV_FACTORY_GODOWN();
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();
                string TranType;
                if (btnSave.Text == "Save")
                {
                    TranType = "Save";
                    invFactoryGodown.FACTORY_ID = Convert.ToInt32(ddlFactory.SelectedValue);
                    invFactoryGodown.GODOWN_ID = Convert.ToInt32(ddlGodown.SelectedValue);
                    invFactoryGodown.COMPANY_ID = "1";
                    invFactoryGodown.SET_DATE = DateTime.Now;
                    invFactoryGodown.SET_USER = loggedUserID;
                    _masterInventoryServise.AddInvFactoryGodown(invFactoryGodown, TranType);
                    this.BindDropdownlist(ddlGodown, "Godown");

                }
                else
                {
                    TranType = "Update";
                    invFactoryGodown.FACTORY_ID = Convert.ToInt32(ddlFactory.SelectedValue);
                    invFactoryGodown.GODOWN_ID = Convert.ToInt32(ddlGodown.SelectedValue);
                    invFactoryGodown.SET_USER = loggedUserID;
                    _masterInventoryServise.AddInvFactoryGodown(invFactoryGodown, TranType);

                }
                this.GetGridData();
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
               

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgbtn = sender as ImageButton;
                int rowindex = ((GridViewRow)imgbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvFactoryGodown.Rows[rowindex].Cells[0].FindControl("lblID")).Text);
                hfID.Value = strID;

                INV_FACTORY_GODOWN invFCTGDW = _masterInventoryServise.GetInvFactoryGodownById(strID);
                ddlFactory.SelectedValue =Convert.ToString( invFCTGDW.FACTORY_ID);
                ddlGodown.SelectedValue =Convert.ToString( invFCTGDW.GODOWN_ID);


            }
            catch (Exception ex)
            {
                
            ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imgbtn = sender as ImageButton;
                int rowindex = ((GridViewRow)imgbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvFactoryGodown.Rows[rowindex].Cells[0].FindControl("lblID")).Text);
                _masterInventoryServise.DeleteInvFactoryGodown(strID);
                this.LoadPageData();
                this.GetGridData();
            }
            catch (Exception ex)
            {
                
              ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlGodown.SelectedIndex = 0;
            ddlFactory.SelectedIndex = 0;
        }
    }
}