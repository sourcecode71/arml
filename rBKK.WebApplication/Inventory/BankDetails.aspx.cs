﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.Entities;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Inventory
{
    public partial class BankDetails : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddBank).ToString());

                if (!IsPostBack)
                {
                    this.loadPageData();
                }
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void loadPageData()
        {

            try
            {
                var dATA = (_masterInventoryServise.GetAllCoreBank()).ToList();
                gvBank.DataSource = dATA;
                gvBank.DataBind();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        private void ResetAll()
        {
            try
            {
                txtname.Text = string.Empty;
                txtShortName.Text = string.Empty;
                txtwebAddress.Text = string.Empty;
                txtAddress.Text = string.Empty;
                btnSave.Text = "Save";
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.EMPTY_MESSAGE, Common.MessageType.Info);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                CORE_BANK core_Bank = new CORE_BANK();
                if (btnSave.Text == "Save")
                {
                    core_Bank.BANK_NAME = txtname.Text;
                    core_Bank.SYMBOLIC_NAME = txtShortName.Text;
                    core_Bank.ADDRESS = txtAddress.Text;
                    core_Bank.WEB_ADDRESS = txtwebAddress.Text;
                    core_Bank.SET_USER = loggedUserID;
                    core_Bank.SET_DATE = DateTime.Now;
                    _masterInventoryServise.AddCoreBankinfo(core_Bank, OperationType.SAVE.ToString());
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    core_Bank.BANK_ID =Convert.ToInt32( hfID.Value);
                    core_Bank.BANK_NAME = txtname.Text;
                    core_Bank.SYMBOLIC_NAME = txtShortName.Text;
                    core_Bank.ADDRESS = txtAddress.Text;
                    core_Bank.WEB_ADDRESS = txtwebAddress.Text;
                    _masterInventoryServise.AddCoreBankinfo(core_Bank, OperationType.UPDATE.ToString());
                    btnSave.Text = "Save";

                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                this.loadPageData();
                this.ResetAll();

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.ResetAll();
        }

        protected void img_editFactory(object sender, ImageClickEventArgs e)
        {

            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                txtname.Text = ((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblname")).Text.ToString();
                txtShortName.Text = ((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblShortName")).Text.ToString();
                txtwebAddress.Text = ((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblWebAddress")).Text.ToString();
                txtAddress.Text = ((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblAddress")).Text.ToString();
                btnSave.Text = "Update";
                hfID.Value = strID;

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strCatID = Convert.ToString(((Label)gvBank.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterInventoryServise.DeleteCoreBankInfo(strCatID);
                this.loadPageData();
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_DELETED_SUCCESS, Common.MessageType.Info);
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
    }
}