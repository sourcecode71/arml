﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateProductPrice.aspx.cs" Inherits="rBKK.WebApplication.Inventory.UpdateProductPrice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="MiniPage">
<fieldset class="flView" style="width:65%">
   <legend class="lgView">Update Price </legend>

         <table class="bgdisplay" width="100%" >

           <tr>
             <td><span> category</span> </td> <td> 
            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" 
            onselectedindexchanged="ddlCategory_SelectedIndexChanged" Width="240px" 
            SkinID="normalDropDownList">
            </asp:DropDownList>
           </td>
        </tr>

    <tr>
    <td> &nbsp;</td> <td> 
        &nbsp;</td>
    </tr>
        <tr>
            <td colspan="3">
                                                            <asp:Panel ID="Panel1" runat="server" Height="250px" ScrollBars="Vertical">
                                                                <asp:GridView ID="gvProductPrice" 
                    runat="server" AutoGenerateColumns="False" 
                                                                
    GridLines="None" Width="100%">
                                                                    <HeaderStyle CssClass="dataScheduleHeader" />
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="ID">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblProdId" runat="server" Text='<%# Bind("PRODUCT_ID") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle CssClass="HiddenGridColumn" />
                                                                            <ItemStyle CssClass="HiddenGridColumn" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Product">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="300px" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Unit">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="150px" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Rate">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtRate" Width="60px" runat="server" Text='<%# Bind("UpdatePrice") %>' 
                                                                                SkinID="normalTextBox"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="50px" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td style="text-align: center">
                                                        <asp:Button ID="btnUpdate" 
                    runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" 
                    Text="Update" Width="70px" />
                                                        </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

</fieldset>
</div>
</asp:Content>
