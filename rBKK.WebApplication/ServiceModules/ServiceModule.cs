﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject.Modules;
using rBKK.WebApplication.Models;
using rBKK.Core.Security;
using rBKK.Infrastructure.Security;
using rBKK.Infrastructure.Inventory;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.Infrastructure.Procurement;

namespace rBKK.WebApplication.ServiceModules
{
    public class ServiceModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IModels>().To<ModelsA>();
            Bind<IUserService>().To<UserService>();
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IMasterInventoryRepository>().To<MasterInventoryRepository>();
            Bind<IMasterInventoryServise>().To<MasterInventoryServise>();
            Bind<IMasterProcurementService>().To<MasterProcurementService>();
            Bind<IMasterProcurementRepository>().To<MasterProcurementRepository>();
            Bind<IFinishGoodsService>().To<FinishGoodsService>();
            Bind<IFinishGoodsRepository>().To<FinishGoodsRepository>();
        }

    }
}