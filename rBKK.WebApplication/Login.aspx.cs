﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Security;
using Resources;
using rBKK.Entities;

namespace rBKK.WebApplication
{
    public partial class Login : System.Web.UI.Page
    {
        [Inject]
        public IUserService _userService { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {

            //var url = Request.Url.AbsoluteUri;
            //Response.Write(url);

            if (!Page.IsCallback)
            {
                //txtUserName.Attributes.Add("autocomplete", "off");
                //this.FillUserGroupsList();
            }
        }

        private void FillUserGroupsList()
        {
            //ddlUserGroups.DataSource = _userService.GetAllUserGroups(); 
            //ddlUserGroups.DataTextField = "GROUP_NAME";
            //ddlUserGroups.DataValueField = "GROUP_ID";
            //ddlUserGroups.DataBind();
            //ddlUserGroups.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, String.Empty));
        }

        protected void btnSign_Click(object sender, EventArgs e)
        {

            try
            {
                //if (_userService.CheckUserAuthentication(txtUserName.Text.ToString(), txtPassword.Text.ToString()) == "1")
                if (_userService.CheckUserAuthentication(txtUserName.Text.ToString(), txtPassword.Text.ToString()) == "1")
                {
                    string strGuid = Guid.NewGuid().ToString() + txtUserName.Text.ToString();
                    string strUserId = txtUserName.Text.ToString();
                    string strPassword = txtPassword.Text.ToString();
                    string strHostIP = Request.UserHostAddress;
                    _userService.SaveUserSession(strGuid, strUserId, strPassword, strHostIP);
                    SEC_USER objSecUser = _userService.GetUserInfoById(strUserId);
                    HttpContext.Current.Session.Add("USER_ID", strUserId);
                    HttpContext.Current.Session.Add("SEC_USER", objSecUser);

                    Response.Redirect("~/Home.aspx", true);//H@n123    
                }
                else
                {
                    lblLoginMessage.Text = Localization.LOGIN_ERROR_MSG.ToString();
                    txtPassword.Text = string.Empty;
                }

            }
            catch (Exception ex)
            {
                //((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {

        }
    }
}