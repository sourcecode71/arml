﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;
using rBKK.WebApplication.Common;
using System.Globalization;
using rBKK.WebApplication.Reports;


namespace rBKK.WebApplication.Procurement
{
   

    public partial class GeneratePurchaseOrder : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.GeneratePurchaseOrder).ToString());
            if (!IsPostBack)
            {
                AllControlsVisibleYN(false);
                rbtMachinePurchageType.Visible = false;
                lblMachinePurchase.Visible = false;
                this.FillFactoryDropDownList();
                this.FillPurchaseTypeDropDownList();
                this.GetRequisitionsList();
                txtDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtDate.Attributes.Add("onblur", "return validateDate(this)");
                txtHatMoney.Text = "0";
                this.SetNullToSessionVariables();
            }
      }


        private void SetNullToSessionVariables()
        {
            Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = null;
            Session[SessionVariable.INV_GODOWN.ToString()] = null;
            Session[SessionVariable.MACHINE_TYPE.ToString()] = null;
            Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            Session[SessionVariable.PUR_PADDY.ToString()] = null;
            Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = null;
        }
        private void FillPurchaseTypeDropDownList()
        {
            try
            {
                List<PUR_REQUISITION_TYPE> lst = _masterProcurementService.GetPurchaseType();
                ddlReqisitionType.DataSource = lst.Where(d => d.SPECIAL_NODE_YN == "0");
                ddlReqisitionType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlReqisitionType.DataValueField = "REQUISITION_TYPE_ID";
                ddlReqisitionType.DataBind();
                ddlReqisitionType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetRequisitionsList()
        {
            try
            {
                gvRequisitionList.DataSource = _masterProcurementService.GetRequisitionsList();
                gvRequisitionList.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillFactoryDropDownList()
        {
            try
            {
                ddlFactoryList.DataSource = _masterInventoryServise.GetAllFactories();
                ddlFactoryList.DataTextField = "NAME";
                ddlFactoryList.DataValueField = "FACTORY_ID";
                ddlFactoryList.DataBind();
                ddlFactoryList.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillPaddyPurchaseTypeDropDownList(string requisitionType)
        {
            try
            {
                if (requisitionType == "0") return;
                lblPaddyPurchaseType.Text =ddlReqisitionType.SelectedItem.Text+ " Purchase Type";
                ddlPaddyPurchaseType.DataSource = _masterProcurementService.GetVendorTypeByGenericId(requisitionType);
                ddlPaddyPurchaseType.DataTextField = "VENDOR_TYPE";
                ddlPaddyPurchaseType.DataValueField = "VENDOR_TYPE_ID";
                ddlPaddyPurchaseType.DataBind();
                ddlPaddyPurchaseType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProductDropDownList(string ProductCategoryID="1",DropDownList ddl=null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                    HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProductByGenericId(string genericId = "1", DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetProductByGenericId(genericId);
                    //IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                    HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillPartsDropDownList(string ProductCategoryID = "1", DropDownList ddl = null)
        {
            try
            {

                IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                ddl.DataSource = lst;
                ddl.DataTextField = "PRODUCT_NAME";
                ddl.DataValueField = "PRODUCT_ID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillConsumptionTypeDropDownList(DropDownList ddl=null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] != null)
                {
                    List<PUR_CONSUMPTION_TYPE> lst = (List<PUR_CONSUMPTION_TYPE>)HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                    ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else 
                {
                    List<PUR_CONSUMPTION_TYPE> lst = _masterProcurementService.GetAllConsumptionTypes();
                    HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                    ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillMeasurementUnisDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] != null)
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = (List<INV_UNIT_OF_MEASUREMENT>)HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = _masterInventoryServise.GetAllUNIT_OF_MEASUREMENT();
                    HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillMachineTypeDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()] != null)
                {
                    List<INV_PRODUCT_CATEGORY> lst = (List<INV_PRODUCT_CATEGORY>)HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CATEGORY_NAME";
                    ddl.DataValueField = "CATEGORY_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_PRODUCT_CATEGORY> lst = _masterInventoryServise.GetProductCategoryByParentId("6");
                    HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CATEGORY_NAME";
                    ddl.DataValueField = "CATEGORY_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadBag()
        {
            DataTable newline = new DataTable("Bagqty");

            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));

            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "" });
            }

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }
        private void LoadNewPreOrder()
        {
 	        DataTable newline = new DataTable("Order");

            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 1;i++ )
                    newline.Rows.Add(new String[] { "", "", "", "", "0", "0" });
            }

             gvPreOrder.DataSource = newline;
             gvPreOrder.DataBind();
        }
        private void LoadNewMachine()
        {
            DataTable newline = new DataTable("Machine");

            newline.Columns.Add("MACHINE_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("MODEL", typeof(String));
            newline.Columns.Add("MODEL_NO", typeof(String));
            newline.Columns.Add("PARTS_NO", typeof(String));
            newline.Columns.Add("EXPIRY_DATE", typeof(String));

            if (gvMachinery.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] {  "", "", "", "", "", "", "", DateTime.Now.AddYears(5).ToString("dd/MM/yyyy")});
            }

            gvMachinery.DataSource = newline;
            gvMachinery.DataBind();
        }
        private void SavePaddyRequisition()
        {
            try
            {
                _masterProcurementService.SavePaddyRequisition();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ShowMessage(Label lblErrorMsg, string msg, MessageType msgType)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(msg, msgType);
            //lblErrorMsg.Visible = true;
            //lblErrorMsg.Text = msg;
            //lblErrorMsg.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
            //AppHelper.Logger.Warn("AdminAreaWarning. UserName: " + _user.Username + " Error: " + msg);
        }
        private string SavePaddyRequisition(PUR_PURCHASE_REQUISITION req, List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails, List<PUR_PURCHASE_REQUISITION_BOSTA> lbag)
        {
            try
            {
                return _masterProcurementService.SavePaddyRequisition(req, lreqDetails, lbag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<PUR_PURCHASE_REQUISITION_DETAILS> GetPurchaseRequisionDetailsForPaddy(string loggedUserID)
        {
            List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = new List<PUR_PURCHASE_REQUISITION_DETAILS>();
            PUR_PURCHASE_REQUISITION_DETAILS reqDetails;
            foreach (GridViewRow row in gvPreOrder.Rows)
            {
                if (((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString() != "0"
                    && ((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim() !="0"
                    && Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim()) >0)
                {
                    reqDetails = new PUR_PURCHASE_REQUISITION_DETAILS();
                    reqDetails.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                    reqDetails.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                    reqDetails.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                    reqDetails.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                    //reqDetails.PRICE

                    //reqDetails.DISCOUNT_AMOUNT = Convert.ToDecimal(((TextBox)row.FindControl("txtDiscunt")).Text.ToString().Trim());
                    //reqDetails.DISCOUNT_PC = Convert.ToDecimal(((TextBox)row.FindControl("txtPC")).Text.ToString().Trim());

                    //reqDetails.CONSUMPTION_TYPE_ID
                    //reqDetails.EXPIRY_DATE
                    //reqDetails.MODEL
                    //reqDetails.MODEL_NO
                    //reqDetails.PARTS_NO
                    //reqDetails.COMMENTS
                    reqDetails.SET_USER = loggedUserID;
                    reqDetails.SET_DATE = DateTime.Now;

                    lreqDetails.Add(reqDetails);
                }
            }
            return lreqDetails;
        }
        private List<PUR_PURCHASE_REQUISITION_DETAILS> GetPurchaseRequisionDetailsForFuel(string loggedUserID)
        {
            List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = new List<PUR_PURCHASE_REQUISITION_DETAILS>();
            PUR_PURCHASE_REQUISITION_DETAILS reqDetails;
            foreach (GridViewRow row in gvPreOrder.Rows)
            {
                if (((DropDownList)row.FindControl("ddlConsumetype")).SelectedValue.ToString() != "0")
                {
                    reqDetails = new PUR_PURCHASE_REQUISITION_DETAILS();
                    reqDetails.CONSUMPTION_TYPE_ID = ((DropDownList)row.FindControl("ddlConsumetype")).SelectedValue.ToString().Trim();
                    reqDetails.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                    reqDetails.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                    reqDetails.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                    reqDetails.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                    reqDetails.DISCOUNT_AMOUNT = Convert.ToDecimal(((TextBox)row.FindControl("txtDiscunt")).Text.ToString().Trim());
                    reqDetails.DISCOUNT_PC = Convert.ToDecimal(((TextBox)row.FindControl("txtPC")).Text.ToString().Trim());
                    reqDetails.SET_USER = loggedUserID;
                    reqDetails.SET_DATE = DateTime.Now;

                    lreqDetails.Add(reqDetails);
                }
            }
            return lreqDetails;
        }
        private List<PUR_PURCHASE_REQUISITION_DETAILS> GetPurchaseRequisionDetailsForMachine(string loggedUserID)
        {
            List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = new List<PUR_PURCHASE_REQUISITION_DETAILS>();
            PUR_PURCHASE_REQUISITION_DETAILS reqDetails;
            foreach (GridViewRow row in gvMachinery.Rows)
            {
                if (((DropDownList)row.FindControl("ddlMachineType")).SelectedValue.ToString() != "0")
                {
                    reqDetails = new PUR_PURCHASE_REQUISITION_DETAILS();
                    reqDetails.MACHINE_CATEGORY_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlMachineType")).SelectedValue.ToString().Trim());
                    reqDetails.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlParts")).SelectedValue.ToString().Trim());
                    //reqDetails.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                    reqDetails.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtPartsQty")).Text.ToString().Trim());
                    reqDetails.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtPartsRate")).Text.ToString().Trim());
                    reqDetails.MODEL = ((TextBox)row.FindControl("txtPartsModel")).Text.ToString().Trim();
                    reqDetails.MODEL_NO = ((TextBox)row.FindControl("txtPartsModelNumber")).Text.ToString().Trim();
                    reqDetails.PARTS_NO = ((TextBox)row.FindControl("txtPartsSerial")).Text.ToString().Trim();
                    
                    string expiryDate = ((TextBox)row.FindControl("txtPartsExpireDate")).Text.ToString().Trim();
                    reqDetails.EXPIRY_DATE = DateTime.ParseExact(expiryDate, "dd/MM/yyyy", null);

                    reqDetails.SET_USER = loggedUserID;
                    reqDetails.SET_DATE = DateTime.Now;

                    lreqDetails.Add(reqDetails);
                }
            }
            return lreqDetails;
        }
        private List<PUR_PURCHASE_REQUISITION_BOSTA> GetPurchaseRequisitionBag()
        {
            List<PUR_PURCHASE_REQUISITION_BOSTA> lbag = new List<PUR_PURCHASE_REQUISITION_BOSTA>();
            PUR_PURCHASE_REQUISITION_BOSTA bag;//
            foreach (GridViewRow row in gvBagInfo.Rows)
            {
                if (((DropDownList)row.FindControl("ddlBagType")).SelectedValue.ToString() != "0")
                {
                    bag = new PUR_PURCHASE_REQUISITION_BOSTA();
                    bag.BOSTA_QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtBagQuantity")).Text.ToString().Trim());
                    bag.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlBagType")).SelectedValue.ToString().Trim());
                    lbag.Add(bag);
                }
            }
            return lbag;
        }
        private List<PUR_VENDOR> GetVendorListByCategoryId(string VendorType)
        {
            return _masterProcurementService.GetVendorByTypeId(VendorType);
        }
        private void PreparationForPaddyRequisition()
        {
            divBag.Visible = true;
            divPaddy.Visible = true;

            gvBagInfo.DataSource = null;
            gvBagInfo.DataBind();
            this.LoadBag();

            gvPreOrder.DataSource = null;
            gvPreOrder.DataBind();
            this.LoadNewPreOrder();
        }
        private void AllControlsVisibleYN(bool pTF)
        {
            divBag.Visible = pTF;
            divPaddy.Visible = pTF;
            divMachine.Visible = pTF;
            rbtMachinePurchageType.Visible = pTF;
            lblMachinePurchase.Visible = pTF;
        }
        private void ClearControls()
        {
            ddlFactoryList.SelectedIndex = -1;
            ddlPaddyPurchaseType.SelectedIndex = -1;
            ddlVendor.SelectedIndex = -1;
            txtPayTo.Text = string.Empty;
            txtHatMoney.Text = "0";
            txtRemarks.Text = string.Empty;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                if (ddlReqisitionType.SelectedValue == ((int) PurchaseType.PADDY).ToString()
                    || ddlReqisitionType.SelectedValue == ((int)PurchaseType.RICE).ToString())
                {
                    #region Paddy && Rice  ///
                    PUR_PURCHASE_REQUISITION req = new PUR_PURCHASE_REQUISITION();
                    req.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue.ToString();
                    req.FACTORY_ID = Convert.ToInt32(ddlFactoryList.SelectedValue.ToString());
                    req.REQUISITION_DATE = DateTime.ParseExact(txtDate.Text.ToString(), "dd/MM/yyyy", null);
                    req.VENDOR_ID = Convert.ToInt32(ddlVendor.SelectedValue.ToString());
                    req.PAY_TO = txtPayTo.Text.Trim();
                    req.AMOUNT = Convert.ToDecimal(txtHatMoney.Text.Trim().ToString());
                    req.PADDY_PURCHASE_TYPE_ID = Convert.ToInt32(ddlPaddyPurchaseType.SelectedValue.ToString());
                    req.PREPARED_BY = loggedUserID;
                    req.SET_USER = loggedUserID;
                    req.MC_PURCHASE_TYPE = "9";// string.Empty;
                    req.COMMETNS = txtRemarks.Text.Trim();

                    List<PUR_PURCHASE_REQUISITION_BOSTA> lbag = this.GetPurchaseRequisitionBag();
                    List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = this.GetPurchaseRequisionDetailsForPaddy(loggedUserID);
                    string reqId = this.SavePaddyRequisition(req, lreqDetails, lbag);

                    ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS + " Order No." + reqId, MessageType.Info);
                    this.GetRequisitionsList();
                    ddlReqisitionType_SelectedIndexChanged(ddlReqisitionType, null);
                    this.ClearControls(); 
                    #endregion
                }
                else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.FUEL).ToString())
                {
                    #region Fuel  ///   
                    PUR_PURCHASE_REQUISITION req = new PUR_PURCHASE_REQUISITION();
                    req.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue.ToString();
                    req.FACTORY_ID = Convert.ToInt32(ddlFactoryList.SelectedValue.ToString());
                    req.REQUISITION_DATE = DateTime.ParseExact(txtDate.Text.ToString(), "dd/MM/yyyy", null);
                    req.VENDOR_ID = Convert.ToInt32(ddlVendor.SelectedValue.ToString());
                    req.PAY_TO = txtPayTo.Text.Trim();
                    req.AMOUNT = Convert.ToDecimal(txtHatMoney.Text.Trim().ToString());
                    req.PADDY_PURCHASE_TYPE_ID = Convert.ToInt32(ddlPaddyPurchaseType.SelectedValue.ToString());
                    req.PREPARED_BY = loggedUserID;
                    req.SET_USER = loggedUserID;
                    req.MC_PURCHASE_TYPE = "9";// string.Empty;
                    req.COMMETNS = txtRemarks.Text.Trim();

                    List<PUR_PURCHASE_REQUISITION_BOSTA> lbag = new List<PUR_PURCHASE_REQUISITION_BOSTA>();
                    List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = this.GetPurchaseRequisionDetailsForFuel(loggedUserID);
                    string reqId = this.SavePaddyRequisition(req, lreqDetails, lbag);

                    ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS + " Order No." + reqId, MessageType.Info);
                    this.GetRequisitionsList();
                    ddlReqisitionType_SelectedIndexChanged(ddlReqisitionType, null);
                    this.ClearControls(); 
                    #endregion
                }
                else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.MACHINE).ToString())
                {
                    #region Machine   ///   
                    PUR_PURCHASE_REQUISITION req = new PUR_PURCHASE_REQUISITION();
                    req.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue.ToString();
                    req.FACTORY_ID = Convert.ToInt32(ddlFactoryList.SelectedValue.ToString());
                    req.REQUISITION_DATE = DateTime.ParseExact(txtDate.Text.ToString(), "dd/MM/yyyy", null);
                    req.VENDOR_ID = Convert.ToInt32(ddlVendor.SelectedValue.ToString());
                    req.PAY_TO = txtPayTo.Text.Trim();
                    req.AMOUNT = Convert.ToDecimal(txtHatMoney.Text.Trim().ToString());
                    req.PADDY_PURCHASE_TYPE_ID = Convert.ToInt32(ddlPaddyPurchaseType.SelectedValue.ToString());
                    req.PREPARED_BY = loggedUserID;
                    req.SET_USER = loggedUserID;
                    req.MC_PURCHASE_TYPE = rbtMachinePurchageType.SelectedValue;
                    req.COMMETNS = txtRemarks.Text.Trim();

                    List<PUR_PURCHASE_REQUISITION_BOSTA> lbag = new List<PUR_PURCHASE_REQUISITION_BOSTA>();
                    List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = this.GetPurchaseRequisionDetailsForMachine(loggedUserID);
                    string reqId = this.SavePaddyRequisition(req, lreqDetails, lbag);

                    ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS + " Order No." + reqId, MessageType.Info);
                    this.GetRequisitionsList();
                    ddlReqisitionType_SelectedIndexChanged(ddlReqisitionType, null);
                    this.ClearControls(); 
                    #endregion
                }
                else //if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.MACHINE).ToString())
                {
                    #region Constructions & Others  ///    
                    PUR_PURCHASE_REQUISITION req = new PUR_PURCHASE_REQUISITION();
                    req.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue.ToString();
                    req.FACTORY_ID = Convert.ToInt32(ddlFactoryList.SelectedValue.ToString());
                    req.REQUISITION_DATE = DateTime.ParseExact(txtDate.Text.ToString(), "dd/MM/yyyy", null);
                    req.VENDOR_ID = Convert.ToInt32(ddlVendor.SelectedValue.ToString());
                    req.PAY_TO = txtPayTo.Text.Trim();
                    req.AMOUNT = Convert.ToDecimal(txtHatMoney.Text.Trim().ToString());
                    req.PADDY_PURCHASE_TYPE_ID = Convert.ToInt32(ddlPaddyPurchaseType.SelectedValue.ToString());
                    req.PREPARED_BY = loggedUserID;
                    req.SET_USER = loggedUserID;
                    req.MC_PURCHASE_TYPE = "9";// rbtMachinePurchageType.SelectedValue;
                    req.COMMETNS = txtRemarks.Text.Trim();

                    List<PUR_PURCHASE_REQUISITION_BOSTA> lbag = new List<PUR_PURCHASE_REQUISITION_BOSTA>();
                    List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails = new List<PUR_PURCHASE_REQUISITION_DETAILS>();
                    string reqId = this.SavePaddyRequisition(req, lreqDetails, lbag);

                    ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS + " Order No." + reqId, MessageType.Info);
                    this.GetRequisitionsList();
                    ddlReqisitionType_SelectedIndexChanged(ddlReqisitionType, null);
                    this.ClearControls(); 
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ShowMessage(lblMessage,ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {

        }

        protected void ibtnAddMoreMCParts_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Machine");

            newline.Columns.Add("MACHINE_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("MODEL", typeof(String));
            newline.Columns.Add("MODEL_NO", typeof(String));
            newline.Columns.Add("PARTS_NO", typeof(String));
            newline.Columns.Add("EXPIRY_DATE", typeof(String));

            if (gvMachinery.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "", "", DateTime.Now.AddYears(5).ToString("dd/MM/yyyy") });
            }

            foreach (GridViewRow gvrow in gvMachinery.Rows)
            {
                DropDownList ddlMachinetype = (DropDownList)gvrow.FindControl("ddlMachineType");
                DropDownList ddlPartsInfo = (DropDownList)gvrow.FindControl("ddlParts");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtPartsQty");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtPartsRate");
                TextBox txtModel = (TextBox)gvrow.FindControl("txtPartsModel");
                TextBox txtModelNo = (TextBox)gvrow.FindControl("txtPartsModelNumber");
                TextBox txtSerialNo = (TextBox)gvrow.FindControl("txtPartsSerial");
                TextBox txtExpiryDate = (TextBox)gvrow.FindControl("txtPartsExpireDate");

                newline.Rows.Add(new String[] { ddlMachinetype.SelectedValue, ddlPartsInfo.SelectedValue, txtQuantity.Text
                    , txtRate.Text, txtModel.Text, txtModelNo.Text, txtSerialNo.Text,txtExpiryDate.Text });
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "", "", DateTime.Now.AddYears(5).ToString("dd/MM/yyyy") });

            gvMachinery.DataSource = newline;
            gvMachinery.DataBind();
        }
        protected void ibtnAddMore_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Order");

            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "0", "0" });
            }

            foreach (GridViewRow gvrow in gvPreOrder.Rows)
            {
                DropDownList ddlConsumetype = (DropDownList)gvrow.FindControl("ddlConsumetype");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                TextBox txtDiscunt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPC = (TextBox)gvrow.FindControl("txtPC");

                newline.Rows.Add(new String[] { ddlConsumetype.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtDiscunt.Text, txtPC.Text });
            }
            newline.Rows.Add(new String[] { "", "", "", "", "0", "0" });

            gvPreOrder.DataSource = newline;
            gvPreOrder.DataBind();
        }
        protected void ibtnAddBag_Click(object sender, ImageClickEventArgs e)
        {

            DataTable newline = new DataTable("Bagqty");

            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));
           
            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i <1; i++)
                    newline.Rows.Add(new String[] { "", "", "" });
            }

            foreach (GridViewRow gvrow in gvBagInfo.Rows)
            {
                DropDownList ddlBag = (DropDownList)gvrow.FindControl("ddlBagType");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtBagQuantity");

                newline.Rows.Add(new String[] { ddlBag.SelectedValue, txtQuantity .Text});
            }
            newline.Rows.Add(new String[] { "", ""});

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }

        protected void gvPreOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.PADDY).ToString()
               || ddlReqisitionType.SelectedValue == ((int)PurchaseType.RICE).ToString())
            {
                #region Paddy & Rice  //  
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlConsumetype = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillConsumptionTypeDropDownList(ddlConsumetype);
                    string strhfConsumetype = ((HiddenField)e.Row.FindControl("hfConsumetype")).Value;
                    ddlConsumetype.SelectedValue = strhfConsumetype;

                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlProduct");
                    string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                    this.FillProductByGenericId(ddlReqisitionType.SelectedValue.ToString(), ddl);
                    ddl.SelectedValue = strhfProduct;

                    DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                    this.FillMeasurementUnisDropDownList(ddlUnit);
                    string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                    ddlUnit.SelectedValue = strhfMeasurementUnit;

                    TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                    TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                    txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                    txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
                } 
                #endregion
            }
            else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.FUEL).ToString())
            {
                #region  fuel   // 
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlConsumetype = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillConsumptionTypeDropDownList(ddlConsumetype);
                    string strhfConsumetype = ((HiddenField)e.Row.FindControl("hfConsumetype")).Value;
                    ddlConsumetype.SelectedValue = strhfConsumetype;

                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlProduct");
                    string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                    this.FillProductByGenericId(ddlReqisitionType.SelectedValue.ToString(), ddl);
                    ddl.SelectedValue = strhfProduct;

                    DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                    this.FillMeasurementUnisDropDownList(ddlUnit);
                    string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                    ddlUnit.SelectedValue = strhfMeasurementUnit;

                    TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                    TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                    txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                    txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    //e.Row.Cells[0].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
                } 
                #endregion
            }

        }
        protected void gvBagInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlBagType");
                string strBagid = ((HiddenField)e.Row.FindControl("hfBagId")).Value;
                this.FillMeasurementUnisDropDownList(ddl);
                ddl.SelectedValue = strBagid;
            }
        }
        protected void gvMachinery_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlMachineType");
                string strhfMachine = ((HiddenField)e.Row.FindControl("hfMachine")).Value;
                this.FillMachineTypeDropDownList(ddl);
                ddl.SelectedValue = strhfMachine;

                if (!string.IsNullOrEmpty(strhfMachine) && strhfMachine != "0")
                {
                    DropDownList ddlParts = (DropDownList)e.Row.FindControl("ddlParts");
                    string hfParts = ((HiddenField)e.Row.FindControl("hfParts")).Value;
                    this.FillPartsDropDownList(strhfMachine, ddlParts);
                    ddlParts.SelectedValue = hfParts;
                }

                //TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                //TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                //txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                //txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
            }
        }
       
        protected void ddlPaddyPurchaseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlPaddyPurchaseType.SelectedValue == "1") {lblVendor.Text="Customer List"; }
            //else if (ddlPaddyPurchaseType.SelectedValue == "2") { lblVendor.Text="Hat List"; }
            //else if (ddlPaddyPurchaseType.SelectedValue == "3") { lblVendor.Text = "Vedor List"; }
            ddlVendor.DataSource = this.GetVendorListByCategoryId(ddlPaddyPurchaseType.SelectedValue.ToString());
            ddlVendor.DataTextField = "VENDOR_NAME";
            ddlVendor.DataValueField = "VENDOR_ID";
            ddlVendor.DataBind();
            ddlVendor.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
        }
        protected void ddlReqisitionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.AllControlsVisibleYN(false);
            this.ClearControls();
            HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            this.FillPaddyPurchaseTypeDropDownList(ddlReqisitionType.SelectedValue.ToString());
            if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.PADDY).ToString()||
                ddlReqisitionType.SelectedValue == ((int)PurchaseType.RICE).ToString())//Paddy or rice
            {
                PreparationForPaddyRequisition();
            }

            else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.MACHINE).ToString())//Machine
            {
                //divBag.Visible = false;
                //rbtMachinePurchageType.Visible = true;
                //lblMachinePurchase.Visible = true;
                //ibtnAddBag.Visible = false;
                //gvBagInfo.DataSource = null;
                //gvBagInfo.DataBind();
                //gvBagInfo.Visible = false;
                lblMachinePurchase.Visible = true;
                rbtMachinePurchageType.Visible = true;
                divMachine.Visible = true;
                gvMachinery.DataSource = null;
                gvMachinery.DataBind();
                this.LoadNewMachine();
            }

            else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.FUEL).ToString())//Machine
            {
                //ibtnAddMore.Visible = true;
                //rbtMachinePurchageType.Visible = false;
                //lblMachinePurchase.Visible = false;
                //gvBagInfo.DataSource = null;
                //gvBagInfo.DataBind();
                //gvPreOrder.DataSource = null;
                //gvPreOrder.DataBind();
                //gvBagInfo.Visible = false;
                //ibtnAddBag.Visible = false;

                gvPreOrder.DataSource = null;
                gvPreOrder.DataBind();
                divPaddy.Visible = true;
                this.LoadNewPreOrder();
            }
        }
        protected void ddlMachineType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get reference to the row
            GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);

            // Get the reference of this DropDownlist
            DropDownList ddlMC = (DropDownList)gvr.FindControl("ddlMachineType");

            // Get the reference of other DropDownlist in the same row.
            DropDownList ddlParts = (DropDownList)gvr.FindControl("ddlParts");

            string strValue = ddlMC.SelectedValue.ToString();

            this.FillPartsDropDownList(strValue, ddlParts);
            //ScriptManager1.SetFocus(ddlParts);
        }

        protected void imgMCDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvMachinery.Rows.Count==1) return;

            DataTable newline = new DataTable("Machine");

            newline.Columns.Add("MACHINE_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("MODEL", typeof(String));
            newline.Columns.Add("MODEL_NO", typeof(String));
            newline.Columns.Add("PARTS_NO", typeof(String));
            newline.Columns.Add("EXPIRY_DATE", typeof(String));
                     

            foreach (GridViewRow gvrow in gvMachinery.Rows)
            {
                DropDownList ddlMachinetype = (DropDownList)gvrow.FindControl("ddlMachineType");
                DropDownList ddlPartsInfo = (DropDownList)gvrow.FindControl("ddlParts");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtPartsQty");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtPartsRate");
                TextBox txtModel = (TextBox)gvrow.FindControl("txtPartsModel");
                TextBox txtModelNo = (TextBox)gvrow.FindControl("txtPartsModelNumber");
                TextBox txtSerialNo = (TextBox)gvrow.FindControl("txtPartsSerial");
                TextBox txtExpiryDate = (TextBox)gvrow.FindControl("txtPartsExpireDate");

                newline.Rows.Add(new String[] { ddlMachinetype.SelectedValue, ddlPartsInfo.SelectedValue, txtQuantity.Text
                    , txtRate.Text, txtModel.Text, txtModelNo.Text, txtSerialNo.Text,txtExpiryDate.Text });
            }

            newline.Rows[rowINdx].Delete();

            gvMachinery.DataSource = newline;
            gvMachinery.DataBind();
        }
        protected void imgBagDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvBagInfo.Rows.Count == 1) return;

            DataTable newline = new DataTable("Bagqty");
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));

            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "" });
            }

            foreach (GridViewRow gvrow in gvBagInfo.Rows)
            {
                DropDownList ddlBag = (DropDownList)gvrow.FindControl("ddlBagType");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtBagQuantity");

                newline.Rows.Add(new String[] { ddlBag.SelectedValue, txtQuantity.Text });
            }
            newline.Rows[rowINdx].Delete();

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }
        protected void imgProductDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvPreOrder.Rows.Count == 1) return;

            DataTable newline = new DataTable("Order");
            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 1; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "0", "0" });
            }

            foreach (GridViewRow gvrow in gvPreOrder.Rows)
            {
                DropDownList ddlConsumetype = (DropDownList)gvrow.FindControl("ddlConsumetype");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                TextBox txtDiscunt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPC = (TextBox)gvrow.FindControl("txtPC");

                newline.Rows.Add(new String[] { ddlConsumetype.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtDiscunt.Text, txtPC.Text });
            }
            newline.Rows[rowINdx].Delete();

            gvPreOrder.DataSource = newline;
            gvPreOrder.DataBind();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvRequisitionList.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> dtAttFrom = _masterProcurementService.GetRequsionReportsData(strID);
                SessionManager.GetReportDataTable = dtAttFrom;
                ShowReport(this, this.GetType(), "PuchaseRequision.rdlc", "PurchaseRequision", dtAttFrom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(Control control, Type type, string sReportFileName, string sReportDataSourceName, List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> dtAttFrom)
        {
            string script = string.Format(@"window.open('CommonReportsViewer.aspx?ReportFileName={0}&&DataSourceName={1}','CommonReportViewer',
                                            'width=1000,height=650,resizable=1,scrollbars=1');", sReportFileName, sReportDataSourceName);
            ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), script, true);
        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {

        }

        
       
    }
 }
