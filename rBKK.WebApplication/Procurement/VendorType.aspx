﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VendorType.aspx.cs" Inherits="rBKK.WebApplication.Procurement.VendorType" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="MiniPage">

        <fieldset class="flView">
            <legend class="lgView"> Vendor Type </legend>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>

                    <table class="bgdisplay">
                        <tr>
                            <td colspan="2">
                                <asp:HiddenField ID="hfVendorID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                                        <span>Generic Type</span></td>
                            <td>
                                <asp:DropDownList ID="ddlReqisitionType" runat="server" AutoPostBack="True" 
                                    SkinID="normalDropDownList" Width="230px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                <span>Type </span></td>
                            <td>
                                <asp:TextBox ID="txtVendorType" runat="server" SkinID="normalTextBox" 
                                    Width="226px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                                        &nbsp;</td>
                            <td align="center" style="text-align: left">
                                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                                &nbsp;
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                &nbsp;
                            </td>
                        </tr>
                    </table>

                </ContentTemplate>
            </asp:UpdatePanel>

        </fieldset>

    </div>

    <div class="MiniDisplay">

 <fieldset class="flView">
 <legend class="lgView"> Vendor List </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvCategory" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="ID">
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("VENDOR_TYPE_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="GenericID">
                         <ItemTemplate>
                             <asp:Label ID="lblGenericTypeId" runat="server" Text='<%# Bind("REQUISITION_TYPE_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle CssClass="HiddenGridColumn" />
                         <ItemStyle CssClass="HiddenGridColumn" />
                     </asp:TemplateField>
                     <asp:BoundField DataField="REQUISITION_TYPE_NAME" HeaderText="Generic Type" />
                     <asp:BoundField DataField="VENDOR_TYPE" HeaderText="Vendor Type" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
