﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;
using rBKK.WebApplication.Reports;
using Microsoft.Reporting.WebForms;
using System.Globalization;


namespace rBKK.WebApplication.Procurement
{
    public partial class rptChallanWisePurchaseOrder : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack==true)
            {
              
            }
        }

     

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> dtAttFrom = _masterProcurementService.GetRequisionOrderReportsData(txtChallanNo.Text);
                SessionManager.GetOrderReportDataTable = dtAttFrom;
                this.ShowReport("Reports/" + "PurchaseReports.rdlc", "PuchaseOrder", dtAttFrom);
               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> dataSourceValue)
        {
            this.CommonViewer.LocalReport.DataSources.Clear();
            this.CommonViewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.CommonViewer.LocalReport.ReportPath = reportName;
            this.CommonViewer.LocalReport.Refresh();
        }

      

        protected void btnReset_Click(object sender, EventArgs e)
        {
            
        }
    }
}