﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;
using rBKK.WebApplication.Reports;
using Microsoft.Reporting.WebForms;
using System.Globalization;


namespace rBKK.WebApplication.Procurement
{
    public partial class rptDateWisePurchaseOrder : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack==true)
            {
                this.PurchaseTypeRequision();
            }
        }

        private void PurchaseTypeRequision()
        {
            try
            {
                List<PUR_REQUISITION_TYPE> lst = _masterProcurementService.GetPurchaseType();

                ddlOrderType.DataSource = lst.Where(d => d.SPECIAL_NODE_YN == "0");
                ddlOrderType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlOrderType.DataValueField = "REQUISITION_TYPE_ID";
                ddlOrderType.DataBind();
                ddlOrderType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime dtLDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
                DateTime dtUDate = DateTime.ParseExact(txtTodate.Text, "dd/MM/yyyy",null);
                int rType = Convert.ToInt32(ddlOrderType.SelectedValue);
                List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> dtAttFrom = _masterProcurementService.GetRequsionReportsData(rType, dtLDate, dtUDate);
                this.ShowReport("Reports/" + "PurchaseStock.rdlc", "PuchaseOrder", dtAttFrom);
               
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> dataSourceValue)
        {
            this.CommonViewer.LocalReport.DataSources.Clear();
            this.CommonViewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.CommonViewer.LocalReport.ReportPath = reportName;
            this.CommonViewer.LocalReport.Refresh();
        }

      

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ddlOrderType.SelectedIndex = 0;
            txtFromDate.Text = string.Empty;
            txtTodate.Text = string.Empty;
        }
    }
}