﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using rBKK.WebApplication.Reports;
using rBKK.Entities;


namespace rBKK.WebApplication.Procurement
{
    public partial class CommonReportsViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString.Count >= 2)
                {
                    PreviewReport(Request.QueryString["ReportFileName"], Request.QueryString["DataSourceName"]);
                }
            }
        }

        private void PreviewReport(string sReportFileName, string reportDataSourceName)
        {
            try
            {
                List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> dsReportDaSourceSet = SessionManager.GetReportDataTable;
                this.ShowReport("Reports/" + sReportFileName, reportDataSourceName, dsReportDaSourceSet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> dataSourceValue)
        {
            this.CommonViewer.LocalReport.DataSources.Clear();
            this.CommonViewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.CommonViewer.LocalReport.ReportPath = reportName;
            this.CommonViewer.LocalReport.Refresh();
        }
    }
}