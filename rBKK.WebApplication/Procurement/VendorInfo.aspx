﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VendorInfo.aspx.cs" Inherits="rBKK.WebApplication.Procurement.VendorInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });

        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WebServices/CommonWebService.asmx/HelloWorld",
                        data: "{" + document.getElementById('<%=txtName.ClientID%>').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert(result);
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="MiniDisplay"> 
       <fieldset class="flView">
                                    <legend class="lgView"> Vendor info </legend>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <table class="bgdisplay">
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:HiddenField ID="hiID" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                       <span>Type </span> </td>
                                                    <td style="width: 315px">
                                                        <asp:DropDownList ID="ddlvendorType" runat="server" Width="236px" 
                                                            AutoPostBack="True" 
                                                            onselectedindexchanged="ddlvendorType_SelectedIndexChanged" SkinID="normalDropDownList">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 13px">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                            ControlToValidate="ddlvendorType" ErrorMessage="*" ForeColor="Red" 
                                                            InitialValue="0"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                       <span> Code</span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtCode" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtCode" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                      <span>Name </span>  </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtName" runat="server" Width="226px" SkinID="normalTextBox" class="autosuggest"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="txtName" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:120px">
                                                       <span>Contact Person </span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtContactPerson" runat="server" Width="226px" 
                                                            SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:90px">
                                                      <span>e-mail</span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtemail" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 90px">
                                                        <span> Web Site</span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtWebsite" runat="server" Width="226px" SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 90px">
                                                       <span> Address</span> </td>
                                                    <td style="width: 315px">
                                                        <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Width="264px" 
                                                            SkinID="normalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 13px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="3">
                                                        &nbsp; &nbsp;
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                                            onclick="btnSave_Click" Text="Save" Width="70px" />
                                                        &nbsp;
                                                        <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                                            CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </fieldset>
</div>

<div class="MiddlePageDisplay">

 <fieldset class="flDeatialView" >
 <legend class="lgView"> vendor list </legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <asp:GridView ID="gvVendor" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" DataKeyNames="VENDOR_ID" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="ID">
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("VENDOR_ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="VENDOR_TYPE_ID" HeaderText="Type" />
                     <asp:BoundField DataField="VENDOR_CODE" HeaderText="Code" />
                     <asp:BoundField DataField="VENDOR_NAME" HeaderText="Name" />
                     <asp:BoundField DataField="CONTACT_PERSON" HeaderText="Contact Person" />
                     <asp:BoundField DataField="EMAIL_ID" HeaderText="e-MAIL" />
                     <asp:BoundField DataField="WEB_SITE_ID" HeaderText="WEB SITE" />
                     <asp:BoundField DataField="ADDRESS_ID" HeaderText="Address" />
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgEdit" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Edit.gif" onclick="img_edit" 
                                 CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" CausesValidation="False" OnClientClick="javascript:return confirm('Do you really want to \ndelete the item?')"/>
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>
</asp:Content>
