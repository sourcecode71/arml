﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CommonReportsViewer.aspx.cs" Inherits="rBKK.WebApplication.Procurement.CommonReportsViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div  style="text-align:center; width:100%;"  >               
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                   
                     <tr style=" margin-left:5px; margin-right:5px;">
                        <td align="center" style="background-color: #fbfbfb; font-weight: bold">
                      
                          </td>
                    </tr>
                   <%-- <tr>
                        <td class="groupHeader" style="width: 100%; text-align:left;">
                            Common ReportViewer</td>
                    </tr>--%>

                
                        <tr>
                            <td  style="text-align:center;">
                            <div class="legendbordered">

                             <rsweb:ReportViewer ID="CommonViewer" runat="server" Height="636px" 
                                    Width="100%" Font-Names="Times New Roman" Font-Size="8pt" 
                                    InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Times New Roman" 
                                    WaitMessageFont-Size="14pt" BackColor="White" >
                                           
                                </rsweb:ReportViewer>     
                                </div>                      
                                </td>
                        </tr>
                   
                                                               
                 </table>
      </div>   
</asp:Content>
