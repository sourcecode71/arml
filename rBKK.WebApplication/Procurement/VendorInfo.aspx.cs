﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Security;
using rBKK.WebApplication.Common;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;

namespace rBKK.WebApplication.Procurement
{
    public partial class VendorInfo : System.Web.UI.Page
    {
        [Inject]
        public IMasterProcurementService _masterProcurementServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddVendor).ToString());

            if (!Page.IsPostBack)
            {
                this.PagedataBound();
               
            }
        }

        private void PagedataBound()
        {
            this.BindDropdownlist(ddlvendorType, "VendorType");
        }

        private void BindDropdownlist(DropDownList ddl, string ddlType)
        {
            try
            {


                ddl.Items.Clear();
                bool selectDefault = true;
                switch (ddlType)
                {
                    case "VendorType":
                        {
                            var dATA = (_masterProcurementServise.GetAllVendorType()).ToList();
                            ddl.DataValueField = "VENDOR_TYPE_ID";
                            ddl.DataTextField = "VENDOR_TYPE";
                            ddl.DataSource = dATA;
                            break;
                        }

                }

                if (selectDefault)
                {

                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("<<----Select--->>", "0"));
                    ddl.SelectedValue = "0";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void BindGridView(GridView gvVendor, string gvType)
        {
            try
            {
                switch (gvType)
                {
                    case "Vendor":
                        {
                            string strcategory = ddlvendorType.SelectedValue;
                            var dATA = (_masterProcurementServise.GetVendorByTypeId(strcategory)).ToList();
                            gvVendor.DataSource = dATA;
                            gvVendor.DataBind();
                            break;
                        }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void Reset()
        {
            try
            {
                ddlvendorType.SelectedIndex = 0;
                txtAddress.Text = string.Empty;
                txtCode.Text = string.Empty;
                txtContactPerson.Text = string.Empty;
                txtemail.Text = string.Empty;
                txtName.Text = string.Empty;
                txtWebsite.Text = string.Empty;
                btnSave.Text = "Save";



            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string TranType;
                PUR_VENDOR pur_Vendor = new PUR_VENDOR();
                if (btnSave.Text == "Save")
                {
                    TranType = "Save";
                    pur_Vendor.VENDOR_NAME = txtName.Text;
                    pur_Vendor.VENDOR_CODE = txtCode.Text;
                    pur_Vendor.CONTACT_PERSON = txtContactPerson.Text;
                    pur_Vendor.VENDOR_TYPE_ID = Convert.ToInt32(ddlvendorType.SelectedValue);
                    pur_Vendor.COMPANY_ID = "1";
                    pur_Vendor.WEB_SITE_ID =txtWebsite.Text;
                    pur_Vendor.EMAIL_ID =txtemail.Text;
                    pur_Vendor.ADDRESS_ID =txtAddress.Text;
                    _masterProcurementServise.AddVendorType(pur_Vendor, TranType);
                }
                else
                {
                    //HttpContext.Current.Session["USER_ID"].ToString();
                    TranType = "Update";
                    pur_Vendor.VENDOR_ID = Convert.ToInt32(hiID.Value);
                    pur_Vendor.VENDOR_NAME = txtName.Text;
                    pur_Vendor.VENDOR_CODE = txtCode.Text;
                    pur_Vendor.CONTACT_PERSON = txtContactPerson.Text;
                    pur_Vendor.VENDOR_TYPE_ID = Convert.ToInt32(ddlvendorType.SelectedValue);
                    pur_Vendor.COMPANY_ID = "1";
                    pur_Vendor.WEB_SITE_ID = txtWebsite.Text;
                    pur_Vendor.EMAIL_ID = txtemail.Text;
                    pur_Vendor.ADDRESS_ID = txtAddress.Text;
                    _masterProcurementServise.AddVendorType(pur_Vendor, TranType);

                }
                this.BindGridView(gvVendor, "Vendor");
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                this.Reset();

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.Reset();
        }
        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvVendor.Rows[rowINdx].Cells[0].FindControl("lblID")).Text.ToString());
                PUR_VENDOR vendor = _masterProcurementServise.GetVendorByID(strID);
                txtName.Text = vendor.VENDOR_NAME;
                txtCode.Text = vendor.VENDOR_CODE;
                txtContactPerson.Text = vendor.CONTACT_PERSON;
                txtemail.Text = vendor.EMAIL_ID;
                txtWebsite.Text = vendor.WEB_SITE_ID;
                txtAddress.Text = vendor.ADDRESS_ID;
                btnSave.Text = "Update";
                hiID.Value = strID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvVendor.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterProcurementServise.DeleteVendor(strID);
                this.BindGridView(gvVendor, "Vendor");

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void ddlvendorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindGridView(gvVendor, "Vendor");
        }

    }
}