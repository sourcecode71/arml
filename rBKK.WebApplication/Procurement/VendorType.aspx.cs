﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Security;
using rBKK.Core.Procurement;
using rBKK.Entities;
using Resources;
using rBKK.WebApplication.Common;

namespace rBKK.WebApplication.Procurement
{
    public partial class VendorType : System.Web.UI.Page
    {
        [Inject]
        public IMasterProcurementService _masterProcurementServise { get; set; }
        [Inject]
        public IUserService _userService { get; set; }
       

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.AddVendorType).ToString());
                if (!Page.IsPostBack)
                {
                    //HttpContext.Current.Session.Add("PAGE_ID", "12001");
                    this.LoadPageData();
                    this.FillPurchaseTypeDropDownList();
                }
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);

            }
        }
        private void FillPurchaseTypeDropDownList()
        {
            try
            {
                ddlReqisitionType.DataSource = _masterProcurementServise.GetPurchaseType();
                ddlReqisitionType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlReqisitionType.DataValueField = "REQUISITION_TYPE_ID";
                ddlReqisitionType.DataBind();
                ddlReqisitionType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadPageData()
        {
            try
            {

                //List<PUR_VENDOR_TYPE> vendorType = _masterProcurementServise.GetAllVendorType();
                IQueryable<object> vendorType = _masterProcurementServise.GetAllVendorTypes();
                gvCategory.DataSource = vendorType;
                gvCategory.DataBind();

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PUR_VENDOR_TYPE purVendorType = new PUR_VENDOR_TYPE();
                string TransType;
                string loggedUserId = HttpContext.Current.Session["USER_ID"].ToString();
                if (btnSave.Text == "Save")
                {
                    TransType = "Save";
                    purVendorType.VENDOR_TYPE = txtVendorType.Text.Trim();
                    purVendorType.COMPANY_ID = "1";
                    purVendorType.SET_USER = loggedUserId;
                    purVendorType.SET_DATE = DateTime.Now;
                    purVendorType.PARENT_ID = ddlReqisitionType.SelectedValue.ToString();
                    _masterProcurementServise.AddVendorType(purVendorType, TransType);
                }
                else
                {
                    TransType = "Update";
                    purVendorType.VENDOR_TYPE_ID = Convert.ToInt32(hfVendorID.Value);
                    purVendorType.VENDOR_TYPE = txtVendorType.Text;
                    purVendorType.SET_USER = loggedUserId;
                    purVendorType.SET_DATE = DateTime.Now;
                    _masterProcurementServise.AddVendorType(purVendorType, TransType);
                    btnSave.Text = "Save";
                }
                ((rBKK.WebApplication.Site)Master).ShowMessage(Localization.RECORD_SAVED_SUCCESS, Common.MessageType.Info);
                txtVendorType.Text = string.Empty;
                ddlReqisitionType.Enabled = true;
                ddlReqisitionType.SelectedValue = "0";
                this.LoadPageData();

            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtVendorType.Text = string.Empty;
            ddlReqisitionType.Enabled = true;
            ddlReqisitionType.SelectedValue = "0";
            btnSave.Text = "Save";

        }

        protected void img_edit(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                string strGenericTypeName = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[2].FindControl("lblGenericTypeId")).Text.ToString());
                PUR_VENDOR_TYPE purVendorType = _masterProcurementServise.GetVendorTypeByID(strID);
                txtVendorType.Text = purVendorType.VENDOR_TYPE;
                ddlReqisitionType.SelectedValue = strGenericTypeName;
                ddlReqisitionType.Enabled = false;
                btnSave.Text = "Update";
                hfVendorID.Value = strID;

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvCategory.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterProcurementServise.DeleteVendorType(strID);
                this.LoadPageData();
            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

       
    }
}