﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.Entities;
using Microsoft.Reporting.WebForms;

namespace rBKK.WebApplication.Procurement
{
    public partial class rptDateWiseRequisionReports : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack == true)
            {

            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dtLDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
                DateTime dtUDate = DateTime.ParseExact(txtTodate.Text, "dd/MM/yyyy", null);
                List<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result> dtAttFrom = _masterProcurementService.GetDateWiseRequision(dtLDate, dtUDate);
                this.ShowReport("Reports/" + "rptDatePuchaseRequision.rdlc", "PurchaseRequision", dtAttFrom);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result> dataSourceValue)
        {
             this.CommonViewer.LocalReport.DataSources.Clear();
            this.CommonViewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.CommonViewer.LocalReport.ReportPath = reportName;
            this.CommonViewer.LocalReport.Refresh();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = string.Empty;
            txtTodate.Text = string.Empty;
        }
    }
}