﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="rptDateWiseTransferReportsViewer.aspx.cs" Inherits="rBKK.WebApplication.Procurement.rptDateWiseTransferReportsViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script type="text/javascript">
     function DatePicker() {
         $(function () {
             $("#<%=txtFromDate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });

             $("#<%=txtTodate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });
         });
     }
     function pageLoad() {
         if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
             DatePicker();
         }
     }
     DatePicker();
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    

    <div class="MiniPage">
        <fieldset class="flView">
            <legend class="lgView">Transfer Report </legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay" width="100%">
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                <span>From Date</span></td>
                            <td>
                                <asp:TextBox ID="txtFromDate" runat="server" SkinID="normalTextBox" 
                                    Width="98px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:120px">
                                <span> TO Date</span></td>
                            <td>
                                <asp:TextBox ID="txtTodate" runat="server" SkinID="normalTextBox" 
                                    Width="98px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp;</td>
                            <td align="center" style="text-align: left">
                                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnPrint_Click" Text="View " Width="70px" />
                                &nbsp;
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
        <br />
    </div>

    <div style="height: 376px">
                <rsweb:ReportViewer ID="CommonViewer" runat="server" 
        Height="344px" Width="100%">
                </rsweb:ReportViewer>
    </div>
</asp:Content>
