﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using rBKK.Entities;
using Microsoft.Reporting.WebForms;

namespace rBKK.WebApplication.Procurement
{
    public partial class rptDateWiseTransferReportsViewer : System.Web.UI.Page
    {

        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack == true)
            {

            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                DateTime dtUDate = DateTime.ParseExact(txtFromDate.Text,"dd/MM/yyyy",null);
                DateTime dtLDate = DateTime.ParseExact(txtTodate.Text,"dd/MM/yyyy",null);
                List<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result> dtAttFrom = _masterProcurementService.GetPSTData(7,dtLDate, dtUDate);

                this.ShowReport("Reports/" + "TransferReports.rdlc", "INVSales", dtAttFrom);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ShowReport(string reportName, string dataSourceName, List<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result> dataSourceValue)
        {
            this.CommonViewer.LocalReport.DataSources.Clear();
            this.CommonViewer.LocalReport.DataSources.Add(new ReportDataSource(dataSourceName, dataSourceValue));
            this.CommonViewer.LocalReport.ReportPath = reportName;
            this.CommonViewer.LocalReport.Refresh();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = string.Empty;
            txtTodate.Text = string.Empty;
        }
    }
}