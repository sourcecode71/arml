﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Ninject;
using rBKK.Core.Inventory;
using rBKK.Core.Procurement;
using Resources;
using rBKK.Entities;
using rBKK.WebApplication.Common;
using System.Collections;
using rBKK.WebApplication.Reports;

namespace rBKK.WebApplication.Procurement
{
    //public enum PurchaseType { PADDY = 1, FUEL = 4, MACHINE = 6 }

    public partial class ProductProcurement : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }
        [Inject]
        public IMasterProcurementService _masterProcurementService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.GeneratePurchaseOrder).ToString());
            if (!IsPostBack)
            {
                rbtMachinePurchageType.Visible = false;
                lblMachinePurchase.Visible = false;
                this.FillFactoryDropDownList();
                this.FillPurchaseTypeDropDownList();
                this.GetRequisitionsList();
                this.GetPurchaseOrderInfo();
                txtDate.Text = DateTime.Now.ToShortDateString();
                txtChallanNo.Text = string.Empty;
                txtChallanDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtReceivedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                this.AllControlsVisibleYN(false);
                this.SetNullToSessionVariables();
            }
        }

        private void GetPurchaseOrderInfo()
        {
            try
            {
                gvPurchaseOrderList.DataSource = _masterProcurementService.GetPurchaseOrderList();
                gvPurchaseOrderList.DataBind();
            }
            catch (Exception ex)
            {
                
                 ShowMessage(lblMessage, ex.Message, Common.MessageType.Error);
            }
        }

        private void SetNullToSessionVariables()
        {
            Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = null;
            Session[SessionVariable.INV_GODOWN.ToString()] = null;
            Session[SessionVariable.MACHINE_TYPE.ToString()] = null;
            Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            Session[SessionVariable.PUR_PADDY.ToString()] = null;
            Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = null;
        }


        private void FillPaddyPurchaseDropDownList(string requisitionType)
        {
            try
            {
                if (requisitionType == "0") return;
                lblPaddyPurchaseType.Text = ddlReqisitionType.SelectedItem.Text + " Purchase Type";
                ddlPaddyPurchaseType.DataSource = _masterProcurementService.GetVendorTypeByGenericId(requisitionType);
                ddlPaddyPurchaseType.DataTextField = "VENDOR_TYPE";
                ddlPaddyPurchaseType.DataValueField = "VENDOR_TYPE_ID";
                ddlPaddyPurchaseType.DataBind();
                ddlPaddyPurchaseType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetRequisitionsList()
        {
            try
            {
                gvRequisitionList.DataSource = _masterProcurementService.GetRequisitionsList();
                gvRequisitionList.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillFactoryDropDownList()
        {
            try
            {
                ddlFactoryList.DataSource = _masterInventoryServise.GetAllFactories();
                ddlFactoryList.DataTextField = "NAME";
                ddlFactoryList.DataValueField = "FACTORY_ID";
                ddlFactoryList.DataBind();
                ddlFactoryList.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, string.Empty));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillPurchaseTypeDropDownList()
        {
            try
            {
                ddlReqisitionType.DataSource = _masterProcurementService.GetPurchaseType();
                ddlReqisitionType.DataTextField = "REQUISITION_TYPE_NAME";
                ddlReqisitionType.DataValueField = "REQUISITION_TYPE_ID";
                ddlReqisitionType.DataBind();
                ddlReqisitionType.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProductDropDownList(string genericId = "1", DropDownList ddl = null)
        {
            try
            {
                ddl.DataSource = _masterInventoryServise.GetProductByGenericId(genericId); 
                ddl.DataTextField = "PRODUCT_NAME";
                ddl.DataValueField = "PRODUCT_ID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, string.Empty));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillConsumptionTypeDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] != null)
                {
                    List<PUR_CONSUMPTION_TYPE> lst = (List<PUR_CONSUMPTION_TYPE>)HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                    ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<PUR_CONSUMPTION_TYPE> lst = _masterProcurementService.GetAllConsumptionTypes();
                    HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                    ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillProductByGenericId(string genericId = "1", DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetProductByGenericId(genericId);
                    //IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                    HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "PRODUCT_NAME";
                    ddl.DataValueField = "PRODUCT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillMeasurementUnisDropDownList(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] != null)
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = (List<INV_UNIT_OF_MEASUREMENT>)HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    List<INV_UNIT_OF_MEASUREMENT> lst = _masterInventoryServise.GetAllUNIT_OF_MEASUREMENT();
                    HttpContext.Current.Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                    ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillMachineTypeDropDownListWithAlias(DropDownList ddl = null)
        {
            try
            {
                if (HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()] != null)
                {
                    IQueryable<object> lst = (IQueryable<object>)HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()];
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CATEGORY_NAME";
                    ddl.DataValueField = "MACHINE_CATEGORY_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
                else
                {
                    IQueryable<object> lst = _masterInventoryServise.GetProductCategoryByParentIdWithAlias("6");
                    HttpContext.Current.Session[SessionVariable.MACHINE_TYPE.ToString()] = lst;
                    ddl.DataSource = lst;
                    ddl.DataTextField = "CATEGORY_NAME";
                    ddl.DataValueField = "MACHINE_CATEGORY_ID";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillPartsDropDownList(string ProductCategoryID = "1", DropDownList ddl = null)
        {
            try
            {
                IQueryable<object> lst = _masterInventoryServise.GetProductByCatID(ProductCategoryID);
                HttpContext.Current.Session[SessionVariable.PRODUCT_LIST.ToString()] = lst;
                ddl.DataSource = lst;
                ddl.DataTextField = "PRODUCT_NAME";
                ddl.DataValueField = "PRODUCT_ID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void FillAgentDropDownList(string VendorType,DropDownList ddl)
        {
            try
            {
                ddl.DataSource = GetVendorListByCategoryId(VendorType);
                ddl.DataTextField = "VENDOR_NAME";
                ddl.DataValueField = "VENDOR_ID";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, string.Empty));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AllClear()
        {
            txtDate.Text = string.Empty;
            txtRequistionOrderId.Text = string.Empty;
            txtHatMoney.Text = string.Empty;
            txtChallanDate.Text = string.Empty;
            txtChallanNo.Text = string.Empty;
            txtPayTo.Text = string.Empty;
            txtComment.Text = string.Empty;
            ddlFactoryList.SelectedIndex = 0;
            ddlPaddyPurchaseType.SelectedIndex = 0;
            ddlReqisitionType.SelectedIndex = 0;
            ddlVendor.SelectedIndex = 0;
        }

        private void LoadBag()
        {
            DataTable newline = new DataTable("Bagqty");

            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));

            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "" });
            }

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }
        private void LoadNewPreOrder()
        {
            DataTable newline = new DataTable("Order");

            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 3; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "0", "0" });
            }

            gvPreOrder.DataSource = newline;
            gvPreOrder.DataBind();
        }
        private void ShowMessage(Label lblErrorMsg, string msg, MessageType msgType)
        {
            ((rBKK.WebApplication.Site)Master).ShowMessage(msg, msgType);
            //lblErrorMsg.Visible = true;
            //lblErrorMsg.Text = msg;
            //lblErrorMsg.ForeColor = (msgType == MessageType.Error ? System.Drawing.Color.Red : System.Drawing.Color.Green);
        }
        private string SavePaddyPurchase(PUR_PURCHASE_ORDER ord, List<PUR_PURCHASE_ORDER_DETAILS> lordDetails,bool bFinishReceived)
        {

            try
            {
                return _masterProcurementService.SavePaddyPurchaseOrder(ord, lordDetails, bFinishReceived);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private List<PUR_PURCHASE_ORDER_DETAILS> GetPurchaseOrderDetails(string loggedUserID)
        {
            List<PUR_PURCHASE_ORDER_DETAILS> lordDetails = new List<PUR_PURCHASE_ORDER_DETAILS>();

            if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.PADDY
                     || Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.RICE
                     || Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.FUEL
                 )
            {
                #region PADDY ,FUEL ////

                PUR_PURCHASE_ORDER_DETAILS ordDetails;
                foreach (GridViewRow row in gvPreOrder.Rows)
                {

                    if (((DropDownList)row.FindControl("ddlGodown")).SelectedValue.ToString() != "0")
                    {
                        ordDetails = new PUR_PURCHASE_ORDER_DETAILS();
                        ordDetails.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlProduct")).SelectedValue.ToString().Trim());
                        ordDetails.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                        ordDetails.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim());
                        ordDetails.PRICE = Convert.ToDecimal(((TextBox)row.FindControl("txtRate")).Text.ToString().Trim()) * Convert.ToDecimal(((TextBox)row.FindControl("txtQuantity")).Text.ToString().Trim());
                        ordDetails.UNIT_OF_MEASUREMENT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlUnit")).SelectedValue.ToString().Trim());
                        if (String.IsNullOrEmpty(((TextBox)row.FindControl("txtDiscunt")).Text.ToString().Trim()))
                            ordDetails.DISCOUNT_AMOUNT = 0;
                        else
                            ordDetails.DISCOUNT_AMOUNT = Convert.ToDecimal(((TextBox)row.FindControl("txtDiscunt")).Text.ToString().Trim());
                        if (string.IsNullOrEmpty(((TextBox)row.FindControl("txtPC")).Text.ToString().Trim()))
                            ordDetails.DISCOUNT_PC = 0;
                        else
                            ordDetails.DISCOUNT_PC = Convert.ToDecimal(((TextBox)row.FindControl("txtPC")).Text.ToString().Trim());
                        if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.PADDY
                            || Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.RICE
                            )
                            ordDetails.GODOWN_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlGodown")).SelectedValue);
                        else
                            ordDetails.CONSUMPTION_TYPE_ID = ((DropDownList)row.FindControl("ddlConsumetype")).SelectedValue;
                        ordDetails.EXPIRY_DATE = DateTime.Now;

                        ordDetails.COMPANY_ID = "1";
                        ordDetails.SET_USER = loggedUserID;
                        ordDetails.SET_DATE = DateTime.Now;
                        lordDetails.Add(ordDetails);
                    }
                }
                #endregion
            }
            else if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.MACHINE)
            {
                #region MACHINE///

                PUR_PURCHASE_ORDER_DETAILS reqDetails;
                foreach (GridViewRow row in gvMachinery.Rows)
                {
                    if (((DropDownList)row.FindControl("ddlMachineType")).SelectedValue.ToString() != "0")
                    {
                        reqDetails = new PUR_PURCHASE_ORDER_DETAILS();
                        reqDetails.MACHINE_CATEGORY_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlMachineType")).SelectedValue.ToString().Trim());
                        reqDetails.PRODUCT_ID = Convert.ToInt32(((DropDownList)row.FindControl("ddlParts")).SelectedValue.ToString().Trim());
                        reqDetails.QUANTITY = Convert.ToDecimal(((TextBox)row.FindControl("txtPartsQty")).Text.ToString().Trim());
                        reqDetails.RATE = Convert.ToDecimal(((TextBox)row.FindControl("txtPartsRate")).Text.ToString().Trim());
                        reqDetails.MODEL = ((TextBox)row.FindControl("txtPartsModel")).Text.ToString().Trim();
                        reqDetails.MODEL_NO = ((TextBox)row.FindControl("txtPartsModelNumber")).Text.ToString().Trim();
                        reqDetails.PARTS_NO = ((TextBox)row.FindControl("txtPartsSerial")).Text.ToString().Trim();

                        string expiryDate = ((TextBox)row.FindControl("txtPartsExpireDate")).Text.ToString().Trim();
                        reqDetails.EXPIRY_DATE = DateTime.ParseExact(expiryDate, "dd/MM/yyyy", null);
                        reqDetails.SET_USER = "Admin";
                        reqDetails.COMPANY_ID = "1";
                        reqDetails.SET_DATE = DateTime.Now;

                        lordDetails.Add(reqDetails);
                    }
                }
                #endregion
            }

            return lordDetails;
        }
        private void FillUpDropDownlist(DropDownList ddl, string ddlType)
        {
            try
            {
                ddl.Items.Clear();
                bool selectDefault = true;

                switch (ddlType)
                {
                    case "Godown":
                        {


                            if (Session[SessionVariable.INV_GODOWN.ToString()] == null)
                            {
                                var dATA = (_masterInventoryServise.GetAllInvGodown()).ToList();

                                Session[SessionVariable.INV_GODOWN.ToString()] = dATA;
                                ddl.DataValueField = "GODOWN_ID";
                                ddl.DataTextField = "NAME";
                                ddl.DataSource = dATA;
                            }
                            else
                            {

                                ddl.DataValueField = "GODOWN_ID";
                                ddl.DataTextField = "NAME";
                                ddl.DataSource = Session[SessionVariable.INV_GODOWN.ToString()];
                            }

                            break;
                        }


                    case "ConsumeType":
                        {


                            if (HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] != null)
                            {
                                List<PUR_CONSUMPTION_TYPE> lst = (List<PUR_CONSUMPTION_TYPE>)HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()];
                                ddl.DataSource = lst;
                                ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                                ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                               
                            }
                            else
                            {
                                List<PUR_CONSUMPTION_TYPE> lst = _masterProcurementService.GetAllConsumptionTypes();
                                HttpContext.Current.Session[SessionVariable.CONSUMPTION_TYPE.ToString()] = lst;
                                ddl.DataSource = lst;
                                ddl.DataTextField = "CONSUMPTION_TYPE_NAME";
                                ddl.DataValueField = "CONSUMPTION_TYPE_ID";
                                
                            }
                            break;
                        }

                    case "Unit":
                        {


                            if (Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] == null)
                            {
                                var dATA = (_masterInventoryServise.GetAllUNIT_OF_MEASUREMENT()).ToList();

                                Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()] = dATA;
                                ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                                ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                                ddl.DataSource = dATA;
                            }
                            else
                            {

                                ddl.DataTextField = "UNIT_OF_MEASUREMENT";
                                ddl.DataValueField = "UNIT_OF_MEASUREMENT_ID";
                                ddl.DataSource = Session[SessionVariable.UNIT_OF_MEASUREMENT_ID.ToString()];
                            }

                            break;
                        }
                }

                        if (selectDefault)
                        {

                            ddl.DataBind();
                            ddl.Items.Insert(0, new ListItem(Localization.SELECT_DDL_ITEMS, "0"));
                            ddl.SelectedValue = "0";
                        }

                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private List<PUR_VENDOR> GetVendorListByCategoryId(string VendorType)
        {
            return _masterProcurementService.GetVendorByTypeId(VendorType);
        }
        private void LoadPurchaseRequisitionOrderDetails(string RequisitionOrderID)
        {
            Session[SessionVariable.PRODUCT_LIST.ToString()] = null;
            Hashtable htReqOrderDetails = _masterProcurementService.LoadPurchaseRequisitionOrderDetails(RequisitionOrderID);

            PUR_PURCHASE_REQUISITION req = (PUR_PURCHASE_REQUISITION)htReqOrderDetails["PUR_PURCHASE_REQUISITION"];
            List<PUR_PURCHASE_REQUISITION_DETAILS> lstDetails = (List<PUR_PURCHASE_REQUISITION_DETAILS>)htReqOrderDetails["PUR_PURCHASE_REQUISITION_DETAILS"];
            List<PUR_PURCHASE_REQUISITION_BOSTA> lstReqBag = (List<PUR_PURCHASE_REQUISITION_BOSTA>)htReqOrderDetails["PUR_PURCHASE_REQUISITION_BOSTA"];
            if (req == null)
            {
                ShowMessage(lblMessage, Localization.RECORD_NOT_FOUND, MessageType.Error);
                return;
            }

            ddlReqisitionType.SelectedValue = req.REQUISITION_TYPE_ID;
            ddlFactoryList.SelectedValue = req.FACTORY_ID.ToString();
            txtDate.Text = ((DateTime)req.REQUISITION_DATE).ToString("dd/MM/yyyy");
            txtPayTo.Text = req.PAY_TO.ToString();
            txtHatMoney.Text = req.AMOUNT.Value.ToString("0.00");
            this.FillPaddyPurchaseDropDownList(ddlReqisitionType.SelectedValue.ToString());
            ddlPaddyPurchaseType.SelectedValue = req.PADDY_PURCHASE_TYPE_ID.ToString();
            this.FillAgentDropDownList(req.PADDY_PURCHASE_TYPE_ID.ToString(), ddlVendor);
            ddlVendor.SelectedValue = req.VENDOR_ID.ToString();
            txtComment.Text = req.COMMETNS;
            lblPaddyPurchaseType.Text = ddlReqisitionType.SelectedItem.Text + " Purchase Type";

            lblMachinePurchase.Visible = false;
            rbtMachinePurchageType.Visible = false;

            this.AllControlsVisibleYN(false);

            if ( (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.PADDY
                || Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.RICE
                || Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.FUEL)                
                )
            {
                //divBag.Visible = true;
                divPaddy.Visible = true;
                divMachine.Visible = false;

                if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.FUEL)
                {
                    //divBag.Visible = false;
                }
                gvBagInfo.DataSource = lstReqBag;
                gvBagInfo.DataBind();

                gvPreOrder.DataSource = lstDetails;
                gvPreOrder.DataBind();
            }
            else if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.MACHINE)
            {
                rbtMachinePurchageType.SelectedValue = req.MC_PURCHASE_TYPE;
                lblMachinePurchase.Visible = true;
                rbtMachinePurchageType.Visible = true;

                //divBag.Visible = false;
                divPaddy.Visible = false;
                divMachine.Visible = true;
                gvMachinery.DataSource = lstDetails;
                gvMachinery.DataBind();
            }

        }

        
        private void AllControlsVisibleYN(bool pTF)
        {
            //divBag.Visible = pTF;
            divPaddy.Visible = pTF;
            divMachine.Visible = pTF;
            rbtMachinePurchageType.Visible = pTF;
            lblMachinePurchase.Visible = pTF;
        }
        private void ClearControls()
        {
            ddlFactoryList.SelectedIndex = -1;
            ddlPaddyPurchaseType.SelectedIndex = -1;
            ddlVendor.SelectedIndex = -1;
            txtPayTo.Text = string.Empty;
            txtHatMoney.Text = "0";
            txtComment.Text = string.Empty;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            txtRequistionOrderId.ReadOnly = true;
            this.LoadPurchaseRequisitionOrderDetails(txtRequistionOrderId.Text.Trim());
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool bFinishReceived = chkReceived.Checked;
                string ordId=string.Empty;
                string loggedUserID = HttpContext.Current.Session["USER_ID"].ToString();

                PUR_PURCHASE_ORDER ord = new PUR_PURCHASE_ORDER();


                ord.CHALLAN_NO = txtChallanNo.Text.Trim(); ;
                ord.VENDOR_ID = Convert.ToInt32(ddlVendor.SelectedValue);
                ord.CHALLAN_DATE = DateTime.ParseExact(txtChallanDate.Text.ToString(),"dd/MM/yyyy",null);
                ord.CHALLAN_PREAPARED_BY = loggedUserID;//login User
                ord.PURCHASE_DATE = DateTime.ParseExact(txtReceivedDate.Text,"dd/MM/yyyy",null);
                ord.HAT_MONEY = Convert.ToDecimal(txtHatMoney.Text);
                ord.COMMENTS = txtComment.Text;
                ord.PURCHASE_REQUISITION_ID = Convert.ToInt32(txtRequistionOrderId.Text);
                ord.REQUISITION_TYPE_ID = ddlReqisitionType.SelectedValue;
                ord.PADDY_PURCHASE_TYPE_ID =Convert.ToInt32( ddlPaddyPurchaseType.SelectedValue.ToString());
                ord.COMPANY_ID = "1";
                ord.SET_USER = loggedUserID;
                ord.SET_DATE = DateTime.Now;
                if (Convert.ToInt32(ddlReqisitionType.SelectedValue) == (int)PurchaseType.MACHINE)
                {
                    ord.MC_PURCHASE_TYPE = rbtMachinePurchageType.SelectedValue;
                }
                List<PUR_PURCHASE_ORDER_DETAILS> lordDetails = this.GetPurchaseOrderDetails(loggedUserID);
               ordId = this.SavePaddyPurchase(ord, lordDetails,bFinishReceived);
                ShowMessage(lblMessage, Localization.RECORD_SAVED_SUCCESS + " Received No." + ordId, MessageType.Info);
                GetRequisitionsList();
               // this.AllClear();
            }
            catch (Exception ex)
            {
                ShowMessage(lblMessage, ex.Message, Common.MessageType.Error);
            }
        }
        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtRequistionOrderId.ReadOnly = false;
            this.AllClear();
        }
        protected void ibtnAddMore_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Order");

            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));
            //newline.Columns.Add("PURCHASE_REQUISITION_DETAILS_ID", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "", "0", "0"});
            }

            foreach (GridViewRow gvrow in gvPreOrder.Rows)
            {
                DropDownList ddlConsumetype = (DropDownList)gvrow.FindControl("ddlConsumetype");
                DropDownList ddlGodownInfo = (DropDownList)gvrow.FindControl("ddlGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                TextBox txtDiscunt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPC = (TextBox)gvrow.FindControl("txtPC");
                newline.Rows.Add(new String[] { ddlConsumetype.SelectedValue, ddlGodownInfo.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtDiscunt.Text, txtPC.Text });
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "", "0", "0" });

            gvPreOrder.DataSource = newline;
            gvPreOrder.DataBind();
        }
        protected void imgDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;

                Int64 intID = Convert.ToInt64(((DropDownList)gvPreOrder.Rows[rowINdx].Cells[0].FindControl("ddlProduct")).SelectedValue.ToString());

                if (intID == 0)

                    return;


                string strCatID = Convert.ToString(((HiddenField)gvPreOrder.Rows[rowINdx].Cells[0].FindControl("hfOrdID")).Value.ToString());

                Hashtable htReqOrderDetails = _masterProcurementService.LoadPurchaseRequisitionOrderDetails(txtRequistionOrderId.Text);

                List<PUR_PURCHASE_REQUISITION_DETAILS> lstReqDetails =(List<PUR_PURCHASE_REQUISITION_DETAILS>) HttpContext.Current.Session[SessionVariable.PUR_PADDY.ToString()];

                PUR_PURCHASE_REQUISITION_DETAILS pord = lstReqDetails.Where(p => p.PURCHASE_REQUISITION_DETAILS_ID == Convert.ToInt64(strCatID)).FirstOrDefault();
                lstReqDetails.Remove(pord);
                HttpContext.Current.Session[SessionVariable.PUR_PADDY.ToString()] = lstReqDetails;
                gvPreOrder.DataSource = lstReqDetails;
                gvPreOrder.DataBind();
               

            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }

        }
        protected void gvBagInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlBagType");
                string strBagid = ((HiddenField)e.Row.FindControl("hfBagId")).Value;
                this.FillUpDropDownlist(ddlUnit, "Unit");
                ddlUnit.SelectedValue = strBagid;
            }
        }
        protected void ibtnAddMoreMCParts_Click(object sender, ImageClickEventArgs e)
        {
            DataTable newline = new DataTable("Machine");

            newline.Columns.Add("MACHINE_CATEGORY_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("MODEL", typeof(String));
            newline.Columns.Add("MODEL_NO", typeof(String));
            newline.Columns.Add("PARTS_NO", typeof(String));
            newline.Columns.Add("EXPIRY_DATE", typeof(String));

            if (gvMachinery.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "", "", DateTime.Now.AddYears(5).ToString("dd/MM/yyyy") });
            }

            foreach (GridViewRow gvrow in gvMachinery.Rows)
            {
                DropDownList ddlMachinetype = (DropDownList)gvrow.FindControl("ddlMachineType");
                DropDownList ddlPartsInfo = (DropDownList)gvrow.FindControl("ddlParts");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtPartsQty");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtPartsRate");
                TextBox txtModel = (TextBox)gvrow.FindControl("txtPartsModel");
                TextBox txtModelNo = (TextBox)gvrow.FindControl("txtPartsModelNumber");
                TextBox txtSerialNo = (TextBox)gvrow.FindControl("txtPartsSerial");
                TextBox txtExpiryDate = (TextBox)gvrow.FindControl("txtPartsExpireDate");

                newline.Rows.Add(new String[] { ddlMachinetype.SelectedValue, ddlPartsInfo.SelectedValue, txtQuantity.Text
                    , txtRate.Text, txtModel.Text, txtModelNo.Text, txtSerialNo.Text,txtExpiryDate.Text });
            }
            newline.Rows.Add(new String[] { "", "", "", "", "", "", "", DateTime.Now.AddYears(5).ToString("dd/MM/yyyy") });

            gvMachinery.DataSource = newline;
            gvMachinery.DataBind();
        }

        protected void rbtPurchaseCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReqisitionType.SelectedItem.ToString() == PurchaseType.PADDY.ToString())//Paddy
            {
                this.LoadBag();
                ibtnAddBag.Visible = true;
                ibtnAddMore.Visible = true;
                gvBagInfo.Visible = true;

                rbtMachinePurchageType.Visible = false;
                lblMachinePurchase.Visible = false;

                gvPreOrder.DataSource = null;
                gvPreOrder.DataBind();

                this.LoadNewPreOrder();
            }

            else if (ddlReqisitionType.SelectedItem.ToString() == PurchaseType.MACHINE.ToString())//Machine
            {
                rbtMachinePurchageType.Visible = true;
                lblMachinePurchase.Visible = true;
                ibtnAddBag.Visible = false;
                gvBagInfo.DataSource = null;
                gvBagInfo.DataBind();
                gvBagInfo.Visible = false;

            }

            else//Fuel
            {
                ibtnAddMore.Visible = true;
                rbtMachinePurchageType.Visible = false;
                lblMachinePurchase.Visible = false;
                gvBagInfo.DataSource = null;
                gvBagInfo.DataBind();
                gvPreOrder.DataSource = null;
                gvPreOrder.DataBind();
                gvBagInfo.Visible = false;
                ibtnAddBag.Visible = false;
                this.LoadNewPreOrder();
            }

        }
        protected void ddlPurchaseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlReqisitionType.SelectedValue == "1") { lblVendor.Text = "Customer List"; }
            else if (ddlReqisitionType.SelectedValue == "2") { lblVendor.Text = "Hat List"; }
            else if (ddlReqisitionType.SelectedValue == "3") { lblVendor.Text = "Vedor List"; }
            ddlVendor.DataSource = this.GetVendorListByCategoryId(ddlReqisitionType.SelectedValue.ToString());
            ddlVendor.DataTextField = "VENDOR_NAME";
            ddlVendor.DataValueField = "VENDOR_ID";
            ddlVendor.DataBind();
        }
        
        protected void ibtnAddBag_Click(object sender, ImageClickEventArgs e)
        {

            DataTable newline = new DataTable("Bagqty");

            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));

            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "" });
            }

            foreach (GridViewRow gvrow in gvBagInfo.Rows)
            {
                DropDownList ddlBag = (DropDownList)gvrow.FindControl("ddlBagType");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtBagQuantity");

                newline.Rows.Add(new String[] { ddlBag.SelectedValue, txtQuantity.Text });
            }
            newline.Rows.Add(new String[] { "", "" });

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }

        protected void ddlMachineType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // get reference to the row
            GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);

            // Get the reference of this DropDownlist
            DropDownList ddlMC = (DropDownList)gvr.FindControl("ddlMachineType");

            // Get the reference of other DropDownlist in the same row.
            DropDownList ddlParts = (DropDownList)gvr.FindControl("ddlParts");

            string strValue = ddlMC.SelectedValue.ToString();

            this.FillPartsDropDownList(strValue, ddlParts);
            //ScriptManagerMaster.SetFocus(ddlParts);
        }

        protected void imgMCDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvMachinery.Rows.Count == 1) return;

            DataTable newline = new DataTable("Machine");

            newline.Columns.Add("MACHINE_CATEGORY_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("MODEL", typeof(String));
            newline.Columns.Add("MODEL_NO", typeof(String));
            newline.Columns.Add("PARTS_NO", typeof(String));
            newline.Columns.Add("EXPIRY_DATE", typeof(String));


            foreach (GridViewRow gvrow in gvMachinery.Rows)
            {
                DropDownList ddlMachinetype = (DropDownList)gvrow.FindControl("ddlMachineType");
                DropDownList ddlPartsInfo = (DropDownList)gvrow.FindControl("ddlParts");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtPartsQty");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtPartsRate");
                TextBox txtModel = (TextBox)gvrow.FindControl("txtPartsModel");
                TextBox txtModelNo = (TextBox)gvrow.FindControl("txtPartsModelNumber");
                TextBox txtSerialNo = (TextBox)gvrow.FindControl("txtPartsSerial");
                TextBox txtExpiryDate = (TextBox)gvrow.FindControl("txtPartsExpireDate");

                newline.Rows.Add(new String[] { ddlMachinetype.SelectedValue, ddlPartsInfo.SelectedValue, txtQuantity.Text
                    , txtRate.Text, txtModel.Text, txtModelNo.Text, txtSerialNo.Text,txtExpiryDate.Text });
            }

            newline.Rows[rowINdx].Delete();

            gvMachinery.DataSource = newline;
            gvMachinery.DataBind();
        }

        protected void imgProductDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvPreOrder.Rows.Count == 1) return;

            DataTable newline = new DataTable("Order");
            newline.Columns.Add("CONSUMPTION_TYPE_ID", typeof(String));
            newline.Columns.Add("GODOWN_ID", typeof(String));
            newline.Columns.Add("PRODUCT_ID", typeof(String));
            newline.Columns.Add("QUANTITY", typeof(String));
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("RATE", typeof(String));
            newline.Columns.Add("DISCOUNT_AMOUNT", typeof(String));
            newline.Columns.Add("DISCOUNT_PC", typeof(String));

            if (gvPreOrder.Rows.Count == 0)
            {
                for (int i = 0; i < 3; i++)
                    newline.Rows.Add(new String[] { "", "", "", "", "", "","0","0" });
            }

            foreach (GridViewRow gvrow in gvPreOrder.Rows)
            {
                DropDownList ddlConsumetype = (DropDownList)gvrow.FindControl("ddlConsumetype");
                DropDownList ddlGodownInfo = (DropDownList)gvrow.FindControl("ddlGodown");
                DropDownList ddlProductInfo = (DropDownList)gvrow.FindControl("ddlProduct");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtQuantity");
                DropDownList ddlUnit = (DropDownList)gvrow.FindControl("ddlUnit");
                TextBox txtRate = (TextBox)gvrow.FindControl("txtRate");
                TextBox txtDiscunt = (TextBox)gvrow.FindControl("txtDiscunt");
                TextBox txtPC = (TextBox)gvrow.FindControl("txtPC");

                newline.Rows.Add(new String[] { ddlConsumetype.SelectedValue, ddlGodownInfo.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtDiscunt.Text, txtPC.Text });
                //newline.Rows.Add(new String[] { ddlConsumetype.SelectedValue, ddlProductInfo.SelectedValue, txtQuantity.Text, ddlUnit.SelectedValue.ToString(), txtRate.Text, txtDiscunt.Text, txtPC.Text });
            }
            newline.Rows[rowINdx].Delete();

            gvPreOrder.DataSource = newline;
            gvPreOrder.DataBind();
        }

        protected void imgBagDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imbtn = sender as ImageButton;
            int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
            if (rowINdx == 0 && gvBagInfo.Rows.Count == 1) return;

            DataTable newline = new DataTable("Bagqty");
            newline.Columns.Add("UNIT_OF_MEASUREMENT_ID", typeof(String));
            newline.Columns.Add("BOSTA_QUANTITY", typeof(String));

            if (gvBagInfo.Rows.Count == 0)
            {
                for (int i = 0; i < 2; i++)
                    newline.Rows.Add(new String[] { "", "", "" });
            }

            foreach (GridViewRow gvrow in gvBagInfo.Rows)
            {
                DropDownList ddlBag = (DropDownList)gvrow.FindControl("ddlBagType");
                TextBox txtQuantity = (TextBox)gvrow.FindControl("txtBagQuantity");

                newline.Rows.Add(new String[] { ddlBag.SelectedValue, txtQuantity.Text });
            }
            newline.Rows[rowINdx].Delete();

            gvBagInfo.DataSource = newline;
            gvBagInfo.DataBind();
        }

        protected void gvMachinery_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlMachineType");
                string strhfMachine = ((HiddenField)e.Row.FindControl("hfMachine")).Value;
                this.FillMachineTypeDropDownListWithAlias(ddl);
                ddl.SelectedValue = strhfMachine;

                if (!string.IsNullOrEmpty(strhfMachine) && strhfMachine != "0")
                {
                    DropDownList ddlParts = (DropDownList)e.Row.FindControl("ddlParts");
                    string hfParts = ((HiddenField)e.Row.FindControl("hfParts")).Value;
                    this.FillPartsDropDownList(strhfMachine, ddlParts);
                    ddlParts.SelectedValue = hfParts;
                }

                //TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                //TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                //txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                //txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
            }
        }
        protected void gvPreOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.PADDY).ToString()
              || ddlReqisitionType.SelectedValue == ((int)PurchaseType.RICE).ToString())
            {
                #region paddy & rice  //   
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlConsumetype = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillConsumptionTypeDropDownList(ddlConsumetype);
                    string strhfConsumetype = ((HiddenField)e.Row.FindControl("hfConsumetype")).Value;
                    ddlConsumetype.SelectedValue = strhfConsumetype;

                    //e.Row.Cells[1].CssClass = "HiddenGridColumn";//1=2nd column (godown Type)
                    DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlGodown");
                    this.FillUpDropDownlist(ddlGodown, "Godown");
                    string strGodownId = ((HiddenField)e.Row.FindControl("hfGodownInfo")).Value;
                    ddlGodown.SelectedValue = strGodownId;

                    DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProduct");
                    string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                    this.FillProductByGenericId(ddlReqisitionType.SelectedValue.ToString(), ddlProduct);
                    ddlProduct.SelectedValue = strhfProduct;

                    DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                    this.FillMeasurementUnisDropDownList(ddlUnit);
                    string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                    ddlUnit.SelectedValue = strhfMeasurementUnit;

                    TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                    TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                    TextBox txtQuantity = (TextBox)e.Row.FindControl("txtQuantity");

                    ddlGodown.Attributes.Add("onchange", "DropDownListChange('" + ddlGodown.ClientID + "','" + txtQuantity.ClientID + "')");
                    ddlProduct.Attributes.Add("onchange", "DropDownListChange('" + ddlProduct.ClientID + "','" + txtQuantity.ClientID + "')");
                    ddlUnit.Attributes.Add("onchange", "DropDownListChange('" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "')");

                    txtQuantity.Attributes.Add("onblur", "return CompareCurrentStock('" + ddlGodown.ClientID + "','" + ddlProduct.ClientID + "','" + ddlUnit.ClientID + "','" + txtQuantity.ClientID + "')");
                    
                    txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                    txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
                } 
                #endregion
            }
            else if (ddlReqisitionType.SelectedValue == ((int)PurchaseType.FUEL).ToString())
            {
                #region fuel   ///   
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlConsumetype = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillConsumptionTypeDropDownList(ddlConsumetype);
                    string strhfConsumetype = ((HiddenField)e.Row.FindControl("hfConsumetype")).Value;
                    ddlConsumetype.SelectedValue = strhfConsumetype;

                    e.Row.Cells[1].CssClass = "HiddenGridColumn";//1=2nd column (godown Type)
                    DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlGodown");
                    this.FillUpDropDownlist(ddlGodown, "Godown");
                    string strGodownId = ((HiddenField)e.Row.FindControl("hfGodownInfo")).Value;
                    ddlGodown.SelectedValue = strGodownId;

                    DropDownList ddl = (DropDownList)e.Row.FindControl("ddlProduct");
                    string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                    this.FillProductByGenericId(ddlReqisitionType.SelectedValue.ToString(), ddl);
                    ddl.SelectedValue = strhfProduct;

                    DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                    this.FillMeasurementUnisDropDownList(ddlUnit);
                    string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                    ddlUnit.SelectedValue = strhfMeasurementUnit;

                    TextBox txtDiscoutAmt = (TextBox)e.Row.FindControl("txtDiscunt");
                    TextBox txtDiscoutPC = (TextBox)e.Row.FindControl("txtPC");
                    txtDiscoutAmt.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutAmt.ClientID + "','" + txtDiscoutPC.ClientID + "')");
                    txtDiscoutPC.Attributes.Add("onblur", "return DiscountCalculation('" + txtDiscoutPC.ClientID + "','" + txtDiscoutAmt.ClientID + "')");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[1].CssClass = "HiddenGridColumn"; //1=2nd column (Godown Type)
                } 
                #endregion
            }
            
            #region godown
            /*
            string str = ddlReqisitionType.SelectedItem.ToString().ToUpper();

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlProduct");
                string strhfProduct = ((HiddenField)e.Row.FindControl("hfProduct")).Value;
                this.FillProductDropDownList(ddlReqisitionType.SelectedValue, ddl);
                ddl.SelectedValue = strhfProduct;

                DropDownList ddlUnit = (DropDownList)e.Row.FindControl("ddlUnit");
                this.FillUpDropDownlist(ddlUnit, "Unit");
                string strhfMeasurementUnit = ((HiddenField)e.Row.FindControl("hfMeasurementUnit")).Value;
                ddlUnit.SelectedValue = strhfMeasurementUnit;


            }

            if (ddlReqisitionType.SelectedItem.ToString().ToUpper() == PurchaseType.PADDY.ToString() || ddlReqisitionType.SelectedItem.ToString().ToUpper() == PurchaseType.RICE.ToString())
            {
                //e.Row.Cells[0].Visible = false;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlGodown");
                    this.FillUpDropDownlist(ddlGodown, "Godown");
                    string strGodownId = ((HiddenField)e.Row.FindControl("hfGodownInfo")).Value;
                    ddlGodown.SelectedValue = strGodownId;

                    DropDownList ddlCunsumetion = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillUpDropDownlist(ddlCunsumetion, "ConsumeType");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
                }
            }

            if (ddlReqisitionType.SelectedItem.ToString().ToUpper() == PurchaseType.FUEL.ToString())
            {
                //e.Row.Cells[0].Visible = false;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[1].CssClass = "HiddenGridColumn";//0=First column (Consume Type)
                    DropDownList ddlCunsumetion = (DropDownList)e.Row.FindControl("ddlConsumetype");
                    this.FillUpDropDownlist(ddlCunsumetion, "ConsumeType");
                    string strGodownId = ((HiddenField)e.Row.FindControl("hfConsumetype")).Value;
                    ddlCunsumetion.SelectedValue = strGodownId;

                    DropDownList ddlGodown = (DropDownList)e.Row.FindControl("ddlGodown");
                    this.FillUpDropDownlist(ddlGodown, "Godown");
                }
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[1].CssClass = "HiddenGridColumn"; //0=First column (Consume Type)
                }

            } */
            #endregion

        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvPurchaseOrderList.Rows[rowINdx].Cells[0].FindControl("lblChallan")).Text.ToString());

                List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> dtAttFrom = _masterProcurementService.GetRequisionOrderReportsData(strID);
                SessionManager.GetOrderReportDataTable = dtAttFrom;
                ShowReport(this, this.GetType(), "PurchaseReports.rdlc", "PuchaseOrder", dtAttFrom);
              
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private void ShowReport(Control control, Type type, string sReportFileName, string sReportDataSourceName, List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> dtAttFrom)
        {
            string script = string.Format(@"window.open('OrderReportsViewer.aspx?ReportFileName={0}&&DataSourceName={1}','CommonReportViewer',
                                            'width=1000,height=650,resizable=1,scrollbars=1');", sReportFileName, sReportDataSourceName);
            ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), script, true);
        }

        protected void rbtnCheck_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtRequistionOrderId.Text))
                {
                    txtRequistionOrderId.ReadOnly = false;
                    this.AllClear();
                }

                CheckBox imbtn = sender as CheckBox;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                if (((RadioButton)gvRequisitionList.Rows[rowINdx].Cells[0].FindControl("rbtnCheck")).Checked)
                {
                    string strID = Convert.ToString(((Label)gvRequisitionList.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                    txtRequistionOrderId.Text = strID.ToString();
                    List<FN_PUR_GET_REQUISITION_Partial_Received_LIST_Result> dtAttFrom = _masterProcurementService.Get_REQUISITION_PARTIAL_RECEIVED_LIST(Convert.ToInt64(strID));
                    gvPartialReceivedInfo.DataSource = dtAttFrom;
                    gvPartialReceivedInfo.DataBind();
                }


            }
            catch (Exception ex)
            {

                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }


        protected void ImageReceivedFinished_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ImageButton imbtn = sender as ImageButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvRequisitionList.Rows[rowINdx].Cells[0].FindControl("lblId")).Text.ToString());
                _masterProcurementService.SavePartialReceivedFinish(strID);
                this.GetRequisitionsList();
               
            }
            catch (Exception ex)
            {
                
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

     

    }
}
