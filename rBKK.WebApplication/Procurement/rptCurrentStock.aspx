﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="rptCurrentStock.aspx.cs" Inherits="rBKK.WebApplication.Procurement.rptCurrentStock" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="MiniPage">
        <fieldset class="flView">
            <legend class="lgView">Current&nbsp; Report </legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay">
                        <tr>
                            <td colspan="2">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp;</td>
                            <td align="center" style="text-align: left">
                                &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnPrint_Click" Text="View " Width="70px" />
                                &nbsp; &nbsp;
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>

    <div style="height: 376px">
                <rsweb:ReportViewer ID="CommonViewer" runat="server" Height="344px" 
                    Width="100%">
                </rsweb:ReportViewer>
    </div>
</asp:Content>
