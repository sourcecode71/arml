﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneratePurchaseOrder.aspx.cs" Inherits="rBKK.WebApplication.Procurement.GeneratePurchaseOrder" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WebService.asmx/GetAutoCompleteData",
                        data: "{'username':'" + document.getElementById('txtSearch').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1]
                                }
                            }));
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    $('#lblUserId').text(ui.item.val);
                }
            });
        }
</script>
   
   <script type="text/javascript">
       function validateDate(objTextbox) {
           var m = parseDate(objTextbox.value);
           if (m == null) {
               alert("Date should be (dd/mm/yyyy) formated.");
               objTextbox.focus();
               return false;
           }
           else
               return true;
       }
       function parseDate(str) {
           var m = str.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
           return (m) ? m : null;
       }
       function DiscountCalculation(txt1,txt2) {
           var txt11 = document.getElementById(txt1);
           var txt22 = document.getElementById(txt2);
           if (txt11.value > 0)
               txt22.value = "0";

           return false;
       }
       function filter(phrase, _id) {
           var words = phrase.value.toLowerCase().split(" ");
           var table = document.getElementById(_id);
           var ele;
           for (var r = 0; r < table.rows.length; r++) {
               ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
               var displayStyle = 'none';
               for (var i = 0; i < words.length; i++) {
                   if (ele.toLowerCase().indexOf(words[i]) >= 0)
                       displayStyle = '';
                   else {
                       displayStyle = 'none';
                       break;
                   }
               }
               table.rows[r].style.display = displayStyle;
           }
       }
       function GotFocus(txtSearch) {
           var text = "Search...";
           var words = txtSearch.value;
           if (words == text)
               txtSearch.value = '';
       }

       function LostFocus(txtSearch) {
           var text = "Search...";
           var words = txtSearch.value.trim();
           if (words == '')
               txtSearch.value = text;
       }

       String.prototype.trim = function () {
           return this.replace(/^\s+|\s+$/g, "");
       }
   </script>
     <%-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDate.ClientID%>").datepicker({
                dateFormat: 'dd M yy',
                autoSize: true
            });
                
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="PageDetial">
        <fieldset class="flDeatialView" style="width:98%">
            <legend class="lgView">Purchase Requisition</legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <table class="bgdisplay" width="100%">
                    <tr>
                    <td colspan="3">
                        <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                            style="text-align: center" Width="100%"></asp:Label>
                       </td>
                    </tr>
                    <tr>
                    <td >
                        <span>Requisition order Type</span></td>
                            <td>
                    
                                <asp:DropDownList ID="ddlReqisitionType" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="ddlReqisitionType_SelectedIndexChanged" 
                                    SkinID="normalDropDownList" Width="230px">
                                </asp:DropDownList>
                    </td>
                        <td rowspan="9" style="width:40%; vertical-align:top" >
                        <div id="divBag" runat="server">
                            <asp:GridView ID="gvBagInfo" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333" GridLines="None" 
                                onrowdatabound="gvBagInfo_RowDataBound" SkinID="gridviewSkin" 
                                style="font-size: 9pt">
                                <HeaderStyle CssClass="dataScheduleHeader" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Bag type">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlBagType" runat="server" 
                                                SkinID="normalGridDropDownList" Width="150px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfBagId" runat="server" 
                                                Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBagQuantity" runat="server" SkinID="normalGridTextBox" 
                                                Text='<%# Bind("BOSTA_QUANTITY") %>' Width="65px" MaxLength="5"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="txtBagQuantity" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator138" runat="server" 
                                                    ControlToValidate="txtBagQuantity" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField ShowHeader="False">
                                             <ItemTemplate>
                                                 <asp:ImageButton ID="imgBagDelete" runat="server" 
                                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                     onclick="imgBagDelete_Click" CausesValidation="False" />
                                             </ItemTemplate>
                                             <ItemStyle Width="15px" />
                                         </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:ImageButton ID="ibtnAddBag" runat="server" Height="20px" 
                                ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                onclick="ibtnAddBag_Click" Width="21px" CausesValidation="False" />
                         </div>
                        </td>
                    </tr>
                        
                            <tr>
                                <td>
                                    <span>Date</span></td>
                                <td>
                                    <asp:TextBox ID="txtDate" runat="server" Width="146px" SkinID="normalTextBox" 
                                        MaxLength="10"></asp:TextBox>
                                    <asp:Label ID="Label6" runat="server" Font-Size="XX-Small" Text="DD/MM/YYYY"></asp:Label>
                                    &nbsp;</td>
                        </tr>
                   
                        <tr>
                            <td>
                               <span> Factory</span></td>
                            <td>
                                <asp:DropDownList ID="ddlFactoryList" runat="server" Width="230px" SkinID="normalDropDownList">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <samp> 
                                <asp:Label ID="lblPaddyPurchaseType" runat="server" Text="Purchase Type "  
                                    Width="160px" ></asp:Label></samp>  </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlPaddyPurchaseType" runat="server" AutoPostBack="True" 
                                            onselectedindexchanged="ddlPaddyPurchaseType_SelectedIndexChanged" 
                                            SkinID="normalDropDownList" Width="230px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                   
                        <tr>
                            <td>
                              <samp> 
                                <asp:Label ID="lblVendor" runat="server" Text="Agent List"  
                                    Width="160px" ></asp:Label></samp>  
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVendor" runat="server" SkinID="normalDropDownList" 
                                    Width="230px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <span>Pay to </span>  </td>
                            <td>
                                <asp:TextBox ID="txtPayTo" runat="server" Width="227px" SkinID="normalTextBox" 
                                    MaxLength="130"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              <span>Payable amount</span> </td>
                            <td>
                                <asp:TextBox ID="txtHatMoney" runat="server" Width="227px" 
                                    SkinID="normalTextBox" MaxLength="14" Text="0"></asp:TextBox>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Remarks</span>&nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" runat="server" Height="30px" 
                                    SkinID="normalTextBox" TextMode="MultiLine" Width="227px" MaxLength="200"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <samp>
                                <asp:Label ID="lblMachinePurchase" runat="server" Text="Machine Purchase Type"></asp:Label>
                                </samp>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtMachinePurchageType" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="True">Local</asp:ListItem>
                                    <asp:ListItem Value="2">LC</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                             <div id="divPaddy" runat="server">
                                <asp:GridView ID="gvPreOrder" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                                    style="font-size: 9pt" Width="70%" 
                                     onrowdatabound="gvPreOrder_RowDataBound">
                                    <HeaderStyle CssClass="dataScheduleHeader"/>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Consume Type">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlConsumetype" runat="server" Width="100%" SkinID="normalGridDropDownList">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfConsumetype" runat="server" Value='<%# Bind("CONSUMPTION_TYPE_ID") %>' />
                                            </ItemTemplate>
                                             <ItemStyle Width="150px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlProduct" runat="server" Width="100%" SkinID="normalGridDropDownList">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfProduct" runat="server" Value='<%# Bind("PRODUCT_ID") %>'/>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" />
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlUnit" runat="server" Width="90px" 
                                                    SkinID="normalGridDropDownList">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfMeasurementUnit" runat="server" Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtQuantity" runat="server"  Width="60px" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("QUANTITY") %>' MaxLength="6"></asp:TextBox>
                                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" 
                                                    ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rate">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRate" runat="server" Width="50px" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("RATE") %>' MaxLength="7"></asp:TextBox>
                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" 
                                                    ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Discount">
                                            <ItemTemplate>
                                            <div> 
                                                <asp:TextBox ID="txtDiscunt" runat="server" Width="50px" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("DISCOUNT_AMOUNT") %>' 
                                                    MaxLength="7"></asp:TextBox>TK.
                                                    
                                                <asp:TextBox ID="txtPC" runat="server" Width="50px" SkinID="normalGridTextBox" 
                                                    Text='<%# Bind("DISCOUNT_PC") %>' MaxLength="7"></asp:TextBox> %
                                              
                                                </div>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="HiddenGridColumn" />
                                            <ItemStyle CssClass="HiddenGridColumn" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                             <ItemTemplate>
                                                 <asp:ImageButton ID="imgProductDelete" runat="server" 
                                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                     onclick="imgProductDelete_Click" CausesValidation="False" />
                                             </ItemTemplate>
                                             <ItemStyle Width="15px" />
                                         </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                                <asp:ImageButton ID="ibtnAddMore" runat="server" 
                                    ImageUrl="~/App_Themes/Green/Images/addMore.jpg" CausesValidation="False"
                                    onclick="ibtnAddMore_Click" Height="20px" Width="21px" />
                             </div>
                            </td>
                        </tr>
                   
                        <tr>
                            <td colspan="3">
                            <div id="divMachine"  runat="server">
                                <asp:GridView ID="gvMachinery" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                                    onrowdatabound="gvMachinery_RowDataBound" style="font-size: 9pt" 
                                    Width="100%">
                                    <HeaderStyle CssClass="dataScheduleHeader" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Machine type">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlMachineType" runat="server" Width="100%" 
                                                    SkinID="normalGridDropDownList" AutoPostBack="True" 
                                                    onselectedindexchanged="ddlMachineType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfMachine" runat="server" Value='<%# Bind("MACHINE_TYPE_ID") %>'/>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parts">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlParts" runat="server" Width="100%" SkinID="normalGridDropDownList">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfParts" runat="server" Value='<%# Bind("PRODUCT_ID") %>'/>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsQty" runat="server" Width="50" SkinID="normalGridTextBox" Text='<%# Bind("QUANTITY") %>' MaxLength="7"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                                                ControlToValidate="txtPartsQty" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>
                                     
                                        <asp:TemplateField HeaderText="Rate">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsRate" runat="server" Width="50" SkinID="normalGridTextBox" Text='<%# Bind("RATE") %>' MaxLength="8"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                                ControlToValidate="txtPartsRate" ErrorMessage="*" ForeColor="Red" 
                                                ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                             <ItemStyle Width="100px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Model ">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsModel" runat="server" SkinID="normalGridTextBox"  Width="80%"
                                                    Text='<%# Bind("MODEL") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                    ControlToValidate="txtPartsModel" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                             <ItemStyle Width="180px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Model Number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsModelNumber" runat="server" SkinID="normalGridTextBox"  Width="80%"
                                                    Text='<%# Bind("MODEL_NO") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                                    ControlToValidate="txtPartsModelNumber" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                             <ItemStyle Width="170px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Serial number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsSerial" runat="server" SkinID="normalGridTextBox" Width="80%"
                                                    Text='<%# Bind("PARTS_NO") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                    ControlToValidate="txtPartsSerial" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                             <ItemStyle Width="160px" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsExpireDate" runat="server" Width="55" SkinID="normalGridTextBox" Text='<%# Bind("EXPIRY_DATE") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                                    ControlToValidate="txtPartsExpireDate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                             <ItemStyle Width="90px" />
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField ShowHeader="False">
                                             <ItemTemplate>
                                                 <asp:ImageButton ID="imgMCDelete" runat="server" 
                                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                     onclick="imgMCDelete_Click" CausesValidation="False" />
                                             </ItemTemplate>
                                             <ItemStyle Width="15px" />
                                         </asp:TemplateField>
                                       
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                                <asp:ImageButton ID="ibtnAddMoreMCParts" runat="server" Height="20px" 
                                    ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                    onclick="ibtnAddMoreMCParts_Click" Width="21px" CausesValidation="False" />
                             </div>
                            </td>
                        </tr>
                   
                        <tr>
                            <td>
                                
                            </td>
                            <td style="text-align:left">
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                                <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                   
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>

  <div class="PageDetial">

 <fieldset class="flDeatialView" style="width:98%">
 <legend class="lgView"> Purchase Requisition List</legend>
 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <input id="htxtSearch" onkeyup="filter(this, 'ctl00_MainContent_gvRequisitionList', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none" />
           <asp:Panel runat="server" ID="pnlContainer" ScrollBars="Auto" Height="200px" 
                 Width="100%">
             <asp:GridView ID="gvRequisitionList" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" DataKeyNames="PURCHASE_REQUISITION_ID" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="98%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField HeaderText="Order ID">
                         <ItemTemplate>
                             <asp:Label ID="lblId" runat="server" Text='<%# Bind("PURCHASE_REQUISITION_ID") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px" />
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Req. Type">
                         <ItemTemplate>
                             <asp:Label ID="Label4" runat="server" Text='<%# Bind("REQUISITION_TYPE_NAME") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="90px" />
                         <ItemStyle Width="90px" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Phurchase Type">
                         <ItemTemplate>
                             <asp:Label ID="Label1" runat="server" Text='<%# Bind("PADDY_PURCHASE_TYPE_NAME") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="150px"/>
                         <ItemStyle Width="150px" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Name">
                         <ItemTemplate>
                             <asp:Label ID="Label2" runat="server" Text='<%# Bind("VENDOR_NAME") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="300px" />
                         <ItemStyle Width="300px" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Money">
                         <ItemTemplate>
                             <asp:Label ID="Label3" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px" />
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText=" Date">
                         <ItemTemplate>
                             <asp:Label ID="Label5" runat="server" Text='<%# Bind("REQ_DATE") %>'></asp:Label>
                         </ItemTemplate>
                          <HeaderStyle Width="60px" />
                          <ItemStyle Width="60px" />
                     </asp:TemplateField>
                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField>
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:ImageButton ID="ImageButton1" runat="server" Height="17px" 
                                 ImageUrl="~/App_Themes/Green/Images/print-button.png" 
                                 onclick="ImageButton1_Click" CausesValidation="False" />
                         </ItemTemplate>
                          <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
           </asp:Panel>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>

</asp:Content>
