﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductProcurement.aspx.cs" Inherits="rBKK.WebApplication.Procurement.ProductProcurement" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
   
   <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
        });
        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "WebService.asmx/GetAutoCompleteData",
                        data: "{'username':'" + document.getElementById('txtSearch').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1]
                                }
                            }));
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (event, ui) {
                    $('#lblUserId').text(ui.item.val);
                }
            });
        }
</script>
   
   <script type="text/javascript">
       function validateDate(objTextbox) {
           var m = parseDate(objTextbox.value);
           if (m == null) {
               alert("Date should be (dd/mm/yyyy) formated.");
               objTextbox.focus();
               return false;
           }
           else
               return true;
       }
       function parseDate(str) {
           var m = str.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
           return (m) ? m : null;
       }
       function DiscountCalculation(txt1, txt2) {
           var txt11 = document.getElementById(txt1);
           var txt22 = document.getElementById(txt2);
           if (txt11.value > 0)
               txt22.value = "0";

           return false;
       }

       function DropDownListChange(ddl,txtbox) {
           //var godown = document.getElementById(ddl);
           var txt = document.getElementById(txtbox);
           txt.value = "0";
       }

       function CompareCurrentStock(godownId, productId, unitId, quantity) {

           var godown = document.getElementById(godownId);
           var product = document.getElementById(productId);
           var units = document.getElementById(unitId);
           var qty = document.getElementById(quantity);

           $.ajax({
               type: "POST",
               contentType: "application/json; charset=utf-8",
               //url: "http://localhost:4345/WebServices/CommonWebService.asmx/GetAutoCompleteAccData",
               url: "../WebServices/CommonWebService.asmx/CompareCurrentStock",
               data: "{'GodownId':'" + godown.options[godown.selectedIndex].value + "','ProductId':'" + product.options[product.selectedIndex].value + "','UnitId':'" + units.options[units.selectedIndex].value + "','Quantity':'" + qty.value + "'}",
               dataType: "json",
               success: function (data) {
                   alert(data.d);
               },
               error: function (result) {
                   alert("Error");
               }
           });

//           alert("g: " + godown.options[godown.selectedIndex].value + " p: " + product.options[product.selectedIndex].value + " u: "
//           + units.options[units.selectedIndex].value + " q: " + qty.value);

           return false;
       }


       function filter(phrase, _id) {
           var words = phrase.value.toLowerCase().split(" ");
           var table = document.getElementById(_id);
           var ele;
           for (var r = 0; r < table.rows.length; r++) {
               ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
               var displayStyle = 'none';
               for (var i = 0; i < words.length; i++) {
                   if (ele.toLowerCase().indexOf(words[i]) >= 0)
                       displayStyle = '';
                   else {
                       displayStyle = 'none';
                       break;
                   }
               }
               table.rows[r].style.display = displayStyle;
           }
       }
       function GotFocus(txtSearch) {
           var text = "Search...";
           var words = txtSearch.value;
           if (words == text)
               txtSearch.value = '';
       }

       function LostFocus(txtSearch) {
           var text = "Search...";
           var words = txtSearch.value.trim();
           if (words == '')
               txtSearch.value = text;
       }

       String.prototype.trim = function () {
           return this.replace(/^\s+|\s+$/g, "");
       }
   </script>
   <script type="text/javascript" language="javascript">

       $(document).ready(function () {
           $(".ddlTest").change(function () {
               alert("i am here !!");
               $(this).parent().next().find($("[ @id *= 'txtQuantity']")).val("0");
               alert("i am here !!");

               //               if ($(this).val() == "Done")
               //                  // $(this).parent().next().find("input:text").attr("disabled", true);
               //                   $(this).parent().next().find($("[ @id *= 'txtEmail']")).val("0");
               //                   
               //               else
               //                   $(this).parent().next().find("input:text").attr("disabled", false);
           });
       });
    
    </script>

   <script type="text/javascript">
   $(document).ready(function() {
    $(".onlynumeric").numeric({
        decimal: false,
        negative: false
    });
});​
   </script>

   <script type="text/javascript">

       function toggleSelectionGrid(source) {
           var isChecked = source.checked;
           $("#<%=gvRequisitionList.ClientID %> input[id*='rbtnCheck']").each(function (index) {
               $(this).attr('checked', false);

           });
           source.checked = isChecked;
       }
   </script>

   <script type="text/javascript">
   
   
         function DatePicker() {
             $(function () {
                 $("#<%=txtChallanDate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });
             });

             $(function () {
                 $("#<%=txtReceivedDate.ClientID%>").datepicker(
                 {
                     changeMonth: true,
                     changeYear: true,
                     yearRange: "1942:2099",
                     dateFormat: 'dd/mm/yy'
                 });
             });
         }
         function pageLoad() {

             if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
                 DatePicker();

             }
         }
         DatePicker();

   </script>

    <style type="text/css">
        #divBag
        {
            height: 155px;
        }
        #div1
        {
            height: 97px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="PageDetial" >
        <fieldset class="flDeatialView" style="width:98%">
            <legend class="lgView">Purchase Order</legend>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                  
                    <table class="bgdisplay" width="100%">
                    <tr>
                    <td colspan="3" >
                        <asp:Label ID="lblMessage" runat="server" CssClass="displaySucces" 
                            style="text-align: center" Width="100%"></asp:Label>
                       </td>
                    </tr>
                        <tr>
                            <td colspan="3">
                                <div class="PageDetial">
                                    <fieldset class="flDeatialView" style="width:98%">
                                        <legend class="lgView">Purchase Requisition </legend>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <input id="htxtSearch0" onkeyup="filter(this, 'ctl00_MainContent_gvRequisitionList', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none" />
                                                <asp:Panel ID="pnlContainer0" runat="server" Height="200px" ScrollBars="Auto" 
                                                    Width="100%">
                                                    <asp:GridView ID="gvRequisitionList" runat="server" 
                                                        AutoGenerateColumns="False" CellPadding="4" 
                                                        DataKeyNames="PURCHASE_REQUISITION_ID" ForeColor="#333333" GridLines="None" 
                                                        style="font-size: 9pt" Width="99%">
                                                        <HeaderStyle CssClass="dataScheduleHeader" />
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Select">
                                                                <ItemTemplate>
                                                                    <asp:RadioButton ID="rbtnCheck" runat="server" 
                                                                        onclick="toggleSelectionGrid(this);" 
                                                                        oncheckedchanged="rbtnCheck_CheckedChanged" AutoPostBack="True"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Order ID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblId" runat="server" 
                                                                        Text='<%# Bind("PURCHASE_REQUISITION_ID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="80px" />
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Req. Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label9" runat="server" 
                                                                        Text='<%# Bind("REQUISITION_TYPE_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="90px" />
                                                                <ItemStyle Width="90px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Phurchase Type">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label10" runat="server" 
                                                                        Text='<%# Bind("PADDY_PURCHASE_TYPE_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="150px" />
                                                                <ItemStyle Width="150px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("VENDOR_NAME") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="300px" />
                                                                <ItemStyle Width="300px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Money">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("AMOUNT") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="80px" />
                                                                <ItemStyle Width="80px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText=" Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("REQ_DATE") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="60px" />
                                                                <ItemStyle Width="60px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImageButton3" runat="server" CausesValidation="False" 
                                                                        Height="18px" ImageUrl="~/App_Themes/Green/Images/okbutton.jpg" 
                                                                        onclick="ImageReceivedFinished_Click" Width="18px" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="False" Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDelete" runat="server" 
                                                                        ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="imgDelete_Click" 
                                                                        CausesValidation="False" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="15px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="False">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImageButton2" runat="server" Height="20px" 
                                                                        ImageUrl="~/App_Themes/Green/Images/print-button.png" 
                                                                        onclick="ImageButton1_Click" CausesValidation="False" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="15px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EditRowStyle BackColor="#2461BF" />
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                        <RowStyle BackColor="#EFF3FB" />
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </fieldset>
                                </div>
                            </td>
                        </tr>
                    <tr>
                    <td style="width: 20%">
                        <span>Requisition order Id</span>
                     </td>
                     <td style="width:20%">
                    
                                <asp:TextBox ID="txtRequistionOrderId" runat="server" Height="17px" 
                                    SkinID="normalTextBox" Width="120px" Wrap="False" style="margin-left: 0px"></asp:TextBox>
                                <asp:Button ID="btnLoad" runat="server" CausesValidation="False" 
                                    CssClass="btnSearch" onclick="btnLoad_Click" Width="35px" />
                    </td>
                         <td rowspan="13" style="width:40%; vertical-align:top" >
                             <table style="width: 100%; height: 4px;">
                                 <tr>
                                     <td valign="top">
                                     <fieldset style="padding:5px;height:auto;min-height:110px;">
                                       <legend style="padding:0px; font-size:10px;margin-left:20px">Partial Received Info. </legend>
                                         <asp:Panel ID="Panel1" runat="server" Height="125px" ScrollBars="Vertical">
                                <asp:GridView ID="gvPartialReceivedInfo" runat="server" 
                                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                                    GridLines="None" SkinID="gridviewSkin" 
                                    style="font-size: 6pt" Width="100%" EmptyDataText="No Record Found">
                                    <HeaderStyle CssClass="dataScheduleHeader" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="CHALLAN_DATE" HeaderText="Received Date">
                                        <HeaderStyle Font-Size="5pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NAME" HeaderText="Godown">
                                        <HeaderStyle Font-Size="5pt" />
                                        <ItemStyle Font-Size="5pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PRODUCT_NAME" HeaderText="Product">
                                        <FooterStyle Font-Size="5pt" />
                                        <HeaderStyle Font-Size="5pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="QUANTITY" HeaderText="Quantity">
                                        <FooterStyle Font-Size="5pt" />
                                        <HeaderStyle Font-Size="5pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UNIT_OF_MEASUREMENT" HeaderText="Unit">
                                        <FooterStyle Font-Size="5pt" />
                                        <HeaderStyle Font-Size="5pt" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                            </asp:Panel>
                                    </fieldset>
                                     </td>
                                 </tr>
                                 <tr>
                                 <td valign="top">
                                       <fieldset style="padding:5px;height:46px; min-height:80px;">
                                       <legend style="padding:0px; font-size:10px;margin-left:20px">Bag Info. </legend>
                                           <asp:GridView ID="gvBagInfo" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333" GridLines="None" 
                                onrowdatabound="gvBagInfo_RowDataBound" SkinID="gridviewSkin" 
                                style="font-size: 9pt" EmptyDataText="No Record Found">
                                <HeaderStyle CssClass="dataScheduleHeader" />
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Bag type">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlBagType" runat="server" 
                                                SkinID="normalGridDropDownList" Width="150px" Enabled="False">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfBagId" runat="server" 
                                                Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBagQuantity" runat="server" SkinID="normalGridTextBox" 
                                                Text='<%# Bind("BOSTA_QUANTITY") %>' Width="65px" MaxLength="5" 
                                                ReadOnly="True"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField ShowHeader="False">
                                             <ItemTemplate>
                                                 <asp:ImageButton ID="imgBagDelete" runat="server" 
                                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                     onclick="imgBagDelete_Click" CausesValidation="False" Visible="False" />
                                             </ItemTemplate>
                                             <ItemStyle Width="15px" />
                                         </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                                           <asp:ImageButton ID="ibtnAddBag" runat="server" CausesValidation="False" 
                                               Height="20px" ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                               onclick="ibtnAddBag_Click" Visible="False" Width="21px" />
                                       </fieldset>
                                     </td>
                                 </tr>
                             </table>

                        
                           
                        </td>
                    </tr>
                        
                            <tr>
                                <td>
                                    <span>Requisition order Type</span></td>
                                <td>
                                    <asp:DropDownList ID="ddlReqisitionType" runat="server" Enabled="False" 
                                        SkinID="normalDropDownList" Width="210px">
                                    </asp:DropDownList>
                                </td>
                        </tr>
                        
                            <tr>
                                <td >
                                    <span>Requisition Date</span></td>
                                <td>
                                    <asp:TextBox ID="txtDate" runat="server" Width="120px" SkinID="normalTextBox" 
                                        ReadOnly="True"></asp:TextBox>
                                    <asp:Label ID="Label6" runat="server" Font-Size="XX-Small" Text="DD/MM/YYYY"></asp:Label>
                                </td>
                        </tr>
                   
                        <tr>
                            <td >
                               <span> Factory</span></td>
                            <td>
                                <asp:DropDownList ID="ddlFactoryList" runat="server" Width="210px" 
                                    SkinID="normalDropDownList" Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <samp>
                                <asp:Label ID="lblPaddyPurchaseType" runat="server" Text="Purchase Type " 
                                    Width="80%"></asp:Label>
                                </samp>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPaddyPurchaseType" runat="server" Width="210px" 
                                    SkinID="normalDropDownList" 
                                    onselectedindexchanged="ddlPurchaseType_SelectedIndexChanged" 
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                   
                        <tr>
                            <td >
                              <samp> 
                                <asp:Label ID="lblVendor" runat="server" Text="Vendor "  
                                    Width="84%" ></asp:Label></samp>  
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVendor" runat="server" SkinID="normalDropDownList" 
                                    Width="210px" Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td >
                              <span>Pay to </span>  &nbsp;</td>
                            <td>
                                <asp:TextBox ID="txtPayTo" runat="server" Width="200px" SkinID="normalTextBox" 
                                    ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td >
                              <span>Payable amount</span> </td>
                            <td>
                                <asp:TextBox ID="txtHatMoney" runat="server" Width="200px" 
                                    SkinID="normalTextBox" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <samp>
                                <asp:Label ID="lblMachinePurchase" runat="server" Text="Machine Purchase Type"></asp:Label>
                                </samp>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rbtMachinePurchageType" runat="server" 
                                    RepeatDirection="Horizontal" Enabled="False">
                                    <asp:ListItem Value="1">Local</asp:ListItem>
                                    <asp:ListItem Value="2">LC</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td >
                               <span> Challan No </span></td>
                            <td>
                                <asp:TextBox ID="txtChallanNo" runat="server" SkinID="normalTextBox" 
                                    Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server" 
                                    ControlToValidate="txtChallanNo" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Challan Date </span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtChallanDate" runat="server" SkinID="normalTextBox" 
                                    Width="120px"></asp:TextBox>
                                <asp:Label ID="Label7" runat="server" Font-Size="XX-Small" Text="DD/MM/YYYY"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server" 
                                    ControlToValidate="txtChallanDate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td >
                                <span>Purchase Date </span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtReceivedDate" runat="server" 
                                    SkinID="normalTextBox" Width="120px"></asp:TextBox>
                                <asp:Label ID="Label8" runat="server" Font-Size="XX-Small" Text="DD/MM/YYYY"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server" 
                                    ControlToValidate="txtReceivedDate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td >
                              <span>Comments</span></td>
                            <td>
                                <asp:TextBox ID="txtComment" runat="server" SkinID="TextArea" Width="200px" 
                                    Height="16px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                             <div id="divPaddy" runat="server">
                                 <asp:GridView ID="gvPreOrder" runat="server" AutoGenerateColumns="False" 
                                     CellPadding="4" ForeColor="#333333" GridLines="None" 
                                     onrowdatabound="gvPreOrder_RowDataBound" style="font-size: 9pt" 
                                     Width="80%">
                                     <HeaderStyle CssClass="dataScheduleHeader" />
                                     <AlternatingRowStyle BackColor="White" />
                                     <Columns>
                                         <asp:TemplateField HeaderText="Consume Type">
                                             <ItemTemplate>
                                                 <asp:DropDownList ID="ddlConsumetype" runat="server" 
                                                     SkinID="normalGridDropDownList" Width="100%">
                                                 </asp:DropDownList>
                                                 <asp:HiddenField ID="hfConsumetype" runat="server" 
                                                     Value='<%# Bind("CONSUMPTION_TYPE_ID") %>' />
                                             </ItemTemplate>
                                             <ItemStyle Width="110px" />
                                         </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Godown">
                                            <ItemTemplate>
                                            
                                                <asp:DropDownList ID="ddlGodown" runat="server" Width="120px" 
                                                    SkinID="normalGridDropDownList" CssClass="ddlTest">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfGodownInfo" runat="server" Value='<%# Bind("GODOWN_ID") %>'/>
                                            </ItemTemplate>
                                             <ItemStyle Width="100px" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Product">
                                             <ItemTemplate>
                                                 <asp:DropDownList ID="ddlProduct" runat="server" 
                                                     SkinID="normalGridDropDownList" Width="100%">
                                                 </asp:DropDownList>
                                                 <asp:HiddenField ID="hfProduct" runat="server" 
                                                     Value='<%# Bind("PRODUCT_ID") %>' />
                                             </ItemTemplate>
                                             <ItemStyle Width="130px" />
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Unit">
                                             <ItemTemplate>
                                                 <asp:DropDownList ID="ddlUnit" runat="server" SkinID="normalGridDropDownList" 
                                                     Width="90px">
                                                 </asp:DropDownList>
                                                 <asp:HiddenField ID="hfMeasurementUnit" runat="server" 
                                                     Value='<%# Bind("UNIT_OF_MEASUREMENT_ID") %>' />
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Quantity">
                                             <ItemTemplate>
                                                 <asp:TextBox ID="txtQuantity" runat="server" MaxLength="6" 
                                                     SkinID="normalGridTextBox" Text='<%# Bind("QUANTITY") %>' Width="60px"></asp:TextBox>
                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                     ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red" 
                                                     ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" 
                                                     ControlToValidate="txtQuantity" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         
                                         <asp:TemplateField HeaderText="Rate">
                                             <ItemTemplate>
                                                 <asp:TextBox ID="txtRate" runat="server" MaxLength="7" 
                                                     SkinID="normalGridTextBox" Text='<%# Bind("RATE") %>' Width="50px" 
                                                     CssClass="onlynumeric"></asp:TextBox>
                                                 <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                     ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red" 
                                                     ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" 
                                                     ControlToValidate="txtRate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                             </ItemTemplate>
                                         </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Discount">
                                             <ItemTemplate>
                                                 <div>
                                                     <asp:TextBox ID="txtDiscunt" runat="server" MaxLength="7" 
                                                         SkinID="normalGridTextBox" Text='<%# Bind("DISCOUNT_AMOUNT") %>' Width="50px"></asp:TextBox>
                                                     TK.
                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                                         ControlToValidate="txtDiscunt" ErrorMessage="*" ForeColor="Red" 
                                                         ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" 
                                                         ControlToValidate="txtDiscunt" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                     <asp:TextBox ID="txtPC" runat="server" MaxLength="7" SkinID="normalGridTextBox" 
                                                         Text='<%# Bind("DISCOUNT_PC") %>' Width="50px"></asp:TextBox>
                                                     %
                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                                         ControlToValidate="txtPC" ErrorMessage="*" ForeColor="Red" 
                                                         ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator133" runat="server" 
                                                         ControlToValidate="txtPC" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                 </div>
                                             </ItemTemplate>
                                             <HeaderStyle CssClass="HiddenGridColumn" />
                                             <ItemStyle CssClass="HiddenGridColumn" />
                                         </asp:TemplateField>
                                         <asp:TemplateField ShowHeader="False">
                                             <ItemTemplate>
                                                 <asp:ImageButton ID="imgProductDelete" runat="server" CausesValidation="False" 
                                                     ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                                     onclick="imgProductDelete_Click" />
                                             </ItemTemplate>
                                             <ItemStyle Width="15px" />
                                         </asp:TemplateField>
                                     </Columns>
                                     <EditRowStyle BackColor="#2461BF" />
                                     <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                     <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                     <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                     <RowStyle BackColor="#EFF3FB" />
                                     <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                     <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                     <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                     <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                     <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                 </asp:GridView>
                                <asp:ImageButton ID="ibtnAddMore" runat="server" 
                                    ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                    onclick="ibtnAddMore_Click" Height="20px" Width="21px" 
                                     CausesValidation="False" />
                             </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            <div id="divMachine"  runat="server">
                                <asp:GridView ID="gvMachinery" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="4" ForeColor="#333333" GridLines="None" 
                                    onrowdatabound="gvMachinery_RowDataBound" style="font-size: 9pt" Width="100%">
                                    <HeaderStyle CssClass="dataScheduleHeader" />
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Machine type">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlMachineType" runat="server" AutoPostBack="True" 
                                                    onselectedindexchanged="ddlMachineType_SelectedIndexChanged" 
                                                    SkinID="normalGridDropDownList" Width="100%">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfMachine" runat="server" Value='<%# Bind("MACHINE_CATEGORY_ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parts">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlParts" runat="server" SkinID="normalGridDropDownList" 
                                                    Width="100%">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="hfParts" runat="server" Value='<%# Bind("PRODUCT_ID") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsQty" runat="server" MaxLength="7" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("QUANTITY") %>' Width="50"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                                                    ControlToValidate="txtPartsQty" ErrorMessage="*" ForeColor="Red" 
                                                    ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rate">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsRate" runat="server" MaxLength="8" 
                                                    SkinID="normalGridTextBox" Text='<%# Bind("RATE") %>' Width="50"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                                    ControlToValidate="txtPartsRate" ErrorMessage="*" ForeColor="Red" 
                                                    ValidationExpression="^[0-9]\d*(?:\.\d{1,3})?$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Model ">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsModel" runat="server" SkinID="normalGridTextBox" 
                                                    Text='<%# Bind("MODEL") %>' Width="80%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                    ControlToValidate="txtPartsModel" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="180px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Model Number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsModelNumber" runat="server" SkinID="normalGridTextBox" 
                                                    Text='<%# Bind("MODEL_NO") %>' Width="80%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                                    ControlToValidate="txtPartsModelNumber" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="170px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Serial number">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsSerial" runat="server" SkinID="normalGridTextBox" 
                                                    Text='<%# Bind("PARTS_NO") %>' Width="80%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                                    ControlToValidate="txtPartsSerial" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="160px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPartsExpireDate" runat="server" SkinID="normalGridTextBox" 
                                                    Text='<%# Eval("EXPIRY_DATE", "{0:dd/MM/yyyy}") %>' Width="55"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                                    ControlToValidate="txtPartsExpireDate" ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                            <ItemStyle Width="90px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ShowHeader="False">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgMCDelete" runat="server" CausesValidation="False" 
                                                    ImageUrl="~/App_Themes/Green/Images/Delete.gif" onclick="imgMCDelete_Click" />
                                            </ItemTemplate>
                                            <ItemStyle Width="15px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                                <asp:ImageButton ID="ibtnAddMoreMCParts" runat="server" Height="20px" 
                                    ImageUrl="~/App_Themes/Green/Images/addMore.jpg" 
                                    onclick="ibtnAddMoreMCParts_Click" Width="21px" CausesValidation="False" />
                             </div>
                            </td>
                        </tr>
                   
                        <tr>
                            <td style="text-align: right" >
                                <asp:CheckBox ID="chkReceived" runat="server" Text="Finished Received" 
                                    TextAlign="Left" style="font-size: xx-small" />
                                &nbsp;</td>
                            <td style="text-align:left">
                                <asp:Button ID="btnSave" runat="server" CssClass="btnStyle" 
                                    onclick="btnSave_Click" Text="Save" Width="70px" />
                                <asp:Button ID="btnReset" runat="server" CausesValidation="False" 
                                    CssClass="btnStyle" onclick="btnReset_Click" Text="Reset" Width="70px" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                   
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </fieldset>
    </div>

  <div class="PageDetial">

 <fieldset class="flDeatialView" style="width:98%">
 <legend class="lgView"> Purchase Received </legend> 
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>
             <input id="htxtSearch" onkeyup="filter(this, 'ctl00_MainContent_gvRequisitionList', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none" />
           <asp:Panel runat="server" ID="pnlContainer" ScrollBars="Auto" Height="200px" 
                 Width="100%">
             <asp:GridView ID="gvPurchaseOrderList" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" DataKeyNames="PURCHASE_ORDER_ID" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="99%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                  
                   
                     <asp:TemplateField HeaderText="Challan No">
                         <ItemTemplate>
                             <asp:Label ID="lblChallan" runat="server" Text='<%# Bind("CHALLAN_NO") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px"/>
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="Challan Date">
                         <ItemTemplate>
                             <asp:Label ID="lblChallanDate" runat="server" Text='<%# Bind("CHALLAN_DATE") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px"/>
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText="Purchase Date">
                         <ItemTemplate>
                             <asp:Label ID="lblPurchaseDate" runat="server" Text='<%# Bind("PURCHASE_DATE") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px" />
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>

                        <asp:TemplateField HeaderText="Godown ">
                         <ItemTemplate>
                             <asp:Label ID="lblGodownName" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px" />
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>

                   

                     <asp:TemplateField HeaderText=" Product">
                         <ItemTemplate>
                             <asp:Label ID="lblProdName" runat="server" Text='<%# Bind("PRODUCT_NAME") %>'></asp:Label>
                         </ItemTemplate>
                          <HeaderStyle Width="60px" />
                          <ItemStyle Width="60px" />
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText=" Quantity">
                         <ItemTemplate>
                             <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                         </ItemTemplate>
                          <HeaderStyle Width="60px" />
                          <ItemStyle Width="60px" />
                     </asp:TemplateField>

                     <asp:TemplateField HeaderText=" Unit">
                         <ItemTemplate>
                             <asp:Label ID="lblUnit1" runat="server" Text='<%# Bind("UNIT_OF_MEASUREMENT") %>'></asp:Label>
                         </ItemTemplate>
                          <HeaderStyle Width="60px" />
                          <ItemStyle Width="60px" />
                     </asp:TemplateField>

                       <asp:TemplateField HeaderText="Money">
                         <ItemTemplate>
                             <asp:Label ID="lblHatMony1" runat="server" Text='<%# Bind("HAT_MONEY") %>'></asp:Label>
                         </ItemTemplate>
                         <HeaderStyle Width="80px" />
                         <ItemStyle Width="80px" />
                     </asp:TemplateField>

                     <asp:TemplateField ShowHeader="False">
                         <ItemTemplate>
                             <asp:ImageButton ID="imgDelete" runat="server" 
                                 ImageUrl="~/App_Themes/Green/Images/Delete.gif" 
                                 onclick="imgDelete_Click" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                     <asp:TemplateField>
                         <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                         </EditItemTemplate>
                         <ItemTemplate>
                             <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" 
                                 ImageUrl="~/App_Themes/Green/Images/print-button.png" 
                                 onclick="ImageButton1_Click" CausesValidation="False" />
                         </ItemTemplate>
                         <ItemStyle Width="15px" />
                     </asp:TemplateField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
           </asp:Panel>
         </ContentTemplate>
     </asp:UpdatePanel>
    
 </fieldset>

</div>

</asp:Content>
