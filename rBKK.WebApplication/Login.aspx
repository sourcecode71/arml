﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterLogin.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="rBKK.WebApplication.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 
        <table align="center" style="height:490px; width: 326px;">
            <tr>
                <td>
                <fieldset class="GroupObjects">
                <legend class="lgView">Sign In</legend>
                    <table style="width:300px; border:0px solid #CFD0CC">
                        
                        <tr>
                            <td align="left"><span>User ID </span></td>
                            <td align="left">
                                <asp:TextBox ID="txtUserName" runat="server" Width="150px" 
                                    AutoCompleteType="Disabled">Admin</asp:TextBox>
                             
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                            ControlToValidate="txtUserName" ErrorMessage="*" 
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                             
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><span>Password</span></td>
                            <td align="left">
                             
                                <asp:TextBox ID="txtPassword" runat="server" Width="150px" TextMode="Password"></asp:TextBox>
                             
                            
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                            ControlToValidate="txtPassword" ErrorMessage="*" 
                                    ForeColor="Red"></asp:RequiredFieldValidator>
                             
                            
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                             
                               
                                        &nbsp;</td>
                            <td align="center" style="text-align: left">
                             
                               
                                    <asp:Button ID="btnSignIn" runat="server" CssClass="btnStyle" Text="Sign In" 
                                        onclick="btnSign_Click"/>

                                        <asp:Button ID="btnReset" runat="server" CssClass="btnStyle" 
                                        Text="Reset" onclick="btnReset_Click" CausesValidation="False" />
                                        
                                
                             
                            
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <asp:Label ID="lblLoginMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>
                 </fieldset>
                </td>
            </tr>
        </table>

</asp:Content>