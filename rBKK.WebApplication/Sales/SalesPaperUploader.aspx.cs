﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rBKK.WebApplication.Common;
using Ninject;
using rBKK.Core.Inventory;
using System.IO;

namespace rBKK.WebApplication.Sales
{
    public partial class SalesPaperUploader : System.Web.UI.Page
    {
        [Inject]
        public IMasterInventoryServise _masterInventoryServise { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            ((rBKK.WebApplication.Site)Master).CheckPageAuthentication(((int)PagesName.SalesPaperAttachment).ToString());

            if (!Page.IsPostBack)
            {
                //var a = 0;
                //var c = 33 / a;
                FillChallanListGridView("10");
            }
        }

        protected void rbtSelect_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton imbtn = sender as RadioButton;
                int rowINdx = ((GridViewRow)imbtn.Parent.NamingContainer).RowIndex;
                string strID = Convert.ToString(((Label)gvChallanList.Rows[rowINdx].Cells[1].FindControl("lblID")).Text.ToString());
                hfID.Value = strID;
                Session["SALES_ID"] = strID;

                DirectoryInfo folder = new DirectoryInfo(Server.MapPath("~/SalesFileUpload/"));
                FileInfo[] files = folder.GetFiles(strID+"_*", SearchOption.AllDirectories);
                if (files.Count() > 0)
                    lblUploadedFileList.Text = "Already Uploaded File List:<br/>";
                else
                    lblUploadedFileList.Text = string.Empty;

                foreach (var fi in files.ToList())
                {
                    lblUploadedFileList.Text += fi.Name + " <br/>";
                }
            }
            catch (Exception ex)
            {
                ((rBKK.WebApplication.Site)Master).ShowMessage(ex.Message, Common.MessageType.Error);
            }
        }

        private void FillChallanListGridView(string TransType)
        {
            IQueryable<object> lst = _masterInventoryServise.GetMasterInventoryData(TransType);
            gvChallanList.DataSource = lst;
            gvChallanList.DataBind();
        }

        protected void btnUploder_Click(object sender, EventArgs e)
        {
            try
            {
                string id = Session["SALES_ID"].ToString();
                lblSavedStratus.Text = string.Empty;
                string msg = "Saved files: ";
                HttpFileCollection multipleFiles = Request.Files;
                for (int fileCount = 0; fileCount < multipleFiles.Count; fileCount++)
                {
                    HttpPostedFile uploadedFile = multipleFiles[fileCount];
                    string fileName = Path.GetFileName(uploadedFile.FileName);

                    fileName = id + "_" + fileCount + "_" + fileName;

                    if (uploadedFile.ContentLength > 0)
                    {
                        uploadedFile.SaveAs(Server.MapPath("~/SalesFileUpload/") + fileName);
                        //lblSavedStratus.Text += fileName + " Saved <BR>";
                        msg += fileName + " &nbsp;&nbsp; ";
                        ((rBKK.WebApplication.Site)Master).ShowMessage("Sales Id: " + id + " &nbsp;&nbsp;" + msg, Common.MessageType.Info);
                    }
                }

                _masterInventoryServise.UpdateSalesPaperStatus(id, "Y");
                this.FillChallanListGridView("10");
            }
            catch (Exception ex)
            {
                lblSavedStratus.Text = ex.Message;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            lblSavedStratus.Text = string.Empty;
            ((rBKK.WebApplication.Site)Master).ShowMessage("", Common.MessageType.Info);
        }
    }
}