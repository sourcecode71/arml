﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SalesPaperUploader.aspx.cs" Inherits="rBKK.WebApplication.Sales.SalesPaperUploader" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

     function toggleSelectionGrid(source) {
         var isChecked = source.checked;
         $("#<%=gvChallanList.ClientID %> input[id*='rbtSelect']").each(function (index) {
             $(this).attr('checked', false);

         });
         source.checked = isChecked;
     }
   </script>
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div align="left" style="background-color:WhiteSmoke;width:87%">
    <div>
    <div> 
        <table style="width: 627px; height: 148px;" cellspacing="0px"; 
            cellpadding="0px">
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Driver Licence" Width="100px"></asp:Label>
                  </td>
                <td>
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="btnStyle" /> 
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="7" valign="top">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                      <asp:Label ID="lblUploadedFileList" runat="server" ForeColor="Green" Height="134px" Width="250px"></asp:Label> 
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Truck Licence"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload2" runat="server" CssClass="btnStyle" /> <br /> 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Licence"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload3" runat="server" CssClass="btnStyle" /> <br /> 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Licence"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload5" runat="server" CssClass="btnStyle" /> <br /> 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Licence"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload6" runat="server" CssClass="btnStyle" /> <br /> 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    </td>
                <td class="style1">
                    <asp:Button ID="btnReset" runat="server" onclick="btnReset_Click" 
                        Text="Reset" CssClass="btnStyle" Width="100px" /> 
                    <asp:Button ID="btnUploder" runat="server" onclick="btnUploder_Click" 
                        Text="Upload Files" CssClass="btnStyle" Width="100px" /> 
                </td>
                <td class="style1">
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblSavedStratus" runat="server" ForeColor="Green"></asp:Label> 
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div> 
</div>
    <asp:HiddenField ID="hfID" runat="server" />
</div>

<div class="PageDetial">
 <fieldset class="flDeatialView" style="width:90%" >
 <legend class="lgView"> Previous Transaction list </legend>
        
         <input id="htxtSearch" onkeyup="filter(this, 'ctl00_MainContent_gvChallanList', '2')" 
                onfocus="GotFocus(this)" onblur="LostFocus(this)"  maxlength="12" type="text" 
                value="Search..." style="background-color: White; font-family: 'Times New Roman', Times, serif; 
                 font-size: 10px; color: Green; font-weight: bold; font-variant: normal; border-style:none" />
            
         <asp:Panel runat="server" ID="pnlContainer" ScrollBars="Auto" Height="200px" Width="100%">
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
             <ContentTemplate>
             <asp:GridView ID="gvChallanList" runat="server" AutoGenerateColumns="False" 
                 CellPadding="4" ForeColor="#333333" GridLines="None" 
                 style="font-size: 9pt" Width="100%">
                 <HeaderStyle CssClass="dataScheduleHeader" />
                 <AlternatingRowStyle BackColor="White" />
                 <Columns>
                     <asp:TemplateField>
                         <ItemTemplate>
                             <asp:RadioButton ID="rbtSelect" runat="server" AutoPostBack="True" 
                                 oncheckedchanged="rbtSelect_CheckedChanged" 
                                 onclick="toggleSelectionGrid(this);" />
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ID">
                         <ItemTemplate>
                             <asp:Label ID="lblID" runat="server" Text='<%# Bind("INV_MASTER_ID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Transaction Type">
                         
                         <ItemTemplate>
                             <asp:Label ID="lblType" runat="server" 
                                 Text='<%# Bind("REQUISITION_TYPE_NAME") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:BoundField DataField="CHALLAN_NO" HeaderText="Issue No" />
                     <asp:BoundField DataField="CHALLAN_DATE" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}"/>
                     <asp:BoundField DataField="REMARKS" HeaderText="Remarks" />
                    
                     <asp:BoundField DataField="SALES_PAPER_UPLODED_YN" 
                         HeaderText="Sales Paper Uploded">
                     <HeaderStyle Width="110px" />
                     <ItemStyle HorizontalAlign="Center" />
                     </asp:BoundField>
                 </Columns>
                 <EditRowStyle BackColor="#2461BF" />
                 <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                 <RowStyle BackColor="#EFF3FB" />
                 <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                 <SortedAscendingCellStyle BackColor="#F5F7FB" />
                 <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                 <SortedDescendingCellStyle BackColor="#E9EBEF" />
                 <SortedDescendingHeaderStyle BackColor="#4870BE" />
             </asp:GridView>
             </ContentTemplate>
             </asp:UpdatePanel>
             </asp:Panel>
 </fieldset>

</div>
</asp:Content>
