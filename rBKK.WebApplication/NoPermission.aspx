﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NoPermission.aspx.cs" Inherits="rBKK.WebApplication.NoPermission" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<fieldset>
<legend class="lgView">No Permission</legend>
<span style="font-size:16pt; color:Green"> You do not have permission to view this page.</span>
</fieldset>
</asp:Content>
