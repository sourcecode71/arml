﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Web.UI;
using rBKK.WebApplication.Reports;


namespace rBKK.WebApplication
{
    public class ReportManager
    {
        public static void ShowReport (Control control,Type type,string sReportFileName,string sReportDataSourceName,DataTable reportDataSourceValue)
        {
            //SessionManager.GetReportDataTable = reportDataSourceValue;
            string script = string.Format(@"window.open('CommonReportsViewer.aspx?ReportFileName={0}&&DataSourceName={1}','CommonReportViewer',
                                            'width=1000,height=650,resizable=1,scrollbars=1');", sReportFileName, sReportDataSourceName);
            ScriptManager.RegisterStartupScript(control, type, Guid.NewGuid().ToString(), script, true);
        }

        
    }
}
