﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using rBKK.Entities;

namespace rBKK.WebApplication.Reports
{
    public class SessionManager
    {
        public static List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetReportDataTable
        {
            get
            {
                if (HttpContext.Current.Session.Contents["GetReportDataTable"] == null)
                    return null;
                else return (List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result>)HttpContext.Current.Session.Contents["GetReportDataTable"];
            }
            set
            {
                HttpContext.Current.Session.Contents["GetReportDataTable"] = value;
            }

        }

        public static List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> GetOrderReportDataTable
        {
            get
            {
                if (HttpContext.Current.Session.Contents["GetReportDataTable"] == null)
                    return null;
                else return (List<FN_PUR_PURCHASE_ORDER_REPORTS_Result>)HttpContext.Current.Session.Contents["GetReportDataTable"];
            }
            set
            {
                HttpContext.Current.Session.Contents["GetReportDataTable"] = value;
            }

        }

        public static List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> GetPurchaseDataTable
        {
            get
            {
                if (HttpContext.Current.Session.Contents["GetReportDataTable"] == null)
                    return null;
                else return (List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result>)HttpContext.Current.Session.Contents["GetReportDataTable"];
            }
            set
            {
                HttpContext.Current.Session.Contents["GetReportDataTable"] = value;
            }

        }


        public static List<FN_RPT_INV_RICE_PRODUCTIONS_Result> GetRPTSalesDataTable
        {
            get
            {
                if (HttpContext.Current.Session.Contents["GetReportDataTable"] == null)
                    return null;
                else return (List<FN_RPT_INV_RICE_PRODUCTIONS_Result>)HttpContext.Current.Session.Contents["GetReportDataTable"];
            }
            set
            {
                HttpContext.Current.Session.Contents["GetReportDataTable"] = value;
            }
        }
    }
}