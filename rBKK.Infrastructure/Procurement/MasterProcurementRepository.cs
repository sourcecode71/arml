﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using System.Collections;
using System.Transactions;
using System.Data;

namespace rBKK.Infrastructure.Procurement
{
    public class MasterProcurementRepository : IMasterProcurementRepository
    {
        private ARMLEntities Context = new ARMLEntities();

        #region Hannan   //////       
        public List<PUR_REQUISITION_TYPE> GetPurchaseType()
        {
            try
            {
                var result = Context.PUR_REQUISITION_TYPE.OrderBy(d => d.REQUISITION_TYPE_ID);
                return result.ToList<PUR_REQUISITION_TYPE>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PUR_PADDY_PURCHASE_TYPE> GetPaddyPurchaseType()
        {
            try
            {
                var result = Context.PUR_PADDY_PURCHASE_TYPE.OrderBy(d => d.PADDY_PURCHASE_TYPE_NAME);
                return result.ToList<PUR_PADDY_PURCHASE_TYPE>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PUR_VENDOR> GetVendorByTypeId(string VendorTypeId)
        {
            try
            {
                int endorTypeId = Convert.ToInt32(VendorTypeId);
                var result = Context.PUR_VENDOR.Where(d => d.VENDOR_TYPE_ID == endorTypeId).OrderBy(d => d.VENDOR_NAME);
                return result.ToList<PUR_VENDOR>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PUR_CONSUMPTION_TYPE> GetAllConsumptionTypes()
        {
            try
            {
                var result = Context.PUR_CONSUMPTION_TYPE.OrderBy(d => d.CONSUMPTION_TYPE_NAME);
                return result.ToList<PUR_CONSUMPTION_TYPE>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        public void SavePaddyRequisition()
        {
            PUR_PURCHASE_REQUISITION pr = new PUR_PURCHASE_REQUISITION();
            pr.REQUISITION_TYPE_ID = "1";
            PUR_PURCHASE_REQUISITION_DETAILS prd = new PUR_PURCHASE_REQUISITION_DETAILS();
            prd.DISCOUNT_PC = 66;
            PUR_PURCHASE_REQUISITION_BOSTA prb = new PUR_PURCHASE_REQUISITION_BOSTA();
            prb.BOSTA_QUANTITY = 33;

            //prd.PUR_PURCHASE_REQUISITION = pr;
            //prb.PUR_PURCHASE_REQUISITION = pr;
            //context.Parents.Attach(parent); // just attach as unchanged
            //context.Childs.Add(child); // add as inserted
            //parent.Childs.Add(child); // make connection between existing and inserted object
            //context.SaveChanges();

            pr.PUR_PURCHASE_REQUISITION_DETAILS.Add(prd);
            pr.PUR_PURCHASE_REQUISITION_BOSTA.Add(prb);

            //Context.PUR_PURCHASE_REQUISITION.Attach(pr);
            //Context.PUR_PURCHASE_REQUISITION_DETAILS.Attach(prd);
            //Context.PUR_PURCHASE_REQUISITION_BOSTA.Attach(prb);
            //pr.PUR_PURCHASE_REQUISITION_DETAILS.ad
            //prd.PUR_PURCHASE_REQUISITION.


            Context.PUR_PURCHASE_REQUISITION.AddObject(pr);
            int ret = Context.SaveChanges();
            long key = Context.PUR_PURCHASE_REQUISITION.Max(d => d.PURCHASE_REQUISITION_ID);
        }
        public string SavePaddyRequisition(PUR_PURCHASE_REQUISITION req, List<PUR_PURCHASE_REQUISITION_DETAILS> lreqDetails, List<PUR_PURCHASE_REQUISITION_BOSTA> lbag)
        {
            using (TransactionScope transaction = new TransactionScope())
            {
                try
                {
                    var result = Context.FN_PUR_ADD_REQUISITION_MASTER(req.REQUISITION_TYPE_ID, req.FACTORY_ID
                        , req.REQUISITION_DATE
                        , req.VENDOR_ID
                        , req.PAY_TO
                        , req.AMOUNT
                        , req.PADDY_PURCHASE_TYPE_ID
                        , req.MC_PURCHASE_TYPE
                        , req.COMMETNS
                        , req.PREPARED_BY
                        , req.SET_USER).SingleOrDefault();

                    Int64 key = Convert.ToInt64(result.ToString());

                    foreach (var rd in lreqDetails.ToList())
                    {
                        rd.PURCHASE_REQUISITION_ID = key;
                        Context.PUR_PURCHASE_REQUISITION_DETAILS.AddObject(rd);
                    }

                    foreach (var bag in lbag.ToList())
                    {
                        bag.PURCHASE_REQUISITION_ID = key;
                        Context.PUR_PURCHASE_REQUISITION_BOSTA.AddObject(bag);
                    }

                    Context.SaveChanges();
                    //long key = Context.PUR_PURCHASE_REQUISITION.Max(d => d.PURCHASE_REQUISITION_ID);

                    Context.AcceptAllChanges();
                    transaction.Complete();

                    return key.ToString();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                    throw ex;
                }

                finally
                {
                    //Context.Connection.Dispose();
                }
            }
        }
        public List<FN_PUR_GET_REQUISITION_LIST_Result> GetRequisitionsList()
        {
            return Context.FN_PUR_GET_REQUISITION_LIST().ToList<FN_PUR_GET_REQUISITION_LIST_Result>();
        }
        public Hashtable LoadPurchaseRequisitionOrderDetails(string RequisitionOrderID)
        {
            try
            {
                Hashtable ht = new Hashtable();
                Int64 RequisitionOrderId=Convert.ToInt64(RequisitionOrderID);
                PUR_PURCHASE_REQUISITION req = Context.PUR_PURCHASE_REQUISITION.Where(d => d.PURCHASE_REQUISITION_ID == RequisitionOrderId).FirstOrDefault();
                ht.Add("PUR_PURCHASE_REQUISITION", req);

                List<PUR_PURCHASE_REQUISITION_DETAILS> lstReqDetails = Context.PUR_PURCHASE_REQUISITION_DETAILS.Where(d => d.PURCHASE_REQUISITION_ID == RequisitionOrderId).ToList<PUR_PURCHASE_REQUISITION_DETAILS>();
                ht.Add("PUR_PURCHASE_REQUISITION_DETAILS", lstReqDetails);

                List<PUR_PURCHASE_REQUISITION_BOSTA> lstReqBag = Context.PUR_PURCHASE_REQUISITION_BOSTA.Where(d => d.PURCHASE_REQUISITION_ID == RequisitionOrderId).ToList<PUR_PURCHASE_REQUISITION_BOSTA>();
                ht.Add("PUR_PURCHASE_REQUISITION_BOSTA", lstReqBag);

                return ht;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IQueryable<object> GetAllVendorTypes()
        {
            var result = from vt in Context.PUR_VENDOR_TYPE
                         join pr in Context.PUR_REQUISITION_TYPE
                         on vt.PARENT_ID equals pr.REQUISITION_TYPE_ID
                         select new { vt.VENDOR_TYPE_ID,vt.VENDOR_TYPE,pr.REQUISITION_TYPE_ID,pr.REQUISITION_TYPE_NAME };

            return result;
        }
        public List<PUR_VENDOR_TYPE> GetVendorTypeByGenericId(string requisitionType)
        {
            var result = Context.PUR_VENDOR_TYPE.Where(d => d.PARENT_ID == requisitionType).OrderBy(d => d.VENDOR_TYPE);
            return result.ToList<PUR_VENDOR_TYPE>();
        }
        public void SaveProductionData(INV_INVENTORY_MASTER invMaster, List<INV_INVENTORY_DETAILS> invDetailsFrom, List<INV_INVENTORY_DETAILS> invDetailsTo, string loggedUserID)
        {

            using (TransactionScope transaction = new TransactionScope())
            {
                try
                {
                    if (invMaster.ReceiveYN==true)
                    {
                        INV_INVENTORY_MASTER inv = (from items in Context.INV_INVENTORY_MASTER
                                                    where items.INV_MASTER_ID == invMaster.INV_MASTER_ID
                                               select items).FirstOrDefault();
                        inv.ReceiveYN = invMaster.ReceiveYN;
                    }

                    foreach (INV_INVENTORY_DETAILS iid in invDetailsFrom.ToList())
                    {
                        invMaster.INV_INVENTORY_DETAILS.Add(iid);
                        Context.FN_INV_UPDATE_CURRENT_STOCK(iid.GODOWN_ID, iid.PRODUCT_ID, iid.UNIT_OF_MEASUREMENT_ID, iid.QUANTITY * iid.MULTIPLICATION_FACTOR, loggedUserID);
                    }
                    foreach (INV_INVENTORY_DETAILS iid in invDetailsTo.ToList())
                    {
                        invMaster.INV_INVENTORY_DETAILS.Add(iid);
                        Context.FN_INV_UPDATE_CURRENT_STOCK(iid.GODOWN_ID, iid.PRODUCT_ID, iid.UNIT_OF_MEASUREMENT_ID, iid.QUANTITY * iid.MULTIPLICATION_FACTOR, loggedUserID);
                    }
                    Context.INV_INVENTORY_MASTER.AddObject(invMaster);
                    Context.SaveChanges();
                   
                    Context.AcceptAllChanges();
                    transaction.Complete();
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                    throw ex;
                }
                finally
                {
                    Context.Connection.Dispose();
                }
            }

        }
        #endregion

        #region Mostafiz ////
        #region Vendor Type
        public PUR_VENDOR_TYPE GetVendorTypeByID(string VendorTypeId)
        {
            try
            {
                int endorTypeId = Convert.ToInt32(VendorTypeId);
                var result = Context.PUR_VENDOR_TYPE.Where(v => v.VENDOR_TYPE_ID == endorTypeId).OrderBy(v => v.VENDOR_TYPE);
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<PUR_VENDOR_TYPE> GetAllVendorType()
        {
            try
            {
                var result = Context.PUR_VENDOR_TYPE.OrderBy(d => d.VENDOR_TYPE);
                return result.ToList<PUR_VENDOR_TYPE>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddVendorType(PUR_VENDOR_TYPE vendorType, string TransType)
        {
            try
            {
                if (TransType == "Save")
                {
                    Context.PUR_VENDOR_TYPE.AddObject(vendorType);
                }
                else
                {
                    PUR_VENDOR_TYPE vendor = (from items in Context.PUR_VENDOR_TYPE
                                              where items.VENDOR_TYPE_ID == vendorType.VENDOR_TYPE_ID
                                              select items).FirstOrDefault();
                    vendor.VENDOR_TYPE = vendorType.VENDOR_TYPE;
                }

                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteVendorType(string strVendorTypeId)
        {
            try
            {
                int endorTypeId = Convert.ToInt32(strVendorTypeId);
                var result = Context.PUR_VENDOR_TYPE.Where(d => d.VENDOR_TYPE_ID == endorTypeId).OrderBy(d => d.VENDOR_TYPE);
                Context.DeleteObject(result.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Vendor

        public PUR_VENDOR GetVendorByID(string VendorTypeId)
        {
            try
            {
                int endorTypeId = Convert.ToInt32(VendorTypeId);
                var result = Context.PUR_VENDOR.Where(v => v.VENDOR_ID == endorTypeId).OrderBy(v => v.VENDOR_NAME);
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<PUR_VENDOR> GetAllVendor()
        {
            try
            {
                var result = Context.PUR_VENDOR.OrderBy(d => d.VENDOR_NAME);
                return result.ToList<PUR_VENDOR>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddVendorType(PUR_VENDOR vendor, string TransType)
        {
            try
            {
                if (TransType == "Save")
                {
                    Context.PUR_VENDOR.AddObject(vendor);
                }
                else
                {
                    PUR_VENDOR vendors = (from items in Context.PUR_VENDOR
                                          where items.VENDOR_ID == vendor.VENDOR_ID
                                          select items).FirstOrDefault();
                    vendors.VENDOR_NAME = vendor.VENDOR_NAME;
                    vendors.VENDOR_CODE = vendor.VENDOR_CODE;
                    vendors.PUR_VENDOR_TYPE = vendor.PUR_VENDOR_TYPE;
                    vendors.EMAIL_ID = vendor.EMAIL_ID;
                    vendors.ADDRESS_ID = vendor.ADDRESS_ID;
                }

                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteVendor(string strVendorId)
        {
            try
            {
                int endorTypeId = Convert.ToInt32(strVendorId);
                var result = Context.PUR_VENDOR.Where(d => d.VENDOR_ID == endorTypeId);
                Context.DeleteObject(result.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
        public string SavePaddyPurchaseOrder(PUR_PURCHASE_ORDER ord, List<PUR_PURCHASE_ORDER_DETAILS> lordDetails, bool loggedUserID)
        {
            try
            {
                var result = Context.FN_PUR_ADD_PURCHASE_MASTER(
                     ord.CHALLAN_NO
                    , ord.VENDOR_ID
                    , ord.CHALLAN_DATE
                    , ord.CHALLAN_PREAPARED_BY
                    , ord.PURCHASE_DATE
                    , ord.HAT_MONEY
                    , ord.COMMENTS
                    , ord.PURCHASE_REQUISITION_ID
                    , ord.REQUISITION_TYPE_ID
                    , ord.PADDY_PURCHASE_TYPE_ID
                    , ord.MC_PURCHASE_TYPE
                    , ord.COMPANY_ID
                    , ord.SET_USER).SingleOrDefault();

                Int64 key = Convert.ToInt64(result.ToString());

                foreach (var rd in lordDetails.ToList())
                {
                    rd.PURCHASE_ORDER_ID = key;
                    Context.PUR_PURCHASE_ORDER_DETAILS.AddObject(rd);
                }


                if (loggedUserID)
                {
                    PUR_PURCHASE_REQUISITION pur_Requisition = (from items in Context.PUR_PURCHASE_REQUISITION
                                                                where items.PURCHASE_REQUISITION_ID == ord.PURCHASE_REQUISITION_ID
                                                                select items).FirstOrDefault();
                    pur_Requisition.ReceivedFinish = true;
                }
                         



                Context.SaveChanges();
                //long key = Context.PUR_PURCHASE_REQUISITION.Max(d => d.PURCHASE_REQUISITION_ID);

                return key.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SavePartialReceivedFinish(string strID)
        {
            try
            {
                Int64 inID = Convert.ToInt64(strID);
                PUR_PURCHASE_REQUISITION pur_Requisition = (from items in Context.PUR_PURCHASE_REQUISITION
                                                            where items.PURCHASE_REQUISITION_ID == inID
                                                            select items).FirstOrDefault();

                pur_Requisition.ReceivedFinish = true;
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        #endregion
        public List<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetRequsionReportsData(string strRequsionId)
        {
            PUR_PURCHASE_REQUISITION pur = new PUR_PURCHASE_REQUISITION();
            pur.PURCHASE_REQUISITION_ID =Convert.ToInt64(strRequsionId);
            return Context.FN_PUR_PURCHASE_REQUISTION_REPORTS(pur.PURCHASE_REQUISITION_ID).ToList<FN_PUR_PURCHASE_REQUISTION_REPORTS_Result>();
        }
        public List<FN_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequisionOrderReportsData(string intOrderId)
        {
            return Context.FN_PUR_PURCHASE_ORDER_REPORTS(intOrderId).ToList<FN_PUR_PURCHASE_ORDER_REPORTS_Result>();
        }
        public List<FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result> GetRequisionOrderReportsData(int rType, DateTime dtLDate, DateTime dtUDate)
        {
            return Context.FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS(rType, dtLDate, dtUDate).ToList < FN_DATEWISE_PUR_PURCHASE_ORDER_REPORTS_Result>();
        }
        public List<FN_RPT_INV_RICE_PRODUCTIONS_Result> GetProductionReportsData(string intOrderId)
        {
            return Context.FN_RPT_INV_RICE_PRODUCTIONS(Convert.ToInt64(intOrderId)).ToList<FN_RPT_INV_RICE_PRODUCTIONS_Result>();
        }


        public List<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result> GetPSTData(int typeId, DateTime dtLDate, DateTime dtUDate)
        {
            return Context.FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS(dtLDate, dtUDate, typeId.ToString()).ToList<FN_RPT_INV_DATEWISE_RICE_PRODUCTIONS_Result>();
        }
        public List<FN_RPT_INV_CURRENT_STOCK_Result> GetCurrentStock(int stockId)
        {
            return Context.FN_RPT_INV_CURRENT_STOCK(stockId).ToList < FN_RPT_INV_CURRENT_STOCK_Result>();
        }
        public List<FN_PUR_GET_REQUISITION_Partial_Received_LIST_Result> Get_REQUISITION_PARTIAL_RECEIVED_LIST(long intRequisionId)
        {
            return Context.FN_PUR_GET_REQUISITION_Partial_Received_LIST(intRequisionId).ToList<FN_PUR_GET_REQUISITION_Partial_Received_LIST_Result>();
        }

        public List<FN_PUR_GET_PURCHASEORDERLIST_LIST_Result> GetPurchaseOrderList()
        {
            return Context.FN_PUR_GET_PURCHASEORDERLIST_LIST().ToList<FN_PUR_GET_PURCHASEORDERLIST_LIST_Result>();
        }


        public List<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result> GetDateWiseRequision(DateTime dtLDate, DateTime dtUDate)
        {
            return Context.FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS(dtLDate, dtUDate).ToList<FN_RPT_DATEWISE_PUR_PURCHASE_REQUISTION_REPORTS_Result>();
        }
    }
}
