﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;
using System.Collections;

namespace rBKK.Infrastructure.Inventory
{

    public class MasterInventoryRepository : IMasterInventoryRepository
    {
        private ARMLEntities Context = new ARMLEntities();

        #region Mostaq  ////

        #region Product Category
        public INV_PRODUCT_CATEGORY GetCategoryByID(string catId)
        {
            int ID = Convert.ToInt32(catId);
            var result = from items in Context.INV_PRODUCT_CATEGORY
                         where items.CATEGORY_ID == ID
                         select items;
            return result.FirstOrDefault();
        }
        public List<INV_PRODUCT_CATEGORY> GetAllCategory()
        {
            var result = from items in Context.INV_PRODUCT_CATEGORY
                         select items;
            return result.ToList<INV_PRODUCT_CATEGORY>();
        }
        public void AddProductType(INV_PRODUCT_CATEGORY productCategory, string TranType)
        {
            if (TranType == "Save")
            {
                Context.INV_PRODUCT_CATEGORY.AddObject(productCategory);
            }
            else
            {
                INV_PRODUCT_CATEGORY productCat = (from items in Context.INV_PRODUCT_CATEGORY
                                                   where items.CATEGORY_ID == productCategory.CATEGORY_ID
                                                   select items).FirstOrDefault();
                productCat.CATEGORY_NAME = productCategory.CATEGORY_NAME;

            }
            Context.SaveChanges();

        }
        public void DeleteProductType(string catId)
        {
            try
            {
                int ID = Convert.ToInt32(catId);
                var result = from items in Context.INV_PRODUCT_CATEGORY
                             where items.CATEGORY_ID == ID
                             select items;
                Context.DeleteObject(result.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        #endregion

        #region Product

        public INV_PRODUCT GetProductByID(string prodId)
        {
            int ID = Convert.ToInt32(prodId);
            var results = from items in Context.INV_PRODUCT
                          where items.PRODUCT_ID == ID
                          select items;
            return results.FirstOrDefault();

        }
        public IQueryable<object> GetProductByCatID(string strcategory)
        {
            int ID = Convert.ToInt32(strcategory);
            var result = from prod in Context.INV_PRODUCT
                         join cat in Context.INV_PRODUCT_CATEGORY
                         on prod.CATEGORY_ID equals cat.CATEGORY_ID
                         join u in Context.INV_UNIT_OF_MEASUREMENT
                         on prod.SIUnitId equals u.UNIT_OF_MEASUREMENT_ID
                         where prod.CATEGORY_ID == ID
                         select new { prod.PRODUCT_ID, prod.PRODUCT_NAME, cat.CATEGORY_NAME, prod.SIUnitId, prod.UpdatePrice, u.UNIT_OF_MEASUREMENT, prod.SET_DATE};

            return result;
        }
        public IQueryable<object> GetAllProduct()
        {

            var results = from prod in Context.INV_PRODUCT
                          join cat in Context.INV_PRODUCT_CATEGORY
                          on prod.CATEGORY_ID equals cat.CATEGORY_ID
                          join u in Context.INV_UNIT_OF_MEASUREMENT
                          on prod.SIUnitId equals u.UNIT_OF_MEASUREMENT_ID
                          select new { prod.PRODUCT_ID, prod.PRODUCT_NAME, cat.CATEGORY_NAME, prod.SIUnitId, prod.UpdatePrice, u.UNIT_OF_MEASUREMENT };
            return results;

        }
        public void AddProduct(INV_PRODUCT products, string TranType)
        {

            if (TranType == "Save")
            {
                Context.INV_PRODUCT.AddObject(products);
            }
            else
            {
                INV_PRODUCT product = (from items in Context.INV_PRODUCT
                                       where items.PRODUCT_ID == products.PRODUCT_ID
                                       select items).FirstOrDefault();
                products.CATEGORY_ID = products.CATEGORY_ID;
                product.PRODUCT_NAME = products.PRODUCT_NAME;
                product.SIUnitId = products.SIUnitId;
            }

            Context.SaveChanges();
        }
        public void DeleteProduct(string prodId)
        {
            try
            {
                int ID = Convert.ToInt32(prodId);
                var result = from items in Context.INV_PRODUCT
                             where items.PRODUCT_ID == ID
                             select items;
                Context.DeleteObject(result.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public void UpdateProductPrice(List<INV_PRODUCT> prodInfos)
        {
            foreach (INV_PRODUCT prod in prodInfos)
            {
                INV_PRODUCT products = (from items in Context.INV_PRODUCT
                                       where items.PRODUCT_ID == prod.PRODUCT_ID
                                       select items).FirstOrDefault();

                products.UpdatePrice = prod.UpdatePrice;
                products.SET_DATE = prod.SET_DATE;
                products.SET_USER = prod.SET_USER;
            }
            Context.SaveChanges();
        }

        #endregion

        #region Machinery Type
        public INV_MACHINERY_TYPE GetMachineryTypeByID(string TypeID)
        {
            int ID = Convert.ToInt32(TypeID);
            var result = from items in Context.INV_MACHINERY_TYPE
                         where items.MACHINERY_TYPE_ID == ID
                         select items;
            return result.FirstOrDefault();


        }
        public List<INV_MACHINERY_TYPE> GetAllMachneryType()
        {
            var result = from items in Context.INV_MACHINERY_TYPE select items;
            return result.ToList<INV_MACHINERY_TYPE>();

        }
        public void AddMachineryTYpe(INV_MACHINERY_TYPE machineryType, string transType)
        {
            if (transType == "Save")
            {
                Context.INV_MACHINERY_TYPE.AddObject(machineryType);
            }
            else
            {
                INV_MACHINERY_TYPE Machinery = (from items in Context.INV_MACHINERY_TYPE
                                                where items.MACHINERY_TYPE_ID == machineryType.MACHINERY_TYPE_ID
                                                select items).FirstOrDefault();
                Machinery.MACHINERY_TYPE = machineryType.MACHINERY_TYPE;

            }
            Context.SaveChanges();
        }
        public void DeleteMachineyType(string TypeID)
        {
            int ID = Convert.ToInt32(TypeID);

            var result = from items in Context.INV_MACHINERY_TYPE
                         where items.MACHINERY_TYPE_ID == ID
                         select items;
            Context.DeleteObject(result.FirstOrDefault());
            Context.SaveChanges();
        }
        #endregion

        #region Machinery Info

        public INV_MACHINERY GetMachineryByID(string prodId)
        {
            int ID = Convert.ToInt32(prodId);
            var result = from items in Context.INV_MACHINERY
                         where items.MACHINERY_ID == ID
                         select items;

            return result.FirstOrDefault();
        }

        public IQueryable<object> GetMachineryByTypeID(string strcategory)
        {
            throw new NotImplementedException();

        }

        public IQueryable<object> GetAllMachinery()
        {
            throw new NotImplementedException();
        }

        public void AddMachinery(INV_PRODUCT products, string TranType)
        {
            throw new NotImplementedException();
        }

        public void DeleteMachinery(string prodId)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region MeasurementUnit

        public INV_UNIT_OF_MEASUREMENT GetUnitOfMeasurmentTById(string catId)
        {
            int ID = Convert.ToInt32(catId);
            var result = from items in Context.INV_UNIT_OF_MEASUREMENT
                         where items.UNIT_OF_MEASUREMENT_ID == ID
                         select items;
            return result.FirstOrDefault();
        }
        public List<INV_UNIT_OF_MEASUREMENT> GetAllUNIT_OF_MEASUREMENT()
        {
            var result = from items in Context.INV_UNIT_OF_MEASUREMENT select items;
            return result.ToList<INV_UNIT_OF_MEASUREMENT>();
        }
        public void AddUNIT_OF_MEASUREMENT(INV_UNIT_OF_MEASUREMENT untiOfMeasurment, string TranType)
        {
            if (TranType == "Save")
            {
                Context.INV_UNIT_OF_MEASUREMENT.AddObject(untiOfMeasurment);
            }
            else
            {
                INV_UNIT_OF_MEASUREMENT measurementUnit = (from items in Context.INV_UNIT_OF_MEASUREMENT
                                                           where items.UNIT_OF_MEASUREMENT_ID == untiOfMeasurment.UNIT_OF_MEASUREMENT_ID
                                                           select items).FirstOrDefault();
                measurementUnit.UNIT_OF_MEASUREMENT = untiOfMeasurment.UNIT_OF_MEASUREMENT;

            }
            Context.SaveChanges();
        }
        public void DeleteUNIT_OF_MEASUREMENT(string catId)
        {
            int ID = Convert.ToInt32(catId);
            var result = from items in Context.INV_UNIT_OF_MEASUREMENT
                         where items.UNIT_OF_MEASUREMENT_ID == ID
                         select items;
            Context.DeleteObject(result.FirstOrDefault());
            Context.SaveChanges();
        }

        #endregion

        #region Factory Info
        public INV_FACTORY GetInvFactoryById(string catId)
        {
            try
            {
                int ID = Convert.ToInt32(catId);
                var result = from items in Context.INV_FACTORY
                             where items.FACTORY_ID == ID
                             select items;
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public List<INV_FACTORY> GetAllInvFactory()
        {
            try
            {
                var result = from items in Context.INV_FACTORY select items;
                return result.ToList<INV_FACTORY>();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void AddInvFactory(INV_FACTORY iNV_FACTORY, string TranType)
        {
            try
            {
                if (TranType == "Save")
                {
                    Context.INV_FACTORY.AddObject(iNV_FACTORY);
                }
                else
                {
                    INV_FACTORY inv_factoryInfo = (from items in Context.INV_FACTORY
                                                   where items.FACTORY_ID == iNV_FACTORY.FACTORY_ID
                                                   select items).FirstOrDefault();
                    inv_factoryInfo.NAME = iNV_FACTORY.NAME;
                    inv_factoryInfo.ADDRESS = iNV_FACTORY.ADDRESS;

                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public void DeleteInvFactory(string FactoryId)
        {
            try
            {
                int ID = Convert.ToInt32(FactoryId);
                var result = from items in Context.INV_GODOWN
                             where items.GODOWN_ID == ID
                             select items;
                Context.DeleteObject(result.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        #endregion

        #region Godown
        public IQueryable<object> GetInvGodownByFct()
        {
            var result = from godown in Context.INV_GODOWN
                         join unit in Context.INV_UNIT_OF_MEASUREMENT
                         on godown.UNIT_OF_MEASUREMENT equals unit.UNIT_OF_MEASUREMENT_ID
                         select new
                         {
                             godown.GODOWN_ID,
                             godown.NAME,
                             godown.ADDRESS
                             ,
                             godown.CAPACITY,
                             unit.UNIT_OF_MEASUREMENT
                         };

            return result;

        }

        public INV_GODOWN GetInvGodownById(string GodownID)
        {
            int ID = Convert.ToInt32(GodownID);
            var results = from items in Context.INV_GODOWN
                          where items.GODOWN_ID == ID
                          select items;
            return results.FirstOrDefault();
        }

        public List<INV_GODOWN> GetAllInvGodown()
        {

            var results = from items in Context.INV_GODOWN
                          orderby items.NAME
                          select items;
            return results.ToList<INV_GODOWN>();
        }

        public void AddInvGodown(INV_GODOWN inv_Godown, string TranType)
        {
            if (TranType == "Save")
            {
                Context.INV_GODOWN.AddObject(inv_Godown);
            }
            else
            {
                INV_GODOWN Godown = (from items in Context.INV_GODOWN
                                     where items.GODOWN_ID == inv_Godown.GODOWN_ID
                                     select items).FirstOrDefault();
                Godown.NAME = inv_Godown.NAME;
                Godown.ADDRESS = inv_Godown.ADDRESS;
                Godown.UNIT_OF_MEASUREMENT = inv_Godown.UNIT_OF_MEASUREMENT;
                Godown.CAPACITY = inv_Godown.CAPACITY;
            }

            Context.SaveChanges();
        }

        public void DeleteInvGodown(string GodownID)
        {
            int ID = Convert.ToInt32(GodownID);
            var result = from items in Context.INV_GODOWN
                         where items.GODOWN_ID == ID
                         select items;
            Context.DeleteObject(result.FirstOrDefault());
            Context.SaveChanges();
        }
        #endregion

        #region FactoryGodown Info
        public IQueryable<object> GetInvFactoryGodown()
        {
            try
            {
                var results = from fctgodown in Context.INV_FACTORY_GODOWN
                              join fct in Context.INV_FACTORY
                              on fctgodown.FACTORY_ID equals fct.FACTORY_ID
                              join gdw in Context.INV_GODOWN
                              on fctgodown.GODOWN_ID equals gdw.GODOWN_ID
                              select new { fctgodown.FACTORY_GODOWN_ID, fct.FACTORY_ID, factoryname = fct.NAME, gdw.GODOWN_ID, godownname = gdw.NAME };

                return results;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public INV_FACTORY_GODOWN GetInvFactoryGodownById(string FactoryGodownID)
        {
            try
            {
                int ID = Convert.ToInt32(FactoryGodownID);
                var results = from items in Context.INV_FACTORY_GODOWN
                              where items.FACTORY_GODOWN_ID == ID
                              select items;
                return results.FirstOrDefault();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddInvFactoryGodown(INV_FACTORY_GODOWN inv_FactoryGodown, string TranType)
        {
            try
            {
                if (TranType == "Save")
                {
                    Context.INV_FACTORY_GODOWN.AddObject(inv_FactoryGodown);
                }
                else
                {
                    INV_FACTORY_GODOWN fctGodown = (from items in Context.INV_FACTORY_GODOWN
                                                    where items.FACTORY_GODOWN_ID == inv_FactoryGodown.FACTORY_GODOWN_ID
                                                    select items).FirstOrDefault();
                    fctGodown.FACTORY_ID = inv_FactoryGodown.FACTORY_ID;
                    fctGodown.GODOWN_ID = inv_FactoryGodown.GODOWN_ID;

                }
                Context.SaveChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteInvFactoryGodown(string GodownFactoryId)
        {
            try
            {
                int ID = Convert.ToInt32(GodownFactoryId);
                var results = from items in Context.INV_FACTORY_GODOWN
                              where items.FACTORY_GODOWN_ID == ID
                              select items;
                Context.DeleteObject(results.FirstOrDefault());
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<INV_GODOWN> GetAllInvGodownForFCTMapping()
        {
            try
            {
                var results = from items in Context.INV_GODOWN
                              where !(from gvFWID in Context.INV_FACTORY_GODOWN
                                      select gvFWID.GODOWN_ID).Contains(items.GODOWN_ID)
                              orderby items.NAME
                              select items;
                return results.ToList<INV_GODOWN>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region BankInfo
        public List<CORE_BANK> GetAllCoreBank()
        {
            try
            {
                var result = from items in Context.CORE_BANK select items;
                return result.ToList<CORE_BANK>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CORE_BANK GetCoreBankById(string CoreBankID)
        {
            try
            {
                int ID = Convert.ToInt32(CoreBankID);
                var result = Context.CORE_BANK.Where(b => b.BANK_ID == ID);
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddCoreBankinfo(CORE_BANK core_BANK, string TranType)
        {

            try
            {
                if (TranType == "SAVE")
                {
                    Context.CORE_BANK.AddObject(core_BANK);
                }
                else
                {
                    CORE_BANK c_Bank = Context.CORE_BANK.Where(b => b.BANK_ID == core_BANK.BANK_ID).FirstOrDefault();
                    c_Bank.BANK_NAME = core_BANK.BANK_NAME;
                    c_Bank.SYMBOLIC_NAME = core_BANK.SYMBOLIC_NAME;
                    c_Bank.ADDRESS = core_BANK.ADDRESS;
                    c_Bank.WEB_ADDRESS = core_BANK.WEB_ADDRESS;

                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCoreBankInfo(string CoreBankID)
        {
            try
            {
                int ID = Convert.ToInt32(CoreBankID);
                CORE_BANK c_Bank = Context.CORE_BANK.Where(b => b.BANK_ID == ID).FirstOrDefault();
                Context.DeleteObject(c_Bank);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        } 
        #endregion

        #region Branch Info

        public IQueryable<object> GetAllBranchDeatils()
        {

            var results = from bank in Context.CORE_BANK
                          join branch in Context.CORE_BANK_BRANCH
                          on bank.BANK_ID equals branch.BANK_ID
                          orderby bank.SYMBOLIC_NAME,branch.BRANCH_NAME
                          select new { bank.BANK_NAME, bank.BANK_ID, branch.BRANCH_ID,
                                       branch.BRANCH_NAME,
                                       branch.CONTACT_PERSON,
                                       branch.EMAIL,
                                       branch.PHONE,
                                       branch.ADDRESS,
                                       branch.FAX,
                                       BANK_BRANCH_NAME = bank.SYMBOLIC_NAME+" - "+branch.BRANCH_NAME
                          };
            return results;

        }

        public List<CORE_BANK_BRANCH> GetAllCoreBranch()
        {
            try
            {
                var result = from items in Context.CORE_BANK_BRANCH select items;
                return result.ToList<CORE_BANK_BRANCH>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CORE_BANK_BRANCH GetCoreBranchById(string CoreBranchID)
        {
            try
            {
                int ID = Convert.ToInt32(CoreBranchID);
                var result = Context.CORE_BANK_BRANCH.Where(b => b.BRANCH_ID == ID);
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddCoreBranchInfo(CORE_BANK_BRANCH core_Branch, string TranType)
        {
            try
            {
                if (TranType == "SAVE")
                {
                    Context.CORE_BANK_BRANCH.AddObject(core_Branch);
                }
                else
                {
                    CORE_BANK_BRANCH c_Branch = Context.CORE_BANK_BRANCH.Where(b => b.BRANCH_ID == core_Branch.BRANCH_ID).FirstOrDefault();
                    c_Branch.BRANCH_NAME = core_Branch.BRANCH_NAME;
                    c_Branch.BANK_ID = core_Branch.BANK_ID;
                    c_Branch.CONTACT_PERSON = core_Branch.CONTACT_PERSON;
                    c_Branch.ADDRESS = core_Branch.ADDRESS;
                    c_Branch.FAX = core_Branch.FAX;
                    c_Branch.PHONE = core_Branch.PHONE;
                    c_Branch.EMAIL = core_Branch.EMAIL;

                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteCoreBranchInfo(string CoreBranchID)
        {
            try
            {
                int ID = Convert.ToInt32(CoreBranchID);
                CORE_BANK_BRANCH c_Branch = Context.CORE_BANK_BRANCH.Where(b => b.BRANCH_ID == ID).FirstOrDefault();
                Context.DeleteObject(c_Branch);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        
        #endregion

        #region Account
        public List<CORE_BANK_ACCOUNTS> GetAllCoreAccount()
        {
            try
            {
                var result = from items in Context.CORE_BANK_ACCOUNTS select items;
                return result.ToList<CORE_BANK_ACCOUNTS>();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public CORE_BANK_ACCOUNTS GetCoreAccountById(string CoreAccountID)
        {
            try
            {
                var result = Context.CORE_BANK_ACCOUNTS.Where(b => b.ACCOUNT_NO == CoreAccountID);
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IQueryable<object> GetACCOUNTINFO()
        {

            var results = from Acc in Context.CORE_BANK_ACCOUNTS
                          join Branch in Context.CORE_BANK_BRANCH
                          on Acc.BRANCH_ID equals Branch.BRANCH_ID
                          join bank in Context.CORE_BANK
                          on Acc.CORE_BANK_BRANCH.CORE_BANK.BANK_ID equals bank.BANK_ID
                          orderby bank.SYMBOLIC_NAME, Branch.BRANCH_NAME, Acc.ACC_OPEN_DATE
                          select new { Acc.ACCOUNT_NO, Acc.BRANCH_ID, Acc.ACCOUNT_HOLDER, Acc.ACC_OPEN_DATE
                              ,BRANCH_NAME=bank.SYMBOLIC_NAME+" - "+ Branch.BRANCH_NAME, bank.BANK_NAME };
            return results;

        }

        public void AddCoreAccountInfo(CORE_BANK_ACCOUNTS core_Account, string TranType)
        {
            try
            {
                if (TranType == "SAVE")
                {
                    Context.CORE_BANK_ACCOUNTS.AddObject(core_Account);
                }
                else
                {
                    CORE_BANK_ACCOUNTS c_Accounts = Context.CORE_BANK_ACCOUNTS.Where(b => b.ACCOUNT_NO == core_Account.ACCOUNT_NO).FirstOrDefault();
                    c_Accounts.ACC_OPEN_DATE = core_Account.ACC_OPEN_DATE;
                    c_Accounts.ACCOUNT_HOLDER = core_Account.ACCOUNT_HOLDER;
                    c_Accounts.BRANCH_ID = core_Account.BRANCH_ID;
                    c_Accounts.ACCOUNT_NO = core_Account.ACCOUNT_NO;


                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteAccountInfo(string core_ACCID)
        {
            try
            {
                CORE_BANK_ACCOUNTS c_Account = Context.CORE_BANK_ACCOUNTS.Where(b => b.ACCOUNT_NO == core_ACCID).FirstOrDefault();
                Context.DeleteObject(c_Account);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<CORE_BANK_ACCOUNTS> GetBankAccountByBankId(string bankId)
        {
            try
            {
                if (!string.IsNullOrEmpty(bankId))
                {
                    Int32 intBankId = Convert.ToInt32(bankId);
                    var result = from items in Context.CORE_BANK_ACCOUNTS
                                 where items.CORE_BANK_BRANCH.BANK_ID == intBankId
                                 select items;
                    return result.ToList<CORE_BANK_ACCOUNTS>();
                }
                else
                {
                    var result = from items in Context.CORE_BANK_ACCOUNTS
                                 select items;
                    return result.ToList<CORE_BANK_ACCOUNTS>();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Direct Client List

        public List<PUR_VENDOR> GetAllDirectClientList()
        {
            var results = Context.PUR_VENDOR.OrderBy(d => d.VENDOR_NAME);
            return results.ToList<PUR_VENDOR>();
        }

        #endregion

        #endregion

        #region Hannan  ///////
        public List<INV_FACTORY> GetAllFactories()
        {
            var results = Context.INV_FACTORY.OrderBy(d => d.NAME);
            return results.ToList<INV_FACTORY>();
        }
        public List<INV_PRODUCT_CATEGORY> GetProductCategoryByParentId(string ParentId)
        {
            var results = Context.INV_PRODUCT_CATEGORY.Where(d => d.PARRENT_CAT_ID == ParentId).OrderBy(d => d.CATEGORY_NAME);
            return results.ToList<INV_PRODUCT_CATEGORY>();
        }
        public IQueryable<object> GetProductCategoryByParentIdWithAlias(string ParentId)
        {
            var results = from itms in Context.INV_PRODUCT_CATEGORY.Where(d => d.PARRENT_CAT_ID == ParentId).OrderBy(d => d.CATEGORY_NAME)
                          select new { MACHINE_CATEGORY_ID = itms.CATEGORY_ID, CATEGORY_NAME = itms.CATEGORY_NAME };

            return results;
        }
        public IQueryable<object> GetProductByGenericId(string genericId)
        {
            var result = from p in Context.INV_PRODUCT
                         join pc in Context.INV_PRODUCT_CATEGORY
                         on p.CATEGORY_ID equals pc.CATEGORY_ID
                         join g in Context.PUR_REQUISITION_TYPE
                          on pc.PARRENT_CAT_ID equals g.REQUISITION_TYPE_ID
                         where g.REQUISITION_TYPE_ID == genericId
                         select new { p.PRODUCT_ID, p.PRODUCT_NAME }
                         ;
            return result;
        }
        public IQueryable<object> GetAllCategoryWithGenericName()
        {
            var result = from pc in Context.INV_PRODUCT_CATEGORY
                         join pr in Context.PUR_REQUISITION_TYPE
                          on pc.PARRENT_CAT_ID equals pr.REQUISITION_TYPE_ID
                         select new { pc.CATEGORY_ID, pc.CATEGORY_NAME, pr.REQUISITION_TYPE_ID, pr.REQUISITION_TYPE_NAME };
            return result;
        }
        public IQueryable<object> GetAllPaddyRice()
        {
            var result = from p in Context.INV_PRODUCT
                         join pc in Context.INV_PRODUCT_CATEGORY
                         on p.CATEGORY_ID equals pc.CATEGORY_ID
                         where pc.PARRENT_CAT_ID == "1" || pc.PARRENT_CAT_ID == "3"  //1=paddy; 3=rice
                         orderby pc.PARRENT_CAT_ID,p.PRODUCT_NAME 
                         select new { p.PRODUCT_ID, p.PRODUCT_NAME }
                         ;
            return result;
        }
        public IQueryable<object> GetMasterInventoryData(string TransType)
        {
            var result = from m in Context.INV_INVENTORY_MASTER
                         join r in Context.PUR_REQUISITION_TYPE
                         on m.REQUISITION_TYPE_ID equals r.REQUISITION_TYPE_ID
                         where m.TRANS_COMPLETE_YN == "0" && r.REQUISITION_TYPE_ID == TransType
                         orderby r.REQUISITION_TYPE_NAME, m.CHALLAN_DATE descending
                         select new { m.INV_MASTER_ID, m.CHALLAN_NO, m.CHALLAN_DATE, m.REMARKS, r.REQUISITION_TYPE_NAME,m.SALES_PAPER_UPLODED_YN };
            return result;
        }
        #region LoanType ///
        public List<INV_LOAN_TYPE> GetAllLoanTypes()
        {
            return Context.INV_LOAN_TYPE.OrderBy(d => d.LOAN_TYPE).ToList<INV_LOAN_TYPE>();
        }
        public List<CORE_BANK_BRANCH> GetBankBranchByBankId(string bankId)
        {
            int id=Convert.ToInt32(bankId);
            return Context.CORE_BANK_BRANCH.Where(d => d.BANK_ID == id).OrderBy(d => d.BRANCH_NAME).ToList<CORE_BANK_BRANCH>();
        }
        public void AddLoanDetails(INV_LOAN_DETAILS objLD, string OperationTypes, string loggedUserId)
        {
            if (OperationTypes == OperationType.SAVE.ToString())
            {
                Context.INV_LOAN_DETAILS.AddObject(objLD);
            }
            else if (OperationTypes == OperationType.UPDATE.ToString())
            {
                INV_LOAN_DETAILS obj = Context.INV_LOAN_DETAILS.Where(d => d.LOAN_ID == objLD.LOAN_ID).FirstOrDefault();
                obj.LOAN_CODE=objLD.LOAN_CODE;
                obj.LOAN_TYPE_ID=objLD.LOAN_TYPE_ID;
                obj.ACCOUNT_NO=objLD.ACCOUNT_NO;
                obj.CREDIT_LIMIT=objLD.CREDIT_LIMIT;
                obj.INTEREST_RATE=objLD.INTEREST_RATE;
                obj.EFFECTIVE_DATE=objLD.EFFECTIVE_DATE;
                obj.EXPIRY_DATE=objLD.EXPIRY_DATE;
                obj.REMARKS=objLD.REMARKS;
                obj.SET_USER=objLD.SET_USER;
                obj.SET_DATE = objLD.SET_DATE;
                //Context.SaveChanges();
            }

            Context.SaveChanges();
        }
        public IQueryable<object> GetLoanDetails()
        {
            var result = from ld in Context.INV_LOAN_DETAILS
                         join lt in Context.INV_LOAN_TYPE
                         on ld.LOAN_TYPE_ID equals lt.LOAN_TYPE_ID
                         join bb in Context.CORE_BANK_ACCOUNTS
                         on ld.ACCOUNT_NO equals bb.ACCOUNT_NO
                         select new
                         {   ld.LOAN_ID,
                             ld.LOAN_CODE,
                             ld.LOAN_TYPE_ID,
                             ld.ACCOUNT_NO,
                             ld.CREDIT_LIMIT,
                             ld.INTEREST_RATE,
                             ld.EFFECTIVE_DATE,
                             ld.EXPIRY_DATE,
                             lt.LOAN_TYPE,
                             bb.ACCOUNT_HOLDER };
            return result;
        }
        public INV_LOAN_DETAILS GetLoanDetailsByID(string strLoanID)
        {
            //int loanId = Convert.ToInt32(strLoanID);
            var result = from ld in Context.INV_LOAN_DETAILS
                         join ba in Context.CORE_BANK_ACCOUNTS
                          on ld.ACCOUNT_NO equals ba.ACCOUNT_NO
                         //where ld.LOAN_ID == loanId
                         where ld.ACCOUNT_NO == strLoanID
                         select new 
                         {
                             ld.LOAN_ID,
                             ld.LOAN_CODE,
                             ld.LOAN_TYPE_ID,
                             ld.ACCOUNT_NO,
                             ld.CREDIT_LIMIT,
                             ld.INTEREST_RATE,
                             ld.EFFECTIVE_DATE,
                             ld.EXPIRY_DATE,
                             ld.REMARKS,
                             ba.ACCOUNT_HOLDER
                         };

             List<INV_LOAN_DETAILS> list = result.AsEnumerable()
                                      .Select(ld => new INV_LOAN_DETAILS
                                      {
                                          LOAN_ID = ld.LOAN_ID,
                                          LOAN_CODE = ld.LOAN_CODE,
                                          LOAN_TYPE_ID = ld.LOAN_TYPE_ID,
                                          ACCOUNT_NO = ld.ACCOUNT_NO,
                                          CREDIT_LIMIT = ld.CREDIT_LIMIT,
                                          INTEREST_RATE = ld.INTEREST_RATE,
                                          EFFECTIVE_DATE = ld.EFFECTIVE_DATE,
                                          EXPIRY_DATE = ld.EXPIRY_DATE,
                                          REMARKS = ld.REMARKS,
                                          ACCOUNT_HOLDER = ld.ACCOUNT_HOLDER
                                      }).ToList();

             return list.FirstOrDefault();
        }
        public List<INV_LOAN_DETAILS> GetAllLoans()
        {
            return Context.INV_LOAN_DETAILS.OrderBy(d => d.ACCOUNT_NO).ToList<INV_LOAN_DETAILS>();
        }
        #endregion

        public decimal GetCurrentStock(int GodownId, int ProductId)
        {
            var iReturnValue = Context.FN_INV_GET_CURRENT_STOCK_BY_ID(GodownId, ProductId).SingleOrDefault();

            return Convert.ToDecimal(iReturnValue);

        }
        public decimal GetSIRateByUnitId(int UnitId)
        {
            var result = Context.INV_UNIT_OF_MEASUREMENT.Where(d => d.UNIT_OF_MEASUREMENT_ID == UnitId).SingleOrDefault();

            return Convert.ToDecimal(result.SI_UNIT_CONVERSION_RATE);
        }
        public decimal GetProductPriceById(string ProductId)
        {
            int pid = Convert.ToInt32(ProductId);
            var result = Context.INV_PRODUCT.Where(d => d.PRODUCT_ID == pid).SingleOrDefault();

            return Convert.ToDecimal(result.UpdatePrice);
        }
        public List<INV_GODOWN> GetOnlyFactoryGodowns()
        {
            var result = from g in Context.INV_GODOWN
                         join fg in Context.INV_FACTORY_GODOWN
                         on g.GODOWN_ID equals fg.GODOWN_ID
                         where fg.IS_FACTORY_GODOWN == true
                         select new { g.GODOWN_ID, g.NAME };

            List<INV_GODOWN> lst = result.AsEnumerable()
                .Select(ig => new INV_GODOWN
                {
                    NAME = ig.NAME,
                    GODOWN_ID = ig.GODOWN_ID
                }).ToList();

            return lst;
        }
        public void UpdateSalesPaperStatus(string SalesId, string Status)
        {
            Int64 id=Convert.ToInt64(SalesId);
            INV_INVENTORY_MASTER result = Context.INV_INVENTORY_MASTER.Where(d => d.INV_MASTER_ID == id).FirstOrDefault();

            result.SALES_PAPER_UPLODED_YN = Status;
            Context.SaveChanges();
        }
        #endregion
        
    }
        
    
}
