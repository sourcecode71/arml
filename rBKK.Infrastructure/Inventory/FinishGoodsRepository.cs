﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;

namespace rBKK.Infrastructure.Inventory
{
    public class FinishGoodsRepository : IFinishGoodsRepository
    {
        private ARMLEntities Context = new ARMLEntities();

        public List<INV_INVENTORY_DETAILS> GetAllProductionGoods(string strTypeId)
        {
            try
            {
               
                var result = from inv in Context.INV_INVENTORY_DETAILS
                             join invm in Context.INV_INVENTORY_MASTER
                             on inv.INV_MASTER_ID equals invm.INV_MASTER_ID 
                             join prod in Context.INV_PRODUCT
                             on inv.PRODUCT_ID equals prod.PRODUCT_ID
                             join unit in Context.INV_UNIT_OF_MEASUREMENT
                             on inv.UNIT_OF_MEASUREMENT_ID equals unit.UNIT_OF_MEASUREMENT_ID
                             join fct in Context.INV_FACTORY
                             on inv.GODOWN_ID equals fct.FACTORY_ID into fctg
                             from f in fctg.DefaultIfEmpty()
                             join gdw in Context.INV_GODOWN
                             on inv.GODOWN_ID2 equals gdw.GODOWN_ID into
                             gdwn
                             from g in gdwn.DefaultIfEmpty()
                             where invm.REQUISITION_TYPE_ID == strTypeId && inv.FROM_TO_PLEDGE_ID == "2" && invm.ReceiveYN==false
                             orderby inv.INV_DETAILS_ID descending

                             select new
                             {
                                 invm.INV_MASTER_ID,
                                 invm.CHALLAN_NO,
                                 inv.QUANTITY,
                                 inv.RATE,
                                 invm.CHALLAN_DATE,
                                 unit.UNIT_OF_MEASUREMENT,
                                 prod.PRODUCT_NAME,
                                 FACTORYNAME = (f == null ? string.Empty : f.NAME),
                                 GODOWNNAME = (g == null ? string.Empty : g.NAME),
                                 inv.GODOWN_ID
                             };

                List<INV_INVENTORY_DETAILS> list = result.AsEnumerable()
                                      .Select(inv => new INV_INVENTORY_DETAILS
                                      {
                                          INV_MASTER_ID=inv.INV_MASTER_ID,
                                          CHALLAN_NO = inv.CHALLAN_NO,
                                          CHALLAN_DATE = inv.CHALLAN_DATE,
                                          PRODUCT_NAME = inv.PRODUCT_NAME,
                                          QUANTITY = inv.QUANTITY,
                                          RATE = inv.RATE,
                                          UNIT_OF_MEASUREMENT = inv.UNIT_OF_MEASUREMENT,
                                          FACTORYNAME = inv.FACTORYNAME,
                                          GODOWNNAME = inv.GODOWNNAME,
                                          GODOWN_ID=inv.GODOWN_ID

                                      }).ToList();
                return list;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        public List<INV_INVENTORY_DETAILS> GetAllFinishGoods(string strTypeId)
        {
            try
            {
                var result = from inv in Context.INV_INVENTORY_DETAILS
                             join invm in Context.INV_INVENTORY_MASTER
                             on inv.INV_MASTER_ID equals invm.INV_MASTER_ID
                             join prod in Context.INV_PRODUCT
                             on inv.PRODUCT_ID equals prod.PRODUCT_ID
                             join unit in Context.INV_UNIT_OF_MEASUREMENT
                             on inv.UNIT_OF_MEASUREMENT_ID equals unit.UNIT_OF_MEASUREMENT_ID
                             join fct in Context.INV_FACTORY 
                             on inv.GODOWN_ID equals fct.FACTORY_ID into fctg 
                             from f in fctg.DefaultIfEmpty()
                             join gdw in Context.INV_GODOWN 
                             on inv.GODOWN_ID2 equals gdw.GODOWN_ID into
                             gdwn from g in gdwn.DefaultIfEmpty()
                             orderby invm.CHALLAN_DATE descending
                             where invm.REQUISITION_TYPE_ID == strTypeId && inv.FROM_TO_PLEDGE_ID == "2" 
                           
                             select new
                             {
                                 invm.INV_MASTER_ID,
                                 invm.CHALLAN_NO,
                                 inv.QUANTITY,
                                 inv.RATE,
                                 invm.CHALLAN_DATE,
                                 unit.UNIT_OF_MEASUREMENT,
                                 prod.PRODUCT_NAME,
                                 FACTORYNAME = (f == null ? string.Empty : f.NAME),
                                 GODOWNNAME=(g==null?string.Empty:g.NAME)
                             };

                List<INV_INVENTORY_DETAILS> list = result.AsEnumerable()
                                      .Select(inv => new INV_INVENTORY_DETAILS
                                      {
                                          INV_MASTER_ID = inv.INV_MASTER_ID,
                                          CHALLAN_NO = inv.CHALLAN_NO,
                                          CHALLAN_DATE = inv.CHALLAN_DATE,
                                          PRODUCT_NAME = inv.PRODUCT_NAME,
                                          QUANTITY = inv.QUANTITY,
                                          RATE = inv.RATE,
                                          UNIT_OF_MEASUREMENT = inv.UNIT_OF_MEASUREMENT,
                                          FACTORYNAME=inv.FACTORYNAME,
                                          GODOWNNAME = inv.GODOWNNAME

                                      }).ToList();
                return list;

            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }

        public List<INV_INVENTORY_DETAILS> GetAllFinishGoodsReceivedByID(Int64 intInvMID)
        {
            try
            {
                var result = from inv in Context.INV_INVENTORY_DETAILS
                             join invm in Context.INV_INVENTORY_MASTER
                             on inv.INV_MASTER_ID equals invm.INV_MASTER_ID
                             join prod in Context.INV_PRODUCT
                             on inv.PRODUCT_ID equals prod.PRODUCT_ID
                             join unit in Context.INV_UNIT_OF_MEASUREMENT
                             on inv.UNIT_OF_MEASUREMENT_ID equals unit.UNIT_OF_MEASUREMENT_ID
                             join fct in Context.INV_FACTORY
                             on inv.GODOWN_ID equals fct.FACTORY_ID into fctg
                             from f in fctg.DefaultIfEmpty()
                             join gdw in Context.INV_GODOWN
                             on inv.GODOWN_ID2 equals gdw.GODOWN_ID into
                             gdwn
                             from g in gdwn.DefaultIfEmpty()
                             orderby invm.CHALLAN_DATE descending
                             where invm.INV_MASTER_Parent_ID == intInvMID && inv.FROM_TO_PLEDGE_ID == "2" && invm.ReceiveYN==false

                             select new
                             {
                                 invm.INV_MASTER_ID,
                                 invm.CHALLAN_NO,
                                 inv.QUANTITY,
                                 inv.RATE,
                                 invm.CHALLAN_DATE,
                                 unit.UNIT_OF_MEASUREMENT,
                                 prod.PRODUCT_NAME,
                                 FACTORYNAME = (f == null ? string.Empty : f.NAME),
                                 GODOWNNAME = (g == null ? string.Empty : g.NAME)
                             };

                List<INV_INVENTORY_DETAILS> list = result.AsEnumerable()
                                      .Select(inv => new INV_INVENTORY_DETAILS
                                      {
                                          INV_MASTER_ID = inv.INV_MASTER_ID,
                                          CHALLAN_NO = inv.CHALLAN_NO,
                                          CHALLAN_DATE = inv.CHALLAN_DATE,
                                          PRODUCT_NAME = inv.PRODUCT_NAME,
                                          QUANTITY = inv.QUANTITY,
                                          RATE = inv.RATE,
                                          UNIT_OF_MEASUREMENT = inv.UNIT_OF_MEASUREMENT,
                                          FACTORYNAME = inv.FACTORYNAME,
                                          GODOWNNAME = inv.GODOWNNAME

                                      }).ToList();
                return list;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
    }
}
