﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;

namespace rBKK.Infrastructure.Inventory
{
   public interface IFinishGoodsRepository
    {
        List<INV_INVENTORY_DETAILS> GetAllProductionGoods(string strTypeId);
        List<INV_INVENTORY_DETAILS> GetAllFinishGoods(string strTypeId);
        List<INV_INVENTORY_DETAILS> GetAllFinishGoodsReceivedByID(Int64 intInvMID);
    }
}
