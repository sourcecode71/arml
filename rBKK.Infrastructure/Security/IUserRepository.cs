﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;

namespace rBKK.Infrastructure.Security
{
    public interface IUserRepository
    {
        List<SEC_USER> GetAllUsers();
        void AddUser(SEC_USER objSecUser, string setUser);

        #region Mostaq  ////
        SEC_USER GetUserByID(string strUserId);
        #endregion


        #region Han  ///////
        SEC_USER GetUserInfoById(string UserId);
        List<COM_MODULE> GetAllModules();
        //List<COM_MODULE> GetAllModulesByUserID(string UserId);
        List<SEC_GROUP> GetAllUserGroups();
        string CheckUserAuthentication(string UserId, string Password);
        string CheckUserPagePermission(string UserId, string PageId);
        void SaveUserSession(string strGuid, string strUserId, string strPassword, string strHostIP);
        List<FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID_Result> GetGroupPermissionByUserId(string UserId);
        void AddUser(SEC_USER objUser, List<SEC_USER_GROUP> lstUserGroup, string OperationType, string SetUser);
        List<FN_SEC_GET_OBJECTS_PERMISSION_BY_GROUP_ID_Result> GetPageObjectsByUserGroupId(string groupID);
        void AddGroupObjectsPermission(List<SEC_OBJECTS_GROUP_PERMISSION> lstGroupPermission, string groupId, string UserId);
        void AddUserPermissionsOverride(List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> lstUserPermission, string UserId);
        List<FN_SEC_GET_USER_OBJECTS_PERMISSION_BY_USER_ID_Result> GetPageObjectsByUserId(string UserId);
        string GetGroupsNameByUserId(string UserId);
        #endregion
    }
}
