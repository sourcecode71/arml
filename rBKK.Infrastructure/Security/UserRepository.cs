﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rBKK.Entities;

namespace rBKK.Infrastructure.Security
{
    public enum OperationTypes { SAVE, UPDATE }

    public class UserRepository:IUserRepository
    {
        private ARMLEntities Context = new ARMLEntities();

        public List<SEC_USER> GetAllUsers()
        {

            var results = from items in Context.SEC_USER
                          select items;
           // var res = Context.SEC_USER.Where(d => d.FIRST_NAME == "han").OrderBy(d => d.LAST_NAME).ToList();

            return results.ToList<SEC_USER>();
        }

        public void AddUser(SEC_USER objSecUser, string setUser)
        {
            Context.FN_SEC_ADD_USER(objSecUser.USER_ID
                , objSecUser.TITLE
                , objSecUser.FIRST_NAME
                , objSecUser.LAST_NAME
                , objSecUser.CELL_PHONE
                , objSecUser.EMAIL
                , objSecUser.ENCRIPTED_PASSWORD
                , objSecUser.RAW_PASSWORD
                , objSecUser.ACTIVE_YN
                , objSecUser.SET_USER
                , objSecUser.SET_DATE);
                
        }

        #region Mostaq  /////    
        public SEC_USER GetUserByID(string strUserId)
        {
            var result = from item in Context.SEC_USER
                         where item.USER_ID == strUserId
                         select item;

            return result.FirstOrDefault();
        }
        #endregion


        #region Han  ///////
        //public SEC_USER GetUserGroup(string UserId)
        //{
        //    var result = from u in Context.SEC_USER
        //                 join ug in Context.SEC_USER_GROUP on u.USER_ID equals ug.USER_ID
        //                 join g in Context.SEC_GROUP on ug.GROUP_ID equals g.GROUP_ID
        //                 where u.USER_ID == UserId
        //                 select new { USER_ID = u.USER_ID, GROUP_NAME = g.GROUP_NAME };
            

        //    return Enumerable.FirstOrDefault(result);
        //}

        public SEC_USER GetUserInfoById(string UserId)
        {
            var result = from items in Context.SEC_USER
                         where items.USER_ID == UserId
                         select items;
            return result.ToList().FirstOrDefault();
        }
        public List<COM_MODULE> GetAllModules()
        {
            var results = from items in Context.COM_MODULE
                          select items;

            return results.ToList();
        }
        
        public List<SEC_GROUP> GetAllUserGroups()
        {
           var results = from items in Context.SEC_GROUP
                          select items;

            return results.ToList();
        }
        public string CheckUserAuthentication(string UserId, string Password)
        {
            string iReturnValue = Context.USP_SEC_CHECK_USER_AUTHENTICATION(UserId, Password).SingleOrDefault();
            return iReturnValue;
        }
        public string CheckUserPagePermission(string UserId, string PageId)
        {
            string iReturnValue = Context.USP_SEC_CHECK_USER_PERMISSION(UserId, PageId).SingleOrDefault();
            return iReturnValue;
        }
        public void SaveUserSession(string strGuid, string strUserId, string strPassword, string strHostIP)
        {
            SEC_LOGIN_HISTORY logHis = Context.SEC_LOGIN_HISTORY.CreateObject();
            logHis.GUID = strGuid;
            logHis.USER_ID = strUserId;
            logHis.PASSWORD = strPassword;
            logHis.HOST_IP = strHostIP;
            logHis.HOST_NAME = strHostIP;
            //logHis.LOGIN_TIME = DateTime.Now;
            logHis.SET_USER = strUserId;
            Context.SEC_LOGIN_HISTORY.AddObject(logHis);
            Context.SaveChanges();
        }
        public List<FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID_Result> GetGroupPermissionByUserId(string UserId)
        {
            try
            {
                var result = Context.FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID(UserId);
                return result.ToList<FN_SEC_GET_GROUP_PERMISSION_BY_USER_ID_Result>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddUser(SEC_USER objUser, List<SEC_USER_GROUP> lstUserGroup, string OperationType, string SetUser)
        {
            try
            {
                if (OperationType == OperationTypes.SAVE.ToString())
                {
                    Context.SEC_USER.AddObject(objUser);
                    //Context.SaveChanges();
                    foreach (SEC_USER_GROUP ug in lstUserGroup.ToList())
                    {
                        Context.SEC_USER_GROUP.AddObject(ug);
                    }
                    Context.SaveChanges();
                }
                if (OperationType == OperationTypes.UPDATE.ToString())
                {
                    SEC_USER secUser = Context.SEC_USER.Where(d => d.USER_ID == objUser.USER_ID).FirstOrDefault();
                    secUser.TITLE = objUser.TITLE;
                    secUser.FIRST_NAME = objUser.FIRST_NAME;
                    secUser.LAST_NAME = objUser.LAST_NAME;
                    secUser.EMAIL = objUser.EMAIL;
                    secUser.CELL_PHONE = objUser.CELL_PHONE;
                    secUser.ENCRIPTED_PASSWORD = objUser.ENCRIPTED_PASSWORD;
                    secUser.RAW_PASSWORD = objUser.RAW_PASSWORD;
                    secUser.SET_USER = SetUser;
                    secUser.SET_DATE = objUser.SET_DATE;
                    secUser.ACTIVE_YN = objUser.ACTIVE_YN;
                    //Context.SaveChanges();

                    List<SEC_USER_GROUP> objUserGroup = Context.SEC_USER_GROUP.Where(d => d.USER_ID == objUser.USER_ID).ToList();
                    foreach (SEC_USER_GROUP ug in objUserGroup.ToList())
                    {
                        Context.SEC_USER_GROUP.DeleteObject(ug);
                    }
                    //Context.SaveChanges();

                    foreach (SEC_USER_GROUP ug in lstUserGroup.ToList())
                    {
                        Context.SEC_USER_GROUP.AddObject(ug);
                    }
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FN_SEC_GET_OBJECTS_PERMISSION_BY_GROUP_ID_Result> GetPageObjectsByUserGroupId(string groupID)
        {
            var result = Context.FN_SEC_GET_OBJECTS_PERMISSION_BY_GROUP_ID(groupID);
            return result.ToList<FN_SEC_GET_OBJECTS_PERMISSION_BY_GROUP_ID_Result>();
        }
        public void AddGroupObjectsPermission(List<SEC_OBJECTS_GROUP_PERMISSION> lstGroupPermission, string groupId, string setUser)
        {
            try
            {
                List<SEC_OBJECTS_GROUP_PERMISSION> objGroup = Context.SEC_OBJECTS_GROUP_PERMISSION.Where(d => d.GROUP_ID == groupId).ToList();
                foreach (SEC_OBJECTS_GROUP_PERMISSION ogp in objGroup.ToList())
                {
                    Context.SEC_OBJECTS_GROUP_PERMISSION.DeleteObject(ogp);
                }
                //Context.SaveChanges();

                foreach (SEC_OBJECTS_GROUP_PERMISSION ogp in lstGroupPermission.ToList())
                {
                    Context.SEC_OBJECTS_GROUP_PERMISSION.AddObject(ogp);
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddUserPermissionsOverride(List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> lstUserPermission, string UserId)
        {
            try
            {
                List<SEC_OBJECT_USER_PERMISSION_OVERRIDE> objUser = Context.SEC_OBJECT_USER_PERMISSION_OVERRIDE.Where(d => d.USER_ID == UserId).ToList();
                foreach (SEC_OBJECT_USER_PERMISSION_OVERRIDE ogp in objUser.ToList())
                {
                    Context.SEC_OBJECT_USER_PERMISSION_OVERRIDE.DeleteObject(ogp);
                }
                //Context.SaveChanges();

                foreach (SEC_OBJECT_USER_PERMISSION_OVERRIDE ogp in lstUserPermission.ToList())
                {
                    Context.SEC_OBJECT_USER_PERMISSION_OVERRIDE.AddObject(ogp);
                }
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FN_SEC_GET_USER_OBJECTS_PERMISSION_BY_USER_ID_Result> GetPageObjectsByUserId(string UserId)
        {
            var result = Context.FN_SEC_GET_USER_OBJECTS_PERMISSION_BY_USER_ID(UserId);
            return result.ToList<FN_SEC_GET_USER_OBJECTS_PERMISSION_BY_USER_ID_Result>();
        }
        public string GetGroupsNameByUserId(string UserId)
        {
            string groupNames=string.Empty;
            var result = from ug in Context.SEC_USER_GROUP 
                         join g in Context.SEC_GROUP on ug.GROUP_ID equals g.GROUP_ID
                         where ug.USER_ID == UserId
                         select new { GROUP_NAME = g.GROUP_NAME };

            foreach (var gn in result.ToList())
                groupNames = groupNames + " " + gn.GROUP_NAME.ToString();

            return groupNames;
        }
        #endregion

        
    }
}
