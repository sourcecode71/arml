SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SEC_ADD_USER](
@P_USER_ID					VARCHAR(5),
@P_TITLE					NCHAR(10),
@P_FIRST_NAME				NCHAR(10),
@P_LAST_NAME				NCHAR(10),
@P_CELL_PHONE				NCHAR(10),
@P_EMAIL					NCHAR(10),
@P_ENCRIPTED_PASSWORD		NCHAR(10),
@P_RAW_PASSWORD				NCHAR(10),
@P_SET_USER					NCHAR(10)
)-- TODO: name
AS
    -- TODO: content
INSERT INTO [dbo].[SEC_USER]
           ([USER_ID]
           ,[TITLE]
           ,[FIRST_NAME]
           ,[LAST_NAME]
           ,[CELL_PHONE]
           ,[EMAIL]
           ,[ENCRIPTED_PASSWORD]
           ,[RAW_PASSWORD]
           ,[SET_USER]
           ,[SET_DATE])
     VALUES
           (@P_USER_ID
		   ,@P_TITLE
		   ,@P_FIRST_NAME
		   ,@P_LAST_NAME
		   ,@P_CELL_PHONE
		   ,@P_EMAIL
		   ,@P_ENCRIPTED_PASSWORD
		   ,@P_RAW_PASSWORD
		   ,@P_SET_USER
		   ,'HAN'
		   );
GO
