CREATE TABLE [dbo].[INV_PRODUCT_CATEGORY]
(
[CATEGORY_ID] [int] NOT NULL IDENTITY(1, 1),
[CATEGORY_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPANY_ID] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SET_USER] [int] NULL,
[SET_DATE] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[INV_PRODUCT_CATEGORY] ADD CONSTRAINT [PK_PROC_CATEGORY] PRIMARY KEY CLUSTERED  ([CATEGORY_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[INV_PRODUCT_CATEGORY] ADD CONSTRAINT [FK_INV_PRODUCT_CATEGORY_COM_COMPANY_SETUP] FOREIGN KEY ([COMPANY_ID]) REFERENCES [dbo].[COM_COMPANY_SETUP] ([COMPANY_ID])
GO
