CREATE TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS]
(
[PURCHASE_ORDER_DETAILS_ID] [int] NOT NULL IDENTITY(1, 1),
[PURCHASE_ORDER_ID] [int] NULL,
[PRODUCT_ID] [int] NULL,
[QUANTITY] [decimal] (10, 2) NULL,
[RATE] [decimal] (10, 2) NULL,
[PRICE] [decimal] (10, 2) NULL,
[UNIT_OF_MEASUREMENT_ID] [int] NULL,
[DISCOUNT] [int] NULL,
[GODOWN_ID] [int] NULL,
[PAYMENT_ID] [int] NULL,
[COMMENTS] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPANY_ID] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SET_USER] [int] NULL,
[SET_DATE] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [PK_PROC_CHALAN] PRIMARY KEY CLUSTERED  ([PURCHASE_ORDER_DETAILS_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_COM_COMPANY_SETUP] FOREIGN KEY ([COMPANY_ID]) REFERENCES [dbo].[COM_COMPANY_SETUP] ([COMPANY_ID])
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_INV_GODOWN] FOREIGN KEY ([GODOWN_ID]) REFERENCES [dbo].[INV_GODOWN] ([GODOWN_ID])
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_CORE_PAYMENT_INFO] FOREIGN KEY ([PAYMENT_ID]) REFERENCES [dbo].[CORE_PAYMENT_INFO] ([PAYMENT_ID])
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_INV_PRODUCT] FOREIGN KEY ([PRODUCT_ID]) REFERENCES [dbo].[INV_PRODUCT] ([PRODUCT_ID])
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_PUR_PERCHASE_ORDER] FOREIGN KEY ([PURCHASE_ORDER_ID]) REFERENCES [dbo].[PUR_PURCHASE_ORDER] ([PURCHASE_ORDER_ID])
GO
ALTER TABLE [dbo].[PUR_PURCHASE_ORDER_DETAILS] ADD CONSTRAINT [FK_PUR_PURCHASE_ORDER_DETAILS_INV_UNIT_OF_MEASUREMENT] FOREIGN KEY ([UNIT_OF_MEASUREMENT_ID]) REFERENCES [dbo].[INV_UNIT_OF_MEASUREMENT] ([UNIT_OF_MEASUREMENT_ID])
GO
