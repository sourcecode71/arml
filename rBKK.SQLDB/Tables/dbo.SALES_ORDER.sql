CREATE TABLE [dbo].[SALES_ORDER]
(
[ORDER_ID] [int] NOT NULL IDENTITY(1, 1),
[ORDER_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_ID] [int] NULL,
[SELLING_DATE] [datetime] NULL,
[COMPANY_ID] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SET_USER] [int] NULL,
[SET_DATE] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SALES_ORDER] ADD CONSTRAINT [PK_SALE_ORDER_1] PRIMARY KEY CLUSTERED  ([ORDER_ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SALES_ORDER] ADD CONSTRAINT [FK_SALES_ORDER_SALES_CLIENT] FOREIGN KEY ([CLIENT_ID]) REFERENCES [dbo].[SALES_CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[SALES_ORDER] ADD CONSTRAINT [FK_SALES_ORDER_COM_COMPANY_SETUP] FOREIGN KEY ([COMPANY_ID]) REFERENCES [dbo].[COM_COMPANY_SETUP] ([COMPANY_ID])
GO
